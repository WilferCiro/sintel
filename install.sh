#!bash

if (( $EUID == 0 )); then
	echo "Please run as not root user"
	exit
fi

echo "Cleaning data..."
sh uninstall.sh
mkdir ~/.local/share/sintel
cp -r Linux/Sintel/* ~/.local/share/sintel

echo "Copying necesary files, this may take a while..."
mkdir ~/.sintel
cp -r data/* ~/.sintel
mkdir ~/.sintel/run_python
touch ~/.sintel/default.py

sudo cp sintel.png /usr/share/icons/sintel.png
sudo cp Linux/sintel.desktop /usr/share/applications
sudo cp Linux/bin/sintel /usr/bin
FILE=/usr/bin/sintel
if [ -f "$FILE" ]; then
	echo "Successfull installation"
	echo "----Search in you program menu the 'Sintel' program----"
else
	echo "Error in the installation proccess, please enter your password"
fi
