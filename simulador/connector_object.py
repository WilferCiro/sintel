#!/usr/bin/env python

'''
	Copyright (c) 2018 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Qt classes
from PyQt5.QtWidgets import QGraphicsItem, QGraphicsEllipseItem, QApplication, QGraphicsObject, QGraphicsTextItem
from PyQt5.QtGui import QPixmap, QPen, QBrush, QColor
from PyQt5.QtCore import QSize, QRectF, Qt, QPointF, pyqtSignal


class ConnectorObject(QGraphicsObject):
	"""
		Connector for the elements
	"""
	moved = pyqtSignal()
	deletedElement = pyqtSignal()

	def __init__(self, data, parent = None):
		"""
			Init the connector
			@param data as dict
			@param parent as QGrapicsItem
		"""
		self._parent = parent
		super(QGraphicsObject, self).__init__(self._parent)

		self._posibleBits = None
		self._default_color = None
		self.connectorID = data["id"]
		if data["type"] == "in":
			self._default_color = QColor(65, 205, 82)
		elif data["type"] == "out":
			self._default_color = QColor(255, 95, 105)
		else:
			self._default_color = QColor(0, 148, 166)

		self._type = data["type"]
		self._connectedSignal = None

		self._isClockOut = False
		self._isClockIn = False
		self._isResetIn = False
		self._currentConnections = None
		self._isInput = False
		self._currentSize = 1
		if data["type"] == "in":
			self._currentConnections = [0] * self._currentSize
			self._isInput = True
			if "clock" in data:
				self._isClockIn = True
				#self._parent.setClockDepends(True)
			if "reset" in data:
				self._isResetIn = True
		else:
			if "clock" in data:
				self._isClockOut = True
				self._parent.setClockGen(True)

		self._isVar = False
		if "var" in data:
			self._isVar = True # Variables -> ["1"]

		self._pen = QPen(self._default_color)
		self._pen.setWidth(2)

		self._rect = QGraphicsEllipseItem(QRectF(0, 0, 5, 5), self)
		self._rect.setPen(self._pen)
		background = QBrush(self._default_color)
		self._rect.setBrush(background)
		self.setAcceptHoverEvents(True)
		self.setCursor(Qt.CrossCursor)
		self._connectorID = data["id"]

		self._signalID = None
		self._defaultValue = None

		self._nameText = QGraphicsTextItem(self._connectorID, self)
		self._nameText.setPos(0, -10)
		self.hideText()
		self._posibleSize = 0

	def setReferenceMoveMacro(self, newIO, macro):
		if self._connectedSignal != None:
			self._connectedSignal.setReferenceMoveMacro(self, newIO, macro)

	def deleteReferenceMoveMacro(self, macro):
		if self._connectedSignal != None:
			self._connectedSignal.deleteReferenceMoveMacro(macro)

	def showConnectedSignal(self):
		self.showFromMacroComponent(None)

	def hideConnectedSignal(self):
		self.hideFromMacroComponent(None)

	def hideFromMacroComponent(self, macro):
		if self._connectedSignal != None:
			self._connectedSignal.setVisible(False)
			if self._connectedSignal.isSignalTerminal():
				self._connectedSignal.getConnections()[1].setVisible(False)

	def showFromMacroComponent(self, macro):
		if self._connectedSignal != None:
			self._connectedSignal.setVisible(True)
			if self._connectedSignal.isSignalTerminal():
				self._connectedSignal.getConnections()[1].setVisible(True)

	def forDeleteMacro(self, macroComponent):
		if self._connectedSignal != None:
			if not self._connectedSignal.isSignalSignal() or not self._connectedSignal.isSignalTerminal():
				connects = self._connectedSignal.getConnections()
				if type(connects[0]) == ConnectorObject and connects[0] != self:
					component = connects[0].getParent()
					return macroComponent.hasComponent(component), self._connectedSignal
				elif type(connects[1]) == ConnectorObject and connects[1] != self:
					component = connects[1].getParent()
					return macroComponent.hasComponent(component), self._connectedSignal
			# TODO: check the connected signal to signal
		return False, self

	def getSignals(self):
		if self._connectedSignal != None:
			return self._connectedSignal
		return None

	def connectSignal(self, signal):
		self._connectedSignal = signal

	def disconnectSignal(self):
		self._connectedSignal = None

	def getConnectedSignal(self):
		return self._connectedSignal

	def getSizeRef(self):
		return self._posibleSize

	def canSetSize(self, size):
		"""
			Checks if all sequency can change size
		"""
		self._posibleSize = size
		if self._connectedSignal != None:
			Value = self._connectedSignal.canSetSize(size)
		else:
			Value = self.canSetSizeVariables(size)
		return Value

	def canSetSizeVariables(self, size):
		"""
			Checks if all sequency can change size
		"""
		valReturn = []
		variable = self._parent.getReferenceBitsByID(self._posibleBits)
		if str(size) in variable["bits"]:
			for conn in variable["connectors"]:
				if int(conn.getSizeRef()) != int(size):
					if conn == self:
						valReturn.append(True)
					else:
						valReturn.append(conn.canSetSize(size))
		else:
			valReturn.append(False)
		return False if False in valReturn else True


	def setCanSize(self, size):
		"""
			When the canSetSize function returns true, this function set the size of all sequency
		"""
		if self._connectedSignal != None:
			self._connectedSignal.setSizeVariables(size)
		else:
			self.setSizeVariables(size)

	def setSizeVariables(self, size):
		"""
			When the canSetSize function returns true, this function set the size of all secuency
		"""
		variable = self._parent.getReferenceBitsByID(self._posibleBits)
		if str(size) in variable["bits"]:
			for conn in variable["connectors"]:
				if int(conn.getSize()) != int(size):
					if conn == self:
						conn.setSize(size)
					else:
						conn.setCanSize(size)


	def setSize(self, bits, posibleBits = None):
		if posibleBits != None:
			self._posibleBits = posibleBits
		self._currentSize = int(bits)
		if self._currentConnections != None:
			newConnections = [0] * self._currentSize
			index = 0
			for con in range(len(self._currentConnections)):
				newConnections[index] = self._currentConnections[index]
			self._currentConnections = newConnections
		self._posibleSize = 0


	def hideText(self):
		self._nameText.setVisible(False)

	def showText(self):
		self._nameText.setVisible(True)

	def isResetIn(self):
		return self._isResetIn

	def isClockIn(self):
		return self._isClockIn

	def isClockOut(self):
		return self._isClockOut

	def isInput(self):
		return self._isInput

	def setConnections(self, connections):
		if self._currentConnections != None:
			for con in connections:
				if self._currentConnections[con] == 0:
					self._currentConnections[con] = 1

	def getSignalID(self):
		return self._signalID

	def setSignalID(self, signalID):
		self._signalID = signalID

	def getOptionSizes(self):
		return self._bitsReference


	def setDefaultValue(self, value):
		if value == -1:
			value = None
		self._defaultValue = value

	def getDefaultValue(self):
		return self._defaultValue

	def isVar(self):
		return self._isVar

	def getSize(self):
		return int(self._currentSize)

	def getType(self):
		return self._type

	def getParentID(self):
		"""
			Gets the parent ID
		"""
		return self._parent.getID()

	def getParent(self):
		"""
			Gets the parent object
		"""
		return self._parent

	def getID(self):
		return self._connectorID

	def boundingRect(self):
		"""
			override boundingRect method, is mandatory, return the item size
		"""
		return QRectF(0, 0, 8, 8)

	def paint(self, *args):
		"""
			override paint method, is mandatory
			@param args
		"""
		None

	def hoverEnterEvent(self, event):
		"""
			When the mouse hover over the button
			@param event as mousehoverevent
		"""
		self._pen.setColor(Qt.black)
		self._rect.setPen(self._pen)

	def hoverLeaveEvent(self, event):
		"""
			When the mouse leave hover over the button
			@param event as mousehoverevent
		"""
		self._pen.setColor(self._default_color)
		self._rect.setPen(self._pen)

	def mousePressEvent(self, event):
		"""
			Emits When the user press the mouse
			@param event as mouseevent
		"""
		if self._connectedSignal == None:
			QApplication.instance().projectsHandler.getWorkspace().wireAction(self)

	def updateWires(self):
		"""
			Update the wires positions
		"""
		#self.moved.emit()
		if self._connectedSignal != None:
			self._connectedSignal.update()

	def movePos(self, newPos):
		if self._connectedSignal != None:
			self._connectedSignal.moveFold(newPos)
		self.updateWires()

	def setDragStart(self, start):
		if self._connectedSignal != None:
			self._connectedSignal.setDragStart(start)

	def delete(self):
		"""
			Emits that the element is deleted for delete the wire
		"""
		self.deletedElement.emit()

	def getPos(self):
		return {"x" : self.scenePos().x(), "y" : self.scenePos().y()}

	def update(self, value, part = "Text"):
		"""
			Shows a value in the visual interface while the program is running
			@param value as int
			@param part as part, (TEXT, PIN)
		"""
		if part == "Text":
			self.__ValueTextWidget.setPlainText(str(value))

	def mouseDoubleClickEvent(self, event):
		"""
			Emits When the user click two times
			@param event as mouseevent
		"""
		print("doble")
