#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries
from simulador.tools.utils import GRIDSPACE, SMSize, MOORE, MEALY
from simulador.src_sm.save_load_sm import SaveLoadSM
from simulador.tools.spinbox import BigIntSpinbox

# Qt libraries
from PyQt5.QtWidgets import (QApplication, QShortcut, QWidget, QMenu, QGraphicsScene, QGraphicsView, QGraphicsObject, QGraphicsEllipseItem, QGraphicsTextItem, QGraphicsRectItem,
	QGraphicsItem, QGraphicsLineItem, QDialog, QHBoxLayout, QPushButton, QLabel, QTableWidget, QTableWidgetItem, QComboBox, QGraphicsPathItem, QLineEdit)
from PyQt5.QtCore import Qt, QSize, QPoint, pyqtSignal, QPointF, QRectF
from PyQt5.QtGui import QPixmap, QKeySequence, QPainter, QIcon, QWheelEvent, QBrush, QColor, QPen, QTransform, QPainterPath
from PyQt5 import uic

# System libraries
import math

# Variables
DELETE_TEXT = "Delete"

def getTextPreview(conditions, outputValues = None):
	text = ""
	for cond in conditions:
		bitsShow = ""
		if cond["bits"] != "":
			bitsShow = "[" + cond["bits"] + "]"
		text += " " + cond["union"] + " " + cond["input"] + bitsShow + " " + cond["condition"] + " " + cond["value"] + " "
	complete = text.count("(") - text.count(")")
	text += ")" * complete

	if outputValues != None and len(outputValues) > 0:
		text += " / "
		for output in outputValues:
			text += str(output["id"]) + " = " + str(output["val"]) + ", "

	return text

def clearLayout(layout):
	if layout != None:
		while layout.count():
			child = layout.takeAt(0)
			if child.widget() is not None:
				child.widget().deleteLater()
			elif child.layout() is not None:
				clearLayout(child.layout())

class SMWidget(QWidget):
	"""
		Handle the state machine creator
	"""
	def __init__(self, parent):
		super(QWidget, self).__init__()
		uic.loadUi(QApplication.instance().get_src("ui", "sm", "sm_view.ui"), self)

		# Input and Output definitions
		self._inputs = []
		self._outputs = []
		self._currentInputIndex = 1
		self._currentOutputIndex = 1
		self._smName = "mySM"
		self._smVersion = "0.1"
		self._smAutor = "Wilfer Daniel Ciro Maya"
		self._smModifDate = "2018-05-29"
		self._smMode = MOORE

		self._parent = parent

		# Modals
		self._statesPropertiesModal = StatePropertiesModal(parent = self)
		self._connectionPropertiesModal = ConnectionModal(parent = self)
		self._connectionPropertiesModalSubState = ConnectionModal(parent = self, substate = True)

		# Tables
		self._tableInputs = QTableWidget()
		self._tableOutputs = QTableWidget()
		self.outputList.addWidget(self._tableOutputs)
		self.inputList.addWidget(self._tableInputs)
		self._tableInputs.itemChanged.connect(self._editedInput)
		self._tableOutputs.itemChanged.connect(self._editedOutput)

		# Workspace
		self.smWorkspace = SMWorkspace(self)
		self._view = QGraphicsView(self.smWorkspace)
		self._view.setRenderHints(QPainter.Antialiasing);
		self._view.setRubberBandSelectionMode(Qt.IntersectsItemShape)
		self.workspaceSpace.addWidget(self._view)

		# Buttons right
		self.addInputBtn.clicked.connect(self._addInputFnc)
		self.addOutputBtn.clicked.connect(self._addOutputFnc)
		self.meType.currentIndexChanged.connect(self._changeType)

		# Save Load project
		self._saveLoadSM = SaveLoadSM()

		# Shortcut
		self.shortcut = QShortcut(QKeySequence("Ctrl+S"), self)
		self.shortcut.activated.connect(self.saveProject)

		self._isSaved = True
		self.setSaved()

	def setSaved(self):
		self._isSaved = True
		self._parent.setSavedProject(self, True)

	def setEdited(self):
		self._isSaved = False
		self._parent.setSavedProject(self, False)

	def getProjectName(self):
		return "SM - " + self.meName.text()

	def isSaved(self):
		return self._isSaved

	def _changeType(self):
		if self.meType.currentIndex() == 0:
			self._smMode = MOORE
		else:
			self._smMode = MEALY
		self.smWorkspace.updateText()
		self._connectionPropertiesModal.changeState()
		self._connectionPropertiesModalSubState.changeState()

	def getMode(self):
		return self._smMode

	def getIndexes(self):
		return {"input" : self._currentInputIndex, "output" : self._currentOutputIndex}

	def setIndexes(self, data):
		self._currentInputIndex = int(data["input"])
		self._currentOutputIndex = int(data["output"])

	def addProject(self):
		self.resetAll()
		self.smWorkspace.addInitialStates()
		self.setSaved()

	def resetAll(self):
		self._inputs = []
		self._outputs = []
		self._currentInputIndex = 1
		self._currentOutputIndex = 1
		self.meType.setCurrentIndex(0)
		self.meName.setText("StateMachine")
		self.meVersion.setText("0.1")
		self.meAutor.setText("Username")
		self.smWorkspace.resetAll()

	def loadProject(self, file):
		self.resetAll()
		dataLoad = self._saveLoadSM.loadProject(file)
		if dataLoad != None:
			if "is_decoder" in dataLoad:
				if dataLoad["is_decoder"]:
					self.setAsDecoder.setChecked(True)

			if dataLoad["mode"] == MOORE:
				self._smMode = MOORE
				self.meType.setCurrentIndex(0)
			else:
				self._smMode = MEALY
				self.meType.setCurrentIndex(1)

			self._inputs = dataLoad["inputs"]
			self._outputs = dataLoad["outputs"]

			for states in dataLoad["states"]:
				int_cond = []
				if "int_cond" in states:
					int_cond = states["int_cond"]
				self.smWorkspace.addState(position = QPoint(states["position"][0] + 25, states["position"][1] + 25), isInitial = states["isInitial"], isFinal = states["isFinal"], stateID = states["id"], parent = self.smWorkspace, outs = states["outs"], int_cond = int_cond)

			states = self.smWorkspace.getStates()
			for conn in dataLoad["connections"]:
				stateA = next((item for item in states if item["id"] == conn["from"]), False)
				stateB = next((item for item in states if item["id"] == conn["to"]), False)
				int_cond = []
				if "int_cond" in conn:
					int_cond = conn["int_cond"]
				if stateA != False and stateB != False:
					self.smWorkspace.addConnection(stateA["object"], stateB["object"], idConn = None, conditions = conn["conditions"], outputs = conn["outputs"], int_cond = int_cond)

			self.smWorkspace.setIndexes(dataLoad["index"])

			self.meName.setText(dataLoad["type"])
			self.meVersion.setText(dataLoad["version"])
			self.meAutor.setText(dataLoad["autor"])
			if dataLoad["mode"] == MOORE:
				self.meType.setCurrentIndex(0)
			else:
				self.meType.setCurrentIndex(1)
		else:
			self.smWorkspace.addInitialStates()

		# Initial data
		self.isNew = True
		self.organizeIO()
		self.isNew = False
		self.setSaved()

	def saveProject(self):
		self.setSaved()
		name = self.meName.text()
		isDecoder = self.setAsDecoder.isChecked()
		self._saveLoadSM.resetFile(name, self.getMode(), isDecoder = isDecoder)

		self._saveLoadSM.addIO(self._inputs, self._outputs)

		for state in self.smWorkspace.getStates():
			self._saveLoadSM.addState(state["object"])

		for conn in self.smWorkspace.getConnections():
			self._saveLoadSM.addConnection(conn["object"])

		indexes = self.smWorkspace.getIndexes()
		self._saveLoadSM.saveIndexes(indexes)

		version = self.meVersion.text()
		autor = self.meAutor.text()
		updated = "-"
		self._saveLoadSM.saveSMData(autor = autor, version = version, updated = updated)

		self._saveLoadSM.writeFile()

	def showPropertiesConnection(self, connection):
		self._connectionPropertiesModal.showProperties(connection)

	def showPropertiesConnectionInternal(self, internalC):
		self._connectionPropertiesModalSubState.showPropertiesInternal(internalC)

	def showPropertiesState(self, state):
		self._statesPropertiesModal.showProperties(state)

	def getModalConnection(self):
		return self._connectionPropertiesModal

	def getInputs(self):
		return self._inputs

	def getOutputs(self):
		return self._outputs

	def _addInputFnc(self):
		self._inputs.append({
			"id" : "pIn" + str(self._currentInputIndex),
			"size" : "1"
		})
		self._currentInputIndex += 1
		self.isNew = True
		self.organizeIO()
		self.isNew = False
		self.setEdited()

	def _addOutputFnc(self):
		self._outputs.append({
			"id" : "pOut" + str(self._currentOutputIndex),
			"size" : "1"
		})
		self._currentOutputIndex += 1
		self.isNew = True
		self.organizeIO()
		self.isNew = False
		self.smWorkspace.addedOutput(self._outputs[-1])
		self.setEdited()


	def clearTables(self):
		self._tableInputs.clear()
		self._tableOutputs.clear()

	def organizeIO(self):
		self.clearTables()

		self._tableOutputs.setRowCount(len(self._outputs))
		self._tableOutputs.setColumnCount(2)
		self._tableInputs.setRowCount(len(self._inputs))
		self._tableInputs.setColumnCount(2)
		self._tableOutputs.setHorizontalHeaderLabels(["ID", "Size"])
		self._tableInputs.setHorizontalHeaderLabels(["ID", "Size"])
		self.isNew = True
		index = 0
		for item in self._outputs:
			self._tableOutputs.setItem(index, 0, QTableWidgetItem(item["id"]))
			self._tableOutputs.setItem(index, 1, QTableWidgetItem(item["size"]))
			index += 1

		index = 0
		for item in self._inputs:
			self._tableInputs.setItem(index, 0, QTableWidgetItem(item["id"]))
			self._tableInputs.setItem(index, 1, QTableWidgetItem(item["size"]))
			index += 1
		self.isNew = False

		self._statesPropertiesModal.reorganizeTable(self._outputs, self._inputs)
		self._connectionPropertiesModal.reorganizeTable(self._outputs, self._inputs)
		self._connectionPropertiesModalSubState.reorganizeTable(self._outputs, self._inputs)

	def _editedInput(self, item, newValue = ""):
		if not self.isNew:
			newText = item.text().replace(" ", "")
			if newText == "":
				self.smWorkspace.deletedInput(self._inputs[item.row()])
				self._inputs.pop(item.row())
			else:
				if item.column() == 1:
					if newText.isdigit():
						self._inputs[item.row()]["size"] = str(newText)
				else:
					newText = newText + newValue
					obj_index = next((item for item in self._inputs if item["id"] == newText), False)
					if obj_index != False:
						self._editedInput(item, newValue + "2")
					else:
						self.smWorkspace.editedInput(self._inputs[item.row()], newText)
						self._inputs[item.row()]["id"] = newText
			self.organizeIO()
			self.setEdited()

	def _editedOutput(self, item, newValue = ""):
		if not self.isNew:
			newText = item.text().replace(" ", "")
			if newText == "":
				self.smWorkspace.deletedOutput(self._outputs[item.row()])
				self._outputs.pop(item.row())
			else:
				if item.column() == 1:
					if newText.isdigit():
						self._outputs[item.row()]["size"] = newText
				else:
					newText = newText + newValue
					obj_index = next((item for item in self._outputs if item["id"] == newText), False)
					if obj_index != False:
						self._editedOutput(item, newValue + "2")
					else:
						self.smWorkspace.editedOutput(self._outputs[item.row()], newText)
						self._outputs[item.row()]["id"] = newText
			self.organizeIO()
			self.setEdited()

class SMWorkspace(QGraphicsScene):
	"""
		Handle the workspace
		@param parent as widget or None
	"""
	def __init__(self, parent = None):
		super(QGraphicsScene, self).__init__(parent)

		self._parent = parent

		self.setSceneRect(QRectF(0, 0, SMSize["x"], SMSize["y"]))

		# Control Variables
		self._stateIndex = 1
		self._allStates = []
		self._allConnections = []
		self._connectionIndex = 1

		# wire action
		self._lastState = None

		# background
		brush = QBrush()
		brush.setColor(QColor('#999'))
		brush.setTexture(QPixmap(QApplication.instance().get_src("images", "grid.png")))
		self.setBackgroundBrush(brush)

		self._pen = QPen(Qt.black)
		self._pen.setWidth(2)

		self._tempLine = QGraphicsLineItem()
		self._tempLine.setPen(self._pen)
		self._tempLine.setVisible(False)
		self._lastPosition = None

		self.addItem(self._tempLine)

	def updateText(self):
		for state in self._allStates:
			state["object"].updateText()
		for conn in self._allConnections:
			conn["object"].updateText()

	def getMode(self):
		return self._parent.getMode()

	def resetAll(self):
		temp_states = self._allStates
		temp_conn = self._allConnections
		for state in temp_states:
			self.removeItem(state["object"])
		for conn in temp_conn:
			conn["object"].deleteData()
			self.removeItem(conn["object"])

		self._stateIndex = 1
		self._allStates = []
		self._allConnections = []
		self._connectionIndex = 1

	def addedOutput(self, output):
		for state in self._allStates:
			state["object"].addedOutput(output)
		for conn in self._allConnections:
			conn["object"].addedOutput(output)

	def deletedInput(self, name):
		for conn in self._allConnections:
			conn["object"].deletedInput(name)

	def deletedOutput(self, name):
		for state in self._allStates:
			state["object"].deletedOutput(name)
		for conn in self._allConnections:
			conn["object"].deletedOutput(name)

	def editedOutput(self, initialName, finalName):
		for state in self._allStates:
			state["object"].editedOutput(initialName, finalName)
		for conn in self._allConnections:
			conn["object"].editedOutput(initialName, finalName)

	def editedInput(self, initialName, finalName):
		for conn in self._allConnections:
			conn["object"].editedInput(initialName, finalName)

	def addInitialStates(self):
		# Add initials
		self.addState(position = QPoint(200, 100), isInitial = True, stateID = "SS", parent = self)
		self.addState(position = QPoint(SMSize["x"] - 200, SMSize["y"] - 100), isFinal = True, stateID = "FS", parent = self)

	def getIndexes(self):
		data = self._parent.getIndexes()
		data["state"] = self._stateIndex
		data["connection"] = self._connectionIndex
		return data

	def setIndexes(self, data):
		self._stateIndex = int(data["state"])
		self._connectionIndex = int(data["connection"])
		self._parent.setIndexes(data)

	def getStates(self):
		return self._allStates

	def getConnections(self):
		return self._allConnections

	def showPropertiesConnection(self, connection):
		self._parent.showPropertiesConnection(connection)

	def showPropertiesConnectionInternal(self, internalC):
		self._parent.showPropertiesConnectionInternal(internalC)

	def showPropertiesState(self, state):
		self._parent.showPropertiesState(state)

	def getModalConnection(self):
		return self._parent.getModalConnection()

	def mousePressEvent(self, event):
		"""
			Emits When the user press the mouse
			@param event as mouseevent
		"""
		item = self.itemAt(event.scenePos(), QTransform())
		if item == None:
			self.noSelected()
			for state in self._allStates:
				state["object"].setSel(False)
		else:
			QGraphicsScene.mousePressEvent(self, event)

	def mouseDoubleClickEvent(self, event):
		"""
			Emits When the user click two times
			@param event as mouseevent
		"""
		item = self.itemAt(event.scenePos(), QTransform())
		if item == None:
			outputs = []
			for out in self._parent.getOutputs():
				outputs.append({
					"id" : out["id"],
					"val" : 0
				})
			self.addState(position = event.scenePos(), stateID = None, parent = self, outs = outputs)
		else:
			QGraphicsScene.mouseDoubleClickEvent(self, event)

	def mouseMoveEvent(self, event):
		if self._lastState != None:
			self._tempLine.setLine(self._lastPosition.x() + 3, self._lastPosition.y() + 3, event.scenePos().x() + 5, event.scenePos().y() + 5)
		QGraphicsScene.mouseMoveEvent(self, event)

	def addState(self, position, isInitial = False, isFinal = False, stateID = None, parent = None, outs = [], int_cond = []):
		if stateID == None:
			stateID = "S" + str(self._stateIndex)
			self._stateIndex += 1
		state = StateWidget(position = position, stateID = stateID, isInitial = isInitial, isFinal = isFinal, parent = parent, outs = outs, int_cond = int_cond)
		self.addItem(state)
		self._allStates.append({
			"id" : stateID,
			"object" : state
		})
		self._parent.setEdited()

	def stateAction(self, state, position):
		if self._lastState == None:
			self._lastPosition = position
			self._lastState = state
			self._lastState.setSel(True)
			self._tempLine.setVisible(True)
		else:
			obj_index = next((conn for conn in self._allConnections if self._lastState.getID() == conn["from"] and state.getID() == conn["to"]), False)
			if obj_index == False:
				newState = state
				self._lastState.setSel(False)
				self.addConnection(self._lastState, newState)
				self._lastState = None
			self.noSelected()

	def noSelected(self):
		self._lastState = None
		self._tempLine.setVisible(False)
		self._tempLine.setLine(0, 0, 0, 0)

	def addConnection(self, stateA, stateB, idConn = None, conditions = None, outputs = None, int_cond = []):
		if idConn == None:
			idConn = "c" + str(self._connectionIndex)
		if outputs == None:
			outputs = []
			for ou in self._parent.getOutputs():
				outputs.append({
					"id" : ou["id"],
					"val" : "0"
				})

		secondWire = False
		obj_index = next((conn for conn in self._allConnections if stateA.getID() == conn["to"] and stateB.getID() == conn["from"]), False)
		if obj_index != False:
			secondWire = True

		newConn = Connection(stateA, stateB, idConn, parent = self, conditions = conditions, outputs = outputs, secondWire = secondWire, int_cond = int_cond)
		self.addItem(newConn)
		self._allConnections.append({
			"id" : idConn,
			"object" : newConn,
			"from" : stateA.getID(),
			"to" : stateB.getID()
		})
		self._connectionIndex += 1
		self._parent.setEdited()

	def deleteState(self, state):
		obj_index = next((item for item in self._allStates if item["object"] == state), False)
		if obj_index != False:
			self._allStates.remove(obj_index)
		self.removeItem(state)
		self._parent.setEdited()
		self.noSelected()

	def deleteConnection(self, connection):
		obj_index = next((item for item in self._allConnections if item["object"] == connection), False)
		if obj_index != False:
			connection.deleteData()
			self._allConnections.remove(obj_index)
		self.removeItem(connection)
		self._parent.setEdited()

class StateWidget(QGraphicsObject):
	"""
		Puts an arrow or point in the wire terminal
		@param connector as elementConnector
		@param parent
	"""
	def __init__(self, position, stateID, isInitial = False, isFinal = False, parent = None, outs = [], int_cond = []):
		super(QGraphicsObject, self).__init__(None)

		# Control variables
		self.__location = position
		self.__dragStart = position
		self.__parent = parent
		self._stateID = stateID
		self._isInitial = isInitial
		self._isFinal = isFinal
		self.outputValues = []
		self.outputValues.extend(outs)
		for out in self.outputValues:
			if not "bits" in out:
				out["bits"] = ""
			if not "input" in out:
				out["input"] = "Number"
		# Ellipse
		self._diameter = 50
		self._ellipse = QGraphicsEllipseItem(0, 0, self._diameter, self._diameter, self)
		self._pen = QPen(Qt.black)

		if self._isFinal:
			self._ellipse.setBrush(QBrush(QColor("#56AEEB"))) #Blue
			self._pen = QPen(Qt.black)
		elif self._isInitial:
			self._ellipse.setBrush(QBrush(QColor("#95AB60"))) # Green
			self._pen = QPen(Qt.black)
		else:
			self._ellipse.setBrush(QBrush(Qt.white))

		self._pen.setWidth(1)
		self._ellipse.setPen(self._pen)
		self.setPos(position - QPoint(self._diameter/2, self._diameter/2))
		self._isFocus = False

		# Control flags
		self.setFlag(QGraphicsItem.ItemIsSelectable, True)
		self.setFlag(QGraphicsItem.ItemSendsGeometryChanges, True)
		self.setFlag(QGraphicsItem.ItemIsFocusable, True)
		self.setAcceptHoverEvents(True)

		# Text
		self.valueText = QGraphicsTextItem(self._stateID, self)
		self.valueText.setPos(QPoint(6, 12))

		self._textView = QGraphicsTextItem("", self)
		self._textView.setPos(QPoint(50, 0))
		self._textView.setVisible(False)

		# connections
		self._internalConditions = []
		self._connections = []
		self.updateText()

		for intC in int_cond:
			newWidget = InternalCondition(self, self.__parent.getModalConnection())
			newWidget.setOutputs(intC["outputs"])
			newWidget.setConditions(intC["conditions"])
			self._internalConditions.append(newWidget)

	def addInternalCondition(self, internalC):
		self._internalConditions.append(internalC)

	def deleteInternalCondition(self, internalC):
		self._internalConditions.remove(internalC)
		internalC.setParent(None)

	def editInternalCondition(self, internalC):
		self.__parent.showPropertiesConnectionInternal(internalC)

	def getInternalConditions(self):
		return self._internalConditions

	def addedOutput(self, output):
		self.outputValues.append({
			"id" : output["id"],
			"input" : "Number",
			"bits" : "",
			"val" : "0"
		})
		self.updateText()

	def deletedOutput(self, name):
		obj_index = next((item for item in self.outputValues if item["id"] == name["id"]), False)
		if obj_index != False:
			self.outputValues.remove(obj_index)
		self.updateText()

	def editedOutput(self, initialName, finalName):
		obj_index = next((item for item in self.outputValues if item["id"] == initialName["id"]), False)
		if obj_index != False:
			obj_index["id"] = finalName
		self.updateText()

	def getPos(self):
		return [self.scenePos().x(), self.scenePos().y()]

	def getOutputs(self):
		return self.outputValues

	def setOutputs(self, outputs):
		self.outputValues = outputs
		self.updateText()

	def updateText(self):
		if self.__parent.getMode() == MOORE:
			text = ""
			for value in self.outputValues:
				if value["input"] == "Number":
					text += str(value["id"]) + " = " + str(value["val"]) + "<br />"
				else:
					text += str(value["id"]) + " = " + str(value["input"]) + "[" + str(value["bits"]) + "] <br />"
			self._textView.setHtml("<div style='background:white;'> " + str(text) + "</div>")
		else:
			self._textView.setPlainText("")

	def isFinal(self):
		return self._isFinal

	def isInitial(self):
		return self._isInitial

	def getID(self):
		return self._stateID

	def addConnection(self, connection, typeConnection):
		self._connections.append({
			"object" : connection,
			"type" : typeConnection
		})

	def boundingRect(self):
		"""
			override boundingRect method, is mandatory, return the item size
		"""
		return QRectF(0, 0, self._diameter, self._diameter)

	def paint(self, *args):
		"""
			override paint method, is mandatory
			@param args
		"""
		None

	def delete(self):
		if not self._isInitial:
			while len(self._connections) > 0:
				self._connections[0]["object"].delete()
			self.__parent.deleteState(self)

	def keyPressEvent(self, event):
		"""
			Detect the key press
			@param event as QKeyEvent
		"""
		if event.key() == Qt.Key_Delete:
			self.delete()
		elif event.key() == Qt.Key_Escape:
			self.setSel(False)
			self.__parent.noSelected()

	def deleteConnection(self, connection):
		obj_index = next((item for item in self._connections if item["object"] == connection), False)
		if obj_index != False:
			self._connections.remove(obj_index)

	def mousePressEvent(self, event):
		"""
			Emits When the user press the mouse
			@param event as mouseevent
		"""
		self.__dragStart = event.pos()
		self.__parent.stateAction(self, event.scenePos())

	def mouseMoveEvent(self, event):
		"""
			Emits When the user moves the mouse prev to press
			@param event as mouseevent
		"""
		newPos = event.pos()
		self.__location += (newPos - self.__dragStart)
		self.setPos(self.__location)
		for a in self._connections:
			a["object"].update()
		self.__parent.noSelected()
		self.setSel(False)

	def hoverEnterEvent(self, event):
		"""
			When the mouse hover over the button
			@param event as mousehoverevent
		"""
		if not self._isFocus:
			self._pen.setColor(Qt.green)
			self._pen.setStyle(Qt.SolidLine)
			self._ellipse.setPen(self._pen)
			self._textView.setVisible(True)

	def hoverLeaveEvent(self, event):
		"""
			When the mouse leave hover over the button
			@param event as mousehoverevent
		"""
		if not self._isFocus:
			self._pen.setStyle(Qt.SolidLine)
			self._pen.setColor(Qt.black)
			self._ellipse.setPen(self._pen)
			self._textView.setVisible(False)

	def setSel(self, selected):
		"""
			Emits when component is released
			@param event as QFocusEvent
		"""
		if not selected:
			self._pen.setColor(Qt.black)
			self._pen.setStyle(Qt.SolidLine)
			self._ellipse.setPen(self._pen)
			self._isFocus = False
			self._textView.setVisible(False)
		else:
			self._pen.setColor(Qt.red)
			self._pen.setStyle(Qt.DashLine)
			self._ellipse.setPen(self._pen)
			self._isFocus = True
			self._textView.setVisible(True)

	def mouseDoubleClickEvent(self, event):
		"""
			Emits When the user click two times
			@param event as mouseevent
		"""
		if self.__parent.getMode() == MOORE:
			self.__parent.showPropertiesState(self)


class Connection(QGraphicsLineItem):
	"""
		Puts a line into two folds
		@param foldFrom as fold
		@param foldTo as fold
		@param parent
	"""
	def __init__(self, stateA, stateB, idConnection, parent = None, conditions = None, outputs = None, secondWire = False, int_cond = []):
		# initial variables
		self.__parent = parent
		# constructor
		super(QGraphicsLineItem, self).__init__(None)

		self._stateA = stateA
		self._stateB = stateB
		self._id = idConnection
		self._secondWire = secondWire
		if conditions == None:
			self.conditionValues = []
		else:
			self.conditionValues = conditions

		for cond in self.conditionValues:
			if not "bits" in cond:
				cond["bits"] = ""
			if not "input" in cond:
				cond["input"] = "Number"

		self.outputValues = []
		if outputs == None:
			self.outputValues = []
		else:
			self.outputValues.extend(outputs)

		for out in self.outputValues:
			if not "bits" in out:
				out["bits"] = ""
			if not "input" in out:
				out["input"] = "Number"

		self._path = QGraphicsPathItem()
		self._path2 = None

		# add connections
		self._stateA.addConnection(self, 1)
		self._stateB.addConnection(self, 2)
		self._textView = textEditConnection(self)
		self._textView.setTextCustom("Edit Connection")
		self.updateText()

		# visual
		self._pen = QPen(Qt.black)
		self._pen.setWidth(3)
		self._path.setPen(self._pen)
		self._isFocus = False

		# control flags
		self.setAcceptHoverEvents(True)
		self.setFlag(QGraphicsItem.ItemSendsGeometryChanges, True)
		self.setFlag(QGraphicsItem.ItemIsFocusable, True)

		self._posX1 = self._stateA.scenePos().x() + 25
		self._posY1 = self._stateA.scenePos().y() + 0
		self._posX2 = self._stateB.scenePos().x() + 25
		self._posY2 = self._stateB.scenePos().y() + 0

		#self.setLine(self._posX1, self._posY1, self._posX2, self._posY2)
		#self.setPen(self._pen)
		self.update()

		# connections
		self._internalConditions = []

		for intC in int_cond:
			newWidget = InternalCondition(self, self.__parent.getModalConnection())
			newWidget.setOutputs(intC["outputs"])
			newWidget.setConditions(intC["conditions"])
			self._internalConditions.append(newWidget)

	def addInternalCondition(self, internalC):
		self._internalConditions.append(internalC)

	def deleteInternalCondition(self, internalC):
		self._internalConditions.remove(internalC)
		internalC.setParent(None)

	def editInternalCondition(self, internalC):
		self.__parent.showPropertiesConnectionInternal(internalC)

	def getInternalConditions(self):
		return self._internalConditions

	def getStateFrom(self):
		return self._stateA

	def getStateTo(self):
		return self._stateB

	def addedOutput(self, output):
		self.outputValues.append({
			"id" : output["id"],
			"val" : "0",
			"bits" : "",
			"input" : "Number"
		})
		self.updateText()

	def deletedOutput(self, name):
		obj_index = next((item for item in self.outputValues if item["id"] == name["id"]), False)
		if obj_index != False:
			self.outputValues.remove(obj_index)
		self.updateText()

	def deletedInput(self, name):
		obj_index = next((item for item in self.conditionValues if item["input"] == name["id"]), False)
		if obj_index != False:
			self.conditionValues.remove(obj_index)
		self.updateText()

	def editedOutput(self, initialName, finalName):
		obj_index = next((item for item in self.outputValues if item["id"] == initialName["id"]), False)
		if obj_index != False:
			obj_index["id"] = finalName
		self.updateText()

	def editedInput(self, initialName, finalName):
		obj_index = next((item for item in self.conditionValues if item["input"] == initialName["id"]), False)
		if obj_index != False:
			obj_index["input"] = finalName
		self.updateText()

	def getFrom(self):
		return self._stateA.getID()

	def getTo(self):
		return self._stateB.getID()

	def getConditions(self):
		return self.conditionValues

	def getOutputs(self):
		return self.outputValues

	def setOutputs(self, outputs):
		self.outputValues = outputs

	def setConditions(self, conditions):
		self.conditionValues = conditions
		self.updateText()

	def updateText(self):
		if self.__parent.getMode() == MOORE:
			text = getTextPreview(self.conditionValues, None)
		else:
			text = getTextPreview(self.conditionValues, self.outputValues)
		if text == "":
			self._textView.setTextCustom("Edit connection")
		else:
			self._textView.setTextCustom(text)

	def getID(self):
		return self._id

	def hoverEnterEvent(self, event):
		"""
			When the mouse hover over the button
			@param event as mousehoverevent
		"""
		if not self._isFocus:
			self._pen = QPen(Qt.green)
			self._pen.setWidth(3)
			self._path2.setPen(self._pen)

	def hoverLeaveEvent(self, event):
		"""
			When the mouse hover over the button
			@param event as mousehoverevent
		"""
		if not self._isFocus:
			self._pen = QPen(Qt.black)
			self._pen.setWidth(3)
			self._path2.setPen(self._pen)
			#if not self._isFocus:
			#	self._pen.setColor(Qt.green)
			#	self._pen.setStyle(Qt.SolidLine)
			#	#self.setPen(self._pen)

	def focusOutEvent(self, event):
		"""
			Emits when component is released
			@param event as QFocusEvent
		"""
		self._pen.setStyle(Qt.SolidLine)
		self._pen.setColor(Qt.black)
		self._path2.setPen(self._pen)
		self._isFocus = False

	def focusInEvent(self, event):
		"""
			Emits when component is selected
			@param event as QFocusEvent
		"""
		self._pen.setColor(Qt.red)
		self._pen.setStyle(Qt.DashLine)
		self._path2.setPen(self._pen)
		self._isFocus = True

	def keyPressEvent(self, event):
		"""
			Detect the key press
			@param event as QKeyEvent
		"""
		if event.key() == Qt.Key_Delete:
			self.delete()

	def delete(self):
		self._stateA.deleteConnection(self)
		self._stateB.deleteConnection(self)
		self.__parent.deleteConnection(self)

	def deleteData(self):
		self.__parent.removeItem(self._arrow3)
		self.__parent.removeItem(self._arrow2)
		self.__parent.removeItem(self._path2)

	def update(self):
		"""
			Update the SubWire Position
		"""
		posX1 = self._stateA.scenePos().x() + 25
		posY1 = self._stateA.scenePos().y() + 0
		posX2 = self._stateB.scenePos().x() + 25
		posY2 = self._stateB.scenePos().y() + 0

		#self.setLine(posX1, posY1, posX2, posY2)

		if self._secondWire:
			self._textView.setPos((self._stateB.scenePos() + self._stateA.scenePos()) / 2 + QPointF(0, 20))
		else:
			self._textView.setPos((self._stateB.scenePos() + self._stateA.scenePos()) / 2)

		if self._path2 != None:
			self.__parent.removeItem(self._path2)
			self.__parent.removeItem(self._arrow2)
			self.__parent.removeItem(self._arrow3)

		self._path = QPainterPath()
		self._path.moveTo(posX1, posY1)

		plusX = 0
		plusY = 0
		if self._secondWire:
			if abs(posX1 - posX2) > abs(posY1 - posY2):
				plusY = 150
			else:
				plusX = -150
		else:
			if abs(posX1 - posX2) > abs(posY1 - posY2):
				plusY = -150
			else:
				plusX = 150

		#angle = math.acos((posX2 - posX1) / (math.sqrt((posX1 - posX2) * (posX1 - posX2) + (posY1 - posY2) * (posY1 - posY2))))
		#angle = (angle *180) / math.pi + 90

		self._arrow2 = QGraphicsLineItem(posX2, posY2, posX2 + 10, posY2 - 10)
		self._arrow3 = QGraphicsLineItem(posX2, posY2, posX2 - 10, posY2 - 10)
		if self._stateA == self._stateB:
			self._path.cubicTo(posX1, posY1, (posX1 + posX2)/2, -100 + (posY1 + posY2)/2, posX2 + 25, posY2 + 25)
		else:
			self._path.cubicTo(posX1, posY1, plusX + (posX1 + posX2)/2, plusY + (posY1 + posY2)/2, posX2, posY2)

		self._path2 = self.__parent.addPath(self._path)
		self.__parent.addItem(self._arrow2)
		self.__parent.addItem(self._arrow3)
		self._path2.setPen(self._pen)

	def mouseDoubleClickEvent(self, event):
		"""
			Emits When the user click two times
			@param event as mouseevent
		"""
		self.__parent.showPropertiesConnection(self)

	def showPropertiesConnection(self):
		self.__parent.showPropertiesConnection(self)

class textEditConnection(QGraphicsTextItem):

	def __init__(self, parent):
		super(QGraphicsTextItem, self).__init__("", parent)
		self._parent = parent

		self.setAcceptHoverEvents(True)
		self.setFlag(QGraphicsItem.ItemIsFocusable, True)
		self._text = ""

	def setTextCustom(self, text):
		self._text = text
		self._hideAllText()

	def mouseDoubleClickEvent(self, event):
		"""
			Emits When the user click two times
			@param event as mouseevent
		"""
		self._parent.showPropertiesConnection()

	def _hideAllText(self):
		self.setHtml("<div style='background:white;'>" + str(self._text)[0:12] + "...</div>")

	def _showAllText(self):
		self.setHtml("<div style='background:white;'>" + str(self._text) + "</div>")

	def hoverEnterEvent(self, event):
		"""
			When the mouse hover over the button
			@param event as mousehoverevent
		"""
		self._parent.hoverEnterEvent(event)
		self._showAllText()

	def hoverLeaveEvent(self, event):
		"""
			When the mouse hover over the button
			@param event as mousehoverevent
		"""
		self._parent.hoverLeaveEvent(event)
		self._hideAllText()

	def focusOutEvent(self, event):
		"""
			Emits when component is released
			@param event as QFocusEvent
		"""
		self._parent.focusOutEvent(event)

	def focusInEvent(self, event):
		self._parent.focusInEvent(event)

class StatePropertiesModal(QDialog):

	def __init__(self, parent):
		super(QDialog, self).__init__()
		uic.loadUi(QApplication.instance().get_src("ui", "sm", "states_properties.ui"), self)

		self._parent = parent
		self._lastState = None

		self._tableOutputs = QTableWidget()
		self.layoutTable.addWidget(self._tableOutputs)

		self.buttonBox.accepted.connect(self._accept)

		self.addInternalConditionBtn.clicked.connect(self._addInternalCondition)
		self.setWindowTitle("Edit state properties...")

	def _addInternalCondition(self):
		newCon = InternalCondition(self._lastState, self)
		self._lastState.addInternalCondition(newCon)
		self.layoutInternal.addWidget(newCon)

	def deleteInternalCondition(self, internalC):
		self.layoutInternal.removeWidget(internalC)

	def _accept(self):
		valuesReturn = []
		for index in range(self._tableOutputs.rowCount()):
			idO = self._tableOutputs.cellWidget(index, 0).text()
			input = self._tableOutputs.cellWidget(index, 1).currentText()
			bits = self._tableOutputs.cellWidget(index, 2).text()
			val = self._tableOutputs.cellWidget(index, 3).value()
			valuesReturn.append({
				"id" : idO,
				"input" : input,
				"bits" : bits,
				"val" : val
			})
			self._tableOutputs.cellWidget(index, 1).setCurrentIndex(0)
			self._tableOutputs.cellWidget(index, 2).setText("")
			self._tableOutputs.cellWidget(index, 3).setValue(0)
		self._lastState.setOutputs(valuesReturn)
		self._lastState = None
		self.accept()

	def reorganizeTable(self, outputs, inputs):
		self._tableOutputs.clear()
		self._tableOutputs.setColumnCount(4)
		self._tableOutputs.setRowCount(len(outputs))
		self._tableOutputs.setHorizontalHeaderLabels(["Output", "Attach", "Bits", "Value"])
		index = 0
		for item in outputs:
			spin = BigIntSpinbox()
			spin.setMinimum(0)
			spin.setMaximum(2 ** int(item["size"]) - 1)

			attachCombo = QComboBox()
			attachCombo.addItem("Number")
			for inp in inputs:
				attachCombo.addItem(inp["id"])

			inputBits = QLineEdit()
			inputBits.textChanged.connect(self._changedText)

			self._tableOutputs.setCellWidget(index, 0, QLabel(item["id"]))
			self._tableOutputs.setCellWidget(index, 1, attachCombo)
			self._tableOutputs.setCellWidget(index, 2, inputBits)
			self._tableOutputs.setCellWidget(index, 3, spin)

			index += 1

	def _changedText(self):
		for index in range(self._tableOutputs.rowCount()):
			bits = self._tableOutputs.cellWidget(index, 2).text()
			bitsIn = bits.split(",")
			#bitsIn.sort(reverse = True)
			bitsIn = ",".join(bitsIn)
			if bits != bitsIn:
				self._tableOutputs.cellWidget(index, 2).setText(bitsIn)

	def showProperties(self, state):
		clearLayout(self.layoutInternal)
		internalAll = state.getInternalConditions()
		for internal in internalAll:
			try:
				self.layoutInternal.addWidget(internal)
			except:
				pass

		self._lastState = state
		text = state.getID()
		if state.isFinal():
			text += " [End]"
		if state.isInitial():
			text += " [Start]"
		self.stateID.setText(text)

		inputs = self._parent.getInputs()
		outputs = self._parent.getOutputs()

		outputValues = state.outputValues
		indexRow = 0
		for out in outputs:
			obj_index = next((item for item in outputValues if item["id"] == out["id"]), False)
			if obj_index != False:
				value = int(obj_index["val"])
				comboInput = self._tableOutputs.cellWidget(indexRow, 1)
				index = comboInput.findText(obj_index["input"])
				comboInput.setCurrentIndex(index)
				self._tableOutputs.cellWidget(indexRow, 2).setText(obj_index["bits"])
				self._tableOutputs.cellWidget(indexRow, 3).setValue(value)
			indexRow += 1

		self.exec_()


class InternalCondition(QWidget):

	def __init__(self, parent, dialog):
		super(QWidget, self).__init__()
		self._parent = parent
		self._dialog = dialog
		uic.loadUi(QApplication.instance().get_src("ui", "sm", "subCondition.ui"), self)

		self.conditionValues = []
		self.outputValues = []

		self.editBtn.clicked.connect(self.edit)
		self.deleteBtn.clicked.connect(self.delete)

	def setOutputs(self, pOutputs):
		self.outputValues = pOutputs
		text = ""
		for value in self.outputValues:
			if value["input"] == "Number":
				text += str(value["id"]) + " = " + str(value["val"]) + ", "
			else:
				text += str(value["id"]) + " = " + str(value["input"]) + "[" + str(value["bits"]) + "], "
		self.outputsLabel.setText(text)

	def getOutputs(self):
		return self.outputValues

	def getConditions(self):
		return self.conditionValues

	def setConditions(self, pConditions):
		self.conditionValues = pConditions
		text = getTextPreview(self.conditionValues)
		self.conditionsLabel.setText(text)

	def edit(self):
		self._parent.editInternalCondition(self)

	def delete(self):
		self._dialog.deleteInternalCondition(self)
		self._parent.deleteInternalCondition(self)

	def updateText(self):
		self.conditionsLabel.setText("Holi")
		self.outputsLabel.setText("Holi2")

class ConnectionModal(QDialog):

	def __init__(self, parent, substate = False):
		super(QDialog, self).__init__()
		uic.loadUi(QApplication.instance().get_src("ui", "sm", "connection_modal.ui"), self)

		self._parent = parent
		self._lastConnection = None
		self._lastInternalC = None
		self._inputs = []
		self._conditions = []
		self._substate = substate

		# Tables
		self._tableConditions = QTableWidget()
		self.conditionsSpace.addWidget(self._tableConditions)

		self._tableOutputs = QTableWidget()
		self.outputsSpace.addWidget(self._tableOutputs)
		self._tableOutputs.setEnabled(False)

		self.buttonBox.accepted.connect(self._accept)
		self.addCondition.clicked.connect(self._addCondition)

		if not self._substate:
			self.addInternalConditionBtn.clicked.connect(self._addInternalCondition)
			self.setWindowTitle("Edit connection...")
		else:
			self.tabData.removeTab(2)
			self.tabData.setTabText(1, "Outputs definition")
			self.labelMealy.hide()
			self.connectionID.setText("Edit subcondition")
			self.fromWhereLabel.hide()
			self.setWindowTitle("Edit subcondition...")

	def _addInternalCondition(self):
		newCon = InternalCondition(self._lastConnection, self)
		self._lastConnection.addInternalCondition(newCon)
		self.layoutInternal.addWidget(newCon)

	def deleteInternalCondition(self, internalC):
		self.layoutInternal.removeWidget(internalC)

	def _addCondition(self):
		if len(self._inputs) > 0:
			self._updateConditions()
			self._conditions.append({
				"union" : "and",
				"input" : self._inputs[0]["id"],
				"bits" : "",
				"condition" : "==",
				"value" : "1"
			})
			self.updateTable()

	def _updateConditions(self):
		self._conditions = []
		for index in range(self._tableConditions.rowCount()):
			union = self._tableConditions.cellWidget(index, 0).currentText()
			inputC = self._tableConditions.cellWidget(index, 1).currentText()
			bits = self._tableConditions.cellWidget(index, 2).text()
			bits = bits.split(",")
			#bits.sort(reverse = True)
			bits = ",".join(bits)

			condition = self._tableConditions.cellWidget(index, 3).currentText()
			value = str(self._tableConditions.cellWidget(index, 4).value())
			if inputC != DELETE_TEXT:
				self._conditions.append({
					"union" : union,
					"input" : inputC,
					"bits" : bits,
					"condition" : condition,
					"value" : value
				})

	def updateTable(self):
		self._tableConditions.clear()
		self._tableConditions.setColumnCount(5)
		self._tableConditions.setRowCount(len(self._conditions))
		self._tableConditions.setHorizontalHeaderLabels(["Union", "Input", "Bits", "condition", "Value"])
		indexTable = 0
		for con in self._conditions:
			comboUnion = QComboBox()
			values = ["", "and", "or", "and (", "or (", ") and (", ") or ("]
			if indexTable == 0:
				values = ["", "("]
			[comboUnion.addItem(a) for a in values]
			try:
				index = values.index(con["union"])
				comboUnion.setCurrentIndex(index)
			except:
				pass
			comboUnion.currentIndexChanged.connect(self._updatePreview)

			comboInput = QComboBox()
			[comboInput.addItem(a["id"]) for a in self._inputs]
			index = comboInput.findText(con["input"])
			comboInput.setCurrentIndex(index)
			comboInput.addItem(DELETE_TEXT)
			comboInput.currentIndexChanged.connect(self._updatePreview)

			comboCondition = QComboBox()
			values = ["==", "<", "<=", ">", ">=", "!="]
			[comboCondition.addItem(a) for a in values]
			index = values.index(con["condition"])
			comboCondition.setCurrentIndex(index)
			comboCondition.currentIndexChanged.connect(self._updatePreview)

			inputValue = BigIntSpinbox()
			inputValue.setMinimum(0)
			obj_index = next((item for item in self._inputs if item["id"] == con["input"]), False)
			if obj_index != False:
				value = 2 ** int(obj_index["size"]) - 1
				inputValue.setMaximum(value)
			inputValue.setValue(int(con["value"]))
			inputValue.valueChanged.connect(self._updatePreview)

			bitsText = QLineEdit(con["bits"])
			bitsText.textChanged.connect(self._updatePreview)

			self._tableConditions.setCellWidget(indexTable, 0, comboUnion)
			self._tableConditions.setCellWidget(indexTable, 1, comboInput)
			self._tableConditions.setCellWidget(indexTable, 2, bitsText)
			self._tableConditions.setCellWidget(indexTable, 3, comboCondition)
			self._tableConditions.setCellWidget(indexTable, 4, inputValue)

			indexTable += 1

	def _updatePreview(self, *args):
		self._updateConditions()
		if self._parent.getMode() == MOORE:
			text = getTextPreview(self._conditions)
			self.previewConn.setText(text)
		#self.updateTable()

	def changeState(self):
		if self._parent.getMode() == MOORE:
			self._tableOutputs.setEnabled(False)
		else:
			self._tableOutputs.setEnabled(True)

	def _accept(self):
		self._updateConditions()
		valuesReturn = []
		for index in range(self._tableOutputs.rowCount()):
			idO = self._tableOutputs.cellWidget(index, 0).text()
			input = self._tableOutputs.cellWidget(index, 1).currentText()
			bits = self._tableOutputs.cellWidget(index, 2).text()
			val = self._tableOutputs.cellWidget(index, 3).value()
			valuesReturn.append({
				"id" : idO,
				"input" : input,
				"bits" : bits,
				"val" : val
			})
			self._tableOutputs.cellWidget(index, 1).setCurrentIndex(0)
			self._tableOutputs.cellWidget(index, 2).setText("")
			self._tableOutputs.cellWidget(index, 3).setValue(0)
		if self._lastInternalC != None:
			self._lastInternalC.setOutputs(valuesReturn)
			self._lastInternalC.setConditions(self._conditions)
		else:
			self._lastConnection.setOutputs(valuesReturn)
			self._lastConnection.setConditions(self._conditions)
		self._lastConnection = None
		self._lastInternalC = None
		self.previewConn.setText("Preview")
		self.accept()

	def reorganizeTable(self, outputs, inputs):
		self._inputs = inputs
		self._tableOutputs.clear()
		self._tableOutputs.setColumnCount(4)
		self._tableOutputs.setRowCount(len(outputs))
		self._tableOutputs.setHorizontalHeaderLabels(["Output", "Attach", "Bits", "Value"])

		index = 0
		for item in outputs:

			spin = BigIntSpinbox()
			spin.setMinimum(0)
			spin.setMaximum(2 ** int(item["size"]) - 1)

			attachCombo = QComboBox()
			attachCombo.addItem("Number")
			for inp in inputs:
				attachCombo.addItem(inp["id"])

			bits = QLineEdit()
			self._tableOutputs.setCellWidget(index, 0, QLabel(item["id"]))
			self._tableOutputs.setCellWidget(index, 1, attachCombo)
			self._tableOutputs.setCellWidget(index, 2, bits)
			self._tableOutputs.setCellWidget(index, 3, spin)
			index += 1

	def showProperties(self, connection):
		clearLayout(self.layoutInternal)
		internalAll = connection.getInternalConditions()
		for internal in internalAll:
			self.layoutInternal.addWidget(internal)

		self._lastConnection = connection
		text = connection.getID()
		connFrom = connection.getStateFrom().getID()
		connTo = connection.getStateTo().getID()

		self.fromWhereLabel.setText(connFrom + " -> " + connTo)
		self.connectionID.setText(text)

		inputs = self._parent.getInputs()
		outputs = self._parent.getOutputs()

		outputValues = connection.outputValues
		indexRow = 0
		for out in outputs:
			obj_index = next((item for item in outputValues if item["id"] == out["id"]), False)
			if obj_index != False:
				value = int(obj_index["val"])
				comboInput = self._tableOutputs.cellWidget(indexRow, 1)
				index = comboInput.findText(obj_index["input"])
				comboInput.setCurrentIndex(index)
				self._tableOutputs.cellWidget(indexRow, 2).setText(obj_index["bits"])
				self._tableOutputs.cellWidget(indexRow, 3).setValue(value)
			indexRow += 1

		self._conditions = connection.conditionValues
		self.updateTable()
		self.changeState()
		self._updatePreview()
		self.exec_()

	def showPropertiesInternal(self, internalC):
		clearLayout(self.layoutInternal)
		self._lastInternalC = internalC

		inputs = self._parent.getInputs()
		outputs = self._parent.getOutputs()

		outputValues = internalC.outputValues
		indexRow = 0
		for out in outputs:
			obj_index = next((item for item in outputValues if item["id"] == out["id"]), False)
			if obj_index != False:
				value = int(obj_index["val"])
				comboInput = self._tableOutputs.cellWidget(indexRow, 1)
				index = comboInput.findText(obj_index["input"])
				comboInput.setCurrentIndex(index)
				self._tableOutputs.cellWidget(indexRow, 2).setText(obj_index["bits"])
				self._tableOutputs.cellWidget(indexRow, 3).setValue(value)
			indexRow += 1

		self._conditions = internalC.conditionValues
		self.updateTable()
		self._tableOutputs.setEnabled(True)
		self._updatePreview()
		self.exec_()
