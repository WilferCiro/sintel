#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries
from simulador.tools.utils import get_pycompile_folder
from simulador.tools.sintel_svg import SintelSVG

# Qt libraries
from PyQt5.QtWidgets import QApplication, QWidget, QToolBox, QPushButton, QGraphicsPixmapItem, QGraphicsItem, QGraphicsScene, QGraphicsRectItem, QGraphicsView, QVBoxLayout, QGraphicsTextItem, QLabel
from PyQt5.QtGui import *
from PyQt5.QtCore import *

# System libraries
import os

class ComponentWidget(QVBoxLayout):

	def __init__(self):
		super(QVBoxLayout, self).__init__()

		self.sideScene = QGraphicsScene()
		sideView = QGraphicsView(self.sideScene)
		sideView.setRenderHints(QPainter.Antialiasing);
		sideView.setFixedWidth(200)
		sideView.setAlignment(Qt.AlignLeft | Qt.AlignTop)
		self.addWidget(sideView)
		sideView.setProperty("class", "backgroundComponents")

		self.addIcons(None)

	def addIcons(self, search):
		"""
			Add the left panel icons
		"""
		# TODO: add a thread to add it, first open application, and later load it to reduce load time
		for item in self.sideScene.items():
			self.sideScene.removeItem(item)
		#for item in self.sideSceneUser.items():
		#	self.sideSceneUser.removeItem(item)
		posX = 0
		posY = 0
		sizeX = 38
		sizeY = 38
		all_components = QApplication.instance().componentsHandler.getComponents(search)
		all_macros = QApplication.instance().componentsHandler.getMacros(search)
		for component in all_components:
			newIcon = Component(component)
			if component["user"] == 0:
				self.sideScene.addItem(newIcon)
				newIcon.setPos((sizeX * posX) + 10, (sizeY * posY) + 10);
				posX += 1
				if posX == 5:
					posX = 0
					posY += 1
			elif component["user"] == 1:
				self.sideScene.addItem(newIcon)
				newIcon.setPos((sizeX * posX) + 10, (sizeY * posY) + 10);
				posX += 1
				if posX == 5:
					posX = 0
					posY += 1

		for mac in all_macros:
			newIcon = Component(mac, isMacro = True)
			self.sideScene.addItem(newIcon)
			newIcon.setPos((sizeX * posX) + 10, (sizeY * posY) + 10);
			posX += 1
			if posX == 5:
				posX = 0
				posY += 1

class Component(QGraphicsItem):
	"""
		Left panel icon, for control the visual components
		@param svgIcon as str
	"""
	def __init__(self, data, isMacro = False):
		super(QGraphicsItem, self).__init__()

		self._isMacro = isMacro
		self._file_path = data["file"]
		self.pix = None

		# Pencil to draw
		self._pen = QPen(Qt.NoPen)
		self._pen.setWidth(0)

		# Rect
		self._rect = QGraphicsRectItem(QRectF(0, 0, 35, 35), self)
		self._rect.setPen(self._pen)
		self._rect.setBrush(QColor("#EDEDED"))

		# Image path
		self._imagePath = QApplication.instance().get_src("images", "svg", "default.svg")
		replace_image = None
		if isMacro:
			if data["icon"] != None:
				replace_image = data["icon"]
		else:
			if data["icon"] != None:
				replace_image = os.path.join(self._file_path.split("json")[0], "svg", "icon", data["icon"])

		if replace_image != None and replace_image[-3:] == "svg":
			if os.path.exists(replace_image):
				self._imagePath = replace_image

		# Tooltip
		if not isMacro:
			self.setToolTip(data["type"] + ", v. " + data["version"] + "\n" + data["category"] + "\n" + data["description"])

		# Hover event
		self.setAcceptHoverEvents(True)

		# Svg image
		self._svg_icon = SintelSVG(self._imagePath, self)
		self._svg_icon.setSize(QSizeF(35, 35))
		self._svg_icon.setPos(0, 0)
		self._svg_icon.setZValue(2)
		self._rectImg = QGraphicsRectItem(QRectF(-1, -1, 37, 37), self)
		self._rectImg.setPen(self._pen)
		self._rectImg.setBrush(QColor("#EDEDED"))

		# Component Data
		if not isMacro:
			self.data = {
				"name" : data["type"],
				"version" : data["version"],
				"date" : "-",
				"created_by" : "-",
				"description" : data["description"],
				"icon" : self._imagePath,
				"file" : data["file"]
			}
		else:
			self.data = {
				"name" : data["name"],
				"version" : "-",
				"date" : "-",
				"created_by" : "-",
				"description" : "User macro",
				"icon" : self._imagePath,
				"file" : data["file"]
			}


	def boundingRect(self):
		"""
			override boundingRect method, is mandatory, return the item size
		"""
		return QRectF(0, 0, 35, 35)

	def paint(self, *args):
		"""
			override paint method, is mandatory
			@param args
		"""
		None

	def hoverEnterEvent(self, event):
		"""
			When the mouse hover over the button
			@param event as mousehoverevent
		"""
		self._rectImg.setBrush(QColor("#FFFFFF"))
		QApplication.instance().projectsHandler.getView().propertiesWindow.showPreview(self.data)

	def hoverLeaveEvent(self, event):
		"""
			When the mouse leave hover over the button
			@param event as mousehoverevent
		"""
		self._rectImg.setBrush(QColor("#EDEDED"))
		QApplication.instance().projectsHandler.getView().propertiesWindow.leavePreview()

	def mouseMoveEvent(self, event):
		"""
			Emits when the mouse is moved
			@param event as mouse event
		"""
		drag = QDrag(event.widget())
		mime = QMimeData()
		drag.setMimeData(mime)
		if self._isMacro:
			mime.setText("macro-" + self._file_path)
		else:
			mime.setText("component-" + self._file_path)

		drag.setPixmap(QPixmap(self._imagePath).scaled(QSize(50, 50), 1))
		drag.setHotSpot(QPoint(15, 20))

		drag.exec_()

	def mouseReleaseEvent(self, event):
		"""
			Emits when the mouse is released
			@param event as moude event
		"""
		self.setCursor(Qt.ArrowCursor)

	def mousePressEvent(self, event):
		"""
			Emits when the mouse is pressed
			@param event as mouse event
		"""
		self.setCursor(Qt.ClosedHandCursor)
