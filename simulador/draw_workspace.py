#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries
from simulador.tools.utils import GRIDSPACE

# Qt libraries
from PyQt5.QtWidgets import QGraphicsRectItem, QGraphicsItem, QGraphicsObject, QGraphicsEllipseItem
from PyQt5.QtGui import QPixmap, QPen, QFont, QColor, QBrush
from PyQt5.QtCore import QSize, QRect, Qt, QPointF, QSizeF, QRectF

# System libraries


class RectangleObject(QGraphicsItem):
	"""
		Object in the workspace
	"""
	def __init__(self, position, type = "box", size = None, parent = None):
		"""
			init component
			@param data as dict
			@param elementID as String
			@param initialPos as QPointF
		"""
		super(QGraphicsItem, self).__init__()
		self.setPos(position)
		self._parent = parent

		if size == None:
			self._size = QPointF(100, 100)
		else:
			self._size = size
		self.setZValue(-1)

		# Control flags
		self.setFlag(QGraphicsItem.ItemIsSelectable, True)
		self.setFlag(QGraphicsItem.ItemSendsGeometryChanges, True)
		self.setFlag(QGraphicsItem.ItemIsFocusable, True)
		self.setFlag(QGraphicsItem.ItemIsMovable, True)
		self.setAcceptHoverEvents(True)

		self._resize = ResizePoint(self._size, self)
		# Draw type
		self._type = type
		if type == "box":
			self._graph = QGraphicsRectItem(0, 0, self._size.x(), self._size.y(), self)
		else:
			self._graph = QGraphicsEllipseItem(0, 0, self._size.x(), self._size.y(), self)
		self._graph.setBrush(Qt.white)

	def hoverEnterEvent(self, event):
		"""
			When the mouse hover over the button
			@param event as mousehoverevent
		"""
		self._resize.setVisible(True)

	def hoverLeaveEvent(self, event):
		"""
			When the mouse leave hover over the button
			@param event as mousehoverevent
		"""
		self._resize.setVisible(False)

	def getPosSave(self):
		return {"W" : self._size.x(), "H" : self._size.y(), "X" : self.scenePos().x(), "Y" : self.scenePos().y()}

	def getType(self):
		return self._type

	def resize(self, change, end = False):
		if change.x() > 20 and change.y() > 20:
			self.prepareGeometryChange();
			self._size = change
			self.update(self.boundingRect());

			self._graph.setRect(QRectF(0, 0, change.x(), change.y()))
			self._graph.prepareGeometryChange()
			self._graph.update()

	def getSize(self):
		return self._size

	def boundingRect(self):
		"""
			override boundingRect method, is mandatory, return the item size
		"""
		return QRectF(0, 0, self._size.x(), self._size.y())

	def paint(self, *args):
		"""
			override paint method, is mandatory
			@param args
		"""
		None

	def delete(self):
		self._parent.deleteDraw(self)

	def keyPressEvent(self, event):
		"""
			Detect the key press
			@param event as QKeyEvent
		"""
		if event.key() == Qt.Key_Delete:
			self.delete()

class ResizePoint(QGraphicsItem):
	def __init__(self, pos, parent):
		self._parent = parent
		self._startPos = pos
		super(QGraphicsItem, self).__init__(parent)

		self._default_color = Qt.black

		self._pen = QPen(self._default_color)
		self._pen.setWidth(2)

		self._rect = QGraphicsEllipseItem(QRectF(0, 0, 5, 5), self)
		self._rect.setPen(self._pen)
		background = QBrush(self._default_color)
		self._rect.setBrush(background)

		self.setVisible(False)

		self._dragStart2 = self._startPos

		self.setPos((self._startPos - QPointF(2.5, 2.5)) / 2)

		self.setZValue(1)


	def mouseReleaseEvent(self, event):
		"""
			Emits When the user releases the mouse
			@param event as mouseevent
		"""
		posEvent = QPointF(int(event.pos().x()/GRIDSPACE) * GRIDSPACE, int(event.pos().y()/GRIDSPACE) * GRIDSPACE)
		newPos = posEvent + self._dragStart
		self._parent.resize(newPos)
		self.setPos((self._parent.getSize() - QPointF(2.5, 2.5)) / 2)
		self.setCursor(Qt.ArrowCursor)

	def mousePressEvent(self, event):
		"""
			Emits When the user press the mouse
			@param event as mouseevent
		"""
		posEvent = QPointF(int(event.pos().x()/GRIDSPACE) * GRIDSPACE, int(event.pos().y()/GRIDSPACE) * GRIDSPACE)
		self._dragStart = self._parent.getSize()
		self.setCursor(Qt.ClosedHandCursor)

	def mouseMoveEvent(self, event):
		"""
			Emits When the user moves the mouse prev to press
			@param event as mouseevent
		"""
		posEvent = QPointF(int(event.pos().x()/GRIDSPACE) * GRIDSPACE, int(event.pos().y()/GRIDSPACE) * GRIDSPACE)
		self._parent.resize(posEvent + self._dragStart)

	def boundingRect(self):
		"""
			override boundingRect method, is mandatory, return the item size
		"""
		return QRectF(0, 0, 20, 20)

	def paint(self, *args):
		"""
			override paint method, is mandatory
			@param args
		"""
		None
