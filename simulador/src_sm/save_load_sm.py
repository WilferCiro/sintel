#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries
from simulador.tools.utils import projectExtension, get_pycompile_folder
from simulador.tools.utils import MOORE, MEALY

# Qt libraries
from PyQt5.QtWidgets import QApplication

# System libraries
import os
import math
import json

class SaveLoadSM():
	"""
		Handle the creation of the json
		@param projectName as str
		@param projectPath as str
	"""
	def __init__(self, smName = ""):
		# reset the data for overwrite the data
		self.resetFile(smName, MOORE)

	def resetFile(self, smName, mode, isDecoder = False):
		# Original files
		self.smName = str(smName)
		self.smMode = str(mode)

		self._isDecoder = isDecoder

		# File of project, in this file the user saves his progress
		self._jsonFile = os.path.join(get_pycompile_folder(), "components_user", "json", "sm", "sm_" + smName + projectExtension)
		self._pyFile = os.path.join(get_pycompile_folder(), "components_user", "source_files", "sm", smName + ".py")
		self._vhdlFile = os.path.join(get_pycompile_folder(), "components_user", "vhdl_file", "sm", smName + ".vhdl")
		self._verilogFile = os.path.join(get_pycompile_folder(), "components_user", "verilog_files", "sm", smName + ".v")

		self._data = {
			"type" : self.smName,
			"version" : "",
			"updated" : "",
			"description" : "",
			"inputs" : [],
			"outputs" : [],
			"file" : {
				"icon" : "sm.svg",
				"workspaceI" : "sm.svg",
				"py" : self.smName + ".py",
				"vhdl" : "sm/" + self.smName + ".vhdl",
				"verilog" : "sm/" + self.smName + ".v"
			},
			"icon" : {
				"width" : 4,
				"height" : 4
			},
			"var_connectors" : [
				{"id" : "bus1", "bits" : ["1"]}
			],
			"inProperties" : [

			],
			"eProperties" : [],
			"connectors" : [
				#{"X" : "4", "Y" : "0", "type" : "in", "reset" : True, "id" : "rst", "var" : "bus1"},
				#{"X" : "4", "Y" : "4", "type" : "in", "clock" : True, "id" : "clk", "var" : "bus1"}
			],
			"states" : [],
			"connections" : [],
			"index" : [],
			"sm" : True,
			"mode" : self.smMode,
			"is_decoder" : self._isDecoder
		}

		if not self._isDecoder:
			self._data["clock"] = True
			self._data["category"] = "State machine"
		else:
			self._data["category"] = "Decoder"
			self._data["clock"] = False

		"""
			{"type" : "select", "base" : "bit", "value" : "0", "name" : "reset", "options" : [
				{"text" : "Low", "value" : "0"},
				{"text" : "High", "value" : "1"}
			]}
		"""

	def getSizeBits(self, text):
		splited = text.split(":")
		bitsPut = text
		if len(splited) == 1:
			bits_size = 1
		else:
			splited = [int(splited[0]), int(splited[1])]
			splited.sort(reverse = True)
			bitsPut = str(splited[0]) + "," + str(splited[1])
			bits_size = (splited[0] - splited[1]) + 1
		return bitsPut, bits_size

	def getConditionsPreview(self, conditions):
		text = ""
		for cond in conditions:
			value = str(cond["value"])
			if cond["bits"] == "":
				text += cond["union"] + " self.read(\"" + cond["input"] + "\") " + cond["condition"] + " " + str(cond["value"]) + " "
			else:
				# 1100100
				# 6543210
				# 6543 -> send User
				# 0123
				bitsPut, bits_size = self.getSizeBits(cond["bits"])
				condBits = "[" + bitsPut.replace(",", ":") + "]"
				text += cond["union"] + " self.read(\"" + cond["input"] + "\", bits = '" + condBits + "') " + cond["condition"] + " " + str(cond["value"]) + " "
		complete = text.count("(") - text.count(")")
		text += ")" * complete
		if text == "":
			text = None
		return text

	def getConditionsPreviewVHDL(self, conditions):
		text = ""
		size = 1
		for cond in conditions:
			value = str(cond["value"])
			obj_index = next((item for item in self._data["inputs"] if item["id"] == cond["input"]), False)
			if obj_index != False:
				size = int(obj_index["size"])
				if size == 1:
					value = "'" + value + "'"
				else:
					if cond["bits"] == "":
						value = "std_logic_vector(to_unsigned(" + value + ", " + str(size) + "))"
					else:
						bitsPut, bits_size = self.getSizeBits(cond["bits"])
						value = "std_logic_vector(to_unsigned(" + value + ", " + str(bits_size) + "))"
				condition = cond["condition"].replace("==", "=")

				if cond["bits"] == "":
					text += cond["union"] + " " + cond["input"]  + " " + condition + " " + value + " "
				else:
					bitsPut, bits_size = self.getSizeBits(cond["bits"])
					condBits = "(" + bitsPut.replace(",", " downto ") + ")"
					text += cond["union"] + " " + cond["input"] + condBits + " " + condition + " " + value + " "

		complete = text.count("(") - text.count(")")
		text += ")" * complete
		if text == "":
			text = None
		return text

	def getConditionsPreviewVerilog(self, conditions):
		text = ""
		size = 1
		unions = {
			"and" : "&&",
			"or" : "||",
			">" : ">",
			">=" : ">=",
			"<" : "<",
			"<=" : "<=",
			"" : ""
		}
		for cond in conditions:
			value = str(cond["value"])
			obj_index = next((item for item in self._data["inputs"] if item["id"] == cond["input"]), False)
			if obj_index != False:
				size = int(obj_index["size"])
				if size == 1:
					value = "'" + value + "'"
				else:
					if cond["bits"] == "":
						value = str(size) + "'d" + value
					else:
						bitsPut, bits_size = self.getSizeBits(cond["bits"])
						value = str(bits_size) + "'d" + value
				condition = cond["condition"]

				if cond["bits"] == "":
					text += unions[cond["union"]] + " " + cond["input"]  + " " + condition + " " + value + " "
				else:
					bitsPut, bits_size = self.getSizeBits(cond["bits"])
					condBits = "[" + bitsPut.replace(",", ":") + "]"
					text += unions[cond["union"]] + " " + cond["input"] + condBits + " " + condition + " " + value + " "

		complete = text.count("(") - text.count(")")
		text += ")" * complete
		if text == "":
			text = None
		return text

	def getOutputsPreview(self, outputs):
		text = ""
		if len(outputs) > 0:
			for out in outputs:
				writeValue = str(out["val"])
				if out["input"] != "Number":
					if out["bits"] == "":
						writeValue = "self.read('" + out["input"] + "')"
					else:
						bitsPut, bits_size = self.getSizeBits(out["bits"])
						bits = "[" + bitsPut.replace(",", ":") + "]"
						writeValue = "self.read('" + out["input"] + "', bits = '" + bits + "')"
				text += "\n\t\t\tself.write(\"" + out["id"] + "\", " + writeValue + ")"
		#elif len(outputsDefault) > 0:
		#	for out in outputsDefault:
		#		text += "\n\t\t\tself.write(\"" + out["id"] + "\", " + str(out["val"][0]) + ")"
		else:
			text = "\n\t\t\tpass"
		return text

	def getOutputsPreviewVHDL(self, outputs):
		text = ""
		if len(outputs) > 0:
			for out in outputs:
				value = str(out["val"])
				condBits = ""
				obj_index = next((item for item in self._data["outputs"] if item["id"] == out["id"]), False)
				if obj_index != False:
					size = int(obj_index["size"])
					condBits = ""
					if out["input"] == "Number":
						if size == 1:
							value = "'" + value + "';"
						else:
							value = "std_logic_vector(to_unsigned(" + value + ", " + str(size) + "));"
					else:
						value = out["input"]
						if out["bits"] != "":
							bitsPut, bits_size = self.getSizeBits(out["bits"])
							condBits = "(" + bitsPut.replace(",", " downto ") + ")"
					text += out["id"] + " <= " + value + condBits + "\n\t\t\t"
		else:
			text = None
		return text

	def getOutputsPreviewVerilog(self, outputs):
		text = ""
		if len(outputs) > 0:
			for out in outputs:
				value = str(out["val"])
				condBits = ""
				obj_index = next((item for item in self._data["outputs"] if item["id"] == out["id"]), False)
				if obj_index != False:
					size = int(obj_index["size"])
					if out["input"] == "Number":
						value = str(size) + "'d" + value
					else:
						value = out["input"]
						if out["bits"] != "":
							bitsPut, bits_size = self.getSizeBits(out["bits"])
							condBits = "[" + bitsPut.replace(",", ":") + "]"
					text += out["id"] + " <= " + value + condBits + ";\n\t\t\t"
		else:
			text = None
		return text

	def sintetizePy(self):
		smConnectors = []
		smConditions = []
		smOutputs = []
		for connector in self._data["inputs"]:
			smConnectors.append("self._addInput(\"" + connector["id"] + "\")")
		for connector in self._data["outputs"]:
			smConnectors.append("self._addOutput(\"" + connector["id"] + "\")")
		textConnectors = "\n\t\t".join(smConnectors)

		smOutputsText = ""
		if not self._isDecoder:
			if self.smMode == MOORE:
				for state in self._data["states"]:
					newText = "if self._currentState == '" + state["id"] + "':"
					subConditions = state["int_cond"]
					replace = "\n\t\t\t"
					if len(subConditions) > 0:
						conditionsList = []
						for sub in subConditions:
							conditions = self.getConditionsPreview(sub["conditions"])
							outputs = self.getOutputsPreview(sub["outputs"])
							conditionsList.append("if " + conditions + ":" + outputs.replace("\n\t\t\t", "\n\t\t\t\t"))
						newText += "\n\t\t\t" + "\n\t\t\tel".join(conditionsList)
						newText += "\n\t\t\telse:"
						replace = "\n\t\t\t\t"

					newText += self.getOutputsPreview(state["outs"]).replace("\n\t\t\t", replace)
					smOutputs.append(newText)
				smOutputsText = "\n\t\tel".join(smOutputs)

			for conn in self._data["connections"]:
				conditions = self.getConditionsPreview(conn["conditions"])
				if conditions != None:
					newText = "if self._currentState == '" + conn["from"] + "' and (" + conditions + "):"
				else:
					newText = "if self._currentState == '" + conn["from"] + "':"

				newText += "\n\t\t\tself._nextState = '" + conn["to"] + "'"
				if self.smMode == MEALY:
					subConditions = conn["int_cond"]
					replace = "\n\t\t\t"
					if len(subConditions) > 0:
						conditionsList = []
						for sub in subConditions:
							conditions = self.getConditionsPreview(sub["conditions"])
							outputs = self.getOutputsPreview(sub["outputs"])
							conditionsList.append("if " + conditions + ":" + outputs.replace("\n\t\t\t", "\n\t\t\t\t"))
						newText += "\n\t\t\t" + "\n\t\t\tel".join(conditionsList)
						newText += "\n\t\t\telse:"
						replace = "\n\t\t\t\t"
					newText += self.getOutputsPreview(conn["outputs"]).replace("\n\t\t\t", replace)
				smConditions.append(newText)
			textConditions = "\n\t\tel".join(smConditions)

			text = ""
			with open(QApplication.instance().get_src("templates", "sm.py"), 'r+') as f:
				text = f.read()
				f.close()
		else:
			state = self._data["states"][0]
			smOutputsText = []
			textConditions = ""
			subConditions = state["int_cond"]
			replace = "\n\t\t"
			newText = ""
			if len(subConditions) > 0:
				conditionsList = []
				for sub in subConditions:
					conditions = self.getConditionsPreview(sub["conditions"])
					outputs = self.getOutputsPreview(sub["outputs"])
					conditionsList.append("if " + conditions + ":" + outputs)
				newText += "\n\t\t" + "\n\t\tel".join(conditionsList)
				newText += "\n\t\telse:"
				replace = "\n\t\t\t"

			newText += self.getOutputsPreview(state["outs"])
			smOutputsText = newText

			with open(QApplication.instance().get_src("templates", "decoder.py"), 'r+') as f:
				text = f.read()
				f.close()

		# Replace text with asignations
		text = text.replace("{{smConnectors}}", textConnectors)
		if smOutputsText != "":
			text = text.replace("{{smOutputs}}", smOutputsText)
		else:
			text = text.replace("{{smOutputs}}", "pass")
		text = text.replace("{{smConditions}}", textConditions)
		text = text.replace("{{smName}}", self._data["type"])

		with open(self._pyFile, 'w+') as f:
			f.write(text)
			f.close()


	def writeFile(self, reset = False):
		self.sintetizePy()
		self.sintetizeVHDL()
		self.sintetizeVerilog()

		sizeY = self._data["icon"]["height"]
		sizeX = sizeY / 2
		self._data["icon"]["width"] = sizeX

		if not self._isDecoder:
			self._data["connectors"].append(
				{"X" : 3, "Y" : sizeY, "type" : "in", "clock" : True, "id" : "clk", "var" : "bus1"}
			)
			self._data["connectors"].append(
				{"X" : 3, "Y" : "0", "type" : "in", "reset" : True, "id" : "rst", "var" : "bus1"},
			)

		with open(self._jsonFile, 'w+') as f:
			json.dump(self._data, f, indent = 1)
			f.close()

	def loadProject(self, path):
		data = {}
		self._jsonFile = path
		with open(self._jsonFile, 'r+') as f:
			if os.path.getsize(self._jsonFile) == 0:
				return None
			else:
				data = json.load(f)
				f.close()
		return data

	def saveSMData(self, autor, version, updated):
		self._data["autor"] = autor
		self._data["version"] = version
		self._data["updated"] = updated

	def addIO(self, inputs, outputs):
		sizeX = (max([len(inputs), len(outputs)]))
		self._data["icon"] = {
			"width" : 6 if sizeX < 6 else sizeX,
			"height" : (max([len(inputs), len(outputs)]))*2
		}
		y = 1
		for inp in inputs:
			size = str(inp["size"])
			obj_index = next((item for item in self._data["var_connectors"] if item["id"] == "bus" + size), False)
			if obj_index == False:
				self._data["var_connectors"].append({
					"id" : "bus" + size,
					"bits" : [size]
				})
			self._data["connectors"].append({
				"X" : "0",
				"Y" : str(y),
				"type" : "in",
				"id" : inp["id"],
				"var" : "bus" + size
			})
			y += 2

		y = 1
		for inp in outputs:
			size = str(inp["size"])
			obj_index = next((item for item in self._data["var_connectors"] if item["id"] == "bus" + size), False)
			if obj_index == False:
				self._data["var_connectors"].append({
					"id" : "bus" + size,
					"bits" : [size]
				})
			self._data["connectors"].append({
				"X" : str(self._data["icon"]["width"]),
				"Y" : str(y),
				"type" : "out",
				"id" : inp["id"],
				"var" : "bus" + size
			})
			y += 2

		self._data["inputs"] = inputs
		self._data["outputs"] = outputs

	def addState(self, state):
		"""
			Add a component in the compilation xml
			@param component as workspace_object
		"""
		position = state.getPos()

		internalC = state.getInternalConditions()
		dataInternal = []
		for inter in internalC:
			dataInternal.append({
				"conditions" : inter.getConditions(),
				"outputs" : inter.getOutputs()
			})

		self._data["states"].append({
			"id" : state.getID(),
			"position" : position,
			"isInitial" : state.isInitial(),
			"isFinal" : state.isFinal(),
			"outs" : state.getOutputs(),
			"int_cond" : dataInternal
		})

	def addConnection(self, connection):
		"""
			Add a signal to compiler file
			@param signalId as str
		"""
		internalC = connection.getInternalConditions()
		dataInternal = []
		for inter in internalC:
			dataInternal.append({
				"conditions" : inter.getConditions(),
				"outputs" : inter.getOutputs()
			})
		self._data["connections"].append({
			"from" : connection.getFrom(),
			"to" : connection.getTo(),
			"conditions" : connection.getConditions(),
			"outputs" : connection.getOutputs(),
			"int_cond" : dataInternal
		})

	def saveIndexes(self, indexes):
		self._data["index"] = indexes

	def sintetizeVHDL(self):
		text = ""
		if self._isDecoder:
			with open(QApplication.instance().get_src("templates", "decoder.vhdl"), 'r+') as f:
				text = f.read()
				f.close()
		else:
			if self.smMode == MOORE:
				with open(QApplication.instance().get_src("templates", "sm_moore.vhdl"), 'r+') as f:
					text = f.read()
					f.close()
			else:
				with open(QApplication.instance().get_src("templates", "sm_mealy.vhdl"), 'r+') as f:
					text = f.read()
					f.close()

		states = []
		for state in self._data["states"]:
			states.append(state["id"])
		states = ", ".join(states)

		inputs_proccess = []
		for input in self._data["inputs"]:
			inputs_proccess.append(input["id"])
		inputs_proccess = ", ".join(inputs_proccess)

		input_output = []
		for input in self._data["inputs"]:
			if int(input["size"]) == 1:
				input_output.append(input["id"] + " : in std_logic")
			else:
				input_output.append(input["id"] + " : in std_logic_vector(bus" + str(input["size"]) + "_LEN - 1 downto 0)")
		for output in self._data["outputs"]:
			if int(output["size"]) == 1:
				input_output.append(output["id"] + " : out std_logic")
			else:
				input_output.append(output["id"] + " : out std_logic_vector(bus" + str(output["size"]) + "_LEN - 1 downto 0)")
		input_output = ";\n\t\t" + ";\n\t\t".join(input_output)

		cases_input = []
		connections_temp = []

		if not self._isDecoder:
			for conn in self._data["connections"]:
				if not conn["from"] in connections_temp:

					connections_temp.append(conn["from"])
					whenText = "when " + conn["from"] + " =>\n\t\t\t"
					newText = ""

					ifConditions = []
					elseConditions = []

					addEndIf = False

					for conn2 in self._data["connections"]:
						if conn2["from"] == conn["from"]:
							conditions = self.getConditionsPreviewVHDL(conn2["conditions"])
							if conditions != None:
								textConditon = "if (" + conditions + ") then\n\t\t\t\tNS <= " + conn2["to"] + ";"
								addEndIf = True
							else:
								textConditon = "NS <= " + conn2["to"] + ";"

							if self.smMode == MEALY:
								subConditions = conn2["int_cond"]
								replace = "\n\t\t\t"
								if len(subConditions) > 0:
									conditionsList = []
									for sub in subConditions:
										conditions = self.getConditionsPreviewVHDL(sub["conditions"])
										outputs = self.getOutputsPreviewVHDL(sub["outputs"])
										conditionsList.append("if " + conditions + " then \n\t\t\t\t" + outputs.replace("\n\t\t\t", "\n\t\t\t\t"))
									textConditon += "\n\t\t\t" + "\n\t\t\tels".join(conditionsList)
									textConditon += "\n\t\t\telse"
									replace = "\n\t\t\t\t"

								textConditon += "\n\t\t\t\t" + self.getOutputsPreviewVHDL(conn2["outputs"]).replace("\n\t\t\t", replace)
								if len(subConditions) > 0:
									textConditon += "end if;"

							if conditions != None:
								ifConditions.append(textConditon)
							else:
								elseConditions.append(textConditon)

					if len(ifConditions) > 0:
						newText = "\n\t\t\tels".join(ifConditions)

					newText += "\n"
					if addEndIf:
						newText += "\t\t\tend if;"

					if len(elseConditions) > 0:
						newText += elseConditions[0] + ""

					newText = whenText + newText

					cases_input.append(newText)

			for state in self._data["states"]:
				if state["id"] not in connections_temp:
					newText = "when " + state["id"] + " =>\n\t\t\tNS <= " + state["id"] + ";"
					cases_input.append(newText)

			cases_input = "\n\t\t".join(cases_input)

			cases_output = []
			if self.smMode == MOORE:
				for state in self._data["states"]:
					outputs = self.getOutputsPreviewVHDL(state["outs"])
					if outputs != None:
						newText = "when " + state["id"] + " =>\n\t\t\t"

						subConditions = state["int_cond"]
						replace = "\n\t\t\t"
						if len(subConditions) > 0:
							conditionsList = []
							for sub in subConditions:
								conditions = self.getConditionsPreviewVHDL(sub["conditions"])
								outputs = self.getOutputsPreviewVHDL(sub["outputs"])
								conditionsList.append("if " + conditions + " then \n\t\t\t\t" + outputs.replace("\n\t\t\t", "\n\t\t\t\t"))
							newText += "\n\t\t\t" + "\n\t\t\tels".join(conditionsList)
							newText += "\n\t\t\telse"
							replace = "\n\t\t\t\t"

						newText += "\n\t\t\t\t" + outputs.replace("\n\t\t\t", replace)
						if len(subConditions) > 0:
							newText += "end if;"

						#newText += outputs
						cases_output.append(newText)
			cases_output = "\n\t\t".join(cases_output)



		else: # Logic for decoder
			state = self._data["states"][0]
			states = ""
			cases_output = ""
			textConditon = ""
			subConditions = state["int_cond"]
			if len(subConditions) > 0:
				conditionsList = []
				for sub in subConditions:
					conditions = self.getConditionsPreviewVHDL(sub["conditions"])
					outputs = self.getOutputsPreviewVHDL(sub["outputs"])
					conditionsList.append("if " + conditions + " then \n\t" + outputs)
				textConditon += "\n" + "\nels".join(conditionsList)
				textConditon += "\nelse"

			textConditon += "\n\t" + self.getOutputsPreviewVHDL(state["outs"])
			if len(subConditions) > 0:
				textConditon += "\nend if;"

			cases_input = textConditon
			input_output = input_output[1:]

		generics = []
		for var in self._data["var_connectors"]:
			if int(var["bits"][0]) > 1:
				generics.append(var["id"] + "_LEN : natural:=" + var["bits"][0])
		if len(generics) > 0:
			generics = "generic(\n\t\t" + ";\n\t\t".join(generics) + "\n\t);"
		else:
			generics = ""

		text = text.replace("{{machine_name}}", self._data["type"])
		text = text.replace("{{states}}", states)
		text = text.replace("{{inputs_proccess}}", inputs_proccess)
		text = text.replace("{{input_output}}", input_output)
		text = text.replace("{{cases_input}}", cases_input)
		text = text.replace("{{cases_output}}", cases_output)
		text = text.replace("{{generics}}", generics)

		with open(self._vhdlFile, 'w+') as f:
			f.write(text)
			f.close()






	def sintetizeVerilog(self):
		text = ""
		if self._isDecoder:
			with open(QApplication.instance().get_src("templates", "decoder.v"), 'r+') as f:
				text = f.read()
				f.close()
		else:
			if self.smMode == MOORE:
				with open(QApplication.instance().get_src("templates", "sm_moore.v"), 'r+') as f:
					text = f.read()
					f.close()
			else:
				with open(QApplication.instance().get_src("templates", "sm_mealy.v"), 'r+') as f:
					text = f.read()
					f.close()

		states = []
		index = 0
		bitsStates = math.ceil(math.log2(len(self._data["states"]) + 2))
		for state in self._data["states"]:
			states.append(state["id"] + " = " + str(bitsStates) +"'d" + str(index))
			index += 1
		states = ", ".join(states)

		input_output = []
		inputs_always = []
		for input in self._data["inputs"]:
			inputs_always.append(input["id"])
			if int(input["size"]) == 1:
				input_output.append("input " + input["id"])
			else:
				input_output.append("input [bus" + str(input["size"]) + "_LEN - 1 : 0]" + input["id"])
		for output in self._data["outputs"]:
			if int(output["size"]) == 1:
				input_output.append("output reg " + output["id"])
			else:
				input_output.append("output reg [bus" + str(output["size"]) + "_LEN - 1 : 0]" + output["id"])
		input_output = ", " + ", ".join(input_output)
		inputs_always = ", ".join(inputs_always)

		cases_input = []
		connections_temp = []

		if not self._isDecoder:
			for conn in self._data["connections"]:
				if not conn["from"] in connections_temp:
					connections_temp.append(conn["from"])
					whenText = conn["from"] + " : begin\n\t\t\t"
					newText = ""

					ifConditions = []
					elseConditions = []

					addEndIf = False

					for conn2 in self._data["connections"]:
						if conn2["from"] == conn["from"]:
							conditions = self.getConditionsPreviewVerilog(conn2["conditions"])
							if conditions != None:
								textConditon = "if (" + conditions + ") begin\n\t\t\t\tNS <= " + conn2["to"] + ";"
								addEndIf = True
							else:
								textConditon = "NS <= " + conn2["to"] + ";"

							if self.smMode == MEALY:
								subConditions = conn2["int_cond"]
								replace = "\n\t\t\t"
								if len(subConditions) > 0:
									conditionsList = []
									for sub in subConditions:
										conditions = self.getConditionsPreviewVerilog(sub["conditions"])
										outputs = self.getOutputsPreviewVerilog(sub["outputs"])
										conditionsList.append("if " + conditions + " begin \n\t\t\t\t" + outputs.replace("\n\t\t\t", "\n\t\t\t\t"))
									textConditon += "\n\t\t\t" + "\n\t\t\tend else ".join(conditionsList)
									textConditon += "\n\t\t\tend else begin"
									replace = "\n\t\t\t\t\t"

								textConditon += "\n\t\t\t\t" + self.getOutputsPreviewVerilog(conn2["outputs"]).replace("\n\t\t\t", replace)
								if len(subConditions) > 0:
									textConditon += "end"

							if conditions != None:
								ifConditions.append(textConditon)
							else:
								elseConditions.append(textConditon)

					if len(ifConditions) > 0:
						newText = "\n\t\t\tend else ".join(ifConditions)

					newText += "\n"
					if addEndIf:
						newText += "\t\t\tend"

					if len(elseConditions) > 0:
						newText += elseConditions[0] + ""

					newText += "\n\t\tend"

					newText = whenText + newText

					cases_input.append(newText)

			for state in self._data["states"]:
				if state["id"] not in connections_temp:
					newText = state["id"] + " : begin \n\t\t\tNS <= " + state["id"] + ";\n\t\t\tend"
					cases_input.append(newText)

			cases_input = "\n\t\t".join(cases_input)

			cases_output = []
			if self.smMode == MOORE:
				for state in self._data["states"]:
					outputsIn = self.getOutputsPreviewVerilog(state["outs"])
					if outputsIn != None:
						newText = state["id"] + " : begin\n\t\t\t"

						subConditions = state["int_cond"]
						replace = "\n\t\t\t"
						if len(subConditions) > 0:
							conditionsList = []
							for sub in subConditions:
								conditions = self.getConditionsPreviewVerilog(sub["conditions"])
								outputs = self.getOutputsPreviewVerilog(sub["outputs"])
								conditionsList.append("if (" + conditions + ") begin \n\t\t\t\t" + outputs.replace("\n\t\t\t", "\n\t\t\t\t"))
							newText += "\n\t\t\t" + "\n\t\t\tend else ".join(conditionsList)
							newText += "\n\t\t\tend else begin"
							replace = "\n\t\t\t\t"

						newText += "\n\t\t\t\t" + outputsIn.replace("\n\t\t\t", replace)
						if len(subConditions) > 0:
							newText += "end "

						newText += "\n\t\tend"
						cases_output.append(newText)
			cases_output = "\n\t\t".join(cases_output)

		else: # Decoder logic
			cases_output = ""
			state = self._data["states"][0]
			outputsIn = self.getOutputsPreviewVerilog(state["outs"])
			subConditions = state["int_cond"]
			newText = ""
			if len(subConditions) > 0:
				conditionsList = []
				for sub in subConditions:
					conditions = self.getConditionsPreviewVerilog(sub["conditions"])
					outputs = self.getOutputsPreviewVerilog(sub["outputs"])
					conditionsList.append("\tif (" + conditions + ") begin \n\t\t" + outputs)
				newText += "\n" + "\nend else ".join(conditionsList)
				newText += "\n\tend else begin"

			newText += "\n\t\t" + outputsIn
			newText += "\n\tend"
			cases_input = newText

		generics = []
		for var in self._data["var_connectors"]:
			if int(var["bits"][0]) > 1:
				generics.append("parameter " + var["id"] + "_LEN = " + var["bits"][0])
		if len(generics) > 0:
			generics = "#(" + ", ".join(generics) + ")"
		else:
			generics = ""

		input_output = input_output[1:]

		text = text.replace("{{machine_name}}", self._data["type"])
		text = text.replace("{{states_assignation}}", states)
		text = text.replace("{{input_output}}", input_output)
		text = text.replace("{{number_states}}", str(bitsStates - 1))
		text = text.replace("{{bitsStates}}", str(bitsStates))

		text = text.replace("{{inputs_always}}", str(inputs_always))

		text = text.replace("{{cases_input}}", cases_input)
		text = text.replace("{{cases_output}}", cases_output)
		text = text.replace("{{generics}}", generics)

		with open(self._verilogFile, 'w+') as f:
			f.write(text)
			f.close()
