#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries
from simulador.tools.utils import projectExtension, get_pycompile_folder

# Qt libraries
from PyQt5.QtWidgets import QApplication

# System libraries
import os
import json

class Precompiler():
	"""
		Handle the creation of the json
		@param projectName as str
		@param projectPath as str
	"""
	def __init__(self, projectPath, projectName):
		self.resetFile()
		# Original files
		projectName = str(projectName)
		projectPath = str(projectPath)

		self._maxClockCicle = 2
		self._hasClock = False

		# Temp files
		self._execFile = os.path.join(get_pycompile_folder(), "default.py")


	def resetFile(self):
		self.dataCompile = {
			"components_files" : [],
			"reload_import" : [],
			"components": [],
			"defaultValues" : [],
			"defaultValuesUpdate" : [],
			"connSubSignal" : [],
			"signals": [],
			"connSignals": [],
			"update_inputs" : [],
			"reset_components" : [],
			"data_update" : [],
			"data_update_graph" : [],
			"reset_signals" : [],
			"clock_gen" : []
		}
		self._hasClock = False

	def addComponentMacro(self, component):
		"""
			Add a component in the compilation xml
			@param component as workspace_object
		"""
		componentID = str(component.getID())
		self.dataCompile["components"].append("self." + componentID + " = " + str(component.getType()) + "(ID = '"+ componentID +"', parent = self, properties = {})")

		inp = component
		if inp.getDefaultValue() != None and inp.getDefaultValue() != "-1":
			self.dataCompile["defaultValues"].append("self." + componentID + ".setDefaultValue('pIn', " + str(inp.getDefaultValue()) + ")")

			self.dataCompile["defaultValuesUpdate"].append("self." + componentID + ".update()")


	def addComponent(self, component):
		"""
			Add a component in the compilation xml
			@param component as workspace_object
		"""
		componentID = str(component.getID())
		properties = "{"
		for prop in component.getProperties():
			properties += "'" + str(prop["name"]) + "' : " + "'"+ str(prop["value"]) +"', "
		properties += "}"
		isStateMachine = str(component.isStateMachine())
		self.dataCompile["components"].append("self." + componentID + " = " + str(component.getType()) + "." + str(component.getType()) + "(ID = '"+ componentID +"', parent = self, isStateMachine = " + isStateMachine + ", properties = " + properties + ")")
		clock = component.getClock()
		if clock != None:
			# Assign clock to component
			self.dataCompile["components"].append("self." + componentID + ".assignClock()")
			#self.dataCompile["update_clock"].append("if self._clkCount % " + str(clock_cicle) + " == 0 and self._clkCount != 0:\n\t\t\tself." + component.getID() + ".upd()\n\t\t\tself._clockUpdated = True")

		if component.isInput() and not component.isClockGen():
			self.dataCompile["update_inputs"].append("self." + component.getID() + ".upd()")
		if component.isClockGen():
			self.dataCompile["clock_gen"].append("self." + component.getID() + ".upd()")
			self._hasClock = True

		file_inc = component.getPrecompilerFiles()
		if file_inc != None:
			#file_add = file_inc["type"] + " = importlib.import_module('.', package='" + file_inc["py"].replace(".py", "") + "')"
			file_add = file_inc["type"] + " = importlib.import_module('" + file_inc["py"].replace(".py", "") + "', package='" + file_inc["type"] + "')"
			if file_add not in self.dataCompile["components_files"]:
				self.dataCompile["components_files"].append(file_add)
			reload_import = "importlib.reload(" + file_inc["type"] + ")"
			if reload_import not in self.dataCompile["reload_import"]:
				self.dataCompile["reload_import"].append(reload_import)

		for field in component.getReferenceBits():
			for conn in field["connectors"]:
				if not conn.isResetIn() and not conn.isClockIn() and not conn.isClockOut() and conn.getType() == "in":
					value = conn.getDefaultValue()
					if value != None and value != "":
						value = str(value)
						self.dataCompile["defaultValues"].append("self." + componentID + ".setDefaultValue('" + str(conn.getID()) + "', " + value + ")")
						if clock:
							self.dataCompile["defaultValuesUpdate"].append("self." + componentID + ".updateClock()")
						else:
							self.dataCompile["defaultValuesUpdate"].append("self." + componentID + ".update()")



	def addSignal(self, signal):
		"""
			Add a signal to compiler file
			@param signalId as str
		"""
		size = str(signal.getSize())
		signalID = signal.getID()
		self.dataCompile["signals"].append("self." + signalID + " = SimulationSignal(size = " + size + ")")

		if signal.hasGraph():
			self.dataCompile["data_update_graph"].append("dataUpdateGraph.append({'ID' : '" + signalID + "', 'value' : self." + signalID + ".read(visual = True)})")

	def addSignalShow(self, signal):
		signalID = signal.getID()
		textUdt = "dataUpdate.append({'ID' : '" + signalID + "', 'value' : self." + signalID + ".read(visual = True)})"
		if not textUdt in self.dataCompile["data_update"]:
			self.dataCompile["data_update"].append(textUdt)

	def addConnectionSignals2(self, ret, original, model, connector, invert = False):
		componentID = connector.getParentID()
		componentID2 = original.getConnections()[0].getParentID()
		isClockIn = connector.isClockIn()
		isResetIn = connector.isResetIn()
		index = 0
		for i in model:
			#self.dataCompile["connSubSignal"].append("self." + signal2 + ".setSubSignal(" + str(index) + ", self." + signal1 + ".getSubSignal(" + str(i) + "))")
			self.dataCompile["connSubSignal"].append("self." + original.getID() + ".setSubSignal(" + str(i) + ", self." + ret.getID() + ".getSubSignal(" + str(index) + ")) #2 " + str(invert))
			index += 1

		if invert:
			self.dataCompile["connSignals"].append("self." + componentID + ".assingSignal('" + connector.getID() + "', self." + original.getID() + ") #2 " + str(invert))
			componentID2 = componentID
		else:
			self.dataCompile["connSignals"].append("self." + componentID + ".assingSignal('" + connector.getID() + "', self." + ret.getID() + ") #2 " + str(invert))
		if isClockIn:
			flank = 0
			clock = connector.getParent().getClock()
			if clock != None:
				flank = int(clock["update"])
			self.dataCompile["connSignals"].append("self." + ret.getID() + ".connectTo(self." + componentID2 + ", clock = True, flank = " + str(flank) + ") #2 " + str(invert))
		elif isResetIn:
			#self.dataCompile["connSignals"].append("self." + ret.getID() + ".connectTo(self." + componentID2 + ", reset = True) #2 " + str(invert))
			self.dataCompile["connSignals"].append("self." + ret.getID() + ".connectTo(self." + componentID2 + ") #2 " + str(invert))
			self.dataCompile["reset_components"].append("self." + componentID2 + ".update() #Res 2 " + str(invert))
		else:
			self.dataCompile["connSignals"].append("self." + ret.getID() + ".connectTo(self." + componentID2 + ") #2 " + str(invert))

	def addConnectionSignals3(self, ret, original, model, connector, original2, invert = False):
		# S1 <- origina
		# S2 <- ret

		# S2.assignSubSignal(0, s1(0)) Y
		# notRet.assignSignal("in", S2)
		# notOrig.assignSignal("out", S1)
		# S1.connectTo(notRet)

		componentID = connector.getParentID()
		componentID2 = original.getConnections()[0].getParentID()
		isClockIn = connector.isClockIn()
		isResetIn = connector.isResetIn()
		index = 0
		for i in model:
			#self.dataCompile["connSubSignal"].append("self." + signal2 + ".setSubSignal(" + str(index) + ", self." + signal1 + ".getSubSignal(" + str(i) + "))")
			if invert:
				self.dataCompile["connSubSignal"].append("self." + original.getID() + ".setSubSignal(" + str(index) + ", self." + ret.getID() + ".getSubSignal(" + str(i) + ")) #3" + str(invert))
			else:
				self.dataCompile["connSubSignal"].append("self." + ret.getID() + ".setSubSignal(" + str(index) + ", self." + original.getID() + ".getSubSignal(" + str(i) + ")) #3" + str(invert))
			index += 1

		if invert:
			self.dataCompile["connSignals"].append("self." + componentID + ".assingSignal('" + connector.getID() + "', self." + original.getID() + ") #3" + str(invert))
		else:
			self.dataCompile["connSignals"].append("self." + componentID + ".assingSignal('" + connector.getID() + "', self." + ret.getID() + ") #3" + str(invert))

		if isClockIn:
			flank = 0
			clock = connector.getParent().getClock()
			if clock != None:
				flank = int(clock["update"])
			self.dataCompile["connSignals"].append("self." + original2.getID() + ".connectTo(self." + componentID + ", clock = True, flank = " + str(flank) + ") #3" + str(invert))
		elif isResetIn:
			#self.dataCompile["connSignals"].append("self." + original2.getID() + ".connectTo(self." + componentID + ", reset = True) #3" + str(invert))
			self.dataCompile["connSignals"].append("self." + original2.getID() + ".connectTo(self." + componentID + ") #3" + str(invert))
			self.dataCompile["reset_components"].append("self." + componentID + ".update() #Res 2 " + str(invert))
		else:
			self.dataCompile["connSignals"].append("self." + original2.getID() + ".connectTo(self." + componentID + ") #3" + str(invert))


	def addConnection(self, component, connector, signal, isInput):
		"""
			Add a signal to compiler file
			@param componentID as str
			@param pinName as str
			@param signalID as str
		"""
		componentID = component.getID()
		connectorID = connector.getID()
		signalID = signal.getID()
		size = connector.getSize()
		isClockIn = connector.isClockIn()
		isResetIn = connector.isResetIn()

		flank = 0
		clock = component.getClock()
		if clock != None:
			flank = int(clock["update"])

		self.dataCompile["connSignals"].append("self." + componentID + ".assingSignal('" + connectorID + "', self." + signalID + ")")
		if isInput:
			if isClockIn:
				self.dataCompile["connSignals"].append("self." + signalID + ".connectTo(self." + componentID + ", clock = True, flank = " + str(flank) + ")")
			elif isResetIn:
				#self.dataCompile["connSignals"].append("self." + signalID + ".connectTo(self." + componentID + ", reset = True)")
				self.dataCompile["connSignals"].append("self." + signalID + ".connectTo(self." + componentID + ")")
				self.dataCompile["reset_components"].append("self." + componentID + ".update()")
			else:
				self.dataCompile["connSignals"].append("self." + signalID + ".connectTo(self." + componentID + ")")

	def generateExec(self):
		"""
			Create the .py file
		"""
		text = ""
		with open(QApplication.instance().get_src("templates", "run.py"), 'r+') as f:
			text = f.read()
			f.close()

		# Replace text with asignations
		text = text.replace("{{hasClock}}", str(self._hasClock))
		text = text.replace("{{components_files}}", "\n".join(self.dataCompile["components_files"]))
		text = text.replace("{{reload_import}}", "\n\t\t\t".join(self.dataCompile["reload_import"]))
		text = text.replace("{{components}}", "\n\t\t".join(self.dataCompile["components"]))
		text = text.replace("{{defaultValues}}", "\n\t\t".join(self.dataCompile["defaultValues"]))
		text = text.replace("{{defaultValuesUpdate}}", "\n\t\t".join(self.dataCompile["defaultValuesUpdate"]).replace("updateClock", "readUpdate") + "\n\t\t" + "\n\t\t".join(self.dataCompile["defaultValuesUpdate"]).replace("updateClock", "writeUpdate"))
		text = text.replace("{{signals}}", "\n\t\t".join(self.dataCompile["signals"]))
		text = text.replace("{{connSubSignals}}", "\n\t\t".join(self.dataCompile["connSubSignal"]))
		text = text.replace("{{connSignals}}", "\n\t\t".join(self.dataCompile["connSignals"]))
		text = text.replace("{{max_clock_cicle}}", str(self._maxClockCicle))

		text = text.replace("{{update_inputs}}", "\n\t\t".join(self.dataCompile["update_inputs"]))
		text = text.replace("{{initial_input}}", "\n\t\t".join(self.dataCompile["update_inputs"]).replace("upd", "initialUpdate"))
		text = text.replace("{{reset_components}}", "\n\t\t".join(self.dataCompile["reset_components"]).replace("update", "readUpdate") + "\n\t\t" + "\n\t\t".join(self.dataCompile["reset_components"]).replace("update", "writeUpdate"))

		text = text.replace("{{data_update}}", "\n\t\t\t".join(self.dataCompile["data_update"]))
		text = text.replace("{{data_update_graph}}", "\n\t\t".join(self.dataCompile["data_update_graph"]))
		text = text.replace("{{reset_signals}}", "\n\t\t".join(self.dataCompile["reset_signals"]))

		textClocks = "if self._clkCount % " + str(self._maxClockCicle) + " == 0:\n\t\t\tdataUpdateGraph = self._updateGraph()\n\t\t\tself._clockUpdated = True\n\t\t\t" + "\n\t\t\t".join(self.dataCompile["clock_gen"])
		text = text.replace("{{update_clock}}", textClocks)

		text = text.replace("{{clock_declarations}}", "")
		#text = text.replace("{{clock_declarations}}", "\n\t\t".join(clock_declarations))

		text = text.replace("{{reset_first}}", "\n\t\t".join(self.dataCompile["clock_gen"])) #+ "\n\t\t".join(resetFirst).replace("update()", "program_start()"))

		with open(self._execFile, 'w+') as f:
			f.write(text)
			f.close()
