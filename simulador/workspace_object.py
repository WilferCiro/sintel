#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries
from simulador.tools.xml_reader import getObjectFiles, getObjectConnectors, getObjectProperties, getsignalReference
from simulador.tools.utils import get_pycompile_folder, GRIDSPACE, workspaceSize
from simulador.connector_object import ConnectorObject
from simulador.tools.sintel_svg import SintelSVG
from simulador.properties_window import PropertiesWindow

# Qt libraries
from PyQt5.QtWidgets import QGraphicsItem, QGraphicsPixmapItem, QGraphicsRectItem, QApplication, QGraphicsTextItem, QGraphicsProxyWidget
from PyQt5.QtGui import QPixmap, QPen, QFont
from PyQt5.QtCore import QSize, QRectF, Qt, QPointF, QSizeF
from PyQt5.QtSvg import QGraphicsSvgItem

# System libraries
import math
import os

class WorkspaceObject(QGraphicsItem):
	"""
		Object in the workspace
	"""
	def __init__(self, data, initialLoad, parent = None):
		"""
			init component
			@param data as dict
			@param elementID as String
			@param initialPos as QPointF
		"""
		super(QGraphicsItem, self).__init__()
		self._parent = parent
		# Variables declarations
		self._initialData = data
		self._initialLoad = initialLoad
		if self._initialLoad["loadID"] != None:
			self.elementID = self._initialLoad["loadID"]
		else:
			self.elementID = str(self._initialData["type"]) + str(self._initialLoad["elementID"])
			while self._parent.getComponentByID(self.elementID) != None:
				self.elementID = str(self._initialData["type"]) + str(self._parent.getComponentIndex(add = True))

		self._isFocus = False
		self._isStateMachine = data["isSM"]
		self.setZValue(1)

		# Control flags
		self.setFlag(QGraphicsItem.ItemIsSelectable, True)
		self.setFlag(QGraphicsItem.ItemSendsGeometryChanges, True)
		self.setFlag(QGraphicsItem.ItemIsFocusable, True)
		self.setAcceptHoverEvents(True)
		self._location = QPointF(0, 0)
		self._dragStart = QPointF(0, 0)

		# User component
		self._isUserComponent = False if self._initialData["file"].find("components_user") == -1 else True

		# Object Parameters
		self._files = getObjectFiles(self._initialData["file"])
		self._editableProperties, self._inlineProperties = getObjectProperties(self._initialData["file"])
		self._connectors = getObjectConnectors(self._initialData["file"])
		self._referenceBits = getsignalReference(self._initialData["file"])
		for ref in self._referenceBits:
			ref["connectors"] = []
		self._hasClock = self._initialData["clock"]

		self._clockValue = None
		self._isClockGen = False

		if self._hasClock:
			self._clockValue = self._initialLoad["clock"]

		self._isInput = self._initialData["input"]
		self._isOutput = self._initialData["output"]

		# Visual configuration to show
		svg_icon = SintelSVG(os.path.join(self._initialData["file"].split("json")[0], "svg", "workspace", self._files["workspaceI"]), self)
		svg_icon.setSize(QSizeF(GRIDSPACE * self._initialData["icon"]["width"], GRIDSPACE * self._initialData["icon"]["height"]))
		self.width = self._initialData["icon"]["width"] * GRIDSPACE
		self.height = self._initialData["icon"]["height"] * GRIDSPACE

		if self.width == 0:
			self.width = 25
		if self.height == 0:
			self.height = 25

		self._nameTextWidget = QGraphicsTextItem(self.elementID, self)
		self._nameTextWidget.setPos(-8, -2 * GRIDSPACE)

		f = QFont()
		f.setFamily("Monaco")
		f.setPointSize(7)
		self._nameTextWidget.setFont(f)

		self.setToolTip(self._initialData["type"])
		self._pen = QPen(Qt.blue)
		self._pen.setWidth(1)
		self._rect = QGraphicsRectItem(QRectF(0, 0, self.width, self.height), self)
		self._rect.setPen(QPen(Qt.NoPen))

		# Dragable
		if self._initialLoad["initialPos"] is not None:
			self._location = self._initialLoad["initialPos"]
			self._location.setX(int(self._location.x() / GRIDSPACE) * GRIDSPACE)
			self._location.setY(int(self._location.y() / GRIDSPACE) * GRIDSPACE)
			self._dragStart = self._initialLoad["initialPos"]
			self.setPos(self._location)

		# Macro component
		self._macro = None

		# Connectors widgets
		self._allConnectorsWidget = []
		for con in self._connectors:
			newCon = ConnectorObject(con, self)
			if "var" in con:
				obj_index = next((item for item in self._referenceBits if item["id"] == con["var"]), False)
				if obj_index != False:
					obj_index["connectors"].append(newCon)
					newCon.setSize(obj_index["bits"][0], posibleBits = obj_index["id"])

			x = float(con["X"]) * GRIDSPACE
			y = float(con["Y"]) * GRIDSPACE

			newCon.setPos(x - 2.5, y - 2.5)
			dataCon = {'connectorID' : con["id"], 'connectorObject' : newCon}
			self._allConnectorsWidget.append(dataCon)

		if self._initialLoad["properties"] != None:
			self.setProperties(self._initialLoad["properties"], False)

		self.setInlineProperties()

	def getSignals(self):
		signals = []
		for conn in self._allConnectorsWidget:
			sig = conn["connectorObject"].getSignals()
			if sig != None:
				signals.append(sig)
		return signals

	def hideFromMacroComponent(self, macro):
		self.setVisible(False)
		for conn in self._allConnectorsWidget:
			conn["connectorObject"].hideFromMacroComponent(macro)

	def showFromMacroComponent(self, macro):
		self.setVisible(True)
		for conn in self._allConnectorsWidget:
			conn["connectorObject"].showFromMacroComponent(macro)

	def getIODeleteMacro(self, macroComponent):
		signals = []
		for conn in self._allConnectorsWidget:
			forDelete, signal = conn["connectorObject"].forDeleteMacro(macroComponent)
			if forDelete:
				signals.append(signal)
		return signals

	def setMacro(self, macro):
		self._macro = macro

	def deleteMacro(self):
		self._macro = None

	def getMacro(self):
		return self._macro

	def setName(self, ID, aditional = 0):
		if aditional > 0:
			ID = ID[0:-1] + str(aditional)
		aditional += 1
		exists = QApplication.instance().projectsHandler.getWorkspace().getComponentByID(ID)
		if exists != None:
			self.setName(ID, aditional = aditional)
		else:
			QApplication.instance().projectsHandler.getWorkspace().changeComponentID(self.elementID, ID)
			self.elementID = ID
			self._nameTextWidget.setHtml(self.elementID)

	def isStateMachine(self):
		return self._isStateMachine

	def setInlineProperties(self):
		i = 1
		for prop in self._inlineProperties:
			if "widget" in prop:
				prop["widget"].setParent(None)
			widget = PropertiesWindow.returnTypeWidget(self, prop, self)
			prop["widget"] = widget
			if prop["type"] == "numeric":
				widget.valueChanged.connect(self._changedInline)
			if prop["type"] == "bit":
				widget.stateChanged.connect(self._changedInline)
			wi = QGraphicsProxyWidget(self)
			wi.setWidget(widget)
			wi.setPos(0, (self.height * i))
			i += 1.5

	def setClockGen(self, value):
		self._isClockGen = value

	def isClockGen(self):
		return self._isClockGen

	def getReferenceBits(self):
		return self._referenceBits

	def getVariableByPin(self, connector):
		for ref in self._referenceBits:
			for conn in ref["connectors"]:
				if conn == connector:
					return ref
		return None

	def getReferenceBitsByID(self, ID):
		pinObj = next((item for item in self._referenceBits if item["id"] == ID), False)
		if pinObj != False:
			return pinObj
		else:
			return None


	def _changedInline(self, value):
		properties = []
		for prop in self._inlineProperties:
			value = None
			if prop["type"] == "numeric":
				value = prop["widget"].value()
			if prop["type"] == "bit":
				value = int(prop["widget"].isChecked())
			new_prop = {
				"ID" : prop["name"],
				"value" : value
			}
			prop["value"] = value
			properties.append(new_prop)
		QApplication.instance().projectsHandler.getView().updateInlineRun(self, properties)

	def getInitialValueIn(self):
		if self._isInput:
			pinObj = next((item for item in self._editableProperties if item["name"] == "start_value"), False)
			if pinObj != False:
				if str(pinObj["value"]) != "-1":
					return int(pinObj["value"])
		return None

	def getValueIn(self):
		if self._isInput:
			if self._isClockGen:
				return 1
			pinObj = next((item for item in self._inlineProperties if item["name"] == "value"), False)
			if pinObj != False:
				if str(pinObj["value"]) != "-1":
					return int(pinObj["value"])
		return None

	def setPinsSize(self, sizes):
		for conn in self._allConnectorsWidget:
			element = [element for element in sizes if element["name"] == conn["connectorID"]]
			if len(element) > 0:
				conn["connectorObject"].setSize(element[0]["value"])
				if "default" in element[0]:
					conn["connectorObject"].setDefaultValue(element[0]["default"])
		self.setInlineProperties()

	def getPrecompilerPins(self):
		data_ret = []
		for conn in self._allConnectorsWidget:
			data_ret.append({
				"name" : conn["connectorID"],
				"value" : conn["connectorObject"].getSize(),
				"default" : conn["connectorObject"].getDefaultValue()
			})
		return data_ret

	def isInput(self):
		return self._isInput

	def isOutput(self):
		return self._isOutput

	def getClock(self):
		return self._clockValue

	def getVersion(self):
		return self._initialData["version"]

	def getDescription(self):
		return self._initialData["description"]

	def getAutor(self):
		return self._initialData["autor"]

	def isUserComponent(self):
		return self._isUserComponent

	def getPixMap(self):
		return self._files["workspaceI"]

	def getAllConnectors(self):
		return self._allConnectorsWidget

	def getPinObj(self, connectorID):
		pinObj = next((item for item in self._allConnectorsWidget if item["connectorID"] == connectorID), False)
		if pinObj != False:
			return pinObj["connectorObject"]
		else:
			return None

	def getID(self):
		return self.elementID

	def getType(self):
		return self._initialData["type"]

	def getJsonFile(self):
		return self._initialData["file"]

	def getPos(self):
		return [self.scenePos().x(), self.scenePos().y()]

	def getPrecompilerProperties(self):
		properties = []
		for prop in self._editableProperties:
			try:
				properties.append({
					"name" : prop["name"],
					"value" : prop["value"]
				})
			except:
				pass
		return properties

	def getProperties(self):
		return self._editableProperties

	def getPrecompilerFiles(self):
		return {
			"py" : self._files["py"],
			"vhdl" : self._files["vhdl"],
			"verilog" : self._files["verilog"],
			"type" : self._initialData["type"]
		}

	def mouseDoubleClickEvent(self, event):
		"""
			Emits When the user press the mouse
			@param event as mouseevent
		"""
		#QApplication.instance().propertiesWindow.showProperties(self)
		QApplication.instance().projectsHandler.getView().propertiesWindow.showProperties(self)

	def boundingRect(self):
		"""
			override boundingRect method, is mandatory, return the item size
		"""
		return QRectF(0, 0, self.width, self.height)

	def paint(self, *args):
		"""
			override paint method, is mandatory
			@param args
		"""
		None

	def hoverEnterEvent(self, event):
		"""
			When the mouse hover over the button
			@param event as mousehoverevent
		"""
		if not self._isFocus:
			self._pen.setWidth(1)
			self._rect.setPen(self._pen)
			self.showAllText()

	def showAllText(self):
		for conn in self._allConnectorsWidget:
			conn["connectorObject"].showText()

	def hideAllText(self):
		for conn in self._allConnectorsWidget:
			conn["connectorObject"].hideText()

	def select(self):
		self._pen.setColor(Qt.green)
		self._pen.setStyle(Qt.DashLine)
		self._rect.setPen(self._pen)
		self._isFocus = True
		self.showAllText()

	def deselect(self):
		self._pen.setStyle(Qt.SolidLine)
		self._pen.setColor(Qt.blue)
		self._rect.setPen(QPen(Qt.NoPen))
		self._isFocus = False
		self.hideAllText()

	def hoverLeaveEvent(self, event):
		"""
			When the mouse leave hover over the button
			@param event as mousehoverevent
		"""
		if not self._isFocus:
			self._rect.setPen(QPen(Qt.NoPen))
			self.hideAllText()

	def focusOutEvent(self, event):
		"""
			Emits when component is released
			@param event as QFocusEvent
		"""
		self._pen.setStyle(Qt.SolidLine)
		self._pen.setColor(Qt.blue)
		self._rect.setPen(QPen(Qt.NoPen))
		self._isFocus = False
		self.hideAllText()

	def focusInEvent(self, event):
		"""
			Emits when component is selected
			@param event as QFocusEvent
		"""
		self._pen.setColor(Qt.green)
		self._pen.setStyle(Qt.DashLine)
		self._rect.setPen(self._pen)
		self._isFocus = True
		self.showAllText()

	def setDragStart(self, start):
		self._dragStart = self.scenePos() - start
		for con in self._allConnectorsWidget:
			con["connectorObject"].setDragStart(start)

	def movePos(self, newPos):
		self._location = (newPos + self._dragStart)
		self._location.setX((int(self._location.x() / GRIDSPACE) * GRIDSPACE))
		self._location.setY((int(self._location.y() / GRIDSPACE) * GRIDSPACE))
		self.setPos(self._location)

		for con in self._allConnectorsWidget:
			con["connectorObject"].movePos(newPos)

	def mouseReleaseEvent(self, event):
		"""
			Emits When the user releases the mouse
			@param event as mouseevent
		"""
		self._location.setX((int(self._location.x() / GRIDSPACE) * GRIDSPACE))
		self._location.setY((int(self._location.y() / GRIDSPACE) * GRIDSPACE))
		self.setPos(self._location)
		self.setCursor(Qt.ArrowCursor)

	def mousePressEvent(self, event):
		"""
			Emits When the user press the mouse
			@param event as mouseevent
		"""
		self._dragStart = event.pos()
		self.setCursor(Qt.ClosedHandCursor)

	def mouseMoveEvent(self, event):
		"""
			Emits When the user moves the mouse prev to press
			@param event as mouseevent
		"""
		newPos = event.pos()
		self._location += (newPos - self._dragStart)
		if self._location.x() < 20:
			self._location.setX(20)
		elif self._location.x() > workspaceSize["x"] - self.width - 20:
			self._location.setX(workspaceSize["x"] - self.width - 20);
		else:
			self._location.setX(int(self._location.x() / GRIDSPACE) * GRIDSPACE)

		if self._location.y() < 20:
			self._location.setY(20)
		elif self._location.y() > workspaceSize["y"] - self.height - 20:
			self._location.setY(workspaceSize["y"] - self.height - 20);
		else:
			self._location.setY(int(self._location.y() / GRIDSPACE) * GRIDSPACE)
		self.setPos(self._location)
		for con in self._allConnectorsWidget:
			con["connectorObject"].updateWires()


	def keyPressEvent(self, event):
		"""
			Detect the key press
			@param event as QKeyEvent
		"""
		if event.key() == Qt.Key_Delete:
			self.delete()

	################
		#PUBLIC
	################
	def delete(self):
		"""
			Deletes the element of the workspace
		"""
		for con in self._allConnectorsWidget:
			con["connectorObject"].delete()
			del con
		QApplication.instance().projectsHandler.getWorkspace().removeWorkspaceObject(self)
		del self

	def setProperties(self, properties, call = True):
		"""
			Edits the element properties
			@param properties as dict
		"""
		for prop in self._editableProperties:
			element = [element for element in properties if element["name"] == prop["name"]]
			if len(element) > 0:
				prop["value"] = element[0]["value"]

	def setClock(self, clock):
		if self._hasClock:
			self._clockValue = clock
