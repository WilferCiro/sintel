#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries
from simulador.menus import MenuNewProject
from simulador.tools.utils import projectExtension, get_pycompile_folder, VERSION

# Qt libraries
from PyQt5.QtWidgets import QWidget, QMenu, QWidgetAction, QApplication, QFileDialog
from PyQt5.QtCore import Qt, QSize, QPoint, QSettings
from PyQt5.QtGui import QPixmap, QIcon
from PyQt5 import uic

# System libraries
import os

class HomeWidget(QWidget):

	def __init__(self):
		super(QWidget, self).__init__()
		#self.setupUi(self)
		uic.loadUi(QApplication.instance().get_src("ui", "home_view.ui"), self)
		self.logoLabel.setPixmap(QPixmap(QApplication.instance().get_src("images", "logo.png")).scaled(QSize(180, 180), Qt.KeepAspectRatio))

		self.versionLabel.setText("version: " + str(VERSION))

		self.newProjectBtn.clicked.connect(self._newProject)
		self.openProjectBtn.clicked.connect(self._openProject)
		self.settingsShowBtn.clicked.connect(self._settingsShow)
		self.aboutBtn.clicked.connect(self._aboutShow)
		self.addComponentBtn.clicked.connect(self._addComponentShow)
		self.addStateMachineBtn.clicked.connect(self._addStateMachineShow)
		self.viewExamplesBtn.clicked.connect(self._viewExamples)
		self.viewRecentsBtn.clicked.connect(self._viewRecents)

		# Icons in buttons
		self.openProjectBtn.setIcon(QIcon(QPixmap(QPixmap(QApplication.instance().get_src("images", "icons", "find_folder.png")))))
		self.newProjectBtn.setIcon(QIcon(QPixmap(QPixmap(QApplication.instance().get_src("images", "icons", "create.png")))))
		self.addComponentBtn.setIcon(QIcon(QPixmap(QPixmap(QApplication.instance().get_src("images", "icons", "bios.png")))))
		self.addStateMachineBtn.setIcon(QIcon(QPixmap(QPixmap(QApplication.instance().get_src("images", "icons", "pi.png")))))
		self.settingsShowBtn.setIcon(QIcon(QPixmap(QPixmap(QApplication.instance().get_src("images", "icons", "settings.png")))))
		self.aboutBtn.setIcon(QIcon(QPixmap(QPixmap(QApplication.instance().get_src("images", "icons", "help.png")))))

		self.widgetNewProject = MenuNewProject()
		self.menuNewProject = QMenu(self)
		widget = QWidgetAction(self)
		widget.setDefaultWidget(self.widgetNewProject)
		self.menuNewProject.addAction(widget)
		self.updateRecentView()

		self.btnLabelExample.setEnabled(False)
		self.btnLabelRecent.setEnabled(False)

		self._viewRecents()

	def _viewRecents(self):
		self.examplesLayout_2.setVisible(False)
		self.recentsLayout.setVisible(True)

	def _viewExamples(self):
		self.examplesLayout_2.setVisible(True)
		self.recentsLayout.setVisible(False)

	def _aboutShow(self):
		QApplication.instance().projectsHandler.showAbout()

	def _addComponentShow(self):
		QApplication.instance().projectsHandler.addComponent()

	def _addStateMachineShow(self):
		QApplication.instance().projectsHandler.addStateMachine()

	def _settingsShow(self):
		QApplication.instance().projectsHandler.showSettings()

	def _newProject(self):
		point = self.newProjectBtn.mapToGlobal(self.newProjectBtn.rect().bottomLeft() - QPoint(self.widgetNewProject.rect().width()/2, 0))
		self.menuNewProject.popup(point)

	def updateRecentView(self):
		for i in reversed(range(self.recentProjectsLayout.count())):
			self.recentProjectsLayout.itemAt(i).widget().setParent(None)
		files = QApplication.instance().settingsHandler.getRecentFiles()
		if len(files) > 0:
			self.btnLabelRecent.setText(self.tr("Recent projects"))
			for fil in files:
				try:
					self.recentProjectsLayout.addWidget(RecentItem(fil["path"], fil["name"]))
				except:
					pass
		else:
			self.btnLabelRecent.setText(self.tr("No recents"))

	def _openProject(self):
		path,_ = QFileDialog.getOpenFileName(self, self.tr('Open file'), get_pycompile_folder(), self.tr("Program files ") + projectExtension + " (*" + projectExtension + ")")
		if path != "" and path != None:
			project_path = os.path.dirname(os.path.abspath(path))
			project_name = os.path.basename(os.path.realpath(path)).replace(projectExtension, "")
			QApplication.instance().projectsHandler.openProject(project_path, project_name)

class RecentItem(QWidget):

	def __init__(self, project_path, project_name):
		super(QWidget, self).__init__()
		uic.loadUi(QApplication.instance().get_src("ui", "recent_widget.ui"), self)

		self.__projectName = project_name
		self.__projectPath = project_path
		self.projectNameLabel.setText(project_name)
		self.projectPathLabel.setText(project_path)
		self.openProjectBtn.clicked.connect(self._openProject)

		# Icons
		self.openProjectBtn.setIcon(QIcon(QPixmap(QApplication.instance().get_src("images", "icons", "open.png"))))

	def _openProject(self):
		QApplication.instance().projectsHandler.openProject(self.__projectPath, self.__projectName)
