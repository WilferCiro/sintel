#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries


# Qt libraries
from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5.QtGui import QPainter
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon

# System libraries

class MainWindow(QMainWindow):
	"""
		Principal Window
	"""
	def __init__(self):
		super(QMainWindow, self).__init__()

		# Call a instance of the application
		self.App = QApplication.instance()

		# Icon
		self.setWindowIcon(QIcon(QApplication.instance().get_src("images", "sintel.svg")))

		# widget properties
		self.setWindowTitle(self.tr("Sintel"))
		self.setCentralWidget(self.App.projectsHandler)

	def closeEvent(self, *args, **kwargs):
		self.App.closeEvent()

	def setTitle(self, title):
		"""
			Changes the window title
			@param title as str
		"""
		title = str(title)
		self.setWindowTitle("µCSim - " + title)

	def changeZoom(self, value):
		"""
			Change the zoom value
			@param value as float > 0 and < 1
		"""
		self.__view.resetTransform()
		self.__view.scale(value, value)
