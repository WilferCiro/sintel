#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries
from simulador.tools.utils import projectExtension, get_pycompile_folder
from simulador.save_load_project import SaveLoadProject

# Qt libraries
from PyQt5.QtWidgets import QApplication

# System libraries
import os
import json

class SaveLoadMacro(SaveLoadProject):
	def __init__(self):
		pass

	def resetFile(self, smName):
		self._data = {
			"name" : smName,
			"image" : "",
			"coords" : [],
			"components" : [],
			"signals" : [],
			"signalsTerminal" : [],
			"signalsSignals" : [],
			"ioMacro" : [],
			"macros" : []
		}


	def loadFile(self, jsonFile):
		self._jsonFile = os.path.join(jsonFile)

	def addComponent(self, component):
		"""
			Add a component in the compilation xml
			@param component as workspace_object
		"""
		self._data["components"].append({
			"id" : component.getID(),
			"file" : component.getJsonFile(),
			"pos" : component.getPos(),
			"properties" : component.getPrecompilerProperties(),
			"pins" : component.getPrecompilerPins(),
			"clock" : component.getClock()
		})

	def saveMacro(self, macro, name, addedSignals):
		self.resetFile(name)
		self._data["ID"] = macro.getID()
		self._data["ioMacro"] = macro.getIOSave()
		self._data["coords"] = macro.getPosSave()
		self._data["image"] = macro.getImagePath()

		internalMacros = macro.getMacros()
		components = macro.getComponents()
		signals = []
		for comp in components:
			self.addComponent(comp)
			newSig = comp.getSignals()
			for new in newSig:
				if new not in addedSignals and new != None:
					signals.append(new)
					addedSignals.append(new)

		newSig = macro.getSignals()
		for new in newSig:
			if new not in addedSignals and new != None:
				signals.append(new)
				addedSignals.append(new)

		for sig in signals:
			if sig.isSignalTerminal():
				self.addSignalTerminal(sig)
			elif sig.isSignalSignal():
				self.addSignalSignal(sig)
			else:
				self.addSignal(sig)

		data = self._data
		for mac in internalMacros:
			data["macros"].append(self.saveMacro(mac, mac.getID(), addedSignals))
		return data


	def writeFile(self, data, name):
		self._jsonFile = os.path.join(get_pycompile_folder(), "components_macros", "s_" + name + projectExtension)
		with open(self._jsonFile, 'w+') as f:
			json.dump(data, f, indent = 1)
			f.close()
