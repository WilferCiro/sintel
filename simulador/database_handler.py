#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries

# Qt libraries
from PyQt5.QtWidgets import QApplication


# System libraries
import sqlite3

class DataBaseHandler:

	def __init__(self):
		self.connection = sqlite3.connect(QApplication.instance().get_src("db" , "db"))

	def saveComponent(self, component):
		c = self.connection.cursor()
		c.execute("INSERT into components (type, family, category, description, file, version) values(?, ?, ?, ?, ?, ?)", component["type"], component["family"], component["category"], component["description"], component["file"], component["version"])
		self.connection.commit()

	def updateComponents(self, components, macros):
		self.removeAll()
		c = self.connection.cursor()
		all_update = []
		for comp in components:
			all_update.append([comp["type"], comp["category"], comp["description"], comp["file"], comp["version"], comp["icon_workspace"], comp["user"]])
		c.executemany("INSERT into components (type, category, description, file, version, icon, user) values(?, ?, ?, ?, ?, ?, ?)", all_update)

		all_update = []
		for mac in macros:
			all_update.append([mac["name"], mac["file"], mac["icon"]])
		c.executemany("INSERT into macros (name, file, image) values(?, ?, ?)", all_update)
		self.connection.commit()

	def getAllComponents(self, search = None):
		components = []
		c = self.connection.cursor()
		if search != None:
			search = str(search)
			c.execute("select * from components where type LIKE '%" + search + "%' OR category LIKE '%" + search + "%' LIMIT 30")
		else:
			c.execute("select * from components LIMIT 20")
		components = []
		for i in c.fetchall():
			c = {
				"type" : i[1],
				"category" : i[2],
				"description" : i[3],
				"version" : i[4],
				"file" : i[5],
				"icon" : i[6],
				"user" : i[7]
			}
			components.append(c)
		return components

	def getAllMacros(self, search = None):
		c = self.connection.cursor()
		if search != None:
			search = str(search)
			c.execute("select * from macros where name LIKE '%" + search + "%' LIMIT 20")
		else:
			c.execute("select * from macros LIMIT 20")
		macros = []
		for i in c.fetchall():
			c = {
				"name" : i[1],
				"file" : i[2],
				"icon" : i[3]
			}
			macros.append(c)
		return macros

	def removeAll(self):
		c = self.connection.cursor()
		c.execute("DELETE from components")
		c.execute("DELETE from macros")
		self.connection.commit()
