#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries

# Qt libraries
from PyQt5.QtCore import QSize, QRectF, QSizeF, QPointF
from PyQt5.QtSvg import QGraphicsSvgItem

# System libraries

class SintelSVG(QGraphicsSvgItem):
	def __init__(self, path, parent):
		super(QGraphicsSvgItem, self).__init__(path, parent)
		self._size = QSizeF(50, 50)
		self.setPos(0,0)

	def size(self):
		width = self._size.width()
		height = self._size.height()
		if self._size.width() < 0:
			width = (self.renderer().boundsOnElement(self.elementId()).size().width())
		if self._size.height() < 0:
			height = (self.renderer().boundsOnElement(self.elementId()).size().width())
		return QSizeF(width, height)

	def setWidth(self, width):
		self.setSize(QSizeF(width, self._size.height()))

	def setHeigth(self, height):
		self.setSize(QSizeF(self._size.width(), height))

	def setSize(self, size):
		self.prepareGeometryChange()
		self._size = size
		self.update(self.boundingRect())
		self.setPos(0,0)

	def setFileName(self, image):
		pass

	def boundingRect(self):
		"""
			override boundingRect method, is mandatory, return the item size
		"""
		return QRectF(QPointF(0.0, 0.0), self.size())

	def paint(self, painter, option, widget, *args):
		self.renderer().render(painter, self.boundingRect())
