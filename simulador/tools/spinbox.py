#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries

# Qt libraries
from PyQt5.QtWidgets import QAbstractSpinBox, QLineEdit
from PyQt5.QtCore import QRegExp, pyqtSignal
from PyQt5.QtGui import QRegExpValidator, QFontMetrics, QFont

class BigIntSpinbox(QAbstractSpinBox):
	valueChanged = pyqtSignal(int)

	def __init__(self, parent=None):
		super(BigIntSpinbox, self).__init__(parent)

		self._singleStep = 1
		self._minimum = -1
		self._maximum = 18446744073709551616

		self.lineEdit = QLineEdit(self)
		self.lineEdit.setText("0")

		rx = QRegExp("[1-9]\\d{0,20}")
		validator = QRegExpValidator(rx, self)

		self.lineEdit.setValidator(validator)
		self.lineEdit.textChanged.connect(self._edited)
		self.setLineEdit(self.lineEdit)

	def _edited(self, value):
		if value == "" or str(value) == "-":
			value = 0
			self.lineEdit.setText("0")
		if int(value) > self._maximum:
			value = self._maximum
			self.lineEdit.setText(str(value))
		else:
			value = int(value)
			self.valueChanged.emit(value)

	def value(self):
		try:
			return int(self.lineEdit.text())
		except:
			pass
		return 0

	def setValue(self, value):
		value = int(value)
		if self._valueInRange(value):
			self.lineEdit.setText(str(value))
			self.valueChanged.emit(value)

	def stepBy(self, steps):
		self.setValue(self.value() + steps*self.singleStep())

	def stepEnabled(self):
		return self.StepUpEnabled | self.StepDownEnabled

	def setSingleStep(self, singleStep):
		assert isinstance(singleStep, int)
		# don't use negative values
		self._singleStep = abs(singleStep)

	def singleStep(self):
		return self._singleStep

	def minimum(self):
		return self._minimum

	def setMinimum(self, minimum):
		assert isinstance(minimum, int) or isinstance(minimum, long)
		self._minimum = minimum

	def maximum(self):
		return self._maximum

	def setMaximum(self, maximum):
		assert isinstance(maximum, int) or isinstance(maximum, long)
		self._maximum = maximum
		fm = QFontMetrics(QFont("", 0))
		self.setFixedSize(fm.width(str(maximum)) + 35, 25)

	def _valueInRange(self, value):
		if value >= self.minimum() and value <= self.maximum():
			return True
		else:
			return False
