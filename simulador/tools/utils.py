#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

import unicodedata
import os

projectExtension = ".sintel"
GRIDSPACE = 8

MOORE = "Mo"
MEALY = "Me"
VERSION = "1.0"

workspaceSize = {"x" : 3200, "y" : 1300}
SMSize = {"x" : 1100, "y" : 600}

def get_pycompile_folder():
	dir_compile = os.path.join(os.path.expanduser('~'), ".sintel")
	if not os.path.exists(dir_compile):
		os.mkdir(dir_compile)
	run_folder = os.path.join(dir_compile, "run")
	if not os.path.exists(run_folder):
		os.mkdir(run_folder)
	return dir_compile

def getSignalSize(size):
	if size == 1:
		return 2
	elif size > 1 and size < 8:
		return 3
	elif size >= 8 and size < 16:
		return 4
	elif size >= 16 and size < 32:
		return 5
	elif size >= 32 and size <= 64:
		return 6
	return 6

def bin2decH(dec):
	try:
		dec = int(str(dec), 2)
		return str(dec) + "<sub>10</sub>"
	except:
		return str(dec) + "<sub>2</sub>"

def bin2dec(dec):
	try:
		dec = int(str(dec), 2)
		return int(dec)
	except:
		return int(dec)

def filterText(cadena):
	text = ''.join((c for c in unicodedata.normalize('NFD',cadena) if unicodedata.category(c) != 'Mn'))
	return "".join(e for e in text if e.isalnum())

def filterTextNumber(string):
	return "".join(e for e in string if e.isalnum() or e == ",")
