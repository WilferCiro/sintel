#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Import required libraries
import json
import os

def returnIconData(jsonFile):
	"""
		Return a dict with the icon data
		@param jsonFile as str
	"""
	data = dict()
	with open(jsonFile, "r") as json_file:
		text = json_file.read()
		y = json.loads(text)
		json_file.close()
		data["type"] = y["type"]
		data["description"] = y["description"]
		data["autor"] = y["autor"]
		data["version"] = y["version"]
		data["category"] = y["category"]
		data["icon_workspace"] = y["file"]["icon"]
		data["clock"] = y["clock"]
		data["icon"] = y["icon"]
		data["isSM"] = False
		if "sm" in y:
			data["isSM"] = True
		data["input"] = False
		if "input" in y:
			data["input"] = y["input"]
		data["output"] = False
		if "output" in y:
			data["output"] = y["output"]
		data["file"] = jsonFile
	return data

def returnMacroData(jsonFile):
	data = dict()
	with open(jsonFile, "r") as json_file:
		text = json_file.read()
		y = json.loads(text)
		json_file.close()
		data["name"] = y["name"]
		data["icon"] = y["image"]
		data["file"] = jsonFile
	return data

def returnExists(jsonFile):
	return os.path.exists(jsonFile)

def getObjectFiles(jsonFile):
	data = []
	with open(jsonFile, "r") as json_file:
		text = json_file.read()
		y = json.loads(text)
		json_file.close()
		data = y["file"]
	return data

def getsignalReference(jsonFile):
	data = []
	with open(jsonFile, "r") as json_file:
		text = json_file.read()
		y = json.loads(text)
		json_file.close()
		data = y["var_connectors"]
	return data

def getObjectConnectors(jsonFile):
	data = []
	with open(jsonFile, "r") as json_file:
		text = json_file.read()
		y = json.loads(text)
		json_file.close()
		data = y["connectors"]
	return data

def getObjectProperties(jsonFile):
	eProperties = []
	sProperties = []
	with open(jsonFile, "r") as json_file:
		text = json_file.read()
		y = json.loads(text)
		json_file.close()
		eProperties = y["eProperties"]
		sProperties = y["inProperties"]
	return eProperties, sProperties
