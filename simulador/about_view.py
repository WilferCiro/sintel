#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries
from simulador.menus import MenuNewProject
from simulador.tools.utils import projectExtension, VERSION

# Qt libraries
from PyQt5.QtWidgets import QWidget, QApplication
from PyQt5.QtCore import Qt, QSize
from PyQt5.QtGui import QPixmap
from PyQt5 import uic

# System libraries
import os

class AboutWidget(QWidget):

	def __init__(self):
		super(QWidget, self).__init__()
		uic.loadUi(QApplication.instance().get_src("ui", "help.ui"), self)
		self.logoLabel.setPixmap(QPixmap(QApplication.instance().get_src("images", "logo.png")).scaled(QSize(180, 180), Qt.KeepAspectRatio))

		self.versionLabel.setText("version: " + str(VERSION))
