#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries
from simulador.tools.utils import projectExtension
from simulador.macro_component import ComponentConnectorMacro
from simulador.workspace_object import WorkspaceObject

# Qt libraries
from PyQt5.QtWidgets import QApplication

# System libraries
import os
import json

class SaveLoadProject():
	"""
		Handle the creation of the json
		@param projectName as str
		@param projectPath as str
	"""
	def __init__(self, projectPath, projectName):
		# Original files
		projectName = str(projectName)
		projectPath = str(projectPath)

		# File of project, in this file the user saves his progress
		self._jsonFile = os.path.join(projectPath, projectName + projectExtension)

		# reset the data for overwrite the data
		self.resetFile()

	def resetFile(self):
		self._data = {
			"components" : [],
			"signals" : [],
			"signalsTerminal" : [],
			"signalsSignals" : [],

			"componentIndex" : 1,
			"signalIndex" : 1,
			"time" : 2,
			"plots" : [],
			"draws" : [],
			"macros" : [],
			"signals_show" : []
		}

	def writeFile(self, reset = False):
		with open(self._jsonFile, 'w+') as f:
			json.dump(self._data, f, indent = 1)
			f.close()

	def loadProject(self):
		data = {}
		with open(self._jsonFile, 'r+') as f:
			if os.path.getsize(self._jsonFile) == 0:
				return -1
			else:
				data = json.load(f)
				f.close()
		return data

	def addMacros(self, macros):
		for mac in macros:
			self._data["macros"].append({
				"ID" : mac.getID(),
				"coords" : mac.getPosSave(),
				"IO" : mac.getIOSave(),
				"components" : mac.getSaveComponents(),
				"macros" : mac.getSaveMacros(),
				"image" : mac.getImagePath()
			})

	def savePlots(self, plots):
		self._data["plots"] = plots

	def saveDraw(self, allDraw):
		for draw in allDraw:
			self._data["draws"].append({
				"pos" : draw.getPosSave(),
				"type" : draw.getType()
			})

	def saveSignalsShow(self, signals):
		for sig in signals:
			self._data["signals_show"].append(sig["ID"])

	def addComponent(self, componentDict):
		"""
			Add a component in the compilation xml
			@param component as workspace_object
		"""
		component = componentDict["object"]
		if "macro" in componentDict:
			self._data["components"].append({
				"id" : component.getIDSave(),
				"macro" : componentDict["macro"].getID()
			})
		else:
			self._data["components"].append({
				"id" : component.getID(),
				"file" : component.getJsonFile(),
				"pos" : component.getPos(),
				"properties" : component.getPrecompilerProperties(),
				"pins" : component.getPrecompilerPins(),
				"clock" : component.getClock()
			})


	def addSignal(self, signalObj):
		"""
			Add a signal to compiler file
			@param signalId as str
		"""
		connections = signalObj.getConnections()

		self._data["signals"].append({
			"id" : signalObj.getID(),
			"folds" : signalObj.getFoldsPoints(),
			"compA" : connections[0].getParentID(),
			"connA" : connections[0].getID(),
			"compB" : connections[1].getParentID(),
			"connB" : connections[1].getID(),
			"indexF" : signalObj.getIndexFold(),
			"control" : signalObj.getControlPath()
		})

	def addSignalTerminal(self, signalObj):
		"""
			Add a signal to compiler file
			@param signalId as str
		"""
		connections = signalObj.getConnections()
		self._data["signalsTerminal"].append({
			"id" : signalObj.getID(),
			"folds" : signalObj.getFoldsPoints(),
			"compA" : connections[0].getParentID(),
			"connA" : connections[0].getID(),
			"connB" : connections[1].getPos(),
			"indexF" : signalObj.getIndexFold()
		})

	def addSignalSignal(self, signalObj):
		"""
			Add a signal to compiler file
			@param signalId as str
		"""
		connections = signalObj.getConnections()
		self._data["signalsSignals"].append({
			"id" : signalObj.getID(),
			"folds" : signalObj.getFoldsPoints(),
			"compA" : connections[0].getParentID(),
			"connA" : connections[0].getID(),
			"compB" : connections[1].getParentID(),
			"connB" : connections[1].getID(),
			"refA" : signalObj.returnReferenceMoveA().getID(),
			"signalRef" : signalObj.getsignalReference().getID(),
			"signalBits" : signalObj.getSignalBits(),
			"indexF" : signalObj.getIndexFold()
		})

	def addConnection(self, componentID, pinName, signalID):
		"""
			Add a signal to compiler file
			@param componentID as str
			@param pinName as str
			@param signalID as str
		"""
		self._data["connections"].append({
			"component" : componentID,
			"pin" : pinName,
			"signal" : signalID
		})

	def addConnectionSignals(self, fold, signal_fold, signal):
		self._data["connSignals"].append({
			"fold" : fold,
			"s_fold" : signal_fold,
			"signal" : signal
		})

	def saveIndexes(self, componentIndex, signalIndex, time):
		self._data["componentIndex"] = componentIndex
		self._data["signalIndex"] = signalIndex
		self._data["time"] = time
