from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from datetime import datetime

class Message(QWidget):
	def __init__(self, title, message, parent=None):
		QWidget.__init__(self, parent)
		self._parent = parent
		self.setLayout(QGridLayout())
		self.titleLabel = QLabel(title, self)
		self.titleLabel.setStyleSheet("font-family: 'Roboto', sans-serif; font-size: 14px; font-weight: bold; padding: 0;")
		self.messageLabel = QLabel(message, self)
		self.messageLabel.setStyleSheet("font-family: 'Roboto', sans-serif; font-size: 12px; font-weight: normal; padding: 0;")
		self.messageLabel.setWordWrap(True)
		self.buttonClose = QPushButton(self)
		self.buttonClose.setIcon(QIcon(QApplication.instance().get_src("images", "icons", "close.png")))
		self.buttonClose.setFixedSize(20, 20)
		self.buttonClose.setProperty("class", "icon-button")
		self.layout().addWidget(self.titleLabel, 0, 0)
		self.layout().addWidget(self.messageLabel, 1, 0)
		self.layout().addWidget(self.buttonClose, 0, 1, 2, 1)

class Notification(QWidget):
	signNotifyClose = pyqtSignal(str)
	def __init__(self, parent = None):
		time = datetime.now()
		self.currentTime = str(time.hour) + ":" + str(time.minute) + "_"
		self.LOG_TAG = self.currentTime + self.__class__.__name__ + ": "
		super(QWidget, self).__init__(parent)

		self.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint)
		resolution = QDesktopWidget().screenGeometry(-1)
		screenWidth = resolution.width()
		screenHeight = resolution.height()
		self.nMessages = 0
		self.mainLayout = QVBoxLayout(self)
		self.move(20, 20)

		self._messages = []

	def setNotify(self, title, message):
		m = Message(str(title) + " [" + str(self.currentTime).replace("_", "") + "]", message, self)
		self._messages.append(m)
		self.mainLayout.addWidget(m)
		m.buttonClose.clicked.connect(self.onClicked)
		self.nMessages += 1
		self.show()

	def onClicked(self):
		self._messages.remove(self.sender().parent())
		self.mainLayout.removeWidget(self.sender().parent())
		self.sender().parent().deleteLater()
		self.nMessages -= 1
		self.adjustSize()
		if self.nMessages == 0:
			self.close()

	def deleteAll(self):
		for mess in self._messages:
			self.mainLayout.removeWidget(mess)
			mess.deleteLater()
			self.nMessages -= 1
			self.adjustSize()
			self._messages.remove(mess)
		if self.nMessages == 0:
			self.close()

def clearLayout(layout):
	if layout != None:
		while layout.count():
			child = layout.takeAt(0)
			if child.layout() is not None:
				clearLayout(child.layout())
			elif child.widget() is not None:
				child.widget().deleteLater()
