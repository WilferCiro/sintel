#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries
from simulador.tools.utils import projectExtension
from simulador.components_widget import Component

# Qt libraries
from PyQt5.QtWidgets import QWidget, QFileDialog, QApplication, QGraphicsView, QGraphicsScene
from PyQt5 import uic
from PyQt5.QtGui import *
from PyQt5.QtCore import *

# System libraries
import os

class SignalsMenu(QWidget):
	def __init__(self, parent, parentMenu):
		self._parent = parent
		self._parentMenu = parentMenu
		super(QWidget, self).__init__()
		uic.loadUi(QApplication.instance().get_src("ui", "menus", "signals_menu.ui"), self)

		self.signalConnectionBtn.clicked.connect(self._signalConnectionShow)
		self.deleteSignalBtn.clicked.connect(self._deleteSignalFnc)
		self.showValueCheck.stateChanged.connect(self._showValueCheck)
		self.showInPlotCheck.stateChanged.connect(self._showInPlotCheck)
		self.isControlPath.stateChanged.connect(self._isControlPath)
		self._signal = None

		self._initial = False

	def showSignal(self, signal, inList):
		self._initial = True
		self._signal = signal
		self.signalID.setText(self._signal.getID())
		if inList:
			self.showValueCheck.setChecked(True)
		else:
			self.showValueCheck.setChecked(False)

		if signal.hasGraph():
			self.showInPlotCheck.setChecked(True)
		else:
			self.showInPlotCheck.setChecked(False)

		if self._signal.isSignalSignal():
			self.signalConnectionBtn.setEnabled(True)
		else:
			self.signalConnectionBtn.setEnabled(False)

		if self._signal.getControlPath():
			self.isControlPath.setChecked(True)
		else:
			self.isControlPath.setChecked(False)
		self._initial = False

	def _isControlPath(self):
		if not self._initial:
			if self.isControlPath.isChecked():
				self._signal.setControlPath(True)
			else:
				self._signal.setControlPath(False)

	def _showValueCheck(self):
		if not self._initial:
			if self.showValueCheck.isChecked():
				self._parent.addSignalShowValue(self._signal)
			else:
				self._parent.deleteSignalShowValue(self._signal)

	def _showInPlotCheck(self):
		if not self._initial:
			if self.showInPlotCheck.isChecked():
				self._parent.addPlotZero(self._signal.getID())
				self.showValueCheck.setChecked(True)
			else:
				self._parent.deletePlotZero(self._signal.getID())

	def _deleteSignalFnc(self):
		self._signal.delete()
		self._parentMenu.close()

	def _signalConnectionShow(self):
		if self._signal.isSignalSignal():
			QApplication.instance().subSignalModal.showConnections(self._signal)

class PrincipalMenu(QWidget):
	def __init__(self, parent):
		self._parent = parent
		super(QWidget, self).__init__()
		uic.loadUi(QApplication.instance().get_src("ui", "simulation_menu.ui"), self)
		self.zoomOutBtn.clicked.connect(self._zoomOut)
		self.zoomInBtn.clicked.connect(self._zoomIn)
		self.vhdlExportBtn.clicked.connect(self._generateVHDL)
		self.addComponentBtn.clicked.connect(self._newComponent)
		self.settingsBtn.clicked.connect(self._settings)
		self.aboutBtn.clicked.connect(self._about)
		self.addSMBtn.clicked.connect(self._addSM)
		self.printBtn.clicked.connect(self._print)
		self.pngBtn.clicked.connect(self._toPNG)
		self.svgBtn.clicked.connect(self._toSVG)

	def _addSM(self):
		QApplication.instance().projectsHandler.addStateMachine()

	def _zoomOut(self):
		self._parent.zoomOut()

	def _zoomIn(self):
		self._parent.zoomIn()

	def _generateVHDL(self):
		self._parent.generateVHDL()

	def setLabelZoom(self, value):
		self.currentZoomLabel.setText(str(value) + " %")

	def _settings(self):
		QApplication.instance().projectsHandler.showSettings()

	def _print(self):
		self._parent.print()

	def _toPNG(self):
		self._parent.toPNG()

	def _toSVG(self):
		self._parent.toSVG()

	def _about(self):
		QApplication.instance().projectsHandler.showAbout()

	def _newComponent(self):
		QApplication.instance().projectsHandler.addComponent()

class MenuProperties(QWidget):

	def __init__(self):
		super(QWidget, self).__init__()
		uic.loadUi(QApplication.instance().get_src("ui", "properties_menu.ui"), self)

		self.simulationPeriod.setValue(QApplication.instance().settingsHandler.getDefaultTime())

	def getCiclesNumber(self):
		return self.ciclesNumber.value()

	def getSimTime(self):
		return self.simulationPeriod.value()

	def startPaused(self):
		return self.startPausedCheck.isChecked()

	def clockSampling(self):
		return self.sampling_clock.isChecked()


class MenuNewProject(QWidget):
	def __init__(self):
		super(QWidget, self).__init__()
		uic.loadUi(QApplication.instance().get_src("ui", "new_project_menu.ui"), self)
		self.selectLocationBtn.clicked.connect(self._selectLocation)
		self.createBtn.clicked.connect(self._createProject)
		self._path = ""
		self.projectNameInput.textChanged.connect(self._changeName)
		self.createBtn.setEnabled(False)

		# Icons
		self.selectLocationBtn.setIcon(QIcon(QPixmap(QApplication.instance().get_src("images", "icons", "find_folder.png"))))

	def _changeName(self):
		name = self.projectNameInput.text()
		if name != "":
			self.createBtn.setEnabled(True)
		else:
			self.createBtn.setEnabled(False)

	def _createProject(self):
		name = self.projectNameInput.text() + projectExtension
		if name != projectExtension:
			f = open(os.path.join(self._path, name), "w+")
			f.close()
			QApplication.instance().projectsHandler.openProject(self._path, self.projectNameInput.text())
			self.projectNameInput.setText("")
			self.parent().hide()

	def _selectLocation(self):
		path = str(QFileDialog.getExistingDirectory(self, "Select Directory"))
		if path:
			self.pathLabel.setText(path)
			self._path = path
		point = self.parent().mapToGlobal(self.parent().rect().topLeft())
		self.parent().popup(point)


class QuickSearchWidget(QWidget):

	def __init__(self):
		super(QWidget, self).__init__()
		uic.loadUi(QApplication.instance().get_src("ui", "search_quick.ui"), self)
		self.sideScene = QGraphicsScene()
		self.sideScene.setSceneRect(QRectF(0, 0, 190, 210))
		sideView = QGraphicsView(self.sideScene)
		sideView.setFixedWidth(200)
		sideView.setAlignment(Qt.AlignLeft | Qt.AlignTop)
		self.componentsList.addWidget(sideView)

		self.searchComponentEntry.textChanged.connect(self._searchComponent)

	def _searchComponent(self):
		search = str(self.searchComponentEntry.text())
		for item in self.sideScene.items():
			self.sideScene.removeItem(item)
		posX = 0
		posY = 0
		all_components = QApplication.instance().componentsHandler.getComponents(search)
		for component in all_components:
			newIcon = Component(component)
			self.sideScene.addItem(newIcon)
			newIcon.setPos((100 * posX) + 10, (35 * posY) + 10);
			posX += 1
			if posX == 2:
				posX = 0
				posY += 1
