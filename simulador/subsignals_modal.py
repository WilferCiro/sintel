#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries

# Qt libraries
from PyQt5.QtWidgets import QApplication, QGraphicsTextItem, QWidget, QDialog, QFormLayout, QComboBox, QSpinBox, QLabel, QLineEdit, QGraphicsObject, QGraphicsEllipseItem, QGraphicsScene, QGraphicsView, QGraphicsLineItem
from PyQt5.QtCore import QSize, QRectF, Qt
from PyQt5.QtGui import QPixmap, QColor, QPen, QBrush
from PyQt5 import uic

# System libraries
import os

class SubSignalModal(QDialog):

	def __init__(self):
		super(QDialog, self).__init__()
		uic.loadUi(QApplication.instance().get_src("ui", "subsignal_connections.ui"), self)

		# Buttons for accept or cancel
		self.buttonBox.accepted.connect(self._accept)

		# Control variables
		self._fields = []
		self._element = None

		self._sceneComp = QGraphicsScene()
		sideView = QGraphicsView(self._sceneComp)
		self.GrapCamp.addWidget(sideView)

		self._lastConnector = None
		self._connectedList = []
		self._connectedListReturn = []

		self._signalObj = None

	def showConnections(self, signal):
		"""
			Excec properties window
			@param obj as workspace_object
		"""
		self._signalObj = signal
		self._deleteItems()
		connectors = self._signalObj.getConnections()

		self.component1ID.setText(connectors[0].getID() + " - " + connectors[0].getParentID())
		self.component2ID.setText(connectors[1].getID() + " - " + connectors[1].getParentID())
		self.signalName.setText(signal.getID())

		delta = 15

		y = delta

		self._connectedListReturn = signal.getSignalBits()
		self._signalReferenceBits = signal.getsignalReference().getSignalBitsOwn()
		self._enableDisable = False
		if connectors[0].getType() == "in" and signal.getsignalReference().isSignalTerminal():
			self._enableDisable = True
		self._lines = []

		self._leftPins = []
		self._rightPins = []

		for con in range(connectors[0].getSize()):
			disable = False
			if con in self._signalReferenceBits and self._enableDisable:
				disable = True
			conObj = Connector(con, 1, disable, self)
			self._leftPins.append(conObj)
			self._sceneComp.addItem(conObj)
			conObj.setPos(10, y)
			y += delta

		y = delta
		for con in range(connectors[1].getSize()):
			conObj = Connector(con, 2, False, self)
			self._rightPins.append(conObj)
			self._sceneComp.addItem(conObj)
			conObj.setPos(150, y)
			y += delta

		index = 0
		for line in self._connectedListReturn:
			lineAdd = QGraphicsLineItem(self._leftPins[line].scenePos().x() + 2.5, self._leftPins[line].scenePos().y() + 2.5, self._rightPins[index].scenePos().x() + 2.5, self._rightPins[index].scenePos().y() + 2.5)
			self._sceneComp.addItem(lineAdd)
			self._lines.append({"index" : line, "object" : lineAdd})
			index += 1

		self.exec_()

	def _deleteItems(self):
		for i in self._sceneComp.items():
			self._sceneComp.removeItem(i)
		self._connectedList = []
		self._connectedListReturn = []
		self._lastConnector = None

	def _accept(self):
		self._signalObj.setSignalBits(self._connectedListReturn)
		self.accept()

	def ConnectorEvent(self, connector):
		if self._lastConnector != None:
			if connector.getSide() != self._lastConnector.getSide() and not connector.getDisable():
				left = 0
				right = 0
				if self._lastConnector.getSide() == 1:
					left = self._lastConnector.getIndex()
					right = connector.getIndex()
					if self._enableDisable:
						self._lastConnector.setDisable(True)
				else:
					right = self._lastConnector.getIndex()
					left = connector.getIndex()
					if self._enableDisable:
						connector.setDisable(True)

				try:
					self._sceneComp.removeItem(self._lines[right]["object"])
					self._leftPins[self._lines[right]["index"]].setDisable(False)
					self._lines[right] = None
				except Exception as es:
					print(es)
					pass

				line = QGraphicsLineItem(connector.scenePos().x() + 2.5, connector.scenePos().y() + 2.5, self._lastConnector.scenePos().x() + 2.5, self._lastConnector.scenePos().y() + 2.5)
				self._lines[right] = {"index" : left, "object" : line}
				self._connectedListReturn[right] = left
				self._sceneComp.addItem(line)

				self._lastConnector = None
			else:
				self._lastConnector = connector
		else:
			if not connector.getDisable():
				self._lastConnector = connector

class Connector(QGraphicsObject):
	"""
		Connector for the elements
	"""

	def __init__(self, index, side, disable = False, parent = None):
		"""
			Init the connector
			@param data as dict
			@param parent as QGrapicsItem
		"""
		self._parent = parent
		self._index = index
		super(QGraphicsObject, self).__init__(None)

		self._disable = disable

		if disable:
			self._default_color = QColor(166, 0, 0)
		else:
			self._default_color = QColor(0, 148, 166)

		self._pen = QPen(self._default_color)
		self._pen.setWidth(2)

		self._rect = QGraphicsEllipseItem(QRectF(0, 0, 8, 8), self)
		self._rect.setPen(self._pen)
		background = QBrush(self._default_color)
		self._rect.setBrush(background)
		self.setAcceptHoverEvents(True)
		self.setCursor(Qt.CrossCursor)

		self._nameTextWidget = QGraphicsTextItem(str(self._index), self)
		if side == 1:
			self._nameTextWidget.setPos(-25, -8)
		else:
			self._nameTextWidget.setPos(25, -8)
		#f = QFont()
		#f.setFamily("Monaco")
		#f.setPointSize(7)
		#self._nameTextWidget.setFont(f)

		self._side = side

		#self._ValueTextWidget = QGraphicsTextItem("0", self)
		#self._ValueTextWidget.setPos(-15, -10)

	def setDisable(self, state):
		self._disable = state
		if self._disable:
			self._default_color = QColor(166, 0, 0)
		else:
			self._default_color = QColor(0, 148, 166)
		self._pen = QPen(self._default_color)
		self._pen.setWidth(2)
		self._rect.setPen(self._pen)
		background = QBrush(self._default_color)
		self._rect.setBrush(background)

	def getDisable(self):
		return self._disable

	def getIndex(self):
		return self._index

	def getSide(self):
		return self._side

	def boundingRect(self):
		"""
			override boundingRect method, is mandatory, return the item size
		"""
		return QRectF(0, 0, 14, 14)

	def paint(self, *args):
		"""
			override paint method, is mandatory
			@param args
		"""
		None

	def mousePressEvent(self, event):
		"""
			Emits When the user press the mouse
			@param event as mouseevent
		"""
		self._parent.ConnectorEvent(self)
