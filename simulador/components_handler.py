#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries
from simulador.tools.xml_reader import returnIconData, returnMacroData
from simulador.tools.utils import projectExtension, get_pycompile_folder

# Qt libraries
from PyQt5.QtWidgets import QApplication

# System libraries
import os
import glob

class ComponentsHandler:
	def updateComponents(self):
		component_files = glob.glob(os.path.join(get_pycompile_folder(), "components", "**", "*" + projectExtension), recursive = True)
		data = []
		data_macros = []
		for xml_f in component_files:
			data_in = returnIconData(xml_f)
			data_in["user"] = False
			data.append(data_in)

		component_files = glob.glob(os.path.join(get_pycompile_folder(), "components_user", "**", "*" + projectExtension), recursive = True)
		for xml_f in component_files:
			data_in = returnIconData(xml_f)
			data_in["user"] = True
			data.append(data_in)

		component_files = glob.glob(os.path.join(get_pycompile_folder(), "components_macros", "**", "*" + projectExtension), recursive = True)
		for xml_f in component_files:
			data_in = returnMacroData(xml_f)
			data_macros.append(data_in)

		QApplication.instance().dataBase.updateComponents(data, data_macros)

	def getComponents(self, filterString):
		data = QApplication.instance().dataBase.getAllComponents(filterString)
		return data

	def getMacros(self, filterString):
		data = QApplication.instance().dataBase.getAllMacros(filterString)
		return data
