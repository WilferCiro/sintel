#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries
from simulador.workspace import Workspace
from simulador.menus import MenuProperties, PrincipalMenu
from simulador.components_widget import ComponentWidget
from simulador.plot_canvas import PlotWidget
from simulador.tools.utils import projectExtension, filterTextNumber
from simulador.precompiler import Precompiler
from simulador.precompiler_hdl import PrecompilerHDL
from simulador.save_load_project import SaveLoadProject
from simulador.properties_window import PropertiesWindow
from simulador.signal import Fold, signalTerminal
from simulador.tools.utils import get_pycompile_folder

# Qt libraries
from PyQt5.QtWidgets import (QWidget, QMenu, QDoubleSpinBox, QWidgetAction,
	QHBoxLayout, QLabel, QVBoxLayout, QGraphicsView, QWidgetAction, QShortcut, QApplication, QInputDialog, QFileDialog)
from PyQt5.QtCore import Qt, QSize, QPoint, pyqtSignal, QPointF, QRect
from PyQt5.QtGui import QPixmap, QKeySequence, QPainter, QIcon, QWheelEvent, QIcon, QImage
from PyQt5.QtPrintSupport import QPrinter
from PyQt5 import uic
from PyQt5.QtSvg import QSvgGenerator

# System libraries
import time
import importlib
import threading
import subprocess
import os
import glob

import sys
sys.path.insert(0, get_pycompile_folder())
default = None

# Macros
current_milli_time = lambda: int(round(time.time() * 1000))

class SimulationWidget(QWidget):
	"""
		Handle the individual simulation
	"""
	counter = pyqtSignal(str)
	excecuteUpdateVerilog = pyqtSignal(list)
	def __init__(self, project_path, project_name, parent):
		super(QWidget, self).__init__()
		self._parent = parent
		uic.loadUi(QApplication.instance().get_src("ui", "simulation_view.ui"), self)
		self._projectPath = project_path
		self._projectName = project_name

		self.is_initial = True

		self.precompiler = Precompiler(self._projectPath, self._projectName)
		self.precompiler_hdl = PrecompilerHDL(self._projectPath, self._projectName)
		self.saveLoad = SaveLoadProject(self._projectPath, self._projectName)

		self.pauseBtn.hide()
		self.nextBtn.hide()
		self.nextBtn.clicked.connect(self.nextBtnFnc)
		self.pauseBtn.clicked.connect(self.pauseFnc)

		self.simulationPropertiesBtn.clicked.connect(self.showSimulationProperties)
		self.searchComponentEntry.textChanged.connect(self._searchComponent)
		self.playBtn.clicked.connect(self._runFunction)
		self.plotsOptions.clicked.connect(self.settingsPlot)

		self.workspace = Workspace(self)
		self._view = GraphicsField(self.workspace, self)
		self._view.setRenderHints(QPainter.Antialiasing)
		self._view.setRubberBandSelectionMode(Qt.IntersectsItemShape)
		self.workspaceSite.addWidget(self._view)

		self.menuProperties = QMenu(self)
		self.widgetProperties = MenuProperties()
		widget = QWidgetAction(self)
		widget.setDefaultWidget(self.widgetProperties)
		self.menuProperties.addAction(widget)

		self.principalMenuPopUp = QMenu(self)
		self.principalMenu = PrincipalMenu(self)
		widget = QWidgetAction(self)
		widget.setDefaultWidget(self.principalMenu)
		self.principalMenuPopUp.addAction(widget)

		# Buttons
		self.addBox.clicked.connect(self._addRectangle)
		self.addCircle.clicked.connect(self._addEllipse)

		# MacroComponent
		self.macroComponentBtn.clicked.connect(self._addMacroComponent)

		# assing data
		self.componentWidget = ComponentWidget()
		self.componentsLayout.addLayout(self.componentWidget)

		self.shortcut = QShortcut(QKeySequence("Ctrl+S"), self)
		self.shortcut.activated.connect(self._saveProject)
		self.shortcutRun = QShortcut(QKeySequence("Ctrl+R"), self)
		self.shortcutRun.activated.connect(self._runFunction)

		self.cancelSimulation = True
		self._timeSimulation = 0.2
		self._ciclesVerilog = 1000
		self._isPaused = False
		self._execOneTime = False
		self._runNative = True

		self._orderInputsVerilog = {}

		# Window for edit component properties
		self.propertiesWindow = PropertiesWindow()
		self.componentsProperties.addWidget(self.propertiesWindow)

		# Signal slot of finish simulation
		self.counter.connect(self.finishSimulation)

		# Icons
		self.playBtn.setIcon(QIcon(QPixmap(QApplication.instance().get_src("images", "icons", "play.png"))))
		self.pauseBtn.setIcon(QIcon(QPixmap(QApplication.instance().get_src("images", "icons", "pause.png"))))
		self.nextBtn.setIcon(QIcon(QPixmap(QApplication.instance().get_src("images", "icons", "next.png"))))
		self.simulationPropertiesBtn.setIcon(QIcon(QPixmap(QApplication.instance().get_src("images", "icons", "settings.png"))))
		self.macroComponentBtn.setIcon(QIcon(QPixmap(QApplication.instance().get_src("images", "icons", "create.png"))))
		self.addCircle.setIcon(QIcon(QPixmap(QApplication.instance().get_src("images", "icons", "circle.png"))))
		self.addLetter.setIcon(QIcon(QPixmap(QApplication.instance().get_src("images", "icons", "font.png"))))
		self.addBox.setIcon(QIcon(QPixmap(QApplication.instance().get_src("images", "icons", "box.png"))))
		self.plotsOptions.setIcon(QIcon(QPixmap(QApplication.instance().get_src("images", "icons", "plot.png"))))

		# Plots
		self._plotWidget = PlotWidget()
		self.plotSite.addWidget(self._plotWidget, 0)

		# Initial actions
		# TODO: add thread to this with a progress bar
		self._loadProject()

		self._isSaved = True

	def toSVG(self):
		path = str(QFileDialog.getExistingDirectory(self, "Select Directory"))
		if path:
			generator = QSvgGenerator()
			generator.setFileName(os.path.join(path, "model.svg"))
			generator.setSize(QSize(6000, 6000))
			generator.setViewBox(QRect(0, 0, 6000, 6000))
			p = QPainter(generator)
			self.workspace.render(p)
			p.end()

	def toPNG(self):
		path = str(QFileDialog.getExistingDirectory(self, "Select Directory"))
		if path:
			pixMap = self._view.grab()
			pixMap.save(os.path.join(path, "model.png"), format = "PNG", quality = 100)

	def print(self):
		path = str(QFileDialog.getExistingDirectory(self, "Select Directory"))
		if path:
			printer = QPrinter(QPrinter.HighResolution)
			printer.setPageSize(QPrinter.A4)
			printer.setOrientation(QPrinter.Landscape)
			printer.setCreator("Sintel")

			printer.setOutputFormat(QPrinter.NativeFormat)
			printer.setOutputFileName(os.path.join(path, "model.pdf"))

			p = QPainter()

			p.begin(printer)
			self.workspace.render(p)
			p.end()

	def bottomNotify(self, text):
		self.stateText.setText(text)

	def setEdited(self):
		self._isSaved = False
		self._parent.setSavedProject(self, False)

	def setSaved(self):
		self._isSaved = True
		self._parent.setSavedProject(self, True)

	def isSaved(self):
		return self._isSaved

	def _addMacroComponent(self):
		self.workspace.addMacroComponent()

	def keyPressEvent(self, event):
		"""
			Detect the key press
			@param event as QKeyEvent
		"""
		if event.key() == Qt.Key_Escape:
			self.workspace.keyPressEvent(event)

	def _addRectangle(self):
		self.workspace.addRectangle()

	def _addEllipse(self):
		self.workspace.addEllipse()

	def _loadProject(self):
		"""
			Loads the project in the workspace
		"""
		self._errores = []
		data = self.saveLoad.loadProject()
		if data != -1:
			self.workspace.setComponentIndex(data["componentIndex"]["comp"])
			self.workspace.setMacroIndex(data["componentIndex"]["macro"])
			self.workspace.setSignalIndex(data["signalIndex"])

			for comp in data["components"]:
				if not "macro" in comp:
					clock = comp["clock"]
					if clock == None:
						clock = {
							"update" : "0"
						}
					self.workspace.addElement(comp["file"], clock = clock, initialPos = QPointF(float(comp["pos"][0]), float(comp["pos"][1])), loadID = comp["id"], properties = comp["properties"], pins = comp["pins"])

			if "macros" in data:
				for macro in data["macros"]:
					self.workspace.addMacroComponent(macro)

			self.loadSignals(data)

			if "draws" in data:
				for draw in data["draws"]:
					self.workspace.addDraw(draw)

			for plot in data["plots"]:
				self.addPlot(plot)
			if "signals_show" in data:
				for sig in data["signals_show"]:
					self.workspace.addSignalShowID(sig)

			if len(self._errores) > 0:
				QApplication.instance().showError(self._errores)

	def loadSignals(self, data, appendText = "", addPosition = QPointF(0, 0)):
		foldsToAdd = []
		indexF = 1
		for sig in data["signals"]:
			if "indexF" in sig:
				indexF = int(sig["indexF"])
			componentA = self.workspace.getComponentByID(sig["compA"])
			componentB = self.workspace.getComponentByID(sig["compB"])
			if componentA != None and componentB != None:
				connA = componentA["object"].getPinObj(sig["connA"])
				connB = componentB["object"].getPinObj(sig["connB"])
				folds = [Fold(QPointF(float(i["x"]), float(i["y"])) + addPosition, isNew = False, initialID = str(i["foldID"])) for i in sig["folds"]]

				control = False
				if "control" in sig:
					control = sig["control"]
				newS = self.workspace.addSignal(connA = connA, connB = connB, referenceMoveA = connA, referenceMoveB = connB, folds = folds, psignalID = sig["id"] + appendText, indexF = indexF, isControlPath = control)
			else:
				if componentA == None:
					self._errores.append("1. Error for load component " + sig["compA"])
				else:
					self._errores.append("2. Error for load component " + sig["compB"])

		for sig in data["signalsTerminal"]:
			componentA = self.workspace.getComponentByID(sig["compA"])
			if componentA != None:
				if "indexF" in sig:
					indexF = int(sig["indexF"])
				connA = componentA["object"].getPinObj(sig["connA"])
				pos = QPointF(sig["connB"]["x"], sig["connB"]["y"])
				connB = signalTerminal(pos + addPosition)
				self.workspace.addItem(connB)
				folds = [Fold(QPointF(float(i["x"]), float(i["y"])) + addPosition, isNew = False, initialID = str(i["foldID"])) for i in sig["folds"]]
				self.workspace.addSignal(connA = connA, connB = connB, referenceMoveA = connA, referenceMoveB = connB, folds = folds, psignalID = sig["id"] + appendText, indexF = indexF)
			else:
				self._errores.append("3. Error for load component " + sig["compA"])

		for sig in data["signalsSignals"]:
			self.loadSignalSignal(sig, data, appendText, addPosition)

		for macro in self.workspace.getAllMacros():
			if macro.getMacro() == None:
				macro.minimizeSH()
		#print(self._errores)

	def addError(self, error):
		self._errores.append(error)

	def loadSignalSignal(self, sig, data, appendText = "", addPosition = QPointF(0, 0)):
		try:
			componentA = self.workspace.getComponentByID(sig["compA"])
			componentB = self.workspace.getComponentByID(sig["compB"])
			connA = componentA["object"].getPinObj(sig["connA"])
			connB = componentB["object"].getPinObj(sig["connB"])
			folds = [Fold(QPointF(float(i["x"]), float(i["y"])) + addPosition, isNew = False, initialID = str(i["foldID"])) for i in sig["folds"]]

			signalBits = sig["signalBits"]
			signalRef = self.workspace.getSignalByID(sig["signalRef"] + appendText)
			if signalRef == None:
				obj_index = next((item for item in data["signalsSignals"] if item["id"] == sig["signalRef"] + appendText), False)
				if obj_index != False:
					self.loadSignalSignal(obj_index, data, appendText, addPosition)
					signalRef = self.workspace.getSignalByID(sig["signalRef"] + appendText)

			signalRef = signalRef["object"]
			refA = signalRef.getFoldByID(sig["refA"])

			control = False
			if "control" in sig:
				control = sig["control"]
			if "indexF" in sig:
				indexF = int(sig["indexF"])
			self.workspace.addSignal(connA = connA, connB = connB, referenceMoveA = refA, referenceMoveB = connB, folds = folds, indexF = indexF, signalReference = signalRef, signalBits = signalBits, psignalID = sig["id"] + appendText, isControlPath = control)
		except Exception as e:
			print(e)
			pass

	def saveProject(self):
		"""
			Saves all data in the project file
		"""
		self.setSaved()
		self.workspace.saveLoad.resetFile()
		allComponents = self.workspace.getAllWorkspaceComponents()
		for comp in allComponents:
			self.workspace.saveLoad.addComponent(comp)
		self.workspace.saveLoad.addMacros(self.workspace.getAllMacros())

		for sig in self.workspace.getAllSignals():
			if sig["object"].isSignalTerminal():
				self.workspace.saveLoad.addSignalTerminal(sig["object"])
			elif sig["object"].isSignalSignal():
				self.workspace.saveLoad.addSignalSignal(sig["object"])
			else:
				self.workspace.saveLoad.addSignal(sig["object"])
			allCons = sig["object"].getConnections()

		self.workspace.saveLoad.savePlots(self.workspace.getSignalsPlot())
		self.workspace.saveLoad.saveIndexes(self.workspace.getComponentIndex(), self.workspace.getSignalsIndex(), time = self._timeSimulation)
		self.workspace.saveLoad.saveDraw(self.workspace.getAllDraw())
		self.workspace.saveLoad.saveSignalsShow(self.workspace.getAllSignalsShow())
		self.workspace.saveLoad.writeFile()

	def settingsPlot(self):
		text, ok = QInputDialog.getText(None, 'Add a signal to plot', 'Enter the signal name:')
		if ok:
			self.addPlot(text)

	def updatePlot(self, value, signalID):
		self._plotWidget.plotValue(value, signalID)

	def addPlot(self, signalID, clock = False):
		signal = self.workspace.getSignalByID(signalID)
		if signal != None:
			#plotWidget = PlotWidget(signal["ID"])
			#self.plotSite.addWidget(plotWidget, 0)
			#signal["object"].setPlotWidget(plotWidget)
			#self.workspace.addToPlot(signal)
			self._plotWidget.addSignal(signal["ID"])
			self.workspace.addToPlot(signal)

	def deletePlot(self, signalID):
		signal = self.workspace.getSignalByID(signalID)
		if signal != None:
			#signal["object"].deletePlotWidget()
			self.workspace.deleteToPlot(signalID)
			self._plotWidget.removeSignal(signal["ID"])

	def _saveProject(self):
		self.saveProject()

	def showSimulationProperties(self):
		point = self.simulationPropertiesBtn.mapToGlobal(self.simulationPropertiesBtn.rect().bottomLeft())
		self.menuProperties.popup(point)

	def showPrincipalMenu(self, point):
		self.principalMenuPopUp.popup(point)

	def getProjectName(self):
		return str(self._projectName.replace(projectExtension, ""))

	def _searchComponent(self):
		text = self.searchComponentEntry.text()
		self.componentWidget.addIcons(str(text))

	def exportData(self, exporter):
		exporter.resetFile()
		allComponents = self.workspace.getAllWorkspaceComponents()
		allIOMacros = self.workspace.getAllIOMacros()
		for comp in allComponents:
			if not "macro" in comp:
				exporter.addComponent(comp["object"])

		# TODO: add components Macro
		for comp in allIOMacros:
			exporter.addComponentMacro(comp["object"])

		for sig in self.workspace.getAllSignals():
			exporter.addSignal(sig["object"])
			allCons = sig["object"].getConnections()
			modelSubSignals = sig["object"].getSignalBits()

			if not sig["object"].isSignalSignal() and not sig["object"].isSignalTerminal():
				for conn in allCons:
					exporter.addConnection(conn.getParent(), conn, sig["object"], isInput = conn.isInput())
				self.addSignalPython(sig["object"], exporter, sig["object"])

			elif sig["object"].isSignalTerminal():
				exporter.addConnection(allCons[0].getParent(), allCons[0], sig["object"], isInput = False)
				self.addSignalPython(sig["object"], exporter, sig["object"])

		for sig in self.workspace.getAllSignalsShow():
			exporter.addSignalShow(sig["object"])

	def addSignalPython(self, sig, exporter, original):
		returned = sig.returnConnectedTerminal()
		for ret in returned:
			#print(original.getID(), ret.getID())
			if ret.getsignalReferencePrecompiler().isSignalTerminal():
				if original.getConnections()[0].getType() == "in":
					#exporter.addConnection(ret.getConnections()[1].getParent(), ret.getConnections()[1], original, isInput = True)

					# S1 = Signal_172
					# S2 = Signal_171

					#original.getID().assignSubSignal("0", ret.getID()) Y
					#connector.getParentID().assignSignal("connector.getID()", ret.getID()) Y
					#not0151.assignSignal("pIn", Signal_171) Y
					#ret.getID().connectTo(original.getConnections()[0].getParentID()) Y

					exporter.addConnectionSignals2(ret, original, ret.getSignalBits(), ret.getConnections()[1])
					#exporter.addConnectionSignals(ret.getID(), original.getID(), ret.getSignalBits(), ret.getConnections()[0], revert = True)
				else:
					#exporter.addConnection(ret.getConnections()[1].getParent(), ret.getConnections()[1], ret, isInput = False)
					#exporter.addConnectionSignals3(original.getID(), ret.getID(), ret.getSignalBits(), ret.getConnections()[1])

					# S1 <- origina
					# S2 <- ret

					# S2.assignSubSignal(0, s1(0))
					# ret.conn[1].getParent().assignSignal("in", S2)
					# notOrig.assignSignal("out", S1)

					# S1.connectTo(notRet)

					exporter.addConnectionSignals3(ret, original, ret.getSignalBits(), ret.getConnections()[1], original)
			else:
				if sig.getConnections()[0].getType() == "in" and not original.isSignalTerminal():
					exporter.addConnectionSignals2(sig.getsignalReferencePrecompiler(), ret, ret.getSignalBits(), ret.getConnections()[1], invert = True)
					#exporter.addConnectionSignals(ret.getID(), original.getID(), ret.getSignalBits(), ret.getConnections()[0], revert = True)
				else:
					#exporter.addConnection(ret.getConnections()[1].getParent(), ret.getConnections()[1], ret, isInput = False)
					#exporter.addConnectionSignals(sig, ret, ret.getSignalBits(), ret.getConnections()[1], invert = True)
					if original.isSignalTerminal() and original.getConnections()[0].getType() == "in":
						exporter.addConnectionSignals3(sig, ret, ret.getSignalBits(), ret.getConnections()[1], sig, invert = True)
					else:
						exporter.addConnectionSignals3(sig, ret, ret.getSignalBits(), ret.getConnections()[1], original, invert = True)
			self.addSignalPython(ret, exporter, original)

			"""else:
				exporter.addConnection(allCons[1].getParent(), allCons[1], sig["object"], isInput = allCons[1].isInput())
				if sig["object"].getsignalReferencePrecompiler().isSignalTerminal():
					conSend = allCons[1]
					invert = False
					if allCons[0].getType() == "in" :
						conSend = allCons[0]
						invert = True
					exporter.addConnectionSignals2(sig["object"].getsignalReferencePrecompiler().getID(), sig["ID"], modelSubSignals, conSend, invert)
				else:
					exporter.addConnectionSignals(sig["object"].getsignalReferencePrecompiler().getID(), sig["ID"], modelSubSignals, allCons[1])
			"""

	def addSignalHDL(self, sig, exporter, original):
		returned = sig.returnConnectedTerminal()
		for ret in returned:
			#print(original.getID(), ret.getID())
			if ret.getsignalReference().isSignalTerminal():
				exporter.addConnection(ret.getConnections()[1].getParent(), ret.getConnections()[1], ret)
				if original.getConnections()[0].getType() == "in":
					exporter.addConnectionSignals2(ret, original, ret.getSignalBits(), ret.getConnections()[1])
				else:
					exporter.addConnectionSignals3(ret, original, ret.getSignalBits(), ret.getConnections()[1], original)
			else:
				exporter.addConnection(ret.getConnections()[1].getParent(), ret.getConnections()[1], ret)
				if sig.getConnections()[0].getType() == "in" and not original.isSignalTerminal():
					exporter.addConnectionSignals2(sig, ret, ret.getSignalBits(), ret.getConnections()[1], invert = True)
				else:
					if original.isSignalTerminal() and original.getConnections()[0].getType() == "in":
						exporter.addConnectionSignals3(sig, ret, ret.getSignalBits(), ret.getConnections()[1], sig, invert = True)
					else:
						exporter.addConnectionSignals3(sig, ret, ret.getSignalBits(), ret.getConnections()[1], original, invert = True)
			self.addSignalHDL(ret, exporter, original)


	def exportDataHDL(self, exporter, forSimulation = False):
		exporter.resetFile(forSimulation = forSimulation, ciclesVerilog = self._ciclesVerilog)
		allComponents = self.workspace.getAllWorkspaceComponents()
		allIOMacros = self.workspace.getAllIOMacros()
		for comp in allComponents:
			if not "macro" in comp:
				exporter.addComponent(comp["object"])

		# TODO: add components Macro
		for comp in allIOMacros:
			exporter.addComponentMacro(comp["object"])

		for sig in self.workspace.getAllSignals():
			allCons = sig["object"].getConnections()
			modelSubSignals = sig["object"].getSignalBits()

			exporter.addSignal(sig["object"])

			if not sig["object"].isSignalSignal() and not sig["object"].isSignalTerminal():
				for conn in allCons:
					exporter.addConnection(conn.getParent(), conn, sig["object"])
				self.addSignalHDL(sig["object"], exporter, sig["object"])

			elif sig["object"].isSignalTerminal():
				exporter.addConnection(allCons[0].getParent(), allCons[0], sig["object"])
				self.addSignalHDL(sig["object"], exporter, sig["object"])

			"""else:
				exporter.addConnection(allCons[1].getParent(), allCons[1], sig["object"])

				if sig["object"].getsignalReferencePrecompiler().isSignalTerminal():
					exporter.addConnectionSignals2(sig["object"].getsignalReference(), sig["object"], modelSubSignals)
				else:
					exporter.addConnectionSignals(sig["object"].getsignalReference(), sig["object"], modelSubSignals)"""
		for sig in self.workspace.getAllSignalsShow():
			exporter.addSignalShow(sig["object"])

	def _runFunction(self):
		"""
			Runs the simulation
		"""
		if not self.cancelSimulation:
			self.cancelSimulation = True
			self.playBtn.setIcon(QIcon(QPixmap(QApplication.instance().get_src("images", "icons", "play.png"))))
			self.pauseBtn.hide()
			self.bottomNotify("Stop Simulation")
		else:
			# Buttons style
			self.playBtn.setIcon(QIcon(QPixmap(QApplication.instance().get_src("images", "icons", "stop.png"))))
			self.pauseBtn.show()
			self.cancelSimulation = False
			self.bottomNotify("Simulating")
			print("running")

			self._plotWidget.emptyAll()
			# Menu properties
			self._ciclesNumber = self.widgetProperties.getCiclesNumber()
			self._isPaused = self.widgetProperties.startPaused()
			self._clockSampling = self.widgetProperties.clockSampling()
			self._timeSimulation = 0.1#self.widgetProperties.getSimTime()
			self.showHideBtn()

			indexRun = self.simCoreSelect.currentIndex()

			if indexRun == 0:
				self._runNative = False
				self.generateVHDL(forSimulation = True)
				self._timeCompileVerilog = None
				self.excecuteUpdateVerilog.connect(QApplication.instance().projectsHandler.getWorkspace().UpdateGuiSimulation)
				thread = threading.Thread(target = self.runVerilog)
				thread.setDaemon(True)
				thread.start()
			else:
				self._runNative = True
				self.exportData(self.precompiler)
				self.precompiler.generateExec()
				try:
					importlib.reload(default)
				except:
					default = importlib.import_module('.', package='default')
				App = QApplication.instance()

				self.SimulationRun = default.SimulationRun(QApplication.instance().projectsHandler.getWorkspace().UpdateGuiSimulation, is_initial = self.is_initial)
				self.SimulationRun.excecuteUpdate.connect(QApplication.instance().projectsHandler.getWorkspace().UpdateGuiSimulation)
				self._execOneTime = False

				thread = threading.Thread(target = self.runCode)
				thread.setDaemon(True)
				thread.start()
				#text = default.run()

	def nextBtnFnc(self):
		self._execOneTime = True

	def pauseFnc(self):
		self._isPaused = not self._isPaused
		self.showHideBtn()

	def showHideBtn(self):
		self.playBtn.setIcon(QIcon(QPixmap(QApplication.instance().get_src("images", "icons", "stop.png"))))
		if self._isPaused:
			self.pauseBtn.setIcon(QIcon(QPixmap(QApplication.instance().get_src("images", "icons", "play.png"))))
			self.nextBtn.show()
		else:
			self.pauseBtn.setIcon(QIcon(QPixmap(QApplication.instance().get_src("images", "icons", "pause.png"))))
			self.nextBtn.hide()

	def updateInlineRun(self, component, properties):
		if not self.cancelSimulation:
			if self._runNative:
				self.SimulationRun.updateComponent(component.getID(), properties)
			else:
				if component.isInput():
					self._orderInputsVerilog[component.getID() + "_out"] = properties[0]["value"]
					self.setInputsVerilog()

	def setInputsVerilog(self):
		inputs = []
		for a in self._orderInputsVerilog:
			inputs.append(str(self._orderInputsVerilog[a]))
		text = ",".join(inputs)
		file = open(os.path.join(get_pycompile_folder(), "run", "io.txt"), "w")
		file.write(text + "\n")
		file.close()

	def executeVerilogSimulation(self):
		time_start = time.time()
		self._orderInputsVerilog = self.precompiler_hdl.generateVerilogTestBench()
		self.setInputsVerilog()
		self._proc = subprocess.Popen(["make"],
			shell = False,
			stdout=subprocess.PIPE,
			stderr = subprocess.PIPE,
			stdin=subprocess.PIPE,
			universal_newlines=True,
			bufsize=0,
			cwd=os.path.join(get_pycompile_folder(), "run")
		)
		if self._timeCompileVerilog == None:
			self._timeCompileVerilog = time.time() - time_start
		self.contCommand()
		print(time.time() - time_start)

	def executeVerilogSimulationWindows(self):
		time_start = time.time()
		self._orderInputsVerilog = self.precompiler_hdl.generateVerilogTestBench()
		self.setInputsVerilog()
		self._proc = subprocess.Popen("iverilog -o simulation verilog\*.v & vvp -i simulation",
			shell = True,
			cwd=os.path.join(get_pycompile_folder(), "run"),
			bufsize=0,
			stdin=subprocess.PIPE,
			stdout = subprocess.PIPE,
			stderr = subprocess.PIPE
		)
		if self._timeCompileVerilog == None:
			self._timeCompileVerilog = time.time() - time_start
		self.contCommand()
		print(time.time() - time_start)

	def contCommand(self):
		if sys.platform == "linux" or sys.platform == "linux2":
			a = self._proc.stdin.write("cont\n")
			text = self._proc.stdout.readline()
			while "ticks." not in text:
				text = self._proc.stdout.readline()
		else:
			a = self._proc.stdin.write(bytes("cont\n", "utf-8"))
			text = self._proc.stdout.readline().decode()
			while "ticks." not in text:
				text = self._proc.stdout.readline().decode()

	def finishCommand(self):
		if sys.platform == "linux" or sys.platform == "linux2":
			self._proc.stdin.write("finish\n")
			self._proc.terminate()
		else:
			self._proc.stdin.write(bytes("finish\n", "utf-8"))
			self._proc.terminate()

	def runVerilog(self):
		try:
			if sys.platform == "linux" or sys.platform == "linux2":
				self.executeVerilogSimulation()
			else:
				self.executeVerilogSimulationWindows()
			time.sleep(0.1)
			filename = os.path.join(get_pycompile_folder(), "run", "output.txt")
			file = open(filename, "r")
			self._textSimulation = file.readlines()
			file.close()
			line_headers = self._textSimulation[0].replace("\n", "").split(",")
			self._initialTime = current_milli_time()
			self._headersVerilog = []#[{"ID" : "simulation_time", "value" : current_milli_time() - self._initialTime}]
			for header in line_headers:
				self._headersVerilog.append({
					"ID" : header,
					"value" : 'U'
				})

			cicle = 1

			plots_signals = self.workspace.getSignalsPlot()

			if self._isPaused:
				for nro_cicle in range(5):
					self.contCommand()

			while True:
				if self.cancelSimulation:
					#self._proc.stdin.write("finish\n")
					#self._proc.terminate()
					self.finishCommand()
					raise CancelException
				exec = True
				if self._isPaused:
					exec = self._execOneTime
					ciclesNum = 4
					self._execOneTime = False

				if exec:
					#a = self._proc.stdin.write("cont\n")
					#text = self._proc.stdout.readline()
					#while "ticks." not in text:
					#	text = self._proc.stdout.readline()
					self.contCommand()
					time.sleep(0.1)
					filename = os.path.join(get_pycompile_folder(), "run", "output.txt")
					file = open(filename, "r")
					self._textSimulation = file.readlines()
					file.close()

					line = self._textSimulation[-1].replace("\n", "").split(",")
					index = 0

					dataSend = [{"ID" : "simulation_time", "value" : current_milli_time() - self._initialTime}]
					#for pl in plots_signals:
					#	obj_index = next((com for com in self._headersVerilog if pl == com["ID"]), False)
					#	if obj_index != False:
					#		dataSend.append({"ID" : pl, "value" : obj_index["value"]})

					#self._headersVerilog[0]["value"] = current_milli_time() - self._initialTime
					for body in line:
						body = body.strip().lower()
						if body == 'x':
							self._headersVerilog[index]["value"] = 'U'
						elif body == 'z':
							self._headersVerilog[index]["value"] = 'Z'
						else:
							body = filterTextNumber(body)
							if body != "":
								self._headersVerilog[index]["value"] = int(body)
						index += 1

					dataSend.extend(self._headersVerilog)

					if not self._clockSampling:
						self.excecuteUpdateVerilog.emit(dataSend)
					else:
						if cicle % 2 == 0:
							self.excecuteUpdateVerilog.emit(dataSend)

					cicle += 1
					if cicle == 200:
						a_file = open(filename, 'w')
						a_file.close()
						cicle = 0
				time.sleep(0.1)
		except CancelException:
			self.playBtn.setIcon(QIcon(QPixmap(QApplication.instance().get_src("images", "icons", "play.png"))))
			self.pauseBtn.hide()
			print("finish")
		except Exception as e:
			self.playBtn.setIcon(QIcon(QPixmap(QApplication.instance().get_src("images", "icons", "play.png"))))
			self.pauseBtn.hide()
			self.counter.emit(str(e))
			print("finish")

	def finishSimulation(self, text):
		QApplication.instance().showError(["Error en la simulación: " + str(text)])

	def runCode(self):
		#try:
		# Ciclo reloj : 0.3 * 2 = 0.6, de esos 0.6, 0.5 para operaciones del reloj y 0.1 para otras operaciones
		self.SimulationRun.inputInitial()
		time.sleep(self._timeSimulation / 2)
		self.SimulationRun.resetComponents()
		self.SimulationRun.updateDefault()
		self.SimulationRun.updateAll()

		for num in range(self._ciclesNumber * 4):
			self.SimulationRun.run(emit = False)
			self.is_initial = False

		while True:
			if self.cancelSimulation:
				raise CancelException
			exec = True
			ciclesNum = 1
			if self._isPaused:
				exec = self._execOneTime
				ciclesNum = 4
				self._execOneTime = False

			if exec:
				for num in range(ciclesNum):
					start_time = time.time()
					self.SimulationRun.run()
					final_time = time.time() - start_time
					print(final_time)
					if self._timeSimulation - final_time < 0:
						time.sleep(final_time - self._timeSimulation)
						print("NEGATIVE", final_time, self._timeSimulation)
					else:
						time.sleep(self._timeSimulation - final_time)

					self.is_initial = False
			else:
				time.sleep(0.1)
		#except CancelException:
		#	print("finish")
		#	self.playBtn.setIcon(QIcon(QPixmap(QApplication.instance().get_src("images", "icons", "play.png"))))
		#	self.pauseBtn.hide()
		#except Exception as e:
		print("finish")
		#print(e)
		self.playBtn.setIcon(QIcon(QPixmap(QApplication.instance().get_src("images", "icons", "play.png"))))
		self.pauseBtn.hide()
		self.counter.emit(str(e))

	def generateVHDL(self, forSimulation = False):
		if forSimulation:
			files = glob.glob(os.path.join(get_pycompile_folder(), "run", "verilog", "*"))
			for f in files:
				os.remove(f)
		self.exportDataHDL(self.precompiler_hdl, forSimulation = forSimulation)
		self.precompiler_hdl.generateHDL()

		if not forSimulation:
			QApplication.instance().showValid("Se ha generado el código VHDL en el directorio del proyecto con éxito")

	def zoomOut(self):
		self._view.zoomOut()

	def zoomIn(self):
		self._view.zoomIn()

	def setLabelZoom(self, value):
		self.principalMenu.setLabelZoom(str(int(value * 100)))


class CancelException(Exception):
	"""
		Class to control the cancel simulation exception
	"""
	pass

class GraphicsField(QGraphicsView):

	def __init__(self, parent, simView):
		super(GraphicsField, self).__init__(parent)
		self._controlKey = False
		self.__zoom_value = 1
		self.min_zoom = 0.4
		self.max_zoom = 2

		self._simView = simView

	def keyPressEvent(self, event):
		"""
			Detect the key press
			@param event as QKeyEvent
		"""
		if event.key() == Qt.Key_Z:
			self._controlKey = not self._controlKey
		elif event.key() == Qt.Key_Plus:
			self.zoomIn()
		elif event.key() == Qt.Key_Minus:
			self.zoomOut()
		QGraphicsView.keyPressEvent(self, event)

	def keyReleaseEvent(self, event):
		QGraphicsView.keyReleaseEvent(self, event)

	def wheelEvent(self, event):
		"""
			Emits when the user made scroll over the scene
			@param event as wheelevent
		"""
		if self._controlKey:
			if event.angleDelta().y() < 0:
				#self.__zoom_value = self.__zoom_value - 0.02 if self.__zoom_value > self.min_zoom else self.min_zoom
				self.zoomOut()
			elif event.angleDelta().y() > 0:
				self.zoomIn()
			#if self.__zoom_value <= self.max_zoom and self.__zoom_value >= self.min_zoom:
			#	self.changeZoom(self.__zoom_value, event)
		else:
			QGraphicsView.wheelEvent(self, event)

	def changeZoom(self, value, event):
		"""
			Change the zoom value
			@param value as float > 0 and < 1
		"""
		oldPos = self.mapToScene(event.pos().x(), event.pos().y())
		self.resetTransform()
		self.scale(value, value)
		newPos = self.mapToScene(event.pos().x(), event.pos().y())
		delta = newPos - oldPos
		self.translate(delta.x() * 10, delta.y() * 10)

	def zoomOut(self):
		if self.__zoom_value > self.min_zoom:
			self.scale(0.95, 0.95)
			self.__zoom_value -= 0.05
			self._simView.setLabelZoom(self.__zoom_value)

	def zoomIn(self):
		if self.__zoom_value < self.max_zoom:
			self.scale(1.05, 1.05)
			self.__zoom_value += 0.05
			self._simView.setLabelZoom(self.__zoom_value)
