#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries
from simulador.tools.utils import projectExtension, get_pycompile_folder

# Qt libraries
from PyQt5.QtWidgets import QApplication

# System libraries
import os
import json
from shutil import copyfile


class PrecompilerHDL():
	"""
		Handle the creation of the json
		@param projectName as str
		@param projectPath as str
	"""
	def __init__(self, projectPath, projectName):
		self.resetFile()
		# Original files
		projectName = str(projectName)
		self._projectPath_initial = str(projectPath)
		self._projectPath = self._projectPath_initial

		self._forSimulation = False

		self._maxClockCicle = 30
		self._minClockCicle = 10

		# Temp files
		self._mainVHDLFile = os.path.join(str(self._projectPath), "vhdl", "main.vhdl")
		self._componentsVHDLFile = os.path.join(str(self._projectPath), "vhdl", "components_package.vhdl")
		self._verilogFile_initial = os.path.join(str(self._projectPath), "verilog", "main.v")
		self._verilogFile = self._verilogFile_initial


	def resetFile(self, forSimulation = False, ciclesVerilog = 1000):
		self._forSimulation = forSimulation
		self.dataCompile = {
			"files" : [],
			"files_verilog" : [],
			"components": [],
			"inputs_outputs" : [],
			"signal_signal" : [],

			"signal_subsignal" : [],
			"signals": [],
			"data_update" : []
		}

		if forSimulation:
			self._ciclesVerilog = ciclesVerilog
			self._projectPath = os.path.join(get_pycompile_folder(), "run")
			self._verilogFile = os.path.join(self._projectPath, "verilog", "main.v")

	def addConnection(self, component, pin, signal):
		if component.isInput():
			self.dataCompile["inputs_outputs"].append({
				"ID" : component.getID() + "_" + str(pin.getID()),
				"clock_gen" : pin.isClockOut(),
				"type" : "in",
				"size" : signal.getSize(),
				"initial_value" : component.getInitialValueIn(),
				"value" : component.getValueIn()
			})
			self.dataCompile["signal_signal"].append([
				str(signal.getID()), str(component.getID()) + "_" + str(pin.getID())
			])
		elif component.isOutput():
			self.dataCompile["inputs_outputs"].append({
				"ID" : component.getID() + "_" + str(pin.getID()),
				"type" : "out",
				"clock_gen" : False,
				"size" : signal.getSize()
			})
			self.dataCompile["signal_signal"].append([
				str(component.getID()) + "_" + str(pin.getID()), signal.getID()
			])
		else:
			component_list = next((item for item in self.dataCompile["components"] if item["ID"] == component.getID()), False)
			variable = component.getVariableByPin(pin)
			if variable != None:
				only_zero = False
				if len(variable["bits"]) == 1 and pin.getSize() == 1:
					only_zero = True
				if component_list != False:
					ports = next((item for item in component_list["ports"] if item["from"] == pin.getID()), False)
					if ports != False:
						ports["to"] = signal.getID()
						ports["only_zero"] = only_zero
					else:
						component_list["ports"].append({
							"from" : pin.getID(),
							"to" : signal.getID(),
							"to_verilog" : signal.getID(),
							"only_zero" : only_zero
						})


	def addConnectionSignals2(self, ret, original, model, connector, invert = False):
		index = 0
		if original.getSize() == ret.getSize():
			self.dataCompile["signal_signal"].append([
				original.getID(), ret.getID()
			])
		else:
			for i in model:
				self.dataCompile["signal_subsignal"].append({
					"signal1" : original.getID(),
					"signal2" : ret.getID(),
					"sub1" : str(i),
					"sub2" : str(index)
				})
				index += 1

	def addConnectionSignals3(self, ret, original, model, connector, original2, invert = False):
		# S1 <- origina
		# S2 <- ret

		# S2.assignSubSignal(0, s1(0)) Y
		# notRet.assignSignal("in", S2)
		# notOrig.assignSignal("out", S1)
		# S1.connectTo(notRet)

		index = 0
		if original.getSize() == ret.getSize():
			if invert:
				self.dataCompile["signal_signal"].append([
					original.getID(), ret.getID()
				])
			else:
				self.dataCompile["signal_signal"].append([
					ret.getID(), original.getID()
				])
		else:
			for i in model:
				if invert:
					self.dataCompile["signal_subsignal"].append({
						"signal1" : original.getID(),
						"signal2" : ret.getID(),
						"sub1" : str(index),
						"sub2" : str(i)
					})
				else:
					self.dataCompile["signal_subsignal"].append({
						"signal1" : ret.getID(),
						"signal2" : original.getID(),
						"sub1" : str(index),
						"sub2" : str(i)
					})
				index += 1

	"""def addConnectionSignals2(self, signal1, signal2, model):
		index = 0
		if signal1.getSize() == signal2.getSize():
			self.dataCompile["signal_signal"].append([
				signal2.getID(), signal1.getID()
			])
		else:
			for i in model:
				self.dataCompile["signal_subsignal"].append({
					"signal1" : signal1.getID(),
					"signal2" : signal2.getID(),
					"sub1" : str(i),
					"sub2" : str(index)
				})
				index += 1
	"""
	def addConnectionSignals(self, signal1, signal2, model):
		index = 0
		if signal1.getSize() == signal2.getSize():
			self.dataCompile["signal_signal"].append([
				signal2.getID(), signal1.getID()
			])
		else:
			for i in model:
				self.dataCompile["signal_subsignal"].append({
					"signal1" : signal2.getID(),
					"signal2" : signal1.getID(),
					"sub1" : str(index),
					"sub2" : str(i)
				})
				index += 1

	def addComponentMacro(self, io):
		"""
			Add a component in the compilation xml
			@param component as workspace_object
		"""
		#self.dataCompile["signals"].append({
		#	"ID" : io.getID() + "_pIn",
		#	"size" : io.getSize()
		#})
		#self.dataCompile["signals"].append({
		#	"ID" : io.getID() + "_pOut",
		#	"size" : io.getSize()
		#})
		pins = io.getPins()
		if io.getTypeIO() == "in":
			if pins[0].getConnectedSignal() == None:
				self.dataCompile["signal_subsignal"].append({
					"signal1" : pins[1].getConnectedSignal().getID(),
					"signal2" : None,
					"default" : str(pins[0].getDefaultValue()),
					"size" : str(io.getSize())
				})
			else:
				for i in range(int(io.getSize())):
					self.dataCompile["signal_subsignal"].append({
						"signal1" : pins[1].getConnectedSignal().getID(),
						"signal2" : pins[0].getConnectedSignal().getID(),
						"sub1" : str(i),
						"sub2" : str(i)
					})
		elif io.getTypeIO() == "out":
			for i in range(int(io.getSize())):
				if pins[0].getConnectedSignal() != None and pins[1].getConnectedSignal() != None:
					self.dataCompile["signal_subsignal"].append({
						"signal1" : pins[1].getConnectedSignal().getID(),
						"signal2" : pins[0].getConnectedSignal().getID(),
						"sub1" : str(i),
						"sub2" : str(i)
					})


	def addComponent(self, component):
		"""
			Add a component in the compilation xml
			@param component as workspace_object
		"""
		componentID = str(component.getID())

		if not (component.isInput() or component.isOutput()):
			type = str(component.getType())
			type_verilog = type
			clock = component.getClock()
			if clock != None:
				if clock["update"] == "0":
					type = "entity work." + type + "(falling_edge_arch)"
					type_verilog = type_verilog + "_falling"
				else:
					type = "entity work." + type + "(rising_edge_arch)"
					type_verilog = type_verilog + "_rising"

			comp = {
				"ID" : componentID,
				"type" : type,
				"type_verilog" : type_verilog,
				"generics" : [],
				"ports" : [],
				"clock" : component.getClock()
			}
			referenceBits = component.getReferenceBits()
			for refB in referenceBits:
				if len(refB["connectors"]) > 0:
					if len(refB["bits"]) > 1:
						comp["generics"].append({
							"ID" : str(refB["id"]) + "_LEN",
							"value" : str(refB["connectors"][0].getSize()),
							"value_verilog" : str(refB["connectors"][0].getSize())
						})
				for conn in refB["connectors"]:
					if not conn.isResetIn() and not conn.isClockIn() and not conn.isClockOut() and conn.getType() == "in":
						value = conn.getDefaultValue()
						if value != None and value != "":
							value = str(value)
							value_verilog = str(value)
							if conn.getSize() == 1:
								value = "'" + value + "'"
								value_verilog = "1'b" + value_verilog
							else:
								value = "std_logic_vector(to_unsigned(" + value + ", " + str(conn.getSize()) + "))"
							comp["ports"].append({
								"from" : conn.getID(),
								"to" : value,
								"to_verilog" : value_verilog,
								"only_zero" : False
							})
			properties = component.getProperties()
			for prop in properties:
				value = str(prop["value"])
				value_verilog = value
				if prop["type"] != "numeric":
					if prop["base"] == "bit":
						value = "'" + str(value) + "'"
						value_verilog = "1'b" + str(value_verilog)
				comp["generics"].append({
					"ID" : str(prop["name"]) + "_PAR",
					"value" : value,
					"value_verilog" : value_verilog
				})
			self.dataCompile["components"].append(comp)

			file_inc = component.getPrecompilerFiles()
			file_add = file_inc["vhdl"]
			file_verilog = file_inc["verilog"]
			if file_add not in self.dataCompile["files"]:
				dataFileVHDL = {"file" : file_add, "user" : component.isUserComponent()}
				if not dataFileVHDL in self.dataCompile["files"]:
					self.dataCompile["files"].append(dataFileVHDL)
					self.dataCompile["files_verilog"].append({"file" : file_verilog, "user" : component.isUserComponent()})


	def addSignal(self, signal):
		"""
			Add a signal to compiler file
		"""
		self.dataCompile["signals"].append({
			"ID" : signal.getID(),
			"size" : signal.getSize()
		})

	def generateHDL(self):
		"""
			Create the .v and .vhdl file
		"""
		if not self._forSimulation:
			self.generateVHDLCode()
		self.generateVerilogCode()

	def generateVHDLCode(self):
		text = ""
		with open(QApplication.instance().get_src("templates", "main.vhd"), 'r+') as f:
			text = f.read()
			f.close()

		subsignalText = []
		for sub in self.dataCompile["signal_subsignal"]:
			if sub["signal2"] == None:
				subsignalText.append(sub["signal1"] + " <= std_logic_vector(to_unsigned(" + sub["default"] + ", " + sub["size"] + "));")
			else:
				subsignalText.append(sub["signal1"] + "(" + str(sub["sub1"]) + ") <= " + sub["signal2"] + "(" + str(sub["sub2"]) + ");")

		signalsText = []
		for signal in self.dataCompile["signals"]:
			signalsText.append("signal " + str(signal["ID"]) + ": std_logic_vector(" + str(int(signal["size"]) - 1) + " downto 0);")

		inputsOutputsText = []
		inputsOutputsSignalsText = []
		connectIOSignals = []
		inputValues = []
		for io in self.dataCompile["inputs_outputs"]:
			inputsOutputsText.append(str(io["ID"]) + ": " + str(io["type"]) + " std_logic_vector(" + str(io["size"] - 1) + " downto 0)")
			inputsOutputsSignalsText.append("signal signal_" + str(io["ID"]) + ": std_logic_vector(" + str(io["size"] - 1) + " downto 0);")
			connectIOSignals.append(str(io["ID"]) + " => signal_" + str(io["ID"]))
			if str(io["type"]) == "in":
				inputValues.append("signal_" + str(io["ID"]) + " <= std_logic_vector(to_unsigned(1, " + str(io["size"]) + "));")

		signalSignal = []
		for signal in self.dataCompile["signal_signal"]:
			signalSignal.append(str(signal[0]) + " <= " + str(signal[1]) + ";")

		componentsText = []
		for component in self.dataCompile["components"]:
			compText = component["ID"] + ": " + component["type"] + "\n"
			generics = []
			for gen in component["generics"]:
				generics.append(str(gen["ID"]) + " => " + str(gen["value"]))
			if len(generics) > 0:
				compText += "\t\tgeneric map(\n\t\t\t" + ",\n\t\t\t".join(generics) + "\n\t\t)\n\t"

			ports = []
			for por in component["ports"]:
				textPort = str(por["from"])
				textPort += " => " + str(por["to"])
				if por["only_zero"]:
					textPort += "(0)"
				ports.append(textPort)
			compText += "\tport map(\n\t\t\t" + ",\n\t\t\t".join(ports) + "\n\t\t);"
			componentsText.append(compText)

		text = text.replace("{{signal_subsignal}}", "\n\t".join(subsignalText))
		text = text.replace("{{input_output}}", ";\n\t\t".join(inputsOutputsText))
		text = text.replace("{{signals}}", "\n".join(signalsText))
		text = text.replace("{{components}}", "\n\t".join(componentsText))
		text = text.replace("{{signal_signal}}", "\n\t".join(signalSignal))

		folderToSave = os.path.join(str(self._projectPath), "vhdl")
		if not os.path.exists(folderToSave):
			os.mkdir(folderToSave)

		with open(self._mainVHDLFile, 'w+') as f:
			f.write(text)
			f.close()

		text = ""
		with open(QApplication.instance().get_src("templates", "components_package.vhd"), 'r+') as f:
			text = f.read()
			f.close()

		componentsList = []
		for fileName in self.dataCompile["files"]:
			if fileName["user"]:
				path = os.path.join(get_pycompile_folder(), "components_user", "vhdl_file", fileName["file"])
			else:
				path = os.path.join(get_pycompile_folder(), "components", "vhdl_file", fileName["file"])
			with open(path, 'r+') as file:
				component = file.read()
				component = component.split("entity")[1].split("entity")[0]
				componentsList.append("component" + component + "component;")
				copyfile(path, os.path.join(folderToSave, fileName["file"].replace("sm/", "")))
				file.close()


		text = text.replace("{{components}}", "\n".join(componentsList))

		with open(self._componentsVHDLFile, 'w+') as f:
			f.write(text)
			f.close()

		text_test = ""
		with open(QApplication.instance().get_src("templates", "testbench.vhdl"), 'r+') as f:
			text_test = f.read()
			f.close()

		text_test = text_test.replace("{{input_output}}", ";\n\t\t".join(inputsOutputsText))
		text_test = text_test.replace("{{input_output_signals}}", "\n\t".join(inputsOutputsSignalsText))
		text_test = text_test.replace("{{connect_io_signals}}", ",\n\t".join(connectIOSignals))
		text_test = text_test.replace("{{input_values}}", "\n\t".join(inputValues))

		with open(os.path.join(folderToSave, "testbench.vhdl"), 'w+') as f:
			f.write(text_test)
			f.close()

		copyfile(QApplication.instance().get_src("templates", "make_testbench_ghdl"), os.path.join(folderToSave, "Makefile"))


	def generateVerilogCode(self):
		"""
			Create the .v file
		"""
		text = ""
		with open(QApplication.instance().get_src("templates", "main.v"), 'r+') as f:
			text = f.read()
			f.close()

		subsignalText = []
		for sub in self.dataCompile["signal_subsignal"]:
			if sub["signal1"] == "Signal_261M112M169":
				print("Signal_261M112M169")
			if sub["signal2"] == None:
				subsignalText.append("assign " + sub["signal1"] + " = " + sub["default"] + ";")
			else:
				subsignalText.append("assign " + sub["signal1"] + "[" + str(sub["sub1"]) + "] = " + sub["signal2"] + "[" + str(sub["sub2"]) + "];")

		signalsText = []
		for signal in self.dataCompile["signals"]:
			signalsText.append("wire [" + str(int(signal["size"]) - 1) + ":0] " + str(signal["ID"]) + ";")

		inputsOutputsText = []
		inputsOutputsTextTest = []
		connectIOSignals = []
		inputValuesTest = []
		for io in self.dataCompile["inputs_outputs"]:
			inputsOutputsText.append(str(io["type"]) + "put [" + str(io["size"] - 1) + ":0] " + str(io["ID"]))
			connectIOSignals.append("." + str(io["ID"]) + "(" + str(io["ID"]) + ")")
			if io["type"] == "in":
				inputsOutputsTextTest.append("reg [" + str(io["size"] - 1) + ":0] " + str(io["ID"]) + ";")
				inputValuesTest.append(str(io["ID"]) + " = 1;")
			else:
				inputsOutputsTextTest.append("wire [" + str(io["size"] - 1) + ":0] " + str(io["ID"]) + ";")

		signalSignal = []
		for signal in self.dataCompile["signal_signal"]:
			signalSignal.append("assign " + str(signal[0]) + " = " + str(signal[1]) + ";")

		componentsText = []
		for component in self.dataCompile["components"]:
			compText = component["type_verilog"] + " "
			generics = []
			for gen in component["generics"]:
				generics.append("." + str(gen["ID"]) + "(" + str(gen["value_verilog"]) + ")")
			compText += "#(" + ", ".join(generics) + ") " + component["ID"]

			ports = []
			for por in component["ports"]:
				textPort = "." + str(por["from"])
				textPort += "(" + str(por["to_verilog"])
				if por["only_zero"]:
					textPort += "[0]"
				textPort += ")"
				ports.append(textPort)
			compText += " (" + ", ".join(ports) + ");"
			componentsText.append(compText)

		text = text.replace("{{input_output}}", ",\n\t".join(inputsOutputsText))
		text = text.replace("{{wires}}", "\n\t".join(signalsText))
		text = text.replace("{{signal_subsignal}}", "\n\t".join(subsignalText))
		text = text.replace("{{components}}", "\n\t".join(componentsText))
		text = text.replace("{{signal_signal}}", "\n\t".join(signalSignal))

		folderToSave = os.path.join(str(self._projectPath), "verilog")
		if not os.path.exists(folderToSave):
			os.mkdir(folderToSave)

		with open(self._verilogFile, 'w+') as f:
			f.write(text)
			f.close()

		componentsList = []
		for fileName in self.dataCompile["files_verilog"]:
			if fileName["user"]:
				path = os.path.join(get_pycompile_folder(), "components_user", "verilog_files", fileName["file"])
			else:
				path = os.path.join(get_pycompile_folder(), "components", "verilog_files", fileName["file"])

			with open(path, 'r+') as file:
				copyfile(path, os.path.join(folderToSave, fileName["file"].replace("sm/", "")))
				file.close()

		self.generateVerilogTestBench()

	def addSignalShow(self, signal):
		signalID = signal.getID()
		if not signalID in self.dataCompile["data_update"]:
			self.dataCompile["data_update"].append(signalID)

	def generateVerilogTestBench(self):
		folderToSave = os.path.join(str(self._projectPath), "verilog")
		inputsOutputsTextTest = []
		connectIOSignals = []
		inputs_read = []
		initial_values = []
		inputs_values = {}
		clockGen = ""
		for io in self.dataCompile["inputs_outputs"]:
			if io["clock_gen"]:
				clockGen = io["ID"]
				initial_values.append(str(io["ID"]) + " = 1'd1;")
			connectIOSignals.append("." + str(io["ID"]) + "(" + str(io["ID"]) + ")")
			if io["type"] == "in":
				inputsOutputsTextTest.append("reg [" + str(io["size"] - 1) + ":0] " + str(io["ID"]) + ";")
				if not io["clock_gen"]:
					inputs_read.append(str(io["ID"]))
					inputs_values[str(io["ID"])] = io["value"]
				if io["initial_value"] != None:
					initial_values.append(str(io["ID"]) + " = " + str(io["size"]) + "'d" + str(io["initial_value"]) + ";")
				else:
					initial_values.append(str(io["ID"]) + " = " + str(io["size"]) + "'d" + str(io["value"]) + ";")
			else:
				inputsOutputsTextTest.append("wire [" + str(io["size"] - 1) + ":0] " + str(io["ID"]) + ";")

		templateTestBench = QApplication.instance().get_src("templates", "testbench.v")
		if self._forSimulation:
			templateTestBench = QApplication.instance().get_src("templates", "testbench_sim.v")
		text_test = ""
		with open(templateTestBench, 'r+') as f:
			text_test = f.read()
			f.close()
		initial_values.append("#5;")

		text_test = text_test.replace("{{input_output_wires}}", "\n\t".join(inputsOutputsTextTest))
		text_test = text_test.replace("{{connect_io_signals}}", ",\n\t\t".join(connectIOSignals))
		text_test = text_test.replace("{{input_values}}", "\n\t\t".join(initial_values))
		if clockGen != "":
			#text_test = text_test.replace("{{clock_signal}}", "always begin\n\t\t\t#2 " + clockGen + " = ~" + clockGen + ";\n\t$stop;\n\t$display(\"HELLO : %d\", clock01181_pOut);\n\tend\n")
			text_test = text_test.replace("{{clock_signal}}", "#1;\n\t\t" + clockGen + " = ~" + clockGen + ";")
		else:
			text_test = text_test.replace("{{clock_signal}}", "")

		if self._forSimulation:
			text_test = text_test.replace("{{format_signals_read}}", "%d" + (",%d") * (len(self.dataCompile["data_update"]) - 1))
			text_test = text_test.replace("{{signals_read_name}}", ",".join(self.dataCompile["data_update"]))
			text_test = text_test.replace("{{signals_read}}", "testbench.UUT." +  ", testbench.UUT.".join(self.dataCompile["data_update"]))
			text_read = "if (cont == 3) begin\n" +\
				"\t\t\tfile_read = $fopenr(\"io.txt\");\n" +\
				"\t\t\tri = $fscanf(file_read, \"{{format_inputs_read}}\\n\", {{inputs_read}});\n" +\
				"\t\t\t$fclose(file_read);\n" +\
			"\t\tend\n"
			if len(inputs_read) > 0:
				text_read = text_read.replace("{{format_inputs_read}}", "%d" + (",%d") * (len(inputs_read) - 1))
				text_read = text_read.replace("{{inputs_read}}", ", ".join(inputs_read))
			else:
				text_read = ""
			text_test = text_test.replace("{{text_read}}", text_read)
		else:
			next_values = []
			for a in inputs_values:
				next_values.append(str(a) + " = " + str(inputs_values[a]))
			text_test = text_test.replace("{{next_values}}", ";\n\t\t".join(next_values))

		with open(os.path.join(folderToSave, "testbench.v"), 'w+') as f:
			f.write(text_test)
			f.close()

		if not self._forSimulation:
			copyfile(QApplication.instance().get_src("templates", "make_testbench_iverilog"), os.path.join(folderToSave, "Makefile"))

		return inputs_values
