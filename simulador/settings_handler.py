#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries
from simulador.tools.utils import projectExtension

# Qt libraries
from PyQt5.QtCore import QSettings
from PyQt5.QtWidgets import QApplication

# System libraries

class SettingsHandler(QSettings):

	def __init__(self):
		super(QSettings, self).__init__('sintel', 'sintel settings')
		#self.setupUi(self)

	def getRecentFiles(self):
		files = self.value('recentFileList', [])
		if files == None:
			files = []
		return files

	def saveRecentFile(self, path, name):
		name = name.replace(projectExtension, "")
		files = self.getRecentFiles()
		new_file = {"name" : name, "path" : path}
		if not new_file in files:
			if len(files) >= 3:
				files.pop(0)
		else:
			files.pop(files.index(new_file))
		files.insert(0, new_file)
		self.setValue('recentFileList', files)

	def deleteRecentFile(self, path, name):
		name = name.replace(projectExtension, "")
		files = self.getRecentFiles()
		new_file = {"name" : name, "path" : path}
		if new_file in files:
			files.remove(new_file)
		self.setValue('recentFileList', files)

	def setDarkTheme(self, isDark):
		if isDark:
			self.setValue('dark_theme', True)
		else:
			self.setValue('dark_theme', False)

		QApplication.instance().updateTheme()

	def getDarkTheme(self):
		dark_theme = self.value("dark_theme")
		if dark_theme == None:
			self.setDarkTheme(True)
			return True
		dark = bool(dark_theme)
		dark = False
		return dark

	def getDefaultTime(self):
		default_time = self.value("default_time")
		if default_time == None:
			self.setDefaultTime(0.2)
			return 0.2
		return float(default_time)

	def setDefaultTime(self, time):
		self.setValue('default_time', time)

	def saveLang(self, lang = "eng"):
		self.setValue("language", lang)

	def getLanguage(self):
		lang = self.value("language")
		if lang == None:
			lang = "eng"
		return lang
