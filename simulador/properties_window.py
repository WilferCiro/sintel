#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries
from simulador.tools.utils import filterText, get_pycompile_folder
from simulador.tools.spinbox import BigIntSpinbox

# Qt libraries
from PyQt5.QtWidgets import QWidget, QDialog, QFormLayout, QComboBox, QSpinBox, QLabel, QLineEdit, QApplication, QCheckBox
from PyQt5.QtCore import QSize, Qt
from PyQt5.QtGui import QPixmap
from PyQt5 import uic

# System libraries
import os

MAX_VAL = 2147483647

class PropertiesWindow(QWidget):

	def __init__(self):
		super(QWidget, self).__init__()
		uic.loadUi(QApplication.instance().get_src("ui", "properties", "menu_properties.ui"), self)

		# Buttons for accept or cancel
		self.saveData.clicked.connect(self._accept)
		self.editComponent.clicked.connect(self._editComponentFcn)
		self.componentID.textChanged.connect(self._changedID)

		# Control variables
		self._fields = []
		self._fieldsReferenced = []
		self._fieldsNoReferenced = []
		self._element = None

		# Hide initial
		self.updateClock.hide()
		self.titleClock.hide()
		self.saveData.hide()
		self.editComponent.hide()

		self.propertiesData.hide()

		# Font
		self.new_font = self.font()
		self.new_font.setFamily("Cantarell")
		self.new_font.setPointSize(9)
		self.setFont(self.new_font)

	def _changedID(self):
		text = self.componentID.text()
		if filterText(text) != text:
			self.componentID.setText(filterText(text))

	def showPreview(self, data):
		self.previewData.show()
		self.propertiesData.hide()
		self.saveData.hide()
		self.namePreview.setText(data["name"])
		self.versionPreview.setText(data["version"])
		self.datePreview.setText(data["date"])
		self.createdPreview.setText(data["created_by"])
		self.descriptionPreview.setText(data["description"])
		pix = QPixmap(data["icon"])
		self.iconPreview.setPixmap(pix.scaled(QSize(80, 80), Qt.KeepAspectRatio))

	def leavePreview(self):
		pass

	def showProperties(self, obj):
		"""
			Excec properties window
			@param obj as workspace_object
		"""
		# Show widgets
		self.isInputOutput.show()
		self.labelType.show()
		self.updateClock.hide()
		self.titleClock.hide()
		self.saveData.show()

		# data widgets
		self.propertiesData.show()
		self.previewData.hide()

		self._element = obj
		if self._element.isInput() or self._element.isOutput():
			self.editComponent.setVisible(False)
		else:
			self.editComponent.setVisible(True)
		type_component = obj.getType()
		id_component = obj.getID()
		version_component = obj.getVersion()
		autor_component = obj.getAutor()
		description_component = obj.getDescription()
		icon = obj.getPixMap()
		if obj.isUserComponent():
			pix = QPixmap(os.path.join(get_pycompile_folder(), "components_user", "svg", "workspace", icon))
		else:
			pix = QPixmap(os.path.join(get_pycompile_folder(), "components", "svg", "workspace", icon))
		self.icon.setPixmap(pix.scaled(QSize(40, 40), Qt.KeepAspectRatio))
		self._isStateMachine = self._element.isStateMachine()
		if self._isStateMachine:
			self.ComponentName.setText(self.tr("State machine") + ": " + type_component)
		else:
			self.ComponentName.setText(type_component)
		self.componentID.setText(id_component)
		self.versionLabel.setText(self.tr("version ") + version_component)
		self.creatorLabel.setText(autor_component)
		self.descriptionLabel.setText(description_component)
		if self._element.isInput():
			self.isInputOutput.setText(self.tr("System input"))
		elif self._element.isOutput():
			self.isInputOutput.setText(self.tr("System output"))
		else:
			if self._isStateMachine:
				self.isInputOutput.setText(self.tr("State machine"))
			else:
				self.isInputOutput.hide()
				self.labelType.hide()
		properties = obj.getProperties()
		clock = obj.getClock()
		self._deleteItems(self.componentsList)
		self._deleteItems(self.connectorsForm)
		self._deleteItems(self.defaultValuesLayout)

		if clock != None:
			self.updateClock.setVisible(True)
			self.titleClock.setVisible(True)
			self.updateClock.setCurrentIndex(int(clock["update"]))

		self._fields = []
		self._fieldsReferenced = []
		self._fieldsNoReferenced = []

		layout = self.componentsList
		if len(properties) > 0:
			self.titleProperties.show()
			self.lineProperties.show()
			for prop in properties:
				field = dict()
				widget = self.returnTypeWidget(prop, obj)
				field["field"] = widget
				field["type"] = prop["type"]
				field["name"] = prop["name"]

				# Label Properties
				label_prop = QLabel(str(prop["name"]))
				label_prop.setWordWrap(True)
				label_prop.setFixedWidth(70)
				label_prop.setFont(self.new_font)
				# Widget properties
				widget.setFont(self.new_font)
				widget.setFixedHeight(20)

				layout.addRow(label_prop, widget)
				self._fields.append(field)
		else:
			self.titleProperties.hide()
			self.lineProperties.hide()

		layout = self.connectorsForm

		referenceBits = obj.getReferenceBits()
		for ref in referenceBits:
			widget = QComboBox()
			widget.currentIndexChanged.connect(self._changeSizes)
			index = 0
			for op in ref["bits"]:
				widget.addItem(op)
				if len(ref["connectors"]) > 0:
					if int(op) == int(ref["connectors"][0].getSize()):
						widget.setCurrentIndex(index)
				index += 1
			title = str(ref["id"]) + " ("
			new = [str(conn.getID()) for conn in ref["connectors"]]
			title += ", ".join(new) + ")"

			# Label properties
			label_prop = QLabel(str(title))
			label_prop.setWordWrap(True)
			label_prop.setFixedWidth(70)
			label_prop.setFont(self.new_font)
			# Widget properties
			widget.setFont(self.new_font)
			widget.setFixedHeight(20)

			layout.addRow(label_prop, widget)
			field = dict()
			field["field"] = widget
			field["object"] = ref

			default = []
			for conn in ref["connectors"]:
				if not conn.isResetIn() and not conn.isClockIn() and not conn.isClockOut() and conn.getType() == "in":
					defaultWidget = BigIntSpinbox()
					defaultWidget.setMinimum(-1)
					defaultWidget.setMaximum(255)
					value = conn.getDefaultValue()
					if value == None:
						value = -1
					defaultWidget.setValue(int(value))
					defField = {
						"widget" : defaultWidget,
						"connector" : conn
					}

					# Label properties
					label_prop = QLabel(str(conn.getID()))
					label_prop.setWordWrap(True)
					label_prop.setFixedWidth(70)
					label_prop.setFont(self.new_font)
					# Widget properties
					defaultWidget.setFont(self.new_font)
					defaultWidget.setFixedHeight(20)

					self.defaultValuesLayout.addRow(label_prop, defaultWidget)
					default.append(defField)

			if len(default) > 0:
				self.titleDefaultValues.show()
				self.lineDefaultValues.show()
			else:
				self.titleDefaultValues.hide()
				self.lineDefaultValues.hide()
			field["default"] = default

			self._fieldsReferenced.append(field)
		#self.exec_()

	def _changeSizes(self):
		for field in self._fieldsReferenced:
			size = int(field["field"].currentText())
			for conn in field["default"]:
				maxVal = 2 ** size - 1
				if maxVal > MAX_VAL:
					maxVal = MAX_VAL
				conn["widget"].setMaximum(maxVal)

	def _editComponentFcn(self):
		file = self._element.getJsonFile()
		if not self._isStateMachine:
			QApplication.instance().projectsHandler.editComponent(file)
		else:
			QApplication.instance().projectsHandler.editStateMachine(file)

	def returnTypeWidget(self, prop, obj):
		"""
			Return the necesary widget
			@param prop as dict
		"""
		default = prop["value"]
		type = prop["type"]
		if type == "select":
			options = prop["options"]
			widget = QComboBox()
			index = 0
			for op in options:
				widget.addItem(str(op["value"]))
				if op["value"] == default:
					widget.setCurrentIndex(index)
				index = index + 1
			return widget
		elif type == "numeric":
			widget = BigIntSpinbox()
			widget.setMinimum(int(prop["min"]))
			if str(prop["max"]).isnumeric():
				widget.setMaximum(int(prop["max"]))
			else:
				ref_in = obj.getReferenceBitsByID(prop["max"])
				if ref_in != None:
					ref = ref_in["connectors"]
					if len(ref) > 0:
						referenced = int(ref[0].getSize())
						maxVal = 2 ** referenced - 1
						if maxVal > MAX_VAL:
							maxVal = MAX_VAL
						widget.setMaximum(maxVal)
					else:
						widget.setMaximum(int(prop["min"]))
				else:
					widget.setMaximum(int(prop["min"]))
			widget.setValue(float(default))
			return widget
		elif type == "bit":
			widget = QCheckBox()
			widget.setText(str(prop["name"]))
			widget.setChecked(True if default == "1" else False)
			return widget
		else:
			return QLabel("N/A")

	def _deleteItems(self, layout):
		if layout is not None:
			while layout.count():
				item = layout.takeAt(0)
				widget = item.widget()
				if widget is not None:
					widget.setParent(None)
					widget.deleteLater()
				else:
					self._deleteItems(item.layout())

	def _accept(self):
		properties = []
		for field in self._fields:
			value = {"name" : field["name"]}
			if field["type"] == "select":
				value["value"] = field["field"].currentText()
			elif field["type"] == "numeric":
				value["value"] = field["field"].value()
			elif field["type"] == "text":
				value["value"] = field["field"].text()
			else:
				value["value"] = -1
			properties.append(value)

		pinsBits = []
		for field in self._fieldsReferenced:
			for conn in field["object"]["connectors"]:
				size = int(field["field"].currentText())
				if conn.canSetSize(size):
					conn.setCanSize(size)
			for conn in field["default"]:
				conn["connector"].setDefaultValue(int(conn["widget"].value()))

		self._element.setInlineProperties()

		newID = filterText(self.componentID.text())
		self._element.setName(newID)
		clock = {
			"update" : str(self.updateClock.currentIndex())
		}

		self._element.setProperties(properties)
		self._element.setClock(clock)
