#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries
from simulador.simulation_view import SimulationWidget
from simulador.tools.utils import projectExtension
from simulador.state_machine_widget import SMWidget
from simulador.src_add_component.add_component_widget import ACWidget
from simulador.tools.spinbox import BigIntSpinbox
from simulador.about_view import AboutWidget
from simulador.settings_widget import SettingsWidget

# Qt libraries
from PyQt5.QtWidgets import QWidget, QTabWidget, QVBoxLayout, QPushButton, QApplication, QMessageBox
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap, QIcon
from PyQt5 import uic

# System libraries
import os

class ProjectsHandler(QWidget):

	def __init__(self):
		super(QWidget, self).__init__()
		self.layout = QVBoxLayout(self)

		# Initialize tab screen
		self.tabs = QTabWidget()
		self.tabs.setMovable(True)
		self.tabs.setTabsClosable(True)
		self.tabs.resize(300, 200)
		self.tabs.setDocumentMode(True)

		# Add tabs
		self.tabs.addTab(QApplication.instance().homeWidget, "Home")
		#self.tabs.addTab(SMWidget(), "State machine")
		self._smWidget = None
		self._acWidget = None
		self._aboutWidget = None
		self._settingsWidget = None
		self.tabAC = None

		# Add tabs to widget
		self.layout.addWidget(self.tabs)
		self.setLayout(self.layout)

		self.topButtons = TopButtons()
		self.tabs.setCornerWidget(self.topButtons, Qt.TopRightCorner);
		self.topButtons.hide()

		self.tabs.currentChanged.connect(self._selectTab)
		self.tabs.tabCloseRequested.connect(self._closeTab)

		f = self.font()
		f.setFamily("Cantarell")
		f.setPointSize(9)
		self.tabs.setFont(f)

	def openProject(self, path, name):
		complete_path = os.path.join(path, name + projectExtension)
		if os.path.exists(complete_path):
			QApplication.instance().settingsHandler.saveRecentFile(path, name)
			QApplication.instance().homeWidget.updateRecentView()
			self.simulation_view = SimulationWidget(path, name, self)
			index = self.tabs.addTab(self.simulation_view, self.simulation_view.getProjectName())
			self.tabs.setCurrentIndex(index)
		else:
			QApplication.instance().settingsHandler.deleteRecentFile(path, name)
			QApplication.instance().homeWidget.updateRecentView()
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Information)
			msg.setText("The project " + str(complete_path) + " doesn't exists")
			msg.setWindowTitle("Error")
			msg.setStandardButtons(QMessageBox.Ok)
			msg.exec_()

	def addStateMachine(self):
		if self._smWidget == None:
			self._smWidget = SMWidget(self)
			self.tabs.addTab(self._smWidget, "Add state machine")
		index = self.tabs.indexOf(self._smWidget)
		self.tabs.setCurrentIndex(index)
		self._smWidget.addProject()

	def editStateMachine(self, path):
		if self._smWidget == None:
			self._smWidget = SMWidget(self)
			self.tabs.addTab(self._smWidget, "Edit state machine")
		index = self.tabs.indexOf(self._smWidget)
		self.tabs.setCurrentIndex(index)
		self._smWidget.loadProject(path)

	def editComponent(self, path):
		if self._acWidget == None:
			self._acWidget = ACWidget(self)
			self.tabAC = self.tabs.addTab(self._acWidget, "Edit component")
		index = self.tabs.indexOf(self._acWidget)
		self.tabs.setCurrentIndex(index)
		self._acWidget.emptyData()
		self._acWidget.loadWidget(path)

	def addComponent(self):
		if self._acWidget == None:
			self._acWidget = ACWidget(self)
			self.tabAC = self.tabs.addTab(self._acWidget, "Add component")
		index = self.tabs.indexOf(self._acWidget)
		self.tabs.setCurrentIndex(index)
		self._acWidget.emptyData()

	def _selectTab(self, index):
		current = self.tabs.currentWidget()
		if type(current) == SimulationWidget:
			self.topButtons.show()
			self.topButtons.setProjectName(self.tabs.widget(index).getProjectName())
			self.topButtons.showAll()
		elif type(current) == SMWidget:
			self.topButtons.setProjectName("Add State machine")
			self.topButtons.hideMenu()
			self.topButtons.hideLoad()
			self.topButtons.show()
		elif type(current) == ACWidget:
			self.topButtons.setProjectName("Add Component")
			self.topButtons.hideMenu()
			self.topButtons.hideLoad()
			self.topButtons.show()
		else:
			self.topButtons.hide()

	def showSettings(self):
		if self._settingsWidget == None:
			self._settingsWidget = SettingsWidget()
			index = self.tabs.addTab(self._settingsWidget, "Settings")
			self.tabs.setCurrentIndex(index)
		index = self.tabs.indexOf(self._settingsWidget)
		self.tabs.setCurrentIndex(index)

	def showAbout(self):
		if self._aboutWidget == None:
			self._aboutWidget = AboutWidget()
			index = self.tabs.addTab(self._aboutWidget, "About Sintel")
			self.tabs.setCurrentIndex(index)
		index = self.tabs.indexOf(self._aboutWidget)
		self.tabs.setCurrentIndex(index)

	def getWorkspace(self):
		return self.tabs.currentWidget().workspace

	def getView(self):
		return self.tabs.currentWidget()

	def setSavedProject(self, project, state):
		index = self.tabs.indexOf(project)
		if state:
			self.tabs.setTabText(index, project.getProjectName())
		else:
			self.tabs.setTabText(index, "*" + project.getProjectName())

	def _closeTab(self, index):
		current = self.tabs.widget(index)
		delete = True
		if current != QApplication.instance().homeWidget:
			if type(current) == SimulationWidget or current == self._smWidget or current == self._acWidget:
				if not current.isSaved():
					msg = QMessageBox()
					msg.setIcon(QMessageBox.Information)

					msg.setText("This project is not saved")
					msg.setInformativeText("save this project?")
					msg.setWindowTitle("Save project")
					msg.setStandardButtons(QMessageBox.Ok | QMessageBox.No | QMessageBox.Cancel)

					retval = msg.exec_()
					if retval == QMessageBox.Ok:
						current.saveProject()
					elif retval == QMessageBox.Cancel:
						delete = False
			if delete:
				self.tabs.removeTab(index)
				if current == self._aboutWidget:
					self._aboutWidget = None
				elif current == self._settingsWidget:
					self._settingsWidget = None
				elif current == self._acWidget:
					self._acWidget = None
				elif current == self._smWidget:
					self._smWidget = None


class TopButtons(QWidget):

	def __init__(self):
		super(QWidget, self).__init__()
		uic.loadUi(QApplication.instance().get_src("ui", "buttons_top.ui"), self)
		self.saveProjectBtn.clicked.connect(self.saveProject)
		self.simulationMenuBtn.clicked.connect(self.showMenu)

		# Icons
		self.loadProjectBtn.setIcon(QIcon(QPixmap(QApplication.instance().get_src("images", "icons", "open.png"))))
		self.saveProjectBtn.setIcon(QIcon(QPixmap(QApplication.instance().get_src("images", "icons", "save.png"))))
		self.simulationMenuBtn.setIcon(QIcon(QPixmap(QApplication.instance().get_src("images", "icons", "menu.png"))))

	def hideMenu(self):
		self.simulationMenuBtn.hide()

	def hideLoad(self):
		self.loadProjectBtn.hide()

	def showAll(self):
		self.simulationMenuBtn.show()

	def saveProject(self):
		QApplication.instance().projectsHandler.getView().saveProject()

	def setProjectName(self, name):
		self.projectNameLabel.setText(name)

	def showMenu(self):
		point = self.simulationMenuBtn.mapToGlobal(self.simulationMenuBtn.rect().bottomLeft())
		QApplication.instance().projectsHandler.getView().showPrincipalMenu(point)
