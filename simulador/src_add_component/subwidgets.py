#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries
from simulador.tools.utils import GRIDSPACE, filterText, filterTextNumber
from simulador.tools.xml_reader import getObjectFiles, getObjectConnectors, getObjectProperties, getsignalReference, returnIconData
from simulador.tools.sintel_svg import SintelSVG

# Qt libraries
from PyQt5.QtWidgets import (QApplication, QWidget, QFileDialog, QGraphicsScene, QGraphicsItem, QGraphicsView, QTableWidget, QTableWidgetItem, QComboBox, QGraphicsRectItem, QGraphicsObject, QGraphicsEllipseItem, QGraphicsTextItem)
from PyQt5.QtCore import Qt, QSize, QRectF, QSizeF, QPoint, QPointF
from PyQt5.QtGui import QPainter, QBrush, QColor, QPen, QPixmap, QFont
from PyQt5 import uic
from PyQt5.QtSvg import QGraphicsSvgItem

# System libraries

LABEL_SIZE_OF = "Size of: "

class EditableIcon(QGraphicsItem):
	def __init__(self, parent):
		super(QGraphicsItem, self).__init__()

		self._parent = parent
		self._svgIcon = QGraphicsSvgItem(self)
		self.setPos(25, 25)

	def emptyAll(self):
		self._svgIcon.setParent(None)
		self._svgIcon = QGraphicsSvgItem(self)

	def changeComponentData(self):
		self._parent.updatePreviews()

	def setPath(self, path, size = None):
		self._svgIcon = SintelSVG(path, self)
		if size == None:
			self._svgIcon.setSize(QSizeF(75, 75))
		else:
			self._svgIcon.setSize(size)

	def setWidth(self, value):
		if type(self._svgIcon) != QGraphicsSvgItem:
			self._svgIcon.setWidth(value)

	def setHeigth(self, value):
		if type(self._svgIcon) != QGraphicsSvgItem:
			self._svgIcon.setHeigth(value)

	def setSize(self, value):
		if type(self._svgIcon) != QGraphicsSvgItem:
			self._svgIcon.setSize(value)

	def boundingRect(self):
		"""
			override boundingRect method, is mandatory, return the item size
		"""
		return QRectF(0, 0, 75, 75)

	def paint(self, *args):
		"""
			override paint method, is mandatory
			@param args
		"""
		None
"""
class SintelSVG(QGraphicsSvgItem):
	def __init__(self, path, parent):
		super(QGraphicsSvgItem, self).__init__(path, parent)
		self._size = QSizeF(50, 50)

	def size(self):
		return self._size

	def setWidth(self, width):
		self.setSize(QSizeF(width, self._size.height()))

	def setHeigth(self, height):
		self.setSize(QSizeF(self._size.width(), height))

	def setSize(self, size):
		self.prepareGeometryChange();
		self._size = size
		self.update(self.boundingRect());

	def boundingRect(self):
		return QRectF(0, 0, self._size.width(), self._size.height())

	def paint(self, painter, option, widget, *args):
		self.renderer().render(painter, self.boundingRect());
"""
class IOWidget(QGraphicsObject):

	def __init__(self, parent, data):
		self._parent = parent
		self._position = data["position"]
		super(QGraphicsObject, self).__init__(self._parent)
		if data["type"] == "in":
			self._default_color = QColor(65, 205, 82)
		elif data["type"] == "out":
			self._default_color = QColor(255, 95, 105)
		else:
			self._default_color = QColor(0, 148, 166)

		self._pen = QPen(self._default_color)
		self._pen.setWidth(2)
		self._rect = QGraphicsEllipseItem(QRectF(0, 0, 5, 5), self)
		self._rect.setPen(self._pen)
		background = QBrush(self._default_color)
		self._rect.setBrush(background)

		self.valueText = QGraphicsTextItem(data["id"], self)
		self.setPos(self._position - QPoint(2.5, 2.5))

		self._location = self._position
		self._dragStart = self._position
		self.setZValue(2)
		self.setCursor(Qt.SizeAllCursor)

	def getX(self):
		return int(self._location.x()/25)

	def getY(self):
		return int(self._location.y()/25)

	def mouseReleaseEvent(self, event):
		"""
			Emits When the user releases the mouse
			@param event as mouseevent
		"""
		self._location.setX((int(self._location.x() / 25) * 25))
		self._location.setY((int(self._location.y() / 25) * 25))
		self.setPos(self._location - QPoint(2.5, 2.5))

		self._parent.changeComponentData()

	def mousePressEvent(self, event):
		"""
			Emits When the user press the mouse
			@param event as mouseevent
		"""
		self._dragStart = event.pos()

	def mouseMoveEvent(self, event):
		"""
			Emits When the user moves the mouse prev to press
			@param event as mouseevent
		"""
		newPos = event.pos()
		self._location += (newPos - self._dragStart)
		if self._location.x() < 0:
			self._location.setX(0)
		elif self._location.x() > 475:
			self._location.setX(475)
		else:
			self._location.setX((int(self._location.x() / 25) * 25))

		if self._location.y() < 0:
			self._location.setY(0)
		elif self._location.y() > 475:
			self._location.setY(475)
		else:
			self._location.setY((int(self._location.y() / 25) * 25))
		self.setPos(self._location)

	def setID(self, id):
		self.valueText.setPlainText(id)

	def boundingRect(self):
		"""
			override boundingRect method, is mandatory, return the item size
		"""
		return QRectF(0, 0, 10, 10)

	def paint(self, *args):
		None


class SVGWorkspace(QGraphicsScene):
	"""
		Handle the workspace
		@param parent as widget or None
	"""
	def __init__(self, parent = None):
		super(QGraphicsScene, self).__init__(parent)

		self._parent = parent

		self.setSceneRect(QRectF(0, 0, 500, 500))

		# background
		brush = QBrush()
		brush.setColor(QColor('#999'))
		brush.setTexture(QPixmap(QApplication.instance().get_src("images", "grid50.png")))
		self.setBackgroundBrush(brush)

	def updatePreviews(self):
		self._parent.updatePreviews()

class PropertyWidget(QWidget):

	def __init__(self, parent, data = None, inline = None):
		super(QWidget, self).__init__()
		self._parent = parent
		uic.loadUi(QApplication.instance().get_src("ui", "add_component", "property.ui"), self)

		# Initial data
		self._type = ""
		self._isBit = False
		self._defaultValue = 0
		self._name = ""
		self._options = []

		self.typeEdit.currentIndexChanged.connect(self.changeType)
		self.maxValueSelect.currentIndexChanged.connect(self.selectMaxValue)
		self.optionsValue.textChanged.connect(self._changeOptions)
		self.nameEdit.textChanged.connect(self.updateData)
		self.isBit.stateChanged.connect(self.updateData)
		self.showInline.stateChanged.connect(self.updateData)
		self.minValue.valueChanged.connect(self.updateData)
		self.maxNumber.valueChanged.connect(self.updateData)
		self.defaultValueEdit.valueChanged.connect(self.updateData)
		self.maxValueSelect.currentIndexChanged.connect(self.updateData)
		self.deleteProperty.clicked.connect(self.delete)

		if inline == True:
			self.showInline.setChecked(True)
		else:
			self.showInline.setChecked(False)

		self._maxValueSelected = None
		if data != None:
			self.nameEdit.setText(data["name"])
			self.defaultValueEdit.setValue(int(data["value"]))
			if data["type"] == "select":
				self.typeEdit.setCurrentIndex(0)
				all_Options = []
				for option in data["options"]:
					all_Options.append(str(option["value"]))
				self.optionsValue.setText(",".join(all_Options))
				if data["base"] == "bit":
					self.isBit.setChecked(True)
			elif data["type"] == "numeric":
				self.typeEdit.setCurrentIndex(1)
				self.minValue.setValue(int(data["min"]))
				if str(data["max"]).isnumeric():
					self.maxNumber.setValue(int(data["max"]))
				else:
					self._maxValueSelected = data["max"]
			elif data["type"] == "bit":
				self.typeEdit.setCurrentIndex(2)

		self.changeType()

	def _changeOptions(self):
		text = filterTextNumber(self.optionsValue.text())
		self.optionsValue.setText(text)
		if text.replace(",", "").replace(" ", "").isdigit():
			self._options = [int(a) for a in text.replace(" ", "").split(",") if a != "" and a != " "]
			if len(self._options) > 0:
				total = [0 if a == 1 or a == 0 else 1 for a in self._options]
				if sum(total) >= 1:
					self.isBit.setEnabled(False)
				else:
					self.isBit.setEnabled(True)
					self.isBit.setChecked(False)
				self.defaultValueEdit.setValue(self._options[0])
			self.updateData()
		else:
			self.optionsValue.setText(text[0:-1])

	def changeType(self):
		text = self.typeEdit.currentText()
		if text == "Selectable":
			self._type = "select"
			self.isBit.show()
			self.minValueLabel.hide()
			self.maxNumberLabel.hide()
			self.maxValueLabel.hide()
			self.maxValueSelect.hide()
			self.minValue.hide()
			self.maxNumber.hide()
			self.optionsValue.show()
			self.optionsLabel.show()
			self.defaultValueEdit.setEnabled(False)
			self.defaultValueEdit.setMinimum(0)
			self.defaultValueEdit.setMaximum(1000000000)
		elif text == "Number":
			self._type = "numeric"
			self.isBit.hide()
			self.minValueLabel.show()
			self.maxNumberLabel.show()
			self.maxValueLabel.show()
			self.maxValueSelect.show()
			self.minValue.show()
			self.maxNumber.show()
			self.optionsValue.hide()
			self.optionsLabel.hide()
			self.defaultValueEdit.setEnabled(True)
			self.isBit.setChecked(False)
			self.defaultValueEdit.setMinimum(self.minValue.value())
			self.defaultValueEdit.setMaximum(1000000000)
		elif text == "Bit":
			self._type = "bit"
			self.isBit.hide()
			self.minValueLabel.hide()
			self.maxNumberLabel.hide()
			self.maxValueLabel.hide()
			self.maxValueSelect.hide()
			self.minValue.hide()
			self.maxNumber.hide()
			self.optionsValue.hide()
			self.optionsLabel.hide()
			self.defaultValueEdit.setEnabled(True)
			self.defaultValueEdit.setValue(0)
			self.isBit.setChecked(False)
			self.defaultValueEdit.setMinimum(0)
			self.defaultValueEdit.setMaximum(1)

	def selectMaxValue(self):
		currentValue = self.maxValueSelect.currentText()
		if currentValue == "Number" and self.typeEdit.currentText() == "Number":
			self.maxNumber.show()
			self.maxNumberLabel.show()
		else:
			self.maxNumber.hide()
			self.maxNumberLabel.hide()

	def setVariablesIO(self, variablesIO):
		currentValue = self.maxValueSelect.currentText()
		self.maxValueSelect.clear()
		self.maxValueSelect.addItem("Number")
		index = 1
		for variable in variablesIO:
			self.maxValueSelect.addItem(LABEL_SIZE_OF + variable["id"])
			if variable["id"] == currentValue or variable["id"] == self._maxValueSelected:
				self.maxValueSelect.setCurrentIndex(index)
			index += 1
		self._maxValueSelected = None

	def updateData(self):
		self._name = filterText(self.nameEdit.text())
		self.nameEdit.setText(self._name)

		self._defaultValue = self.defaultValueEdit.value()
		self._minValue = self.minValue.value()
		typeMaxValue = self.maxValueSelect.currentText()
		if typeMaxValue == "Number":
			self._maxValue = self.maxNumber.value()
		else:
			self._maxValue = typeMaxValue.replace(LABEL_SIZE_OF, "")
		self._isBit = self.isBit.isChecked()
		self._isInline = self.showInline.isChecked()

		self._parent.changeComponentData()

	def getJSONFormat(self):
		data = None
		if self._name != "":
			if self._type == "select":
				if len(self._options) > 0:
					options = []
					for op in self._options:
						textOption = {
							"text" : op,
							"value" : op
						}
						options.append(textOption)
					data = {
						"type" : "select",
						"value" : self._options[0],
						"name" : self._name,
						"options" : options
					}
					if self._isBit:
						data["base"] = "bit"
			elif self._type == "numeric":
				data = {
					"type" : "numeric",
					"value" : self._defaultValue,
					"min" : self._minValue,
					"max" : self._maxValue,
					"name" : self._name
				}
			elif self._type == "bit":
				data = {
					"type" : "bit",
					"value" : self._defaultValue,
					"name" : self._name
				}
		return data, self._isInline

	def delete(self):
		self._parent.deleteProperty(self)



class IOForm(QWidget):

	def __init__(self, parent, data = None, point = None, variables = None):
		super(QWidget, self).__init__()
		self._parent = parent
		uic.loadUi(QApplication.instance().get_src("ui", "add_component", "IO.ui"), self)
		self._initial = True
		self.setVariablesIO(variables)

		# Initial data
		self._ID = data["ID"]
		self.IDText.setText(self._ID)
		self.IDText.textChanged.connect(self._updateGUI)
		self.variableCombo.currentIndexChanged.connect(self._updateGUI)
		self.typeCombo.currentIndexChanged.connect(self._updateGUI)
		self.isClockOutput.currentIndexChanged.connect(self._updateGUI)
		self.setRegOutput.stateChanged.connect(self._updateGUI)
		self.deleteBtn.clicked.connect(self.delete)
		self._type = data["type"]

		self.setVariable(data["variable"])
		if self._type == "in":
			self.isClockOutput.hide()
			self.clockLabel.hide()
			typeInput = data["typeInput"]
			index = self.typeCombo.findText(typeInput)
			if index != -1:
				self.typeCombo.setCurrentIndex(index)
			self.setRegOutput.hide()
		else:
			setReg = data["setRegOutput"]
			self.typeCombo.hide()
			self.typeLabel.hide()
			clock = data["clock"]
			index = self.isClockOutput.findText(clock)
			if index != -1:
				self.isClockOutput.setCurrentIndex(index)
			self.setRegOutput.show()
			if setReg:
				self.setRegOutput.setChecked(True)
			else:
				self.setRegOutput.setChecked(False)

		self._icon = point

		self._initial = False

		self._updateGUI()

	def disableIcon(self):
		self._icon.setParent(None)

	def delete(self):
		self._icon.setParent(None)
		if self._type == "in":
			self._parent.deleteInput(self)
		else:
			self._parent.deleteOutput(self)

	def setVariable(self, value):
		index = 0
		variables = [self.variableCombo.itemText(i) for i in range(self.variableCombo.count())]
		for var in variables:
			if var == value:
				self.variableCombo.setCurrentIndex(index)
			index += 1

	def setVariablesIO(self, variables, last = None, new = None):
		current = self.variableCombo.currentText()
		self.variableCombo.clear()
		index = 0
		for var in variables:
			self.variableCombo.addItem(var["id"])
			if var["id"] == current or (var["id"] == new and current == last):
				self.variableCombo.setCurrentIndex(index)
			index += 1

	def _updateGUI(self):
		if not self._initial:
			self._parent.updateClock()
			self._parent.updatePreviews()

	def returnDataJson(self):
		self._ID = filterText(self.IDText.text())
		self.IDText.setText(self._parent.isValidIOID(self._ID, self))
		self._variableID = self.variableCombo.currentText()
		self._typeInput = self.typeCombo.currentText()
		self._isClockOutput = self.isClockOutput.currentText()
		if self._type == "in":
			return {
				"id" : self._ID,
				"variable" : self._variableID,
				"type" : self._typeInput,
				"icon" : self._icon
			}
		else:
			return {
				"id" : self._ID,
				"variable" : self._variableID,
				"clock" : self._isClockOutput,
				"icon" : self._icon,
				"setRegOutput" : self.setRegOutput.isChecked()
			}
