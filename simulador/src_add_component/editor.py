#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries

# Qt libraries
from PyQt5.QtCore import QFile, QRegExp, Qt
from PyQt5.QtGui import QFont, QSyntaxHighlighter, QTextCharFormat, QColor
from PyQt5.QtWidgets import QTextEdit

# System libraries

class Highlighter(QSyntaxHighlighter):
	def __init__(self, parent=None):
		super(Highlighter, self).__init__(parent)
		self.getCharFormat()

	def getCharFormat(self):
		pass

	def highlightBlock(self, text):
		for pattern, format in self.highlightingRules:
			expression = QRegExp(pattern)
			index = expression.indexIn(text)
			while index >= 0:
				length = expression.matchedLength()
				self.setFormat(index, length, format)
				index = expression.indexIn(text, index + length)

		self.setCurrentBlockState(0)

		startIndex = 0
		if self.previousBlockState() != 1:
			startIndex = self.commentStartExpression.indexIn(text)

		while startIndex >= 0:
			endIndex = self.commentEndExpression.indexIn(text, startIndex)

			if endIndex == -1:
				self.setCurrentBlockState(1)
				commentLength = len(text) - startIndex
			else:
				commentLength = endIndex - startIndex + self.commentEndExpression.matchedLength()

			self.setFormat(startIndex, commentLength, self.multiLineCommentFormat)
			startIndex = self.commentStartExpression.indexIn(text, startIndex + commentLength)


class HighlighterPython(Highlighter):
	def __init__(self, parent=None):
		super(HighlighterPython, self).__init__(parent)

	def getCharFormat(self):
		keywordFormat = QTextCharFormat()
		keywordFormat.setForeground(QColor("#C778D8"))
		keywordFormat.setFontWeight(QFont.Bold)

		keywordPatterns = ["\\bfrom\\b", "\\bwrite\\b", "\\bread\\b",
			"\\bint\\b", "\\bbin2dec\\b", "\\bpass\\b", "\\bself\\b",
			"\\bfloat\\b", "\\bchar\\b", "\\bdef\\b", "\\bimport\\b", "\\bclass\\b"]

		self.highlightingRules = [(QRegExp(pattern), keywordFormat) for pattern in keywordPatterns]

		classFormat = QTextCharFormat()
		classFormat.setFontWeight(QFont.Bold)
		classFormat.setForeground(Qt.darkMagenta)
		self.highlightingRules.append((QRegExp("\\bQ[A-Za-z]+\\b"), classFormat))

		singleLineCommentFormat = QTextCharFormat()
		singleLineCommentFormat.setForeground(QColor("#53615E"))
		self.highlightingRules.append((QRegExp("#[^\n]*"), singleLineCommentFormat))

		numberFormat = QTextCharFormat()
		numberFormat.setForeground(QColor("#53615E"))
		self.highlightingRules.append((QRegExp("[0-9]"), numberFormat))

		self.multiLineCommentFormat = QTextCharFormat()
		self.multiLineCommentFormat.setForeground(QColor("#53615E"))

		quotationFormat = QTextCharFormat()
		quotationFormat.setForeground(QColor("#DBAC60"))
		self.highlightingRules.append((QRegExp("\".*\""), quotationFormat))

		functionFormat = QTextCharFormat()
		functionFormat.setFontItalic(True)
		functionFormat.setForeground(QColor("#DBAC60"))
		self.highlightingRules.append((QRegExp("\\b[A-Za-z0-9_]+(?=\\()"), functionFormat))

		self.commentStartExpression = QRegExp("'''")
		self.commentEndExpression = QRegExp("'''")



class HighlighterJSON(Highlighter):
	def __init__(self, parent=None):
		super(HighlighterJSON, self).__init__(parent)

	def getCharFormat(self):
		self.highlightingRules = []

		classFormat = QTextCharFormat()
		classFormat.setFontWeight(QFont.Bold)
		classFormat.setForeground(Qt.darkMagenta)
		self.highlightingRules.append((QRegExp("\\bQ[A-Za-z]+\\b"), classFormat))

		numberFormat = QTextCharFormat()
		numberFormat.setForeground(QColor("#53615E"))
		self.highlightingRules.append((QRegExp("[0-9]"), numberFormat))
		self.multiLineCommentFormat = QTextCharFormat()
		self.multiLineCommentFormat.setForeground(QColor("#53615E"))

		quotationFormat = QTextCharFormat()
		quotationFormat.setForeground(QColor("#DBAC60"))
		self.highlightingRules.append((QRegExp("\".*\""), quotationFormat))

		functionFormat = QTextCharFormat()
		functionFormat.setFontItalic(True)
		functionFormat.setForeground(QColor("#DBAC60"))
		self.highlightingRules.append((QRegExp("\\b[A-Za-z0-9_]+(?=\\()"), functionFormat))

		self.commentStartExpression = QRegExp("'''")
		self.commentEndExpression = QRegExp("'''")

#Hola Mundo
class HighlighterVerilog(Highlighter):
	def __init__(self, parent=None):
		super(HighlighterVerilog, self).__init__(parent)

	def getCharFormat(self):
		keywordFormat = QTextCharFormat()
		keywordFormat.setForeground(QColor("#C778D8"))
		keywordFormat.setFontWeight(QFont.Bold)

		keywordPatterns = ["\\balways\\b", "\\bmodule\\b", "\\binput\\b",
			"\\boutput\\b", "\\bparameter\\b", "\\bendmodule\\b", "\\bwire\\b", "\\breg\\b","\\bbegin\\b", "\\bend\\b", "\\bif\\b", "\\belse\\b"]

		self.highlightingRules = [(QRegExp(pattern), keywordFormat) for pattern in keywordPatterns]

		classFormat = QTextCharFormat()
		classFormat.setFontWeight(QFont.Bold)
		classFormat.setForeground(Qt.darkMagenta)
		self.highlightingRules.append((QRegExp("\\bQ[A-Za-z]+\\b"), classFormat))

		singleLineCommentFormat = QTextCharFormat()
		singleLineCommentFormat.setForeground(QColor("#53615E"))
		self.highlightingRules.append((QRegExp("--[^\n]*"), singleLineCommentFormat))

		numberFormat = QTextCharFormat()
		numberFormat.setForeground(QColor("#53615E"))
		self.highlightingRules.append((QRegExp("[0-9]"), numberFormat))

		self.multiLineCommentFormat = QTextCharFormat()
		self.multiLineCommentFormat.setForeground(QColor("#53615E"))

		quotationFormat = QTextCharFormat()
		quotationFormat.setForeground(QColor("#DBAC60"))
		self.highlightingRules.append((QRegExp("\".*\""), quotationFormat))

		functionFormat = QTextCharFormat()
		functionFormat.setFontItalic(True)
		functionFormat.setForeground(QColor("#DBAC60"))
		self.highlightingRules.append((QRegExp("\\b[A-Za-z0-9_]+(?=\\()"), functionFormat))

		self.commentStartExpression = QRegExp("---")
		self.commentEndExpression = QRegExp("")



class HighlighterVHDL(Highlighter):
	def __init__(self, parent=None):
		super(HighlighterVHDL, self).__init__(parent)

	def getCharFormat(self):
		keywordFormat = QTextCharFormat()
		keywordFormat.setForeground(QColor("#C778D8"))
		keywordFormat.setFontWeight(QFont.Bold)

		keywordPatterns = ["\\bentity\\b", "\\barchitecture\\b", "\\bport\\b",
			"\\bgeneric\\b", "\\bis\\b", "\\bin\\b", "\\bout\\b", "\\bstd_logic\\b", "\\bstd_logic_vector\\b", "\\b<=\\b",
			"\\bnatural\\b", "\\bbegin\\b"]

		self.highlightingRules = [(QRegExp(pattern), keywordFormat) for pattern in keywordPatterns]

		classFormat = QTextCharFormat()
		classFormat.setFontWeight(QFont.Bold)
		classFormat.setForeground(Qt.darkMagenta)
		self.highlightingRules.append((QRegExp("\\bQ[A-Za-z]+\\b"), classFormat))

		singleLineCommentFormat = QTextCharFormat()
		singleLineCommentFormat.setForeground(QColor("#53615E"))
		self.highlightingRules.append((QRegExp("--[^\n]*"), singleLineCommentFormat))

		numberFormat = QTextCharFormat()
		numberFormat.setForeground(QColor("#53615E"))
		self.highlightingRules.append((QRegExp("[0-9]"), numberFormat))

		self.multiLineCommentFormat = QTextCharFormat()
		self.multiLineCommentFormat.setForeground(QColor("#53615E"))

		quotationFormat = QTextCharFormat()
		quotationFormat.setForeground(QColor("#DBAC60"))
		self.highlightingRules.append((QRegExp("\".*\""), quotationFormat))

		functionFormat = QTextCharFormat()
		functionFormat.setFontItalic(True)
		functionFormat.setForeground(QColor("#DBAC60"))
		self.highlightingRules.append((QRegExp("\\b[A-Za-z0-9_]+(?=\\()"), functionFormat))

		self.commentStartExpression = QRegExp("---")
		self.commentEndExpression = QRegExp("")
