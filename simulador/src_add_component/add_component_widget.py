#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries
from simulador.tools.utils import GRIDSPACE, SMSize, filterText, get_pycompile_folder
from simulador.tools.xml_reader import getObjectFiles, getObjectConnectors, getObjectProperties, getsignalReference, returnIconData
from simulador.src_add_component.editor import HighlighterPython, HighlighterJSON, HighlighterVHDL, HighlighterVerilog
from simulador.src_add_component.subwidgets import *

# Qt libraries
from PyQt5.QtWidgets import (QApplication, QWidget, QFileDialog, QGraphicsScene, QGraphicsItem, QGraphicsView, QTableWidget, QTableWidgetItem, QComboBox, QGraphicsRectItem, QGraphicsObject, QGraphicsEllipseItem, QGraphicsTextItem)
from PyQt5.QtCore import Qt, QSize, QRectF, QSizeF, QPoint, QPointF
from PyQt5.QtGui import QPainter, QBrush, QColor, QPen, QPixmap, QFont
from PyQt5 import uic
from PyQt5.QtSvg import QGraphicsSvgItem

# System libraries
import math
import json
import os
from shutil import copyfile
from datetime import date

# Macros
YES = "Yes"
NO = "No"
NORMAL_S = "Normal"
CLOCK_S = "Clock"
RESET_S = "Reset"
LABEL_SIZE_OF = "Size of: "

class ACWidget(QWidget):
	"""
		Handle the state machine creator
	"""
	def __init__(self, parent = None):
		super(QWidget, self).__init__()
		uic.loadUi(QApplication.instance().get_src("ui", "add_component", "main_component.ui"), self)
		self._parent = parent

		self.componentName.textChanged.connect(self.changeComponentData)
		self.componentVersion.textChanged.connect(self.changeComponentData)
		self.componentAutor.textChanged.connect(self.changeComponentData)
		self.componentDescription.textChanged.connect(self.changeComponentData)
		self.selectSVGIcon.clicked.connect(self._setSVGIcon)

		self.isNew = True

		# Fields format
		font = QFont()
		font.setFamily('Courier')
		font.setFixedPitch(True)
		font.setPointSize(10)
		self.librariesPython.setFont(font)
		self.librariesPythonHighlighter = HighlighterPython(self.librariesPython.document())
		self.pythonUserSetup.setFont(font)
		self.pythonUserSetupHighlighter = HighlighterPython(self.pythonUserSetup.document())
		self.pythonSetup.setFont(font)
		self.pythonSetupHighlighter = HighlighterPython(self.pythonSetup.document())
		self.pythonReadUpdate.setFont(font)
		self.pythonReadUpdateHighlighter = HighlighterPython(self.pythonReadUpdate.document())
		self.pythonWriteUpdate.setFont(font)
		self.pythonWriteUpdateHighlighter = HighlighterPython(self.pythonWriteUpdate.document())
		self.pythonWriteUpdate.hide()
		self.writeUpdateLabel.hide()

		self.jsonPreview.setFont(font)
		self.jsonPreviewHighlighter = HighlighterJSON(self.jsonPreview.document())
		self.vhdlSetup.setFont(font)
		self.vhdlSetupHighlighter = HighlighterVHDL(self.vhdlSetup.document())
		self.librariesVHDL.setFont(font)
		self.librariesVHDLHighlighter = HighlighterVHDL(self.librariesVHDL.document())
		self.risingVHDL.setFont(font)
		self.risingVHDLHighlighter = HighlighterVHDL(self.risingVHDL.document())
		self.fallingVHDL.setFont(font)
		self.fallingVHDLHighlighter = HighlighterVHDL(self.fallingVHDL.document())

		self.verilogSetup.setFont(font)
		self.verilogSetupHighlighter = HighlighterVerilog(self.verilogSetup.document())
		self.verilogSetupFalling.setFont(font)
		self.verilogSetupFallingHighlighter = HighlighterVerilog(self.verilogSetupFalling.document())
		self.verilogBodyRising.setFont(font)
		self.verilogBodyRisingHighlighter = HighlighterVerilog(self.verilogBodyRising.document())
		self.verilogBodyFalling.setFont(font)
		self.verilogBodyFallingHighlighter = HighlighterVerilog(self.verilogBodyFalling.document())

		self.librariesPython.textChanged.connect(self.setEdited)
		self.pythonWriteUpdate.textChanged.connect(self.setEdited)
		self.pythonReadUpdate.textChanged.connect(self.setEdited)
		self.pythonUserSetup.textChanged.connect(self.setEdited)
		self.librariesVHDL.textChanged.connect(self.setEdited)
		self.risingVHDL.textChanged.connect(self.setEdited)
		self.fallingVHDL.textChanged.connect(self.setEdited)
		self.verilogBodyRising.textChanged.connect(self.setEdited)
		self.verilogBodyFalling.textChanged.connect(self.setEdited)

		# Loaded?
		self._loaded = False
		self._isUserComponent = True
		self._lastName = None

		# IO definitions
		self.addIOVariables.clicked.connect(self._addIOVariables)
		self.addInputBtn.clicked.connect(self._addInputFnc)
		self.addOutputBtn.clicked.connect(self._addOutputFnc)
		self._currentVariablesIOIndex = 0
		self._currentInputIndex = 0
		self._currentOutputIndex = 0
		self._variablesIO = []
		self._outputs = []
		self._inputs = []
		#self._tableInputs = QTableWidget()
		#self._tableInputs.itemChanged.connect(self._editedInput)

		#self._tableOutputs = QTableWidget()
		#self._tableOutputs.itemChanged.connect(self._editedOutput)

		self._tableVariablesIO = QTableWidget()
		self._tableVariablesIO.itemChanged.connect(self._editedVariable)

		#self.outputList.addWidget(self._tableOutputs)
		#self.inputList.addWidget(self._tableInputs)
		self.variablesIOList.addWidget(self._tableVariablesIO)

		self.svgWorkspace = SVGWorkspace(self)
		self._view = QGraphicsView(self.svgWorkspace)
		self._view.setRenderHints(QPainter.Antialiasing);
		self._view.setRubberBandSelectionMode(Qt.IntersectsItemShape)
		self.workspaceSpace.addWidget(self._view)

		self._hasClock = False
		self._hasReset = False

		# Icon
		self._iconItem = EditableIcon(self.svgWorkspace)
		self.svgWorkspace.addItem(self._iconItem)
		self.heightSlider.setEnabled(False)
		self.widthSlider.setEnabled(False)

		# Properties
		self.addProperty.clicked.connect(self._addPropertyFnc)
		self._properties = []

		# Json preview
		self._jsonPreview = {}
		self._svgPath = None

		# Python config
		self._pythonHeader = "class {{component_name}}(Component):\n" +\
			"\tdef setup(self, properties):\n" +\
			"\t\t{{io}}\n" +\
			"\t\tself.userSetup(properties)"

		# VHDL config
		self._vhdlHeader = "entity {{component_name}} is\n" +\
		"\t{{generics}}\n" +\
		"\tport(\n\t\t{{io}}\n\t);\n" +\
		"end entity;"

		# Verilog config
		self._verilogHeader = "{{header_verilog}}"

		# Initial show
		self.changeComponentData()
		self._isSaved = True

	def setSaved(self):
		self._isSaved = True
		self._parent.setSavedProject(self, True)

	def setEdited(self):
		self._isSaved = False
		self._parent.setSavedProject(self, False)

	def getProjectName(self):
		return "AC - " + self.componentName.text()

	def isSaved(self):
		return self._isSaved

	def emptyData(self):
		self.SaveCopy.setEnabled(False)
		self._lastName = None
		self.savePython.setEnabled(False)
		self.saveVHDL.setEnabled(False)
		self.saveVerilog.setEnabled(False)
		self.savePython.setChecked(True)
		self.saveVHDL.setChecked(True)
		self.saveVerilog.setChecked(True)

		self.componentName.setText("component")
		self.componentVersion.setText("0.1")
		self.componentAutor.setText("Username")
		self.componentDescription.setPlainText("Component for sintel")
		self._variablesIO = []
		for input in self._inputs:
			input["object"].disableIcon()
			input["object"].setParent(None)
		for output in self._outputs:
			output["object"].disableIcon()
			output["object"].setParent(None)
		self._inputs = []
		self._outputs = []
		for prop in self._properties:
			prop.setParent(None)
		self._properties = []
		self.pythonSetup.setPlainText(self._pythonHeader)
		self.pythonUserSetup.setPlainText("# Insert your setup code here\npass")
		self.pythonReadUpdate.setPlainText("# Insert your update code here\npass")
		self.vhdlSetup.setPlainText(self._vhdlHeader)
		self.risingVHDL.setPlainText("-- Signals declarations\nbegin\n-- Put the code here")
		self.fallingVHDL.setPlainText("-- Signals declarations\nbegin\n-- Put the code here")
		self.verilogSetup.setPlainText(self._verilogHeader)
		self.verilogBodyRising.setPlainText("// Put the component code here")

		self._tableVariablesIO.clear()

		self._svgPath = None
		self._iconItem.emptyAll()
		self.heightSlider.setEnabled(False)
		self.widthSlider.setEnabled(False)
		self.heightSlider.setValue(75)
		self.widthSlider.setValue(75)

		self._loaded = False

		self.organizeIO()
		self.changeComponentData()

		self.setSaved()

	def loadWidget(self, path):
		self.SaveCopy.setEnabled(True)
		self.savePython.setEnabled(True)
		self.saveVHDL.setEnabled(True)
		self.saveVerilog.setEnabled(True)

		self._loaded = True
		self._isUserComponent = False if path.find("components_user") == -1 else True

		iconData = returnIconData(path)
		self._lastName = iconData["type"]
		if not self._isUserComponent:
			self.componentName.setText("E" + iconData["type"])
		else:
			self.componentName.setText(iconData["type"])
		self.componentVersion.setText(iconData["version"])
		self.componentAutor.setText(iconData["autor"])
		self.componentDescription.setPlainText(iconData["description"])

		_connectors = getObjectConnectors(path)
		_referenceBits = getsignalReference(path)
		_files = getObjectFiles(path)
		for refer in _referenceBits:
			self._addIOVariables(refer)

		for conn in _connectors:
			if conn["type"] == "in":
				self._addInputFnc(conn)
			elif conn["type"] == "out":
				self._addOutputFnc(conn)

		self.updateClock()

		_editableProperties, _inlineProperties = getObjectProperties(path)
		for prop in _editableProperties:
			self._addPropertyFnc(prop, inline = False)
		for prop in _inlineProperties:
			self._addPropertyFnc(prop, inline = True)

		path = path.replace(get_pycompile_folder() + "/", "")
		self._insertSVG(os.path.join(get_pycompile_folder(), path.split("/")[0], "svg", "workspace", _files["workspaceI"]), QSizeF(iconData["icon"]["width"] * 25, iconData["icon"]["height"] * 25))


		# Files Update
		with open(os.path.join(get_pycompile_folder(), path.split("/")[0], "source_files", _files["py"]), "r+") as f:
			python_code = f.read()
			f.close()
		with open(os.path.join(get_pycompile_folder(), path.split("/")[0], "vhdl_file", _files["vhdl"]), "r+") as f:
			vhdl_code = f.read()
			f.close()
		with open(os.path.join(get_pycompile_folder(), path.split("/")[0], "verilog_files", _files["verilog"]), "r+") as f:
			verilog_code = f.read()
			f.close()

		# Python code
		pythonLibraries = python_code.split("class")[0]
		self.librariesPython.setPlainText(pythonLibraries)
		pythonUserSetup = ""
		splitUserSetup = python_code.split("def userSetup(self, properties):")
		if len(splitUserSetup) > 1:
			pythonUserSetup = splitUserSetup[1].split("def")[0].replace("\n\t\t","\n").replace("\n", "", 1)
		self.pythonUserSetup.setPlainText(pythonUserSetup)
		if self._hasClock:
			try:
				pythonReadUpdate = python_code.split("def readUpdate(self):")[1].split("def")[0].replace("\n\t\t","\n").replace("\n", "", 1)
				self.pythonReadUpdate.setPlainText(pythonReadUpdate)
			except:
				pass
			try:
				pythonWriteUpdate = python_code.split("def writeUpdate(self):")[1].split("def")[0].replace("\n\t\t","\n").replace("\n", "", 1)
				self.pythonWriteUpdate.setPlainText(pythonWriteUpdate)
			except:
				pass
		else:
			try:
				pythonReadUpdate = python_code.split("def update(self):")[1].split("def")[0].replace("\n\t\t","\n").replace("\n", "", 1)
				self.pythonReadUpdate.setPlainText(pythonReadUpdate)
			except:
				pass

		# VHDL code
		vhdlLibraries = vhdl_code.split("entity")[0]
		self.librariesVHDL.setPlainText(vhdlLibraries)
		if self._hasClock:
			try:
				risingVHDL = vhdl_code.split("architecture falling_edge_arch of " + iconData["type"] + " is")[1].split("end architecture")[0].replace("\n", "", 1)
				self.risingVHDL.setPlainText(risingVHDL)
			except:
				pass
			try:
				fallingVHDL = vhdl_code.split("architecture rising_edge_arch of " + iconData["type"] + " is")[1].split("end architecture")[0].replace("\n", "", 1)
				self.fallingVHDL.setPlainText(fallingVHDL)
			except:
				pass
		else:
			try:
				vhdlBody = vhdl_code.split("architecture rtl of " + iconData["type"] + " is")[1].split("end architecture")[0].replace("\n", "", 1)
				self.risingVHDL.setPlainText(vhdlBody)
			except:
				pass

		# Verilog code
		if self._hasClock:
			try:
				risingVerilog = verilog_code.split("module " + iconData["type"] + "_rising")[1].split("endmodule")[0]
				risingVerilog = risingVerilog[risingVerilog.find(";") + 1 : ].replace("\n", "", 1)
				self.verilogBodyRising.setPlainText(risingVerilog)
			except:
				pass
			try:
				fallingVerilog = verilog_code.split("module " + iconData["type"] + "_falling")[1].split("endmodule")[0]
				fallingVerilog = fallingVerilog[fallingVerilog.find(";") + 1 : ].replace("\n", "", 1)
				self.verilogBodyFalling.setPlainText(fallingVerilog)
			except:
				pass
		else:
			try:
				risingVerilog = verilog_code.split("module " + iconData["type"])[1].split("endmodule")[0]
				risingVerilog = risingVerilog[risingVerilog.find(";") + 1 : ].replace("\n", "", 1)
				self.verilogBodyRising.setPlainText(risingVerilog)
			except:
				pass


		self.changeComponentData()
		self.setSaved()


	def _addPropertyFnc(self, data = None, inline = None):
		if type(data) == bool:
			data = None
		newProperty = PropertyWidget(self, data, inline)
		newProperty.setVariablesIO(self._variablesIO)
		self._properties.append(newProperty)
		self.propertiesSpace.addWidget(newProperty)

	def deleteProperty(self, property):
		property.setParent(None)
		self._properties.remove(property)
		self.changeComponentData()

	def _addIOVariables(self, data = None):
		if data == None or type(data) == bool:
			newID = "Bus" + str(self._currentVariablesIOIndex)
			bits = ["1"]
		else:
			newID = data["id"]
			bits = data["bits"]
		obj_index = next((item for item in self._variablesIO if item["id"] == newID), False)
		if obj_index != False:
			self._currentVariablesIOIndex += 1
			self._addIOVariables()
		else:
			self._variablesIO.append({
				"id" : newID,
				"bits" : bits
			})
			self._updateComboIO()
			self._currentVariablesIOIndex += 1
			self.isNew = True
			self.organizeIO()
			self.isNew = False

	def _updateComboIO(self, last = None, new = None):
		for prop in self._properties:
			prop.setVariablesIO(self._variablesIO)
		for io in self._inputs + self._outputs:
			io["object"].setVariablesIO(self._variablesIO, last, new)

	def updateClock(self):
		self._hasClock = False
		for inp in self._inputs:
			data = inp["object"].returnDataJson()
			if data["type"] == CLOCK_S:
				self._hasClock = True

	def deleteInput(self, input):
		obj_index = next((item for item in self._inputs if item["object"] == input), False)
		if obj_index != False:
			obj_index["object"].setParent(None)
			self._inputs.remove(obj_index)
		self.updateClock()
		self.updatePreviews()

	def deleteOutput(self, output):
		obj_index = next((item for item in self._outputs if item["object"] == output), False)
		if obj_index != False:
			obj_index["object"].setParent(None)
			self._outputs.remove(obj_index)
		self.updatePreviews()

	def isValidIOID(self, newID, object, valueIndex = 0):
		if valueIndex > 0:
			newID += str(valueIndex)
		obj_index = next((item for item in self._inputs + self._outputs if item["id"] == newID and item["object"] != object), False)
		if obj_index != False:
			newID = self.isValidIOID(newID, object, valueIndex + 1)
		return newID

	def _addInputFnc(self, data = None):
		if type(data) == bool:
			data = None
		if len(self._variablesIO) == 0:
			self._addIOVariables()
		if data == None:
			newID = "In" + str(self._currentInputIndex)
			variable = self._variablesIO[0]["id"]
			position = QPoint(0, (len(self._inputs)*25) + 25)
		else:
			newID = data["id"]
			variable = data["var"]
			position = QPoint(int(data["X"]) * 25, int(data["Y"])*25)
		obj_index = next((item for item in self._inputs if item["id"] == newID), False)
		if obj_index != False:
			self._currentInputIndex += 1
			self._addInputFnc()
		else:
			typeInput = NORMAL_S
			if data != None:
				if "clock" in data:
					typeInput = CLOCK_S
					self._hasClock = True
				elif "reset" in data:
					typeInput = RESET_S
					self._hasReset = True
			data = {
				"ID" : newID,
				"variable" : variable,
				"type" : "in",
				"typeInput" : typeInput
			}
			point = IOWidget(self._iconItem, {"type" : "out", "id" : newID, "position" : position})
			newInput = IOForm(parent = self, data = data, point = point, variables = self._variablesIO)
			self.inputList.addWidget(newInput)
			self._inputs.append({"id" : newID, "object" : newInput})
			self.changeComponentData()

	def _addOutputFnc(self, data = None):
		if type(data) == bool:
			data = None
		if len(self._variablesIO) == 0:
			self._addIOVariables()
		if data == None:
			newID = "Out" + str(self._currentOutputIndex)
			variable = self._variablesIO[0]["id"]
			position = QPoint(self.widthSlider.value(), len(self._outputs) * 25 + 25)
		else:
			newID = data["id"]
			variable = data["var"]
			position = QPoint(int(data["X"]) * 25, int(data["Y"])*25)
		obj_index = next((item for item in self._outputs if item["id"] == newID), False)
		if obj_index != False:
			self._currentOutputIndex += 1
			self._addOutputFnc()
		else:
			setRegOutput = False
			if data != None:
				if "setRegOutput" in data:
					setRegOutput = data["setRegOutput"]
			isClockOutput = "No"
			if data != None:
				if YES in data:
					isClockOutput = "Yes"
			data = {
				"ID" : newID,
				"variable" : variable,
				"type" : "out",
				"clock" : isClockOutput,
				"setRegOutput" : setRegOutput
			}
			point = IOWidget(self._iconItem, {"type" : "in", "id" : newID, "position" : position})
			newInput = IOForm(parent = self, data = data, point = point, variables = self._variablesIO)
			self.outputList.addWidget(newInput)
			self._outputs.append({"id" : newID, "object" : newInput})

			self.changeComponentData()


	def organizeIO(self):
		self._tableVariablesIO.clear()
		self._tableVariablesIO.setRowCount(len(self._variablesIO))
		self._tableVariablesIO.setColumnCount(2)
		self._tableVariablesIO.setHorizontalHeaderLabels(["ID", "Bus sizes"])

		self.isNew = True
		index = 0
		for item in self._variablesIO:
			self._tableVariablesIO.setItem(index, 0, QTableWidgetItem(item["id"]))
			self._tableVariablesIO.setItem(index, 1, QTableWidgetItem(",".join(item["bits"])))
			index += 1
		self.isNew = False

		self.changeComponentData()

	def _editedVariable(self, item, indexData = 0):
		if not self.isNew:
			noContinue = False
			if item.text() == "":
				if len(self._variablesIO) > 1:
					row = item.row()
					self._variablesIO.pop(row)
					self._updateComboIO()
					noContinue = True
			if not noContinue:
				if item.column() == 1:
					newText = item.text()
					if newText.replace(",", "").replace(" ", "").isdigit():
						self._variablesIO[item.row()]["bits"] = [a for a in newText.replace(" ", "").split(",") if a != "" and a != " "]
						self._updateComboIO()
				else:
					newText = filterText(item.text())
					if newText == "":
						newText = "Bus"
					if indexData > 0:
						newText += str(indexData)
					obj_index = next((item for item in self._variablesIO if item["id"] == newText), False)
					if obj_index != False:
						indexData += 1
						self._editedVariable(item, indexData = indexData)
					else:
						last = self._variablesIO[item.row()]["id"]
						self._variablesIO[item.row()]["id"] = newText
						self._updateComboIO(last, newText)
			self.organizeIO()

	def organizeProperties(self):
		self._tableProperties.clear()
		self._tableProperties.setRowCount(len(self._properties))
		self._tableProperties.setColumnCount(2)
		self._tableProperties.setHorizontalHeaderLabels(["Name", "Type", "base", ""])

	def changeComponentData(self):
		today = date.today()
		date2 = today.strftime("%Y-%m-%d")
		self.setEdited()
		self.modificationDate.setText(str(date2))
		self._name = filterText(self.componentName.text())
		self.componentName.setText(self._name)
		version = filterText(self.componentVersion.text())
		self.componentVersion.setText(version)
		autor = self.componentAutor.text()
		description = self.componentDescription.toPlainText()

		# Properties
		static_properties = []
		inline_properties = []
		for prop in self._properties:
			data, isInline = prop.getJSONFormat()
			if data != None:
				if isInline:
					inline_properties.append(data)
				else:
					static_properties.append(data)

		# Python config
		ioList = []
		for inputData in self._inputs:
			input = inputData["object"].returnDataJson()
			ioList.append("self._addInput(\"" + input["id"] + "\")")
		for outputData in self._outputs:
			output = outputData["object"].returnDataJson()
			ioList.append("self._addOutput(\"" + output["id"] + "\")")
		pythonHeader = self._pythonHeader
		pythonHeader = pythonHeader.replace("{{component_name}}", self._name)
		pythonHeader = pythonHeader.replace("{{io}}", "\n\t\t".join(ioList))
		self.pythonSetup.setPlainText(pythonHeader)

		if self._hasClock:
			self.pythonWriteUpdate.show()
			self.writeUpdateLabel.show()
			self.readUpdateLabel.setText("def readUpdate(self):")
		else:
			self.pythonWriteUpdate.hide()
			self.writeUpdateLabel.hide()
			self.readUpdateLabel.setText("def update(self):")

		# VHDL config
		generics = []
		parametersVerilog = []
		for variable in self._variablesIO:
			if len(variable["bits"]) > 1:
				generics.append(variable["id"] + "_LEN:natural := " + str(variable["bits"][0]) + "")
				parametersVerilog.append("parameter " + variable["id"] + "_LEN = " + str(variable["bits"][0]))

		for prop in static_properties:
			value = prop["name"] + "_PAR:"
			valueVerilog = "parameter " + prop["name"] + "_PAR = "
			if ("base" in prop and prop["base"] == "bit") or prop["type"] == "bit":
				value += "std_logic := '" + str(prop["value"]) + "'"
				valueVerilog += "1'b" + str(prop["value"])
			else:
				value += "natural := " + str(prop["value"]) + ""
				valueVerilog += str(prop["value"])
			generics.append(value)
			parametersVerilog.append(valueVerilog)

		if len(generics) > 0:
			generics = "generic(\n\t\t" + ";\n\t\t".join(generics) + "\n\t);"
		else:
			generics = ""

		if len(parametersVerilog) > 0:
			parametersVerilog = "\t#(\n\t\t" + ",\n\t\t".join(parametersVerilog) + "\n\t)"
		else:
			parametersVerilog = ""

		ioVHDL = []
		ioVerilog = []
		for inputWidget in self._inputs:
			input = inputWidget["object"].returnDataJson()
			is_vector = True
			defaultSize = None
			obj_index = next((item for item in self._variablesIO if item["id"] == input["variable"]), False)
			if obj_index != False:
				if len(obj_index["bits"]) == 1:
					defaultSize = str(int(obj_index["bits"][0]) - 1)
				if len(obj_index["bits"]) == 1 and obj_index["bits"][0] == "1":
					is_vector = False
			else:
				is_vector = False
			if is_vector:
				if defaultSize != None:
					ioVHDL.append(input["id"] + ": in std_logic_vector(" + defaultSize + " downto 0)")
					ioVerilog.append("input [" + defaultSize + " : 0] " + input["id"])
				else:
					ioVHDL.append(input["id"] + ": in std_logic_vector(" + str(input["variable"]) + "_LEN - 1 downto 0)")
					ioVerilog.append("input [" + str(input["variable"]) + "_LEN - 1 : 0] " + input["id"])
			else:
				ioVHDL.append(input["id"] + ": in std_logic")
				ioVerilog.append("input " + input["id"])

		for outputWidget in self._outputs:
			output = outputWidget["object"].returnDataJson()
			is_vector = True
			defaultSize = None
			obj_index = next((item for item in self._variablesIO if item["id"] == output["variable"]), False)
			if obj_index != False:
				if len(obj_index["bits"]) == 1:
					defaultSize = str(int(obj_index["bits"][0]) - 1)
				if len(obj_index["bits"]) == 1 and obj_index["bits"][0] == "1":
					is_vector = False
			else:
				is_vector = False
			aditionalVerilog = ""
			if output["setRegOutput"]:
				aditionalVerilog = "reg "
			if is_vector:
				if defaultSize != None:
					ioVHDL.append(output["id"] + ": out std_logic_vector(" + defaultSize + " downto 0)")
					ioVerilog.append("output " + aditionalVerilog + "[" + defaultSize + " - 1 : 0] " + output["id"])
				else:
					ioVHDL.append(output["id"] + ": out std_logic_vector(" + str(output["variable"]) + "_LEN - 1 downto 0)")
					ioVerilog.append("output " + aditionalVerilog + "[" + str(output["variable"]) + "_LEN - 1 : 0] " + output["id"])
			else:
				ioVHDL.append(output["id"] + ": out std_logic")
				ioVerilog.append("output " + aditionalVerilog + " " + output["id"])

		vhdlHeader = self._vhdlHeader
		vhdlHeader = vhdlHeader.replace("{{generics}}", generics)
		vhdlHeader = vhdlHeader.replace("{{io}}", ";\n\t\t".join(ioVHDL))
		vhdlHeader = vhdlHeader.replace("{{component_name}}", self._name)
		self.vhdlSetup.setPlainText(vhdlHeader)

		if not self._hasClock:
			self.labelFallingEdge.hide()
			self.fallingVHDL.hide()
			self.endArchitectureFalling.hide()
			self.labelRissingEdge.setText("architecture rtl of {{component}} is".replace("{{component}}", self._name))
		else:
			self.labelFallingEdge.show()
			self.fallingVHDL.show()
			self.endArchitectureFalling.show()
			self.labelFallingEdge.setText("architecture falling_edge_arch of {{component}} is".replace("{{component}}", self._name))
			self.labelRissingEdge.setText("architecture rising_edge_arch of {{component}} is".replace("{{component}}", self._name))

		# Verilog Config
		ioVerilog = ",\n\t\t".join(ioVerilog)
		aditionalRising = ""
		if self._hasClock:
			aditionalRising = "_rising"
		headerVerilogRising = "module " + self._name + aditionalRising + "\n" + parametersVerilog + "\n\t(\n\t\t" + ioVerilog + "\n\t);"
		verilogHeaderRising = self._verilogHeader.replace("{{header_verilog}}", headerVerilogRising)
		self.verilogSetup.setPlainText(verilogHeaderRising)

		if self._hasClock:
			headerVerilogFalling = "module " + self._name + "_falling\n" +  parametersVerilog + "\n\t(\n\t\t" + ioVerilog + "\n\t);"
			verilogHeaderFalling = self._verilogHeader.replace("{{header_verilog}}", headerVerilogFalling)
			self.verilogSetupFalling.setPlainText(verilogHeaderFalling)

		if self._hasClock:
			self.verilogBodyFalling.show()
			self.endModuleVerilogLabel.show()
			self.verilogSetupFalling.show()
		else:
			self.verilogBodyFalling.hide()
			self.endModuleVerilogLabel.hide()
			self.verilogSetupFalling.hide()


		# Json Preview
		connectors = []
		for inputWidget in self._inputs:
			input = inputWidget["object"].returnDataJson()
			last = {"id" : input["id"], "X" : input["icon"].getX(), "Y" : input["icon"].getY(), "type" : "in", "var" : input["variable"]}
			if input["type"] == CLOCK_S:
				last["clock"] = True
			elif input["type"] == RESET_S:
				last["reset"] = True
			connectors.append(last)

		for outputWidget in self._outputs:
			output = outputWidget["object"].returnDataJson()
			last = {"id" : output["id"], "X" : output["icon"].getX(), "Y" : output["icon"].getY(), "type" : "out", "var" : output["variable"], "setRegOutput" : output["setRegOutput"]}
			if output["clock"] == YES:
				last["clock"] = True
			connectors.append(last)

		self._jsonPreview = {
			"type" : self._name,
			"autor" : autor,
			"version" : version,
			"category" : "basic",
			"description" : description,
			"clock" : self._hasClock,
			"file" : {
				"icon" : self._name + ".svg",
				"workspaceI" : self._name + ".svg",
				"py" : self._name + ".py",
				"vhdl" : "s_" + self._name + ".vhdl",
				"verilog" : "s_" + self._name + ".v"
			},
			"icon":{
				"width" : int(self.widthSlider.value()/25),
				"height" : int(self.heightSlider.value()/25)
			},
			"inProperties" : inline_properties,
			"eProperties" : static_properties,
			"var_connectors" : self._variablesIO,
			"connectors" : connectors
		}

		json_preview = json.dumps(self._jsonPreview, indent = 2)
		self.jsonPreview.setPlainText(json_preview)

		#self._parent.setSaved(False)

	def saveProject(self):
		self.changeComponentData()
		if self._svgPath != None and self._svgPath != "":
			# Python setup
			python_preview = self.librariesPython.toPlainText() + "\n"
			python_preview += self.pythonSetup.toPlainText() + "\n"
			python_preview += "\tdef userSetup(self, properties):\n\t\t"
			python_preview += self.pythonUserSetup.toPlainText().replace("\n", "\n\t\t") + "\n"
			if self._hasClock:
				python_preview += "\tdef readUpdate(self):\n\t\t"
				python_preview += self.pythonReadUpdate.toPlainText().replace("\n", "\n\t\t") + "\n"
				python_preview += "\tdef writeUpdate(self):\n\t\t"
				python_preview += self.pythonWriteUpdate.toPlainText().replace("\n", "\n\t\t") + "\n"
			else:
				python_preview += "\tdef update(self):\n\t\t"
				python_preview += self.pythonReadUpdate.toPlainText().replace("\n", "\n\t\t") + "\n"

			# VHDL Code
			vhdl_preview = self.librariesVHDL.toPlainText() + "\n"
			vhdl_preview += self.vhdlSetup.toPlainText() + "\n"
			vhdl_preview += self.labelRissingEdge.text() + "\n"
			vhdl_preview += self.risingVHDL.toPlainText().replace("\n", "\n") + "\n"
			vhdl_preview += self.endArchitectureRising.text() + "\n"
			if self._hasClock:
				vhdl_preview += self.labelFallingEdge.text() + "\n"
				vhdl_preview += self.fallingVHDL.toPlainText().replace("\n", "\n") + "\n"
				vhdl_preview += self.endArchitectureFalling.text() + "\n"

			# Verilog Code
			verilog_preview = self.verilogSetup.toPlainText() + "\n"
			verilog_preview += self.verilogBodyRising.toPlainText() + "\n"
			verilog_preview += self.endModuleVerilogLabel.text() + "\n"
			if self._hasClock:
				verilog_preview += "\n" + self.verilogSetupFalling.toPlainText() + "\n"
				verilog_preview += self.verilogBodyFalling.toPlainText() + "\n"
				verilog_preview += self.endModuleVerilogLabel.text() + "\n"

			# Save files
			if self.savePython.isChecked():
				if self._lastName!= None and self._name != self._lastName:
					if not self.SaveCopy.isChecked():
						try:
							os.rename(os.path.join(get_pycompile_folder(), "components_user", "source_files", self._lastName + ".py"), os.path.join(get_pycompile_folder(), "components_user", "source_files", self._name + ".py"))
						except:
							pass
				python_file = os.path.join(get_pycompile_folder(), "components_user", "source_files", self._name + ".py")
				with open(python_file, 'w+') as f:
					f.write(python_preview)
					f.close()

			if self.saveVHDL.isChecked():
				if self._lastName!= None and self._name != self._lastName:
					if not self.SaveCopy.isChecked():
						try:
							os.rename(os.path.join(get_pycompile_folder(), "components_user", "vhdl_file", "s_" + self._lastName + ".vhdl"), os.path.join(get_pycompile_folder(), "components_user", "vhdl_file", "s_" + self._name + ".vhdl"))
						except:
							pass
				vhdl_file = os.path.join(get_pycompile_folder(), "components_user", "vhdl_file", "s_" + self._name + ".vhdl")
				with open(vhdl_file, 'w+') as f:
					f.write(vhdl_preview)
					f.close()

			if self.saveVerilog.isChecked():
				if self._lastName!= None and self._name != self._lastName:
					if not self.SaveCopy.isChecked():
						try:
							os.rename(os.path.join(get_pycompile_folder(), "components_user", "verilog_files", "s_" + self._lastName + ".v"), os.path.join(get_pycompile_folder(), "components_user", "verilog_files", "s_" + self._name + ".v"))
						except:
							pass
				verilog_file = os.path.join(get_pycompile_folder(), "components_user", "verilog_files", "s_" + self._name + ".v")
				with open(verilog_file, 'w+') as f:
					f.write(verilog_preview)
					f.close()

			if self._lastName!= None and self._name != self._lastName:
				if not self.SaveCopy.isChecked():
					try:
						os.rename(os.path.join(get_pycompile_folder(), "components_user", "json", self._lastName + ".sintel"), os.path.join(get_pycompile_folder(), "components_user", "json", self._name + ".sintel"))
					except:
						pass
			json_file = os.path.join(get_pycompile_folder(), "components_user", "json", self._name + ".sintel")
			with open(json_file, 'w+') as f:
				json.dump(self._jsonPreview, f, indent = 2)
				f.close()

			# Copy SVG Image
			try:
				workspace_icon = os.path.join(get_pycompile_folder(), "components_user", "svg", "workspace")
				copyfile(self._svgPath,  os.path.join(workspace_icon, self._name + ".svg"))
				menu_icon = os.path.join(get_pycompile_folder(), "components_user", "svg", "icon")
				copyfile(self._svgPath,  os.path.join(menu_icon, self._name + ".svg"))
			except:
				pass
		self.setSaved()
		QApplication.instance().componentsHandler.updateComponents()

	def _setSVGIcon(self):
		path,_ = QFileDialog.getOpenFileName(self, 'Open file', get_pycompile_folder(), "Program files svg (*svg)")
		if path != "" and path != None:
			self._insertSVG(path)

	def _insertSVG(self, path, size = None):
		if size != None:
			try:
				self.heightSlider.valueChanged.disconnect(self._changeIconHeight)
				self.widthSlider.valueChanged.disconnect(self._changeIconWidth)
			except:
				pass
			self.widthSlider.setValue(size.width())
			self.heightSlider.setValue(size.height())
		self._svgPath = path
		self._iconItem.emptyAll()
		self._iconItem.setPath(path, size)
		self.heightSlider.setEnabled(True)
		self.widthSlider.setEnabled(True)
		self.heightSlider.valueChanged.connect(self._changeIconHeight)
		self.widthSlider.valueChanged.connect(self._changeIconWidth)

	def _changeIconWidth(self, value):
		value = int(value/25)*25
		self._iconItem.setWidth(value)
		self.widthSlider.setValue(value)
		self.changeComponentData()

	def _changeIconHeight(self, value):
		value = int(value/25)*25
		self._iconItem.setHeigth(value)
		self.heightSlider.setValue(value)
		self.changeComponentData()

	def updatePreviews(self):
		self.changeComponentData()
