#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries
from simulador.tools.utils import GRIDSPACE, getSignalSize, bin2dec, bin2decH
from simulador.connector_object import ConnectorObject

# Qt libraries
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

# System libraries

COLOR_CONTROL = "#3498DB"

"""
	Signal has:
		Folds
		Row start, row end:
			Bidirectional with two rows
			Unidirectional with a row and a point
		SubSignals
"""

class Signal(QGraphicsObject):
	"""
		Handle the wire and SubWires
		@param connA as elementConnector
		@param connB as elementConnector
		@param folds as array of QPointF
	"""
	def __init__(self, connA, connB, folds, signalID, referenceMoveA = None, referenceMoveB = None, signalReference = None, signalBits = None, indexF = None, isControlPath = False):
		super(QGraphicsItem, self).__init__()

		# Initial assignations
		self._connA = connA
		self._connB = connB
		self._referenceMoveA = referenceMoveA
		self._referenceMoveB = referenceMoveB
		self._signalID = str(signalID)
		self._signalReference = signalReference
		self._signalBits = signalBits # pines a los cuales está conectada la señal de la subseñal
		self._connectedSignalTerminal = []
		self._isControlPath = isControlPath

		if type(self._connB) == signalTerminal:
			self._connB.setParent(self)

		# Subwires control
		self._currentSubWires = []

		# Bus size:
		if type(self._connB) == signalTerminal:
			self._busSize = int(self._connA.getSize())
		else:
			self._busSize = int(self._connB.getSize())

		# Define default pins of maxWire
		if self._signalReference != None:
			if signalBits == None:
				self._signalBits = [a for a in range(self._busSize)]
			else:
				self._signalBits = signalBits

		# Visual text
		self.valueText = QGraphicsTextItem("", self)
		f = self.valueText.font()
		f.setFamily("Cantarell")
		f.setPointSize(9)
		self.valueText.setFont(f)

		# Folds assignations
		if self._signalID == "Signal_14":
			print(indexF)
		self._currentFoldIndex = 1
		if indexF != None:
			self._currentFoldIndex = indexF


		self._addFold = True
		self._allFolds = []
		for fo in folds:
			self.addFoldFunc(fo, initial = True)
		self._currentFoldIndex += 1

		# End wire terminal
		#self._terminalA = signalTerminal(self._connA)
		#if type(self._connB) != signalTerminal:
		#	self._terminalB = signalTerminal(self._connB)

		# Plot and update
		self.setZValue(2)
		self.update()

	def setControlPath(self, state):
		self._isControlPath = state
		self.update()

	def getControlPath(self):
		return self._isControlPath

	def selectAllSubSignals(self):
		for wire in self._currentSubWires:
			wire.select()
	def deselectAllSubSignals(self):
		for wire in self._currentSubWires:
			wire.deselect()

	def showContextMenu(self, event):
		QApplication.instance().projectsHandler.getWorkspace().showSignalMenu(self, event)

	def getIndexFold(self):
		return self._currentFoldIndex

	def connectSignalTerminal(self, signal):
		if not signal in self._connectedSignalTerminal:
			self._connectedSignalTerminal.append(signal)

	def returnConnectedTerminal(self):
		return self._connectedSignalTerminal

	def canSetSize(self, size):
		valReturn = []
		if not self.isSignalSignal():
			if type(self._connA) == ConnectorObject:
				valReturn.append(self._connA.canSetSizeVariables(size))
			if type(self._connB) == ConnectorObject:
				valReturn.append(self._connB.canSetSizeVariables(size))
		else:
			if type(self._connB) == ConnectorObject:
				valReturn.append(self._connB.canSetSizeVariables(size))
		return False if False in valReturn else True

	def setSizeVariables(self, size):
		if type(self._connA) == ConnectorObject:
			self._connA.setSizeVariables(size)
		if type(self._connB) == ConnectorObject:
			self._connB.setSizeVariables(size)
		self.setSize(size)

	def hasGraph(self):
		#return True if self.plotWidget != None else False
		return QApplication.instance().projectsHandler.getWorkspace().hasGraph(self.getID())

	def getSignalBits(self):
		return self._signalBits

	def getSignalBitsOwn(self):
		forRet = []
		for con in self._connectedSignalTerminal:
			forRet.extend(con.getSignalBits())
		return forRet

	def setSignalBits(self, value):
		self._signalBits = value

	def getSize(self):
		return self._busSize

	def setSize(self, size):
		lastSize = self._busSize
		self._busSize = int(size)
		for subwire in self._currentSubWires:
			subwire.setSize(size)
		if lastSize != self._busSize:
			for signal in self._connectedSignalTerminal:
				conns = signal.getConnections()
				#print(conns[1].canSetSize(size))
				#if type(conns[1]) == ConnectorObject and conns[1].canSetSize(size):
				#	conns[1].setCanSize(size)
				#else:
				signal.delete()
		self._signalBits = [a for a in range(self._busSize)]
		#self.valueText.setHtml("-\n/" + str(self.getSize()))

	def deleteReferenced(self, signal):
		if signal in self._connectedSignalTerminal:
			self._connectedSignalTerminal.remove(signal)

	def isSignalSignal(self):
		return True if self._signalReference != None else False

	def isSignalTerminal(self):
		return True if type(self._connB) == signalTerminal else False

	def isBusBus(self):
		return True if type(self._referenceMoveA) == Fold and type(self._referenceMoveB) == Fold else False

	def returnReferenceMoveA(self):
		return self._referenceMoveA

	def getsignalReference(self):
		if self._signalReference == None:
			return self
		else:
			return self._signalReference

	def getsignalReferencePrecompiler(self):
		if self._signalReference == None:
			return self
		else:
			lastSignal = self._signalReference
			signalRef = self._signalReference
			while signalRef != signalRef.getsignalReference():
				signalRef = signalRef.getsignalReference()
				if signalRef != None:
					lastSignal = signalRef
			return lastSignal

	def updateGui(self, value, time):
		"""
			Updates the text of the signal value
			@param value as int or str or float
		"""
		if type(value) == list:
			value = [str(a) for a in value]
			newValue = bin2decH("".join(value))
			self.valueText.setHtml(str(newValue))
		else:
			self.valueText.setPlainText(str(value))

	def getConnections(self):
		return [self._connA, self._connB]

	def getID(self):
		return self._signalID

	def getFoldsPoints(self):
		points = [i.getPos() for i in self._allFolds]
		return points

	def getFoldByID(self, foldID):
		folds = iter(self._allFolds)
		fold = next((item for item in folds if item.getID() == foldID), None)
		return fold

	@pyqtSlot()
	def updateByConnect(self):
		"""
			Update the wires positions when the element is dragged
		"""
		self.update()

	@pyqtSlot()
	def delete(self, fromWire = None):
		"""
			Disconnect the signal slot from the connectors
			@param fromWire
		"""
		if not self.isSignalSignal():
			if type(self._connA) == ConnectorObject:
				self._connA.disconnectSignal()
			if type(self._connB) == ConnectorObject:
				self._connB.disconnectSignal()
		else:
			if type(self._connB) == ConnectorObject:
				self._connB.disconnectSignal()

		try:
			self._connA.deletedElement.disconnect(self.delete)
			if type(self._connB) != signalTerminal:
				self._connB.deletedElement.disconnect(self.delete)
		except:
			pass

		for fold in self._allFolds:
			if fold.getFromSignal() == True:
				fold.delete()

		if type(self._connB) == signalTerminal or type(self._connB) == Fold:
			self._connB.delete()

		for signal in self._connectedSignalTerminal:
			signal.delete()

		if self._signalReference != None:
			self._signalReference.deleteReferenced(self)

		QApplication.instance().projectsHandler.getWorkspace().removeSignal(self)

	def moveFold(self, newPos):
		for fold in self._allFolds:
			fold.moveTo(newPos)
		if type(self._referenceMoveB) == signalTerminal:
			self._referenceMoveB.moveTo(newPos)
		for sub in self._connectedSignalTerminal:
			sub.update()
		self.update()

	def setDragStart(self, start):
		for fold in self._allFolds:
			fold.setDragStart(start)
		if type(self._referenceMoveB) == signalTerminal:
			self._referenceMoveB.setDragStart(start)

	def update(self):
		"""
			Update the position of all SubWires
		"""
		if not self._addFold:
			for subW in self._currentSubWires:
				subW.update()
			self._addFold = False
		else:
			for subW in self._currentSubWires:
				subW.setParentItem(None)
				subW.hide()
				del subW
			self._currentSubWires = []
			referenceMoveA = self._referenceMoveA
			referenceMoveB = self._referenceMoveB
			if len(self._allFolds) > 0:
				prev_fold = referenceMoveA
				for fo in self._allFolds:
					newSubWire = SubWire(foldFrom = prev_fold, foldTo = fo, size = self.getSize(), parent = self)
					prev_fold = fo
					self._currentSubWires.append(newSubWire)

				newSubWire = SubWire(foldFrom = prev_fold, foldTo = referenceMoveB, size = self.getSize(), parent = self)
				self._currentSubWires.append(newSubWire)
			else:
				newSubWire = SubWire(foldFrom = referenceMoveA, foldTo = referenceMoveB, size = self.getSize(), parent = self)
				self._currentSubWires.append(newSubWire)
			self._addFold = False

		connA = self._connA
		connB = self._connB

		if len(self._allFolds) > 0:
			self.valueText.setPos(((connB.scenePos() + self._allFolds[-1].scenePos()) / 2) - QPointF(0, 2))
		else:
			self.valueText.setPos(((connA.scenePos() + connB.scenePos()) / 2) - QPointF(0, 2))

	def addFoldFunc(self, newFold, initial = False):
		"""
			Add a fold in the wire
			@param newFold as fold
		"""
		newFold.setParent(self)
		if newFold.getID() == None or newFold.getID() == "":
			newFold.setID("f" + str(self._currentFoldIndex))
			self._currentFoldIndex += 1

		newFold.setParentItem(self)

		maxDispl = 0
		selected_index = 0
		index = 0
		if not initial:
			referenceMoveA = self._referenceMoveA
			referenceMoveB = self._referenceMoveB

			for foldMa in range(0, len(self._allFolds) + 1):
				last_point = [referenceMoveA.getPos()["x"], referenceMoveA.getPos()["y"]]
				displacement = 0.0
				for fol in self._allFolds:
					if self._allFolds.index(fol) == index:
						displacement += abs(abs(last_point[0]) - abs(newFold.getPos()["x"])) + abs(abs(last_point[1]) - abs(newFold.getPos()["y"]))
						last_point = [newFold.getPos()["x"], newFold.getPos()["y"]]

					displacement += abs(abs(last_point[0]) - abs(fol.getPos()["x"])) + abs(abs(last_point[1]) - abs(fol.getPos()["y"]))
					last_point = [fol.getPos()["x"], fol.getPos()["y"]]

				if len(self._allFolds) == index:
					displacement += abs(abs(last_point[0]) - abs(newFold.getPos()["x"])) + abs(abs(last_point[1]) - abs(newFold.getPos()["y"]))
					last_point = [newFold.getPos()["x"], newFold.getPos()["y"]]

				displacement += abs(abs(last_point[0]) - abs(referenceMoveB.getPos()["x"])) + abs(abs(last_point[1]) - abs(referenceMoveB.getPos()["y"]))
				if displacement < maxDispl or maxDispl == 0:
					selected_index = index
					maxDispl = displacement
				index += 1

			self._allFolds.insert(selected_index, newFold)
		else:
			self._allFolds.append(newFold)
		self._addFold = True
		self.update()

	def removeFold(self, foldRemove):
		"""
			Remove a fold in the wire
			@param foldRemove as fold
		"""
		foldRemove.setParentItem(None)
		foldRemove.hide()
		self._allFolds.remove(foldRemove)
		self._addFold = True
		self.update()

	def boundingRect(self):
		"""
			override boundingRect method, is mandatory, return the item size
		"""
		return QRectF(0, 0, 10, 10)

	def paint(self, *args):
		"""
			override paint method, is mandatory
			@param args
		"""
		None

class Fold(QGraphicsObject):
	"""
		Point that shows a wire fold
		@param parent
		@parent position as QRectF
	"""
	deletedElement = pyqtSignal()
	moved = pyqtSignal()

	def __init__(self, position, parent = None, isNew = True, initialID = "", parentID = ""):
		super(QGraphicsObject, self).__init__(parent)
		self._parent = parent

		self._foldID = initialID
		self._parentID = parentID
		self._getFromSignal = False

		self._rect = QGraphicsEllipseItem(QRectF(0, 0, 5, 5), self)
		self._pen = QPen(Qt.black)
		self._pen.setWidth(2)
		self._rect.setPen(self._pen)
		background = QBrush(Qt.gray)
		self._rect.setBrush(background)

		self.setAcceptHoverEvents(True)
		if isNew:
			self.setPos(position.x() - 2.5, position.y() - 2.5)
		else:
			self.setPos(position.x(), position.y())

		self.__location = position
		self.__dragStart = position


	def getFromSignal(self):
		return self._getFromSignal

	def setFromSignal(self, value):
		self._getFromSignal = value

	def getSignalID(self):
		if self._parent != None:
			return self._parent.getID()
		else:
			return self._parentID

	def getPos(self):
		return {"x" : self.scenePos().x(), "y" : self.scenePos().y(), "foldID" : self._foldID}

	def delete(self):
		self.deletedElement.emit()
		self._parent.removeFold(self)

	def setParent(self, parent):
		self._parent = parent

	def setID(self, ID):
		if self._foldID == "":
			self._foldID = ID

	def getID(self):
		return str(self._foldID)
	################
		#PRIVATE
	################
		#OVERRIDE
	def hoverEnterEvent(self, event):
		"""
			When the mouse hover over the button
			@param event as mousehoverevent
		"""
		self._pen = QPen(Qt.green)
		self._pen.setWidth(2)
		self._rect.setPen(self._pen)
		background = QBrush(Qt.green)
		self._rect.setBrush(background)

	def hoverLeaveEvent(self, event):
		"""
			When the mouse leave hover over the button
			@param event as mousehoverevent
		"""
		self._pen = QPen(Qt.black)
		self._pen.setWidth(2)
		self._rect.setPen(self._pen)
		background = QBrush(Qt.gray)
		self._rect.setBrush(background)

	def mousePressEvent(self, event):
		"""
			Emits When the user press the mouse
			@param event as mouseevent
		"""
		self.__dragStart = event.pos()

	def mouseMoveEvent(self, event):
		"""
			Emits When the user moves the mouse prev to press
			@param event as mouseevent
		"""
		newPos = event.pos()
		self.__location += (newPos - self.__dragStart)
		self.__location.setX((int(self.__location.x() / GRIDSPACE) * GRIDSPACE) - 2.5)
		self.__location.setY((int(self.__location.y() / GRIDSPACE) * GRIDSPACE) - 2.5)
		self.setPos(self.__location)
		self.parentItem().update()
		self.moved.emit()

	def moveTo(self, newPos):
		self._location = (newPos + self.__dragStart)
		self.setPos(self._location)

	def setDragStart(self, start):
		self.__dragStart = self.scenePos() - start

	def boundingRect(self):
		"""
			override boundingRect method, is mandatory, return the item size
		"""
		return QRectF(0, 0, 20, 20)

	def paint(self, *args):
		"""
			override paint method, is mandatory
			@param args
		"""
		None

	def mouseDoubleClickEvent(self, event):
		"""
			Emits When the user click two moments
			@param event as mouseevent
		"""
		self.deletedElement.emit()
		self.parentItem().removeFold(self)

class SubWire(QGraphicsLineItem):
	"""
		Puts a line into two folds
		@param foldFrom as fold
		@param foldTo as fold
		@param parent
	"""
	def __init__(self, foldFrom, foldTo, size = 1, parent = None):
		self._parent = parent
		super(QGraphicsLineItem, self).__init__(self._parent)
		self._size = getSignalSize(size)

		if self._parent.getControlPath():
			self._pen = QPen(QColor(COLOR_CONTROL))
		else:
			self._pen = QPen(Qt.black)
		self._pen.setWidth(self._size)
		self.setPen(self._pen)
		self.__foldFrom = foldFrom
		self.__foldTo = foldTo
		self.update()
		self.setAcceptHoverEvents(True)

		self._isFocus = False

		#self.setFlag(QGraphicsItem.ItemIsSelectable, True)
		self.setFlag(QGraphicsItem.ItemSendsGeometryChanges, True)
		self.setFlag(QGraphicsItem.ItemIsFocusable, True)

	def setSize(self, size):
		self._size = getSignalSize(size)
		self._pen = QPen(Qt.black)
		self._pen.setWidth(self._size)
		self.setPen(self._pen)

	def getParent(self):
		return self._parent

	def update(self):
		"""
			Update the SubWire Position
		"""
		if self.__foldFrom == None:
			posX1 = 2.5
			posY1 = 2.5
		else:
			posX1 = self.__foldFrom.scenePos().x() + 2.5
			posY1 = self.__foldFrom.scenePos().y() + 2.5
		posX2 = self.__foldTo.scenePos().x() + 2.5
		posY2 = self.__foldTo.scenePos().y() + 2.5
		self.setLine(posX1, posY1, posX2, posY2)

	def addFoldFunc(self, newFold):
		self._parent.addFoldFunc(newFold)

	def mousePressEvent(self, event):
		"""
			Emits When the user press the mouse
			@param event as mouseevent
		"""
		pos = QPointF(0, 0)
		pos.setX(int(event.scenePos().x() / GRIDSPACE) * GRIDSPACE)
		pos.setY(int(event.scenePos().y() / GRIDSPACE) * GRIDSPACE)
		QApplication.instance().projectsHandler.getWorkspace().wireAction(self, pos)
		if event.button() == Qt.RightButton:
			self._parent.showContextMenu(event)
			print(self._parent.getConnections()[0].getID())
			#if self._parent.isSignalSignal():
			#	QApplication.instance().subSignalModal.showConnections(self.getParent())

	def hoverEnterEvent(self, event):
		"""
			When the mouse hover over the button
			@param event as mousehoverevent
		"""
		if not self._isFocus:
			self._pen.setWidth(self._size)
			self._pen.setColor(Qt.red)
			self.setPen(self._pen)

	def hoverLeaveEvent(self, event):
		"""
			When the mouse leave hover over the button
			@param event as mousehoverevent
		"""
		if not self._isFocus:
			self._pen.setWidth(self._size)
			if self._parent.getControlPath():
				self._pen.setColor(QColor(COLOR_CONTROL))
			else:
				self._pen.setColor(Qt.black)
			self.setPen(self._pen)

	def mouseDoubleClickEvent(self, event):
		"""
			Emits When the user press the mouse
			@param event as mouseevent
		"""
		pos = QPointF(0, 0)
		pos.setX(int(event.scenePos().x()))
		pos.setY(int(event.scenePos().y()))
		newFold = Fold(pos)
		self._parent.addFoldFunc(newFold)

	def select(self):
		self._pen.setColor(Qt.green)
		self._pen.setStyle(Qt.DashLine)
		self.setPen(self._pen)
		self._isFocus = True

	def deselect(self):
		if self._parent.getControlPath():
			self._pen.setColor(QColor(COLOR_CONTROL))
		else:
			self._pen.setColor(Qt.black)
		self._pen.setStyle(Qt.SolidLine)
		self.setPen(self._pen)
		self._isFocus = False

	def focusOutEvent(self, event):
		"""
			Emits when component is released
			@param event as QFocusEvent
		"""
		self._parent.deselectAllSubSignals()

	def focusInEvent(self, event):
		"""
			Emits when component is selected
			@param event as QFocusEvent
		"""
		self._parent.selectAllSubSignals()

	def keyPressEvent(self, event):
		"""
			Detect the key press
			@param event as QKeyEvent
		"""
		if event.key() == Qt.Key_Delete:
			self._parent.delete(True)

class signalTerminal(QGraphicsObject):
	"""
		Puts an arrow or point in the wire terminal
		@param connector as elementConnector
		@param parent
	"""
	moved = pyqtSignal()
	deletedElement = pyqtSignal()
	def __init__(self, position, parent = None):
		super(QGraphicsObject, self).__init__(parent)
		self.elipse = QGraphicsEllipseItem(0, 0, 8, 8, self)
		self.elipse.setBrush(QBrush(Qt.blue))
		self.elipse.setAcceptHoverEvents(True)
		self.setPos(position)

		self.__location = position
		self.__dragStart = position

		self._parent = parent

	def moveTo(self, newPos):
		self._location = (newPos + self.__dragStart)
		self.setPos(self._location)

	def setDragStart(self, start):
		self.__dragStart = self.scenePos() - start

	def boundingRect(self):
		"""
			override boundingRect method, is mandatory, return the item size
		"""
		return QRectF(0, 0, 15, 15)

	def paint(self, *args):
		"""
			override paint method, is mandatory
			@param args
		"""
		None

	def delete(self):
		QApplication.instance().projectsHandler.getWorkspace().removeTerminal(self)

	def setParent(self, parent):
		self._parent = parent

	def getPos(self):
		return {"x" : self.scenePos().x(), "y" : self.scenePos().y()}

	def mousePressEvent(self, event):
		"""
			Emits When the user press the mouse
			@param event as mouseevent
		"""
		self.__dragStart = event.pos()

	def mouseMoveEvent(self, event):
		"""
			Emits When the user moves the mouse prev to press
			@param event as mouseevent
		"""
		newPos = event.pos()
		self.__location += (newPos - self.__dragStart)
		self.__location.setX((int(self.__location.x() / GRIDSPACE) * GRIDSPACE) - 2.5)
		self.__location.setY((int(self.__location.y() / GRIDSPACE) * GRIDSPACE) - 2.5)
		self.setPos(self.__location)
		if self._parent != None:
			self._parent.update()
