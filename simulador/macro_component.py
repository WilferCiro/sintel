#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries
from simulador.tools.utils import GRIDSPACE, filterText, get_pycompile_folder
from simulador.connector_object import ConnectorObject
from simulador.tools.sintel_svg import SintelSVG

# Qt libraries
from PyQt5.QtWidgets import QGraphicsRectItem, QGraphicsItem, QGraphicsObject, QGraphicsEllipseItem, QPushButton, QGraphicsProxyWidget, QGraphicsTextItem, QDialog, QApplication, QTableWidget, QTableWidgetItem, QGraphicsScene, QGraphicsPixmapItem, QFileDialog
from PyQt5.QtGui import QPixmap, QPen, QFont, QColor, QBrush
from PyQt5.QtCore import QSize, QRect, Qt, QPointF, QSizeF, QRectF
from PyQt5 import uic

# System libraries
import os

class MacroComponent(QGraphicsItem):
	"""
		Object in the workspace
	"""
	def __init__(self, coords, ID, IOs = None, image = None, parent = None):
		"""
			init component
			@param data as dict
			@param elementID as String
			@param initialPos as QPointF
		"""
		super(QGraphicsItem, self).__init__()
		self._parent = parent
		self._id = ID

		self._size = QPointF(coords[2], coords[3])
		self._zValue = -1
		self.setZValue(-1)
		self._position = QPointF(coords[0], coords[1])
		self.setPos(self._position)

		self._initialSize = self._size

		# Show Hide
		self._shBtn = Point(self)
		self._hide = True

		# Control flags
		self.setFlag(QGraphicsItem.ItemIsSelectable, True)
		self.setFlag(QGraphicsItem.ItemSendsGeometryChanges, True)
		self.setFlag(QGraphicsItem.ItemIsFocusable, True)
		self.setAcceptHoverEvents(True)

		# Draw type
		self._graph = QGraphicsRectItem(0, 0, self._size.x(), self._size.y(), self)
		self._graph.setBrush(Qt.white)
		self._components = []
		self._macros = []
		self._macroParent = None

		# IO
		self._inputs = []
		self._outputs = []

		self._resize = ResizePoint(self._size, self)

		# Drag and drop
		self._location = self._position
		self._dragStart = QPointF(0, 0)

		self._minimizedSize = QPointF(0, 0)

		self._allComponents = []

		self._imagePath = QApplication.instance().get_src("images", "svg", "default.svg")
		if image != None and image[-3:] == "svg":
			if os.path.exists(image):
				self._imagePath = image

		self._svg_icon = SintelSVG(self._imagePath, self)
		self._svg_icon.setSize(QSizeF(GRIDSPACE * 2, GRIDSPACE * 2))
		self._svg_icon.setPos(GRIDSPACE, GRIDSPACE)

		if IOs != None:
			for io in IOs["inputs"]:
				self.AddInput(io)
			for io in IOs["outputs"]:
				self.AddOutput(io)

			#self.setSH()

	def focusOutEvent(self, event):
		"""
			Emits when component is released
			@param event as QFocusEvent
		"""
		pen = QPen()
		pen.setStyle(Qt.SolidLine)
		pen.setColor(Qt.black)
		self._graph.setPen(pen)

	def focusInEvent(self, event):
		"""
			Emits when component is selected
			@param event as QFocusEvent
		"""
		pen = QPen()
		pen.setColor(Qt.green)
		pen.setStyle(Qt.DashLine)
		self._graph.setPen(pen)

	def getSignals(self):
		ret = []
		for io in self._inputs + self._outputs:
			ret.extend(io["object"].getSignals())
		return ret

	def setImage(self, imagePath):
		self._imagePath = imagePath
		self._svg_icon.setFileName(self._imagePath)
		self._svg_icon.setSize(QSizeF(self._minimizedSize.x() - 2 * GRIDSPACE, self._minimizedSize.x() - 2 * GRIDSPACE))
		self._svg_icon.setPos(QPointF(8, (self._minimizedSize.x() + 1 * GRIDSPACE)/2))

	def getImagePath(self):
		return self._imagePath

	def isInsideGui(self, location):
		#if self.scenePos().x() < location.x() and self.scenePos().y() < location.y():
		return True
		#return False

	def isInPosition(self, location):
		if not self._hide:
			if self.scenePos().x() < location.x() and self.scenePos().x() + self._size.x() > location.x() and self.scenePos().y() < location.y() and self.scenePos().y() + self._size.y() > location.y():
				return True
		return False

	def getComponents(self):
		return self._components

	def getMacros(self):
		return self._macros

	def getComponentByID(self, componentID):
		obj_index = next((com for com in self._components if componentID == com["ID"]), False)
		if obj_index != False:
			return obj_index
		return None

	def getIOByID(self, componentID):
		obj_index = next((com for com in self._inputs + self._outputs if componentID == com["ID"]), False)
		if obj_index != False:
			return obj_index
		return None

	def changeComponentID(self, last, newID):
		component = self.getIOByID(last)
		component["ID"] = newID

	def getID(self):
		return self._id

	def getInputs(self):
		return self._inputs

	def getOutputs(self):
		return self._outputs

	def removeInput(self, input):
		iDelete = self.getIOByID(input.getIDSave())
		self._inputs.remove(iDelete)
		iDelete["object"].delete()
		self.changePositions()

	def removeOutput(self, output):
		oDelete = self.getIOByID(input.getIDSave())
		self._outputs.remove(oDelete)
		oDelete["object"].delete()
		self.changePositions()

	def setMacro(self, macro):
		self._macroParent = macro

	def deleteMacro(self):
		self._macroParent = None

	def getMacro(self):
		return self._macroParent

	def AddInput(self, pData):
		data = {
			"ID" : pData["ID"],
			"size" : pData["size"],
			"type" : "in"
		}
		newComp = ComponentConnectorMacro(data, self)
		if "default" in pData:
			if pData["default"] != "-1" and pData["default"] != None:
				newComp.setDefaultValue(int(pData["default"]))
		self._parent.appendMacroComponent(newComp, self)
		self._inputs.append({"ID" : pData["ID"], "object" : newComp})
		self.setSH(False)

	def AddOutput(self, pData):
		data = {
			"ID" : pData["ID"],
			"size" : pData["size"],
			"type" : "out"
		}
		newComp = ComponentConnectorMacro(data, self)
		self._parent.appendMacroComponent(newComp, self)
		self._outputs.append({"ID" : pData["ID"], "object" : newComp})
		self.setSH(False)

	def changePositions(self):
		posY = 2
		for input in self._inputs:
			input["object"].setPos(-2.5, posY * GRIDSPACE-2.5)
			posY += 2
		posY = 2
		for output in self._outputs:
			output["object"].setPos(self._size.x() -2.5, posY * GRIDSPACE-2.5)
			posY += 2

	def getIOSave(self):
		Ins = []
		for input in self._inputs:
			Ins.append({
				"ID" : input["object"].getIDSave(),
				"size" : input["object"].getSize(),
				"default" : input["object"].getDefaultValue()
			})
		Outs = []
		for output in self._outputs:
			Outs.append({
				"ID" : output["object"].getIDSave(),
				"size" : output["object"].getSize()
			})
		return {"inputs" : Ins, "outputs" : Outs}

	def getIOComponents(self):
		return self._inputs + self._outputs

	def getSaveComponents(self):
		components = []
		for comp in self._components:
			components.append(comp.getID())
		return components

	def getSaveMacros(self):
		macros = []
		for mac in self._macros:
			macros.append(mac.getID())
		return macros

	def addComponents(self, component, macros = None):
		self._zValue = self.getZValue()
		if component != None:
			for comp in component:
				if comp.getMacro() == None:
					comp.setMacro(self)
					self._components.append(comp)
		if macros != None:
			for macro in macros:
				if macro.getMacro() == None:
					macro.setMacro(self)
					self._macros.append(macro)
				if macro.getZValue() < self._zValue:
					self._zValue = macro.getZValue() - 1
				elif macro.getZValue() == self._zValue:
					self._zValue = macro.getZValue() - 1
		self.setZValue(self._zValue)
		self.updateIO()
		self.maximizeSH()
		#self.setSH()

	def getZValue(self):
		return self._zValue

	def select(self):
		pass

	def deselect(self):
		pass

	def setSH(self, type = True):
		if type:
			self._hide = not self._hide

		if self._hide:
			self.minimizeSH()
		else:
			self.maximizeSH()

	def maximizeSH(self):

		max_size = max([len(self._inputs), len(self._outputs)])
		if max_size == 0:
			max_size = 1
		self._minimizedSize = QPointF(max_size * GRIDSPACE + 2 * GRIDSPACE, max_size * 2 * GRIDSPACE + 2 * GRIDSPACE)

		self._hide = False
		self.resize(self._initialSize)
		self.setImage(self._imagePath)
		self._svg_icon.setVisible(False)
		self._resize.setVisible(True)
		self.changePositions()

		for io in self._inputs + self._outputs:
			io["object"].showFromMacroComponent()
		for comp in self._components:
			comp.showFromMacroComponent(self)
		for mac in self._macros:
			mac.showFromMacroComponent(self)
			mac.minimizeSH()

		data = {
			"x" : self._position.x(),
			"y" : self._position.y(),
			"w" : self._size.x(),
			"h" : self._size.y()
		}
		self._parent.hideAllMacrosExcept(self)

	def minimizeSH(self):

		max_size = max([len(self._inputs), len(self._outputs)])
		if max_size == 0:
			max_size = 1
		self._minimizedSize = QPointF(max_size * GRIDSPACE + 2 * GRIDSPACE, max_size * 2 * GRIDSPACE + 2 * GRIDSPACE)

		self._hide = True
		self.resize(self._minimizedSize)
		self.setImage(self._imagePath)
		self._svg_icon.setVisible(True)
		self._resize.setVisible(False)
		self.changePositions()

		for io in self._inputs + self._outputs:
			io["object"].hideFromMacroComponent()
		for comp in self._components + self._macros:
			comp.hideFromMacroComponent(self)


	def hideFromMacroComponent(self, macro):
		"""
			si minimiza
				componentes ocultan
				macros ocultan
					componentes ocultan
					macros ocultan

			si muestra
				componentes muestran
				macros ocultan
					componentes ocultan
		"""
		self.setVisible(False)
		for comp in self._components:
			comp.hideFromMacroComponent(self)
		for mac in self._macros:
			mac.hideFromMacroComponent(self)
		for conn in self._inputs + self._outputs:
			conn["object"].hideFromMacroComponent(checkMacro = macro)

	def showFromMacroComponent(self, macro):
		self.setVisible(True)
		for comp in self._components:
			comp.hideFromMacroComponent(self)
		for mac in self._macros:
			mac.hideFromMacroComponent(self)
		for conn in self._inputs + self._outputs:
			conn["object"].hideFromMacroComponent(checkMacro = macro)

	def updateIO(self):
		self._signals = []
		for comp in self._components + self._macros:
			signals = comp.getIODeleteMacro(self)
			if len(signals) > 0:
				self._signals.extend(signals)

		for sig in self._signals:
			sig.delete()

	def getIODeleteMacro(self, macro):
		signals = []
		for conn in self._inputs + self._outputs:
			signal = conn["object"].forDeleteMacro(macro)
			if len(signal) > 0:
				signals.extend(signal)
		return signals

	def updateGui(self, movePos):
		for io in self._inputs + self._outputs:
			io["object"].movePos(movePos)
		for component in self._components + self._macros:
			component.movePos(movePos)

	def setDragStart(self, start):
		self._dragStart = self.scenePos() - start
		for component in self._components + self._macros:
			component.setDragStart(start)
		for io in self._inputs + self._outputs:
			io["object"].setDragStart(start)
		#for con in self._allConnectorsWidget:
		#	con["connectorObject"].setDragStart(start)

	def movePos(self, newPos):
		self._location = (newPos + self._dragStart)
		self._location.setX((int(self._location.x() / GRIDSPACE) * GRIDSPACE))
		self._location.setY((int(self._location.y() / GRIDSPACE) * GRIDSPACE))
		self.setPos(self._location)

		self.updateGui(newPos)

		#for con in self._allConnectorsWidget:
		#	con["connectorObject"].movePos(newPos)

	def hasComponent(self, component):
		if component in self._components:
			return False
		for io in self._inputs + self._outputs:
			if io["object"] == component:
				return False
		for macro in self._macros:
			if component in macro.getConnections():
				return False
		return True

	def getConnections(self):
		return [a["object"] for a in self._inputs + self._outputs]

	def hoverEnterEvent(self, event):
		"""
			When the mouse hover over the button
			@param event as mousehoverevent
		"""
		if not self._hide:
			self._resize.setVisible(True)
		for io in self._inputs + self._outputs:
			io["object"].setVisibleLabel(True)

	def hoverLeaveEvent(self, event):
		"""
			When the mouse leave hover over the button
			@param event as mousehoverevent
		"""
		self._resize.setVisible(False)
		for io in self._inputs + self._outputs:
			io["object"].setVisibleLabel(False)

	def getPosSave(self):
		return {"W" : self._initialSize.x(), "H" : self._initialSize.y(), "X" : self.scenePos().x(), "Y" : self.scenePos().y()}

	def getType(self):
		return self._type

	def resize(self, change, end = False):
		if change.x() > 20 and change.y() > 20:
			self.prepareGeometryChange();
			self._size = change
			self.update(self.boundingRect());

			self._graph.setRect(QRectF(0, 0, change.x(), change.y()))
			self._graph.prepareGeometryChange()
			self._graph.update()

			if end:
				self._initialSize = self._size
				self.changePositions()
				#self.updateIO()

			#self.setSH(False)

	def getSize(self):
		return self._size

	def boundingRect(self):
		"""
			override boundingRect method, is mandatory, return the item size
		"""
		return QRectF(0, 0, self._size.x(), self._size.y())

	def paint(self, *args):
		"""
			override paint method, is mandatory
			@param args
		"""
		None

	def deleteMedium(self):
		for comp in self._components + self._macros:
			comp.showFromMacroComponent(self)
			comp.deleteMacro()
		for io in self._inputs + self._outputs:
			self._parent.deleteElementMacro(io["object"])
			io["object"].delete()
		self._parent.deleteMacro(self)

	def delete(self):
		for comp in self._components:
			comp.delete()
		for macro in self._macros:
			macro.delete()
		self.deleteMedium()

	def keyPressEvent(self, event):
		"""
			Detect the key press
			@param event as QKeyEvent
		"""
		if event.key() == Qt.Key_Delete:
			self.delete()

	def mouseReleaseEvent(self, event):
		"""
			Emits When the user releases the mouse
			@param event as mouseevent
		"""
		self._location.setX(int(self._location.x() / GRIDSPACE) * GRIDSPACE)
		self._location.setY(int(self._location.y() / GRIDSPACE) * GRIDSPACE)
		self.setPos(self._location)

	def mousePressEvent(self, event):
		"""
			Emits When the user press the mouse
			@param event as mouseevent
		"""
		for component in self._components + self._macros:
			component.setDragStart(self.scenePos())

		for component in self._inputs + self._outputs:
			component["object"].setDragStart(self.scenePos())
		self._dragStart = event.pos()

	def mouseMoveEvent(self, event):
		"""
			Emits When the user moves the mouse prev to press
			@param event as mouseevent
		"""
		newPos = event.pos()
		self._location += (newPos - self._dragStart)
		self._location.setX((int(self._location.x() / GRIDSPACE) * GRIDSPACE))
		self._location.setY((int(self._location.y() / GRIDSPACE) * GRIDSPACE))
		setPos = True
		if self._macroParent != None:
			setPos = False
			if self._macroParent.isInsideGui(self._location):
				setPos = True

		if setPos:
			self.setPos(self._location)
			self.updateGui(self._location)
			for io in self._inputs + self._outputs:
				io["object"].update()

	def mouseDoubleClickEvent(self, event):
		"""
			Emits When the user press the mouse
			@param event as mouseevent
		"""
		QApplication.instance().propertiesMacroWindow.showProperties(self)

class Point(QGraphicsItem):
	def __init__(self, parent):
		self._parent = parent
		super(QGraphicsItem, self).__init__(parent)

		self._default_color = Qt.black

		self._pen = QPen(self._default_color)
		self._pen.setWidth(2)

		self._rect = QGraphicsEllipseItem(QRectF(0, 0, 10, 10), self)
		self._rect.setPen(self._pen)
		background = QBrush(self._default_color)
		self._rect.setBrush(background)

		self.setZValue(1)

	def mousePressEvent(self, event):
		"""
			Emits When the user press the mouse
			@param event as mouseevent
		"""
		self._parent.setSH()

	def boundingRect(self):
		"""
			override boundingRect method, is mandatory, return the item size
		"""
		return QRectF(0, 0, 10, 10)

	def paint(self, *args):
		"""
			override paint method, is mandatory
			@param args
		"""
		None


class ComponentConnectorMacro(QGraphicsItem):
	def __init__(self, data, parent):
		self._parent = parent
		super(QGraphicsItem, self).__init__(parent)
		self._id = data["ID"]
		self._var = [data["size"]]
		self._type = data["type"]

		self._isHide = False

		self._connA = ConnectorObject({"id" : "pIn", "type" : "in"}, self)
		self._connA.setSize(data["size"], posibleBits = self._var)
		#self._connA.setPos(-2.5, -2.5)

		self._connB = ConnectorObject({"id" : "pOut", "type" : "out"}, self)
		self._connB.setSize(data["size"], posibleBits = self._var)
		#self._connB.setPos(-2.5, -2.5)

		self._textView = QGraphicsTextItem(self._id, self)
		self._textView.setPos(QPointF(5, -10))
		self._textView.setVisible(False)

	def setVisibleLabel(self, value):
		if value:
			self._textView.setVisible(True)
		else:
			self._textView.setVisible(False)

	def getReferenceBitsByID(self, ID):
		return self.getVariableByPin()

	def isInput(self):
		return False

	def isOutput(self):
		return False

	def getVariableByPin(self, pin = None):
		return {'id': 'buses', 'bits': self._var, 'connectors': [self._connA, self._connB]}

	def showFromMacroComponent(self, checkMacro = None):
		if self._type == "in":
			self._connB.setVisible(True)
			self._connA.setVisible(False)
		else:
			self._connB.setVisible(False)
			self._connA.setVisible(True)

		if self._parent.getMacro() == None or checkMacro == None:
			if self._type == "in":
				self._connA.showConnectedSignal()
				self._connB.showConnectedSignal()
			else:
				self._connB.showConnectedSignal()
				self._connA.showConnectedSignal()
		else:
			self._connA.hideConnectedSignal()
			self._connB.hideConnectedSignal()

		self._isHide = False
		self.update()

	def getDefaultValue(self):
		if self._type == "in":
			default = self._connA.getDefaultValue()
			if default != None:
				return str(default)
		return "-1"

	def setDefaultValue(self, value):
		if self._type == "in":
			self._connA.setDefaultValue(value)

	def hideFromMacroComponent(self, checkMacro = None):
		if self._type == "in":
			self._connB.setVisible(False)
			self._connA.setVisible(True)
		else:
			self._connB.setVisible(True)
			self._connA.setVisible(False)

		if self._parent.getMacro() == None or checkMacro == None:
			if self._type == "in":
				self._connB.hideConnectedSignal()
				self._connA.showConnectedSignal()
			else:
				self._connB.showConnectedSignal()
				self._connA.hideConnectedSignal()
		else:
			self._connB.hideConnectedSignal()
			self._connA.hideConnectedSignal()

		self._isHide = True
		self.update()

	def movePos(self, newPos):
		self._connA.movePos(newPos)
		self._connB.movePos(newPos)
		self.update()

	def setDragStart(self, start):
		self._connA.setDragStart(start)
		self._connB.setDragStart(start)

	def forDeleteMacro(self, macro):
		signals = []
		conn = self._connA
		if self._type == "out":
			conn = self._connB
		forDelete, signal = conn.forDeleteMacro(macro)
		if forDelete:
			signals.append(signal)
		return signals

	def getPinObj(self, name):
		if name == "pIn":
			return self._connA
		else:
			return self._connB

	def delete(self):
		self._connA.delete()
		self._connB.delete()
		self.setParentItem(None)

	def setID(self, newID):
		self._parent.changeComponentID(self._id, newID)
		self._id = newID

	def getID(self):
		return self._parent.getID() + "_" + self._id

	def getIDSave(self):
		return self._id

	def getParentID(self):
		return self._parent.getID()

	def getSize(self):
		return self._var[0]

	def getMacro(self):
		if not self._isHide:
			return self._parent
		else:
			return self._parent.getMacro()

	def update(self):
		self._connA.updateWires()
		self._connB.updateWires()

	def setSize(self, size):
		self._var = [str(size)]
		self._connA.setSize(int(size))
		self._connB.setSize(int(size))

	def getSignals(self):
		if self._parent.getMacro() != None:
			return [self._connA.getConnectedSignal(), self._connB.getConnectedSignal()]
		else:
			if self._type == "in":
				return [self._connB.getConnectedSignal()]
			else:
				return [self._connA.getConnectedSignal()]

	def getPins(self):
		return [self._connA, self._connB]

	def getType(self):
		return "UnionMacro"

	def getTypeIO(self):
		return self._type

	def getClock(self):
		return None

	def boundingRect(self):
		"""
			override boundingRect method, is mandatory, return the item size
		"""
		return QRectF(0, 0, 10, 10)

	def paint(self, *args):
		"""
			override paint method, is mandatory
			@param args
		"""
		None

class ResizePoint(QGraphicsItem):
	def __init__(self, pos, parent):
		self._parent = parent
		self._startPos = pos
		super(QGraphicsItem, self).__init__(parent)

		self._default_color = Qt.black

		self._pen = QPen(self._default_color)
		self._pen.setWidth(2)

		self._rect = QGraphicsEllipseItem(QRectF(0, 0, 5, 5), self)
		self._rect.setPen(self._pen)
		background = QBrush(self._default_color)
		self._rect.setBrush(background)

		self.setVisible(False)

		self._dragStart2 = self._startPos

		self.setPos((self._startPos - QPointF(5, 5)))

		self.setZValue(1)

	def mouseReleaseEvent(self, event):
		"""
			Emits When the user releases the mouse
			@param event as mouseevent
		"""
		posEvent = QPointF(int(event.pos().x()/GRIDSPACE) * GRIDSPACE, int(event.pos().y()/GRIDSPACE) * GRIDSPACE)
		newPos = posEvent + self._dragStart
		self._parent.resize(newPos, True)
		self.setPos((self._parent.getSize() - QPointF(5, 5)))
		self.setCursor(Qt.ArrowCursor)

	def mousePressEvent(self, event):
		"""
			Emits When the user press the mouse
			@param event as mouseevent
		"""
		self._dragStart = self._parent.getSize()
		self.setCursor(Qt.ClosedHandCursor)

	def mouseMoveEvent(self, event):
		"""
			Emits When the user moves the mouse prev to press
			@param event as mouseevent
		"""
		posEvent = QPointF(int(event.pos().x()/GRIDSPACE) * GRIDSPACE, int(event.pos().y()/GRIDSPACE) * GRIDSPACE)
		posEventNormal = event.pos()
		self._parent.resize(posEvent + self._dragStart, True)

	def boundingRect(self):
		"""
			override boundingRect method, is mandatory, return the item size
		"""
		return QRectF(0, 0, 20, 20)

	def paint(self, *args):
		"""
			override paint method, is mandatory
			@param args
		"""
		None


class PropertiesMacroWindow(QDialog):

	def __init__(self):
		super(QDialog, self).__init__()
		uic.loadUi(QApplication.instance().get_src("ui", "macro", "properties_window.ui"), self)

		# Buttons for accept or cancel
		self.buttonBox.accepted.connect(self._accept)

		self._imagePath = None

		self._indexOutput = 1
		self._indexInput = 1
		self.isNew = False

		# Control variables
		self._inputs = []
		self._outputs = []
		self._tableInputs = QTableWidget()
		self._tableInputs.itemChanged.connect(self._editedInput)
		self._tableOutputs = QTableWidget()
		self._tableOutputs.itemChanged.connect(self._editedOutput)
		self.addInputBtn.clicked.connect(self._addInputFnc)
		self.addOutputBtn.clicked.connect(self._addOutputFnc)

		self.imageSelectBtn.clicked.connect(self._selectImage)

		self.saveBtn.clicked.connect(self._saveMacro)

		self.inputLayout.addWidget(self._tableInputs)
		self.outputLayout.addWidget(self._tableOutputs)

		self._element = None

	def _selectImage(self):
		path, _ = QFileDialog.getOpenFileName(self, 'Open file', get_pycompile_folder(), "Program files svg (*svg)")
		if path != "" and path != None:
			self._imagePath = path
			self.imageLabel.setText(path)

	def _saveMacro(self):
		name = filterText(self.nameSave.text())
		addedSignals = []
		data = QApplication.instance().saveLoadMacro.saveMacro(self._element, name, addedSignals)
		QApplication.instance().saveLoadMacro.writeFile(data, name)

	def _addInputFnc(self):
		self._inputs.append({
			"object" : None,
			"ID" : "pIn" + str(self._indexInput),
			"size" : "1",
			"default" : "-1"
		})
		self._indexInput += 1
		self.updateTables()

	def _addOutputFnc(self):
		self._outputs.append({
			"object" : None,
			"ID" : "pOut" + str(self._indexOutput),
			"size" : "1"
		})
		self._indexOutput += 1
		self.updateTables()

	def updateTables(self):
		self._tableInputs.clear()
		self._tableOutputs.clear()

		self._tableInputs.setRowCount(len(self._inputs))
		self._tableInputs.setColumnCount(3)
		self._tableOutputs.setRowCount(len(self._outputs))
		self._tableOutputs.setColumnCount(2)

		self._tableInputs.setHorizontalHeaderLabels(["ID", "Bus size", "Default Value"])
		self._tableOutputs.setHorizontalHeaderLabels(["ID", "Bus size"])

		self.isNew = True
		index = 0
		for item in self._inputs:
			self._tableInputs.setItem(index, 0, QTableWidgetItem(item["ID"]))
			self._tableInputs.setItem(index, 1, QTableWidgetItem(item["size"]))
			self._tableInputs.setItem(index, 2, QTableWidgetItem(item["default"]))
			index += 1

		index = 0
		for item in self._outputs:
			self._tableOutputs.setItem(index, 0, QTableWidgetItem(item["ID"]))
			self._tableOutputs.setItem(index, 1, QTableWidgetItem(item["size"]))
			index += 1

		self.isNew = False

	def _editedOutput(self, item):
		if not self.isNew:
			newText = filterText(item.text())
			if newText == "":
				row = item.row()
				self._outputs[row]["ID"] = ""
			else:
				column = item.column()
				if column == 0:
					self._outputs[item.row()]["ID"] = newText
				else:
					if newText.replace(",", "").replace(" ", "").isdigit():
						self._outputs[item.row()]["size"] = newText.replace(" ", "")
			self.updateTables()

	def _editedInput(self, item):
		if not self.isNew:
			newText = filterText(item.text())
			column = item.column()
			if newText == "":
				if column == 2:
					self._inputs[item.row()]["default"] = "-1"
				else:
					row = item.row()
					self._inputs[row]["ID"] = ""
			else:
				if column == 0:
					self._inputs[item.row()]["ID"] = newText
				else:
					if newText.replace(",", "").replace(" ", "").isdigit():
						if column == 1:
							self._inputs[item.row()]["size"] = newText.replace(" ", "")
						else:
							value = newText.replace(" ", "")
							max = 2 ** int(self._inputs[item.row()]["size"]) - 1
							if int(value) < max:
								self._inputs[item.row()]["default"] = newText.replace(" ", "")
							else:
								self._inputs[item.row()]["default"] = str(max)
			self.updateTables()

	def showProperties(self, obj):
		"""
			Excec properties window
			@param obj as workspace_object
		"""
		self._element = obj
		self._initialInputs = self._element.getInputs()
		self._initialOutputs = self._element.getOutputs()

		self.imageLabel.setText(self._element.getImagePath())
		self.macroID.setText(self._element.getID())

		self._inputs = []
		self._outputs = []

		for input in self._initialInputs:
			self._inputs.append({
				"object" : input["object"],
				"ID" : input["object"].getIDSave(),
				"size" : input["object"].getSize(),
				"default" : input["object"].getDefaultValue()
			})
		for output in self._initialOutputs:
			self._outputs.append({
				"object" : output["object"],
				"ID" : output["object"].getIDSave(),
				"size" : output["object"].getSize()
			})

		self.updateTables()

		self.exec_()

	def _accept(self):
		for input in self._inputs:
			if input["ID"] == "" or input["size"] == "":
				self._element.removeInput(input["object"])
			elif input["object"] == None:
				self._element.AddInput(input)
			else:
				if input["object"].getID() != input["ID"]:
					input["object"].setID(input["ID"])
				if input["object"].getSize() != int(input["size"]):
					input["object"].setSize(int(input["size"]))
				if input["object"].getDefaultValue() != int(input["default"]):
					input["object"].setDefaultValue(int(input["default"]))

		for output in self._outputs:
			if output["ID"] == "" or output["size"] == "":
				self._element.removeOutput(output["object"])
			elif output["object"] == None:
				self._element.AddOutput(output)
			else:
				if output["object"].getID() != output["ID"]:
					output["object"].setID(output["ID"])
				if output["object"].getSize() != int(output["size"]):
					output["object"].setSize(int(output["size"]))
		self._element.changePositions()
		if self._imagePath != None:
			self._element.setImage(self._imagePath)

		self._imagePath = None
		self.imageLabel.setText("Select image")
		self.accept()
