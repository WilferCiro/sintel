#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries
from simulador.menus import QuickSearchWidget, SignalsMenu
from simulador.workspace_object import WorkspaceObject
from simulador.tools.xml_reader import returnIconData, returnExists
from simulador.tools.utils import GRIDSPACE, workspaceSize, getSignalSize, get_pycompile_folder
from simulador.workspace_utils import TempLine
from simulador.signal import Signal, Fold, SubWire, signalTerminal
from simulador.connector_object import ConnectorObject
from simulador.draw_workspace import RectangleObject
from simulador.macro_component import MacroComponent

# Qt libraries
from PyQt5.QtWidgets import QGraphicsScene, QMenu, QWidgetAction, QGraphicsLineItem, QApplication, QGraphicsRectItem, QGraphicsBlurEffect
from PyQt5.QtCore import QRectF, QPointF, Qt, pyqtSlot
from PyQt5.QtGui import QTransform, QBrush, QColor, QPixmap, QPen

# System libraries
import json
import os

class Workspace(QGraphicsScene):
	"""
		Handle the workspace
		@param parent as widget or None
	"""
	def __init__(self, parent = None):
		super(QGraphicsScene, self).__init__(parent)

		self._parent = parent

		self.__zoom_value = 1
		self.max_zoom = 1.6;
		self.min_zoom = 0.2;

		self.setSceneRect(QRectF(0, 0, workspaceSize["x"], workspaceSize["y"]))
		# Search menu
		searchWidget = QuickSearchWidget()
		self.menuQuick = QMenu()
		widget = QWidgetAction(self.menuQuick)
		widget.setDefaultWidget(searchWidget)
		self.menuQuick.addAction(widget)

		# menu for signals
		self.menuSignals = QMenu()
		self._signalsMenuWidget = SignalsMenu(self, self.menuSignals)
		widget = QWidgetAction(self.menuSignals)
		widget.setDefaultWidget(self._signalsMenuWidget)
		self.menuSignals.addAction(widget)

		#self.menuSignals.setGraphicsEffect(QGraphicsBlurEffect(self.menuSignals))

		# Save Load
		self.saveLoad = parent.saveLoad

		# Control Variables
		self._componentIndex = 1
		self._allWorkspaceComponents = []
		self._allSignals = []
		self._signalIndex = 1
		self._currentFolds = []
		self._signalsShow = []

		# current points of temp line
		self._linePoints = []
		self._signalPlot = []
		self._connA = None
		self._allTmpLines = []
		self._tempLine = TempLine(self)
		self.addItem(self._tempLine)
		self._sizeTmnSignal = 2
		self._lastPosition = None

		# Draw
		self._allDraw = []
		#def drawBackground(self):

		# Adding geometry figures
		self._addingRectangle = False
		self._addingEllipse = False

		# MacroComponent
		self._allMacroComponents = []
		self._macroIndex = 1

		# For select
		self._selectStart = None
		self._selectCurrent = None
		self._emptyLastSelected()
		self._lastCoords = []
		self._square = QGraphicsRectItem(0, 0, 0, 0)
		self.addItem(self._square)
		self._square.setVisible(False)

		# Background
		brush = QBrush()
		brush.setColor(QColor('#999'))
		brush.setTexture(QPixmap(QApplication.instance().get_src("images", "grid.png")))
		self.setBackgroundBrush(brush)

	def setEdited(self):
		self._parent.setEdited()

	def hideAllMacrosExcept(self, macro):
		allExceptions = [macro]
		parentMacro = macro
		while parentMacro != None:
			allExceptions.append(parentMacro.getMacro())
			parentMacro = parentMacro.getMacro()

		for macro in self._allMacroComponents:
			if macro["object"] not in allExceptions:
				macro["object"].minimizeSH()

	def deleteMacro(self, macro):
		macro = self.getMacroByID(macro.getID())
		self.removeItem(macro["object"])
		self._allMacroComponents.remove(macro)

		self.setEdited()

	def getAllMacros(self):
		macros = []
		for macro in self._allMacroComponents:
			macros.append(macro["object"])
		return macros

	def getAllIOMacros(self):
		IOs = []
		for macro in self._allMacroComponents:
			IOs.extend(macro["object"].getIOComponents())
		return IOs

	def addMacroComponent(self, data = None, isNew = False):
		self._square.setVisible(False)
		if len(self._lastSelected["components"] + self._lastSelected["macros"]) > 0 or data != None:
			components = self._lastSelected["components"]
			macros = self._lastSelected["macros"]
			IOS = None
			image = None
			newID = "Macro" + str(self._macroIndex)
			if data != None:
				self._lastCoords = [data["coords"]["X"], data["coords"]["Y"], data["coords"]["W"], data["coords"]["H"]]
				IOS = data["IO"]
				if "image" in data:
					image = data["image"]
				components = []
				for comp in data["components"]:
					returned = self.getComponentByID(comp)
					if returned != None:
						components.append(returned["object"])
				if "macros" in data:
					for macro in data["macros"]:
						returned = self.getMacroByID(macro)
						if returned != None:
							macros.append(returned["object"])
				if not isNew:
					newID = data["ID"]

			newMacro = MacroComponent(self._lastCoords, newID, IOS, image, self)
			self.addItem(newMacro)
			newMacro.addComponents(components, macros)
			newMacro.minimizeSH()
			self._allMacroComponents.append({"ID" : newID, "object" : newMacro})
			self._lastCoords = None
			self._macroIndex += 1

			self.setEdited()

			return newMacro

	def showSignalMenu(self, signal, event):
		inList = False
		obj_index = next((com for com in self._signalsShow if signal.getID() == com["ID"]), False)
		if obj_index != False:
			#self._signalsShow.remove(obj_index)
			inList = True
		self._signalsMenuWidget.showSignal(signal, inList)
		self.menuSignals.exec_(event.screenPos())

	def addSignalShowValue(self, signal):
		self._signalsShow.append({"ID" : signal.getID(), "object" : signal})
		self.setEdited()

	def addSignalShowID(self, signalID):
		newSignal = self.getSignalByID(signalID)
		if newSignal != None:
			self._signalsShow.append(newSignal)
			self.setEdited()

	def deleteSignalShowValue(self, signal):
		obj_index = next((com for com in self._signalsShow if signal.getID() == com["ID"]), False)
		if obj_index != False:
			self._signalsShow.remove(obj_index)
			self.setEdited()

	def getAllSignalsShow(self):
		return self._signalsShow

	def setMacroIndex(self, index):
		self._macroIndex = index

	def setComponentIndex(self, componentIndex):
		self._componentIndex = componentIndex

	def setSignalIndex(self, signalIndex):
		self._signalIndex = int(signalIndex)

	def getSignalsPlot(self):
		return self._signalPlot

	def getComponentIndex(self, add = False):
		if add:
			self._componentIndex += 1
		return {"comp" : self._componentIndex, "macro" : self._macroIndex}

	def getAllWorkspaceComponents(self):
		return self._allWorkspaceComponents

	def getAllSignals(self):
		return self._allSignals

	def getSignalsIndex(self):
		return self._signalIndex

	def getSignalByID(self, signalID):
		try:
			signal = next(si for si in self._allSignals if signalID == si["ID"])
			return signal
		except:
			return None

	def deleteElementMacro(self, io):
		component = self.getComponentByID(io.getID())
		self._allWorkspaceComponents.remove(component)
		self.setEdited()

	def getComponentByID(self, componentID):
		obj_index = next((com for com in self._allWorkspaceComponents if componentID == com["ID"]), False)
		if obj_index != False:
			return obj_index
		return None

	def getMacroByID(self, macroID):
		obj_index = next((com for com in self._allMacroComponents if macroID == com["ID"]), False)
		if obj_index != False:
			return obj_index
		return None

	def changeComponentID(self, lastID, ID):
		component = self.getComponentByID(lastID)
		if component != None:
			component["ID"] = ID
			self.setEdited()

	def contextMenuEvent(self, event):
		"""
			Emits when right click is push
			@param event as contextEvent
		"""
		item = self.itemAt(event.scenePos(), QTransform())
		if item == None:
			self.menuQuick.exec_(event.screenPos())

	def getAllDraw(self):
		return self._allDraw

	def addDraw(self, data):
		pos = QPointF(data["pos"]["X"], data["pos"]["Y"])
		size = QPointF(data["pos"]["W"], data["pos"]["H"])
		if data["type"] == "box":
			self._addRectangleFinal(pos, size)
		else:
			self._addEllipseFinal(pos, size)
		self.setEdited()

	def addEllipse(self):
		self._addingEllipse = True
		self._addingRectangle = False
		self._resetAddSignals()

	def _addEllipseFinal(self, pos, size = None):
		draw = RectangleObject(pos, type = "ellipse", size = size, parent = self)
		self.addItem(draw)
		self._allDraw.append(draw)

	def addRectangle(self):
		self._addingRectangle = True
		self._addingEllipse = False
		self._resetAddSignals()

	def _addRectangleFinal(self, pos, size = None):
		draw = RectangleObject(pos, type = "box", size = size, parent = self)
		self.addItem(draw)
		self._allDraw.append(draw)

	def deleteDraw(self, draw):
		self.removeItem(draw)
		self._allDraw.remove(draw)
		self.setEdited()

	### Drop Events
	def dropEvent(self, event):
		"""
			Emits when a drop is detected
			@param event as dropEvent
		"""
		if event.mimeData().hasText():
			text = event.mimeData().text()
			text_div = text.split("-")

			macroToAdd = None
			for macro in self._allMacroComponents:
				if macro["object"].isInPosition(event.scenePos()):
					macroToAdd = macro["object"]

			if text_div[0] == "macro":
				self.loadMacro(text_div[1], initialPos = event.scenePos(), macroToAdd = macroToAdd)
			elif text_div[0] == "component":
				clock = {
					"update" : "0"
				}
				self.addElement(text_div[1], clock = clock, initialPos = event.scenePos(), macroToAdd = macroToAdd)
				event.setAccepted(True)
			self.setEdited()

	def loadMacro(self, jsonFile, initialPos, macroToAdd = None):
		initialPos.setX(int(initialPos.x() / GRIDSPACE) * GRIDSPACE)
		initialPos.setY(int(initialPos.y() / GRIDSPACE) * GRIDSPACE)
		QApplication.instance().saveLoadMacro.loadFile(jsonFile)
		data = QApplication.instance().saveLoadMacro.loadProject()
		dataString = json.dumps(data)
		self._emptyLastSelected()
		positionMacro = QPointF(data["coords"]["X"], data["coords"]["Y"])

		appendText = "M" + str(self._macroIndex)
		macro, dataString = self.addLoadMacro(data, dataString, initialPos, appendText)

		data = json.loads(dataString)
		self.addLoadMacroSignals(data, initialPos, positionMacro, appendText)

		#for macroData in data["macros"]:
		#	print(macroData, "\n\n")
		#	newMacro = self.addLoadMacroSignals(macroData, initialPos, positionMacro, appendText)
		self.prevAddSignalsMacro(data, initialPos, appendText, positionMacro)

		if macroToAdd != None:
			macroToAdd.addComponents(None, [macro])

	def prevAddSignalsMacro(self, data, initialPos, appendText, positionMacroIn = None):
		positionMacro = QPointF(data["coords"]["X"], data["coords"]["Y"])
		for macroData in data["macros"]:
			newMacro = self.addLoadMacroSignals(macroData, initialPos, positionMacro + positionMacroIn - positionMacro, appendText)
		if len(data["macros"]) > 0:
			for macroData in data["macros"]:
				self.prevAddSignalsMacro(macroData, initialPos, appendText, positionMacroIn)

	def addLoadMacro(self, data, dataString, initialPos, appText, positionMacroIn = None, lastCoords = QPointF(0, 0)):
		myMacros = []
		positionMacro = QPointF(data["coords"]["X"], data["coords"]["Y"])

		if positionMacroIn == None:
			data["coords"]["X"] = initialPos.x()
			data["coords"]["Y"] = initialPos.y()
			positionMacroIn = positionMacro
		else:
			data["coords"]["X"] = initialPos.x() + positionMacro.x() - positionMacroIn.x() + lastCoords.x()
			data["coords"]["Y"] = initialPos.y() + positionMacro.y() - positionMacroIn.y() + lastCoords.y()


		for macroData in data["macros"]:
			newMacro, dataString = self.addLoadMacro(macroData, dataString, initialPos, appText, positionMacroIn = positionMacro, lastCoords = QPointF(positionMacro.x() - positionMacroIn.x(), positionMacro.y() - positionMacroIn.y()))
			myMacros.append(newMacro.getID())

		image = data["image"]

		components = []
		for comp in data["components"]:
			clock = comp["clock"]
			if clock == None:
				clock = {
					"update" : "0"
				}
			newPos = QPointF(float(comp["pos"][0]), float(comp["pos"][1]))
			newPos = initialPos + (newPos - positionMacroIn) + lastCoords
			self.addElement(os.path.join(get_pycompile_folder(), comp["file"]), clock = clock, initialPos = newPos, loadID = comp["id"] + appText, properties = comp["properties"], pins = comp["pins"])
			dataString = dataString.replace('"' + comp["id"] + '"', '"' + comp["id"] + appText + '"')
			components.append(comp["id"] + appText)

		ioMacro = {"inputs" : [], "outputs" : []}
		for io in data["ioMacro"]["inputs"]:
			ioMacro["inputs"].append({"ID" : io["ID"], "size" : io["size"]})
		for io in data["ioMacro"]["outputs"]:
			ioMacro["outputs"].append({"ID" : io["ID"], "size" : io["size"]})
		newMacro = self.addMacroComponent({"coords" : data["coords"], "IO" : ioMacro, "components" : components, "macros" : myMacros, "image" : image}, isNew = True)
		dataString = dataString.replace('"' + data["ID"] + '"', '"' + newMacro.getID() + '"')
		dataString = dataString.replace('"' + data["ID"] + '_', '"' + newMacro.getID() + '_')
		return newMacro, dataString

	def addLoadMacroSignals(self, data, initialPos, positionMacro, appendText):
		self._parent.loadSignals(data, appendText, (initialPos - positionMacro))

	def dragMoveEvent(self, event):
		"""
			Emits when a drop move is detected
			@param event as dropEvent
		"""
		pass

	def dragEnterEvent(self, event):
		"""
			Emits when a drop is called
			@param event as dropEvent
		"""
		if event.mimeData().hasText():
			event.setAccepted(True)

	def appendMacroComponent(self, component, macro):
		self._allWorkspaceComponents.append({"ID" : component.getID(), "object" : component, "macro" : macro})

	def addElement(self, elementJSonPath, clock = None, initialPos = None, loadID = None, properties = None, pins = None, macroToAdd = None):
		"""
			Add a new component to workspace
			@param elementXmlPath as str
		"""
		existsFile = returnExists(elementJSonPath)
		if existsFile:
			data = returnIconData(elementJSonPath)
			dataLoad = {
				"elementID" : self._componentIndex,
				"initialPos" : initialPos,
				"loadID" : loadID,
				"properties" : properties,
				"clock" : clock
			}
			element = WorkspaceObject(data, dataLoad, parent = self)
			if pins != None:
				element.setPinsSize(pins)
			self.addItem(element)
			data = dict()
			data["ID"] = element.elementID
			data["object"] = element
			self._allWorkspaceComponents.append(data)

			if loadID == None:
				self._componentIndex = self._componentIndex + 1

			if macroToAdd != None:
				macroToAdd.addComponents([element])

			self.setEdited()

			return element
		else:
			QApplication.instance().showError("The file " + str(elementJSonPath) + " doesn't exists")

	def addToPlot(self, signal):
		self._signalPlot.append(signal["ID"])
		self.addSignalShowValue(self.getSignalByID(signal["ID"])["object"])

	def deleteToPlot(self, signal):
		self._signalPlot.remove(signal)

	def deletePlotZero(self, signal):
		self._parent.deletePlot(signal)

	def addPlotZero(self, id):
		self._parent.addPlot(id)

	def hasGraph(self, ID):
		return ID in self._signalPlot

	def _resetAddSignals(self):
		"""
			Resets the temp line
		"""
		self._connA = None
		self._linePoints = []
		self._currentFolds = []
		self._tempLine.setLineP(0, 0, 0, 0)
		self._updateLines()

	def mouseDoubleClickEvent(self, event):
		"""
			Emits When the user click two times
			@param event as mouseevent
		"""
		if len(self._linePoints) > 0:
			pos = QPointF(0, 0)
			pos.setX(int(event.scenePos().x() / GRIDSPACE) * GRIDSPACE)
			pos.setY(int(event.scenePos().y() / GRIDSPACE) * GRIDSPACE)
			self.wireAction(None, pos)
		else:
			QGraphicsScene.mouseDoubleClickEvent(self, event)

	def wireAction(self, elementConnector, position = None):
		"""
			Define when click to a connector for add a wire
			@param elementConnector as elementConnector
		"""
		if len(self._linePoints) == 0:
			if type(elementConnector) == ConnectorObject:
				self._connA = elementConnector
				self._linePoints.append(self._connA.scenePos() + QPointF(3, 3))
				self._sizeTmnSignal = getSignalSize(self._connA.getSize())
				pen = QPen(Qt.black)
				pen.setWidth(self._sizeTmnSignal)
				self._tempLine.setPen(pen)
				return True

			"""elif type(elementConnector) == SubWire and elementConnector.getParent().isSignalTerminal():
				self._connA = elementConnector
				self._linePoints.append(position)
				self._sizeTmnSignal = getSignalSize(self._connA.getParent().getSize())
				pen = QPen(Qt.black)
				pen.setWidth(self._sizeTmnSignal)
				self._tempLine.setPen(pen)
				self._lastPosition = position
				return True"""
		else:
			connB = elementConnector
			connectedSignal = None
			conBMacro = None
			conAMacro = self._connA.getParent().getMacro()
			if connB != None and type(connB) == ConnectorObject:
				conBMacro = connB.getParent().getMacro()
				connectedSignal = connB.getConnectedSignal()
			elif connB != None and type(connB) == SubWire:
				conBMacro = connB.getParent().getConnections()[0].getParent().getMacro()
			else:
				conBMacro = conAMacro

			if self._connA != connB and conBMacro == conAMacro and connectedSignal == None:
				# Add wire to none
				if connB == None:
					terminal = signalTerminal(position)
					self.addItem(terminal)
					self.addSignal(connA = self._connA, connB = terminal, referenceMoveA = self._connA, referenceMoveB = terminal, folds = self._currentFolds)
					self._resetAddSignals()

					"""elif type(self._connA) == SubWire and type(connB) == SubWire:
						parentA = self._connA.getParent()
						parentB = connB.getParent()

						newFoldA = Fold(self._lastPosition, parent = parentA)
						newFoldA.setFromSignal(True)
						parentA.addFoldFunc(newFoldA)

						newFoldB = Fold(position, parent = parentB)
						newFoldB.setFromSignal(True)
						parentB.addFoldFunc(newFoldB)

						self.addSignal(connA = parentA.getConnections()[0], connB = parentB.getConnections()[0], referenceMoveA = newFoldA, referenceMoveB = newFoldB, folds = self._currentFolds)
						self._resetAddSignals()
					"""
				# Add a signal from connector to connector
				elif type(connB) == ConnectorObject and self._connA.getType() != connB.getType() and self._connA.getSize() == connB.getSize():
					if self._connA.getType() == "out":
						self.addSignal(connA = self._connA, connB = connB, referenceMoveA = self._connA, referenceMoveB = connB, folds = self._currentFolds)
					else:
						self._currentFolds.reverse()
						self.addSignal(connA = connB, connB = self._connA, referenceMoveA = connB, referenceMoveB = self._connA, folds = self._currentFolds)
					self._resetAddSignals()

				# Add a signal from connector to signal
				elif type(connB) == SubWire and type(self._connA) != SubWire:# and self._connA.getType() == "in":
					subSignalB = connB
					signalB = connB.getParent()
					if self._connA.getSize() <= signalB.getSize():
						if (type(signalB.getConnections()[1]) == signalTerminal and self._connA.getType() != signalB.getConnections()[0].getType()) or \
							(type(signalB.getConnections()[1]) != signalTerminal and self._connA.getType() == "in") or \
							self._connA.getType() == "in":
							modelB = signalB.getSignalBitsOwn()
							if not signalB.isSignalTerminal() or (signalB.isSignalTerminal() and (self._connA.getSize() <= signalB.getSize() - len(modelB) and self._connA.getType() == "out") or self._connA.getType() == "in"):

								model = None
								if signalB.isSignalTerminal() and self._connA.getType() == "out":
									model = list(range(signalB.getSize()))
									[model.remove(a) for a in modelB]
									model = model[0 : self._connA.getSize()]

								newFold = Fold(position, parent = signalB)
								newFold.setFromSignal(True)
								subSignalB.addFoldFunc(newFold)
								self._currentFolds.reverse()

								if signalB.isSignalSignal():
									self.addSignal(connA = signalB.getConnections()[1], connB = self._connA, referenceMoveA = newFold, referenceMoveB = self._connA, folds = self._currentFolds, signalReference = signalB, model = model)
								else:
									self.addSignal(connA = signalB.getConnections()[0], connB = self._connA, referenceMoveA = newFold, referenceMoveB = self._connA, folds = self._currentFolds, signalReference = signalB.getsignalReference(), model = model)

								self._resetAddSignals()
				return True
		return False

	def addSignal(self, connA, connB, referenceMoveA, referenceMoveB, folds, psignalID = None, signalReference = None, signalBits = None, indexF = None, model = None, isControlPath = False):
		if connB != None:
			if psignalID != None:
				signalID = psignalID
			else:
				self._signalIndex += 1
				signalID = "Signal_" + str(self._signalIndex)

			if connA != None:
				newSignal = Signal(connA = connA, connB = connB, folds = folds, signalID = signalID, referenceMoveA = referenceMoveA, referenceMoveB = referenceMoveB, signalReference = signalReference, signalBits = signalBits, indexF = indexF, isControlPath = isControlPath)
				self.addItem(newSignal)
				if model != None:
					newSignal.setSignalBits(model)

				if type(referenceMoveA) == Fold:
					referenceMoveA.moved.connect(newSignal.updateByConnect)
					referenceMoveA.deletedElement.connect(newSignal.delete)
				else:
					#connA.moved.connect(newSignal.updateByConnect)
					connA.deletedElement.connect(newSignal.delete)

				if type(connA) == ConnectorObject and not newSignal.isSignalSignal():
					connA.connectSignal(newSignal)

				if signalReference != None and signalReference.isSignalTerminal():
					signalReference.connectSignalTerminal(newSignal)
				elif signalReference != None and newSignal.isSignalSignal():
					signalReference.connectSignalTerminal(newSignal)

				if type(referenceMoveB) == Fold:
					referenceMoveB.moved.connect(newSignal.updateByConnect)
					referenceMoveB.deletedElement.connect(newSignal.delete)
				else:
					#connB.moved.connect(newSignal.updateByConnect)
					connB.deletedElement.connect(newSignal.delete)

				if type(connB) == ConnectorObject:
					connB.connectSignal(newSignal)

				self._allSignals.append({
					"ID" : signalID,
					"object" : newSignal
				})
				self.setEdited()

				return newSignal

	def removeWorkspaceObject(self, w_object):
		obj_index = next((item for item in self._allWorkspaceComponents if item["object"] == w_object), False)
		if obj_index != False:
			self._allWorkspaceComponents.remove(obj_index)
			self.removeItem(w_object)
			self.setEdited()

	def removeSignal(self, signal):
		signal_index = next((item for item in self._allSignals if item["object"] == signal), False)
		if signal_index != False:
			self._allSignals.remove(signal_index)
			self.removeItem(signal)
		obj_index = next((item for item in self._signalsShow if item["object"] == signal), False)
		if obj_index != False:
			self._signalsShow.remove(obj_index)
			self.setEdited()

	def removeTerminal(self, terminal):
		self.removeItem(terminal)
		self.setEdited()

	def _updateLines(self):
		"""
			Update the temporal lines in the workspace
		"""
		for line in self._allTmpLines:
			self.removeItem(line)
		self._allTmpLines = []
		i = 0
		pen = QPen(Qt.black)
		pen.setWidth(self._sizeTmnSignal)
		for line in self._linePoints:
			if i > 0:
				new_l = QGraphicsLineItem(self._linePoints[i-1].x(), self._linePoints[i-1].y(), self._linePoints[i].x(), self._linePoints[i].y())

				new_l.setPen(pen)

				self.addItem(new_l)
				self._allTmpLines.append(new_l)
			i += 1

	def mousePressEvent(self, event):
		"""
			Emits When the user press the mouse
			@param event as mouseevent
		"""
		for item in self._lastSelected["components"] + self._lastSelected["macros"]:
			item.deselect()
		self._emptyLastSelected()
		self._selectCurrent = None
		self._square.setVisible(False)
		self._selectStart = event.scenePos()
		item = self.itemAt(event.scenePos(), QTransform())
		if item != None:
			self._selectStart = None
		if item == None or item == self._tempLine:
			pos = QPointF(0, 0)
			pos.setX(int(event.scenePos().x() / GRIDSPACE) * GRIDSPACE)
			pos.setY(int(event.scenePos().y() / GRIDSPACE) * GRIDSPACE)
			if len(self._linePoints) > 0:
				self._linePoints.append(pos)
				self._updateLines()

				newFold = Fold(pos)
				self._currentFolds.append(newFold)
			elif self._addingRectangle:
				self._addingRectangle = False
				self._addRectangleFinal(pos)
			elif self._addingEllipse:
				self._addingEllipse = False
				self._addEllipseFinal(pos)
			else:
				QGraphicsScene.mousePressEvent(self, event)
		else:
			QGraphicsScene.mousePressEvent(self, event)

	def mouseMoveEvent(self, event):
		"""
			Emits when the user moves the mouse across the workspace
			@param event as mouseevent
		"""
		if len(self._linePoints) > 0:
			line = self._linePoints[-1]
			x = (int(event.scenePos().x() / GRIDSPACE) * GRIDSPACE)
			y = (int(event.scenePos().y() / GRIDSPACE) * GRIDSPACE)
			self._tempLine.setLineP(line.x(), line.y(), x, y)
		else:
			if self._selectStart != None:
				self._selectCurrent = event.scenePos()
				self._square.setVisible(True)
				self._square.setRect(self._selectStart.x(), self._selectStart.y(), self._selectCurrent.x() - self._selectStart.x(), self._selectCurrent.y() - self._selectStart.y())
		QGraphicsScene.mouseMoveEvent(self, event)

	def mouseReleaseEvent(self, event):
		"""
			Emits When the user releases the mouse
			@param event as mouseevent
		"""
		self._square.setVisible(False)
		if self._selectCurrent != None:
			x = min(self._selectStart.x(), self._selectCurrent.x())
			y = min(self._selectStart.y(), self._selectCurrent.y())
			w = abs(self._selectStart.x() - self._selectCurrent.x())
			h = abs(self._selectStart.y() - self._selectCurrent.y())
			self._lastCoords = [x, y, w, h]
			allItems = self.items(x, y, w, h, Qt.ContainsItemShape, Qt.AscendingOrder)
			self._emptyLastSelected()
			for item in allItems:
				if type(item) == WorkspaceObject:
					item.select()
					self._lastSelected["components"].append(item)
				elif type(item) == MacroComponent:
					self._lastSelected["macros"].append(item)
		self._selectStart = None
		self._selectCurrent = None
		QGraphicsScene.mouseReleaseEvent(self, event)

	def keyPressEvent(self, event):
		"""
			Detect the key press
			@param event as QKeyEvent
		"""
		if event.key() == Qt.Key_Escape:
			if len(self._linePoints) > 0:
				self._resetAddSignals()
			self._addingRectangle = False
			self._addingEllipse = False
		elif event.key() == Qt.Key_Delete:
			for item in self._lastSelected["components"] + self._lastSelected["macros"]:
				item.delete()
			self._emptyLastSelected()

		QGraphicsScene.keyPressEvent(self, event)

	def _emptyLastSelected(self):
		 self._lastSelected = {
		 	"components" : [],
			"macros" : []
		 }

	@pyqtSlot(list)
	def UpdateGuiSimulation(self, data):
		"""
			Updates the elements gui
			@param data as dict
		"""
		sim_time = 0
		for signal in data:
			if signal["ID"] == "simulation_time":
				sim_time = int(signal["value"])
			else:
				update_wire = next(si for si in self._signalsShow if signal["ID"] == si["ID"])
				update_wire["object"].updateGui(signal["value"], sim_time)

				if signal["ID"] in self._signalPlot:
					self._parent.updatePlot(signal["value"], signal["ID"])
