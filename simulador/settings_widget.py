#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries
from simulador.menus import MenuNewProject
from simulador.tools.utils import projectExtension

# Qt libraries
from PyQt5.QtWidgets import QWidget, QApplication
from PyQt5.QtCore import Qt, QSize
from PyQt5.QtGui import QPixmap, QIcon
from PyQt5 import uic

# System libraries
import os
import time

class SettingsWidget(QWidget):

	def __init__(self):
		super(QWidget, self).__init__()
		uic.loadUi(QApplication.instance().get_src("ui", "settings.ui"), self)
		self.logoLabel.setPixmap(QPixmap(QApplication.instance().get_src("images", "logo.png")).scaled(QSize(180, 180), Qt.KeepAspectRatio))

		self.updateComponentsBtn.clicked.connect(self.updateComponentsFnc)
		self.progressUpdate.setValue(0)

		self.checkDark.stateChanged.connect(self._setDarkTheme)
		self.checkDark.setChecked(QApplication.instance().settingsHandler.getDarkTheme())

		# Language initialization
		lang = QApplication.instance().settingsHandler.getLanguage()
		if lang == "esp":
			self.languageCombo.setCurrentIndex(0)
		elif lang == "eng":
			self.languageCombo.setCurrentIndex(1)
		self.languageCombo.currentIndexChanged.connect(self._changeLanguage)

		self.defaultDelayTime.valueChanged.connect(self._changeDefaultTime)
		self.defaultDelayTime.setValue(QApplication.instance().settingsHandler.getDefaultTime())

		# Icons
		self.updateComponentsBtn.setIcon(QIcon(QPixmap(QApplication.instance().get_src("images", "icons", "update.png"))))

	def _changeLanguage(self):
		index = self.languageCombo.currentIndex()
		if index == 0:
			lang = "esp"
		else:
			lang = "eng"
		QApplication.instance().setLanguage(lang)

	def _changeDefaultTime(self):
		QApplication.instance().settingsHandler.setDefaultTime(self.defaultDelayTime.value())

	def _setDarkTheme(self):
		QApplication.instance().settingsHandler.setDarkTheme(self.checkDark.isChecked())

	def updateComponentsFnc(self):
		self.progressUpdate.setValue(0)
		time.sleep(0.1)
		QApplication.instance().componentsHandler.updateComponents()
		self.progressUpdate.setValue(100)
