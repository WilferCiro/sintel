#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Custom libraries

# Qt libraries
from PyQt5.QtWidgets import QSizePolicy, QApplication, QSizePolicy, QWidget, QGraphicsItem, QGraphicsScene, QGraphicsRectItem, QGraphicsView, QVBoxLayout, QGraphicsTextItem, QLabel
from PyQt5 import uic
from PyQt5.QtGui import QPainter, QPen
from PyQt5.QtCore import Qt, QRectF

# System libraries
#from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
#from matplotlib.figure import Figure
#import matplotlib.pyplot as plt
import sys
import random
#import numpy as np

#plt.style.use('dark_background')

class PlotWidget(QWidget):
	def __init__(self):
		super(QWidget, self).__init__()
		uic.loadUi(QApplication.instance().get_src("ui", "plot.ui"), self)

		widthCanvas = 950
		self.plotCanvas = PlotCanvas(widthCanvas)
		sideView = QGraphicsView(self.plotCanvas)
		sideView.setRenderHints(QPainter.Antialiasing)

		sideView.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
		sideView.setAlignment(Qt.AlignLeft | Qt.AlignTop)
		self.layoutPlot.addWidget(sideView)

		#self.signalLabel.setText(str(signalID))
	def removeSignal(self, signalID):
		self.plotCanvas.removeSignal(signalID)

	def addSignal(self, signalID):
		self.plotCanvas.addSignal(signalID)

	def plotValue(self, value, signalID):#, time, size):
		self.plotCanvas.plotValue(value, signalID)

	def emptyAll(self):
		self.plotCanvas.emptyAll()

class PlotCanvas(QGraphicsScene):

	def __init__(self, width):
		super(QGraphicsScene, self).__init__()

		self._boxNumber = 0
		self._boxWidth = 40
		self._boxHeight = 20
		self._maxWidth = width

		#self.obj_index["lastValue"] = None
		self._allSignals = []

	def addSignal(self, signalID):
		self._allSignals.append({
			"signalID" : signalID,
			"allValues" : [],
			"allRects" : [],
			"allTexts" : [],
			"lastValue" : None,
			"titleText" : None
		})

	def removeSignal(self, signalID):
		obj_index = next((com for com in self._allSignals if signalID == com["signalID"]), False)
		if obj_index != False:
			self._allSignals.remove(obj_index)

	def plotValue(self, value, signalID):
		value = str(value)

		obj_index = next((com for com in self._allSignals if signalID == com["signalID"]), False)
		if obj_index != False:
			nro_obj = self._allSignals.index(obj_index)

			if value == obj_index["lastValue"]:
				obj_index["allValues"][-1]["size"] += 1
			else:
				obj_index["allValues"].append({
					"size" : 1,
					"value" : value
				})
			obj_index["lastValue"] = value

			self.empty(obj_index["signalID"], False)

			text = QGraphicsTextItem(str(obj_index["signalID"]))
			text.setPos(0, nro_obj * self._boxHeight)
			f = text.font()
			f.setFamily("Cantarell")
			f.setPointSize(7)
			text.setFont(f)
			self.addItem(text)
			obj_index["titleText"] = text

			boxNumber = 0
			for val_int in obj_index["allValues"]:
				new_size = self._boxWidth * int(val_int["size"])
				pen = QPen(Qt.green)
				if "U" in val_int["value"] or "Z" in val_int["value"]:
					pen = QPen(Qt.red)
				rect = QGraphicsRectItem(0, 0, new_size, self._boxHeight)
				rect.setPos(boxNumber * self._boxWidth + 80, nro_obj * self._boxHeight)
				rect.setPen(pen)
				self.addItem(rect)

				text = QGraphicsTextItem(str(val_int["value"]))
				text.setPos(boxNumber * self._boxWidth + 80, nro_obj * self._boxHeight)
				f = text.font()
				f.setFamily("Cantarell")
				f.setPointSize(7)
				text.setFont(f)
				self.addItem(text)
				obj_index["allTexts"].append(text)
				obj_index["allRects"].append(rect)

				boxNumber += int(val_int["size"])

			if boxNumber > 30:
				obj_index["allValues"][0]["size"] -= 1
				if obj_index["allValues"][0]["size"] == 0:
					obj_index["allValues"].pop(0)

		"""rect = QGraphicsRectItem(self._boxNumber * self._boxWidth, 0, self._boxWidth, self._boxHeight)
		if value == self.obj_index["lastValue"]:
			#rect.setLeftLine(-1)
			self.obj_index["allRects"][-1].setLeftLine(-1)
			#rect.setRect(QRectF(0, 0, last_rect.rect().width() + self._boxWidth, self._boxHeight))
		self.addItem(rect)
		color = Qt.red
		if "U" in value:
			text = QGraphicsTextItem("U")
		elif "Z" in value:
			text = QGraphicsTextItem("Z")
		else:
			color = Qt.green
			text = QGraphicsTextItem(str(value))
		pen = QPen(color)
		rect.setPen(pen)

		text.setPos(self._boxNumber * self._boxWidth, 0)
		f = text.font()
		f.setFamily("Cantarell")
		f.setPointSize(7)
		text.setFont(f)
		self.addItem(text)
		self.obj_index["allRects"].append(rect)
		self.obj_index["allTexts"].append(text)

		if self._boxNumber * self._boxWidth > self._maxWidth:
			self.removeItem(self.obj_index["allRects"][0])
			self.removeItem(self.obj_index["allTexts"][0])
			self.obj_index["allRects"].pop(0)
			self.obj_index["allTexts"].pop(0)
			index = 0
			for rectIn in self.obj_index["allRects"]:
				rectIn.setRect(index * self._boxWidth, 0, self._boxWidth, self._boxHeight)
				index += 1
			index = 0
			for textIn in self.obj_index["allTexts"]:
				textIn.setPos(index * self._boxWidth, 0)
				index += 1
		else:
			self._boxNumber += 1

		self.obj_index["lastValue"] = value"""

	def emptyAll(self):
		for signal in self._allSignals:
			self.empty(signal["signalID"])

	def empty(self, signalID, empty_values = True):
		signal = next((com for com in self._allSignals if signalID == com["signalID"]), False)
		if signal != False:
			for rect in signal["allRects"]:
				self.removeItem(rect)
			for text in signal["allTexts"]:
				self.removeItem(text)
			signal["allRects"] = []
			signal["allTexts"] = []
			if signal["titleText"] != None:
				self.removeItem(signal["titleText"])
				signal["titleText"] = None
			if empty_values:
				signal["allValues"] = []
				signal["lastValue"] = None

	#def plotValue(self, value, time, size):
	#	self.plotCanvas.plotValue(value, time, size)

	#def empty(self):
	#	self.plotCanvas.empty()

"""class PlotCanvas(FigureCanvas):

	def __init__(self, parent=None, width=4, height=4, dpi=30):
		fig = Figure(figsize=(width, height), dpi=dpi)

		FigureCanvas.__init__(self, fig)
		self.setParent(parent)

		FigureCanvas.setSizePolicy(self,
			QSizePolicy.Expanding,
			QSizePolicy.Expanding)
		FigureCanvas.updateGeometry(self)

		self.figure.subplots_adjust(left=0, right=1, top=1, bottom=0)
		self.ax = self.figure.add_subplot(111)
		self.ax.axis('off')

		self.data = []
		self.dataTime = []
		self._savedData = []
		#self.plotValue(0, 0)

	def plotValue(self, value, time, size):
		if len(self.data) < 600:
			self.ax.cla()
			if size > 1:
				#value
				if len(self._savedData) > 0 and self._savedData[-1] == value:
					self.data.append(4)
					self.ax.text(100, 0, str("value"), fontsize = 20)
				else:
					self.data.append(0)
					self.data.append(4)
					#self.ax.text(0, 0, str(value), fontsize = 16)
					self.dataTime.append(time - 2)
			else:
				self.data.append(value)

			self._savedData.append(value)
			self.dataTime.append(time)
			self.ax.plot(self.dataTime, self.data, 'g-')
			if size > 1:
				self.ax.fill_between(self.dataTime, 0, self.data, facecolor = 'none', edgecolor="g", hatch="/")
			self.draw_idle()

	def empty(self):
		self.data = []
		self.dataTime = []
		self.ax.cla()
		self.draw_idle()
"""
