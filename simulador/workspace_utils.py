#!/usr/bin/env python

'''
	Copyright (c) 2018 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Qt classes
from PyQt5.QtWidgets import QGraphicsLineItem, QGraphicsScene

class TempLine(QGraphicsLineItem):
	"""
		Connector for the elements
	"""
	
	def __init__(self, parent = None):
		"""
			Init the connector
			@param data as dict
			@param parent as QGrapicsItem
		"""
		self._parent = parent
		super(QGraphicsLineItem, self).__init__(0, 0, 0, 0, None)
	
	def setLineP(self, x1, y1, x2, y2):
		self.setLine(x1, y1, x2, y2)
		
		
