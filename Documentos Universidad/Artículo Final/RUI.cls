%% 
%% It may be distributed under the conditions of the LaTeX Project Public
%% License, either version 1.2 of this license or (at your option) any
%% later version.  The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%% and version 1.2 or later is part of all distributions of LaTeX
%% version 1999/12/01 or later.
%% 
%% The list of all files belonging to the 'RUI Bundle' is
%% given in the file `manifest.txt'.
%% 
%% TO FIX: 
%% requires txfonts: Times math support
%
%% %RUIEDIT: EDITABLE PARTS


 \def\RCSfile{RUI}%
 \def\RCSversion{1.0}%
 \def\RCSdate{2014/02/03}%
 \def\@shortjnl{\relax}
 \def\@journal{Revista UIS Ingenierias} \def\@company{Publicaciones UIS}
 \def\@issn{2145-8456}
 \def\@shortjid{RUI}
\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{\@shortjid}[\RCSdate, \RCSversion: \@journal]
\def\ABD{\AtBeginDocument}
\newif\ifpreprint \preprintfalse
\newif\iflongmktitle \longmktitlefalse
\newif\ifnopreprintline \nopreprintlinefalse

\def\@blstr{1}
\newdimen\@bls
\@bls=\baselineskip

\def\@finalWarning{%
  *****************************************************\MessageBreak
   This document is typeset in the CRC style which\MessageBreak
   is not suitable for submission.\MessageBreak
   \MessageBreak
   Please typeset again using 'preprint' option\MessageBreak
   for creating PDF suitable for submission.\MessageBreak
  ******************************************************\MessageBreak
}

\DeclareOption{procedia}{\global\artUIS@nsmodeltrue}
\newif\ifartUIS@nsmodel\artUIS@nsmodelfalse
\DeclareOption{preprint}{\global\preprinttrue
  \gdef\@blstr{1}\xdef\jtype{0}%
   \AtBeginDocument{\@twosidetrue\@mparswitchfalse}}
\DeclareOption{nopreprintline}{\global\nopreprintlinetrue}
\DeclareOption{final}{\@twosidetrue\gdef\@blstr{1}\global\preprintfalse}
\DeclareOption{review}{\global\preprinttrue\gdef\@blstr{1.5}}
\DeclareOption{authoryear}{\xdef\@biboptions{round,authoryear}}
\DeclareOption{number}{\xdef\@biboptions{numbers}}
\DeclareOption{numbers}{\xdef\@biboptions{numbers}}
\DeclareOption{longtitle}{\global\longmktitletrue}
\DeclareOption{5p}{\xdef\jtype{5}\global\preprintfalse\global\nopreprintlinetrue
  \ExecuteOptions{twocolumn,twoside}\AtBeginDocument{\usepackage[pdflatex,center]{crop}}}
  \def\jtype{0}
\DeclareOption{3p}{\xdef\jtype{3}\global\preprintfalse
\ExecuteOptions{twocolumn,twoside}}
\DeclareOption{1p}{\xdef\jtype{1}\gdef\@blstr{1.5}\global\preprintfalse\AtBeginDocument{\@twocolumnfalse}}
\DeclareOption{times}{\IfFileExists{txfonts.sty}%
  {\AtEndOfClass{\RequirePackage{txfonts}%
  \gdef\ttdefault{cmtt}%
   \let\iint\relax
  \let\iiint\relax
  \let\iiiint\relax
  \let\idotsint\relax
  \let\openbox\relax}}{\AtEndOfClass{\RequirePackage{times}}}}
\ExecuteOptions{letter,10pt,oneside,onecolumn,number,preprint}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions
\LoadClass{article}
\RequirePackage{graphicx}
\let\comma\@empty
\let\tnotesep\@empty
\def\title#1{\gdef\@title{#1}}
\let\@title\@empty

\def\uisLabel#1{\@bsphack\protected@write\@auxout{}%
         {\string\Newlabel{#1}{\@currentlabel}}\@esphack}
\def\Newlabel#1#2{\expandafter\xdef\csname X@#1\endcsname{#2}}

\def\uisRef#1{\@ifundefined{X@#1}{0}{\csname X@#1\endcsname}%
}

\def\tnotemark[#1]{\textsuperscript{\@for\@@tmark:=#1\do{%
      \edef\tnotenum{\@ifundefined{X@\@@tmark}{1}{\uisRef{\@@tmark}}}%
      \ifcase\tnotenum\or\ding{73}\or,\ding{73}\ding{73}\fi}}%
}
\let\@tnotemark\@empty

\let\@tnotes\@empty
\RequirePackage{pifont}
\newcounter{tnote}
\def\tnotetext[#1]#2{\g@addto@macro\@tnotes{%
  \refstepcounter{tnote}\uisLabel{#1}%
   \def\thefootnote{\ifcase\c@tnote\or\ding{73}\or\ding{73}\ding{73}\fi}%
    \footnotetext{#2}}}

\let\@nonumnotes\@empty
\def\nonumnote#1{\g@addto@macro\@nonumnotes{%
     \let\thefootnote\relax\footnotetext{#1}}}

\newcounter{fnote}
\def\fnmark[#1]{\let\comma\@empty
    \def\@fnmark{\@for\@@fnmark:=#1\do{%
    \edef\fnotenum{\@ifundefined{X@\@@fnmark}{1}{\uisRef{\@@fnmark}}}%
  \unskip\comma\fnotenum\let\comma,}}%
}

\let\@fnotes\@empty\let\@fnmark\@empty
\def\fntext[#1]#2{\g@addto@macro\@fnotes{%
     \refstepcounter{fnote}\uisLabel{#1}%
   \def\thefootnote{\thefnote}%
   \global\setcounter{footnote}{\thefnote}%
   \footnotetext{#2}}}

\def\cormark[#1]{\edef\cnotenum{\uisRef{#1}}%
    \unskip\textsuperscript{\sep\ifcase\cnotenum\or
       $\ast$\or$\ast\ast$\fi\hspace{-1pt}}\let\sep=,}

\let\@cormark\@empty
\let\@cornotes\@empty
\newcounter{cnote}
\def\cortext[#1]#2{\g@addto@macro\@cornotes{%
    \refstepcounter{cnote}\uisLabel{#1}%
    \def\thefootnote{\ifcase\thecnote\or$\ast$\or
    $\ast\ast$\fi}%
    \footnotetext{#2}}}

\let\@corref\@empty
\def\corref#1{\edef\cnotenum{\uisRef{#1}}%
    \edef\@corref{\ifcase\cnotenum\or
         $\ast$\or$\ast\ast$\fi\hskip-1pt}}

\def\fnref#1{\fnmark[#1]}
\def\tnoteref#1{\tnotemark[#1]}

\def\resetTitleCounters{\c@cnote=0
   \c@fnote=0 \c@tnote=0 \c@footnote=0}

\let\eadsep\@empty
\let\@uiseads\@empty
\let\@uisuads\@empty
\let\@cormark\@empty
\def\hashchar{\expandafter\@gobble\string\~}
\def\underscorechar{\expandafter\@gobble\string\_}
\def\lbracechar{\expandafter\@gobble\string\{}
\def\rbracechar{\expandafter\@gobble\string\}}

\def\ead{\@ifnextchar[{\@uad}{\@ead}}
\gdef\@ead#1{\bgroup\def\_{\string\underscorechar\space}%
  \def\{{\string\lbracechar\space}%
   \def~{\hashchar\space}%
   \def\}{\string\rbracechar\space}%
   \edef\tmp{\the\@eadauthor}
   \immediate\write\@auxout{\string\emailauthor
     {#1}{\expandafter\strip@prefix\meaning\tmp}}%
  \egroup
}
\newcounter{ead}
\gdef\emailauthor#1#2{\stepcounter{ead}%
     \g@addto@macro\@uiseads{\raggedright%
      \let\corref\@gobble
      \eadsep\texttt{#1} (#2)\def\eadsep{\unskip,\space}}%
}
\gdef\@uad[#1]#2{\bgroup
  \def~{\string\hashchar\space}%
  \def\_{\string\underscorechar\space}%
   \edef\tmp{\the\@eadauthor}
   \immediate\write\@auxout{\string\urlauthor
     {#2}{\expandafter\strip@prefix\meaning\tmp}}%
  \egroup
}
\def\urlauthor#1#2{\g@addto@macro\@uisuads{\let\corref\@gobble%
     \raggedright\eadsep\texttt{#1}\space(#2)%
     \def\eadsep{\unskip,\space}}%
}

\def\titulo#1{\gdef\@titulo{#1}}
\def\@titulo{\@latex@error{No \noexpand\titulo given}\@ehc}

\def\doi#1{\gdef\@doi{#1}}
\def\@doi{\@latex@error{No \noexpand\doi given}\@ehc}

\def\anno#1{\gdef\@anno{#1}}
\def\@anno{\@latex@error{No \noexpand\anno given}\@ehc}

\def\faculty#1{\gdef\@faculty{#1}}
\def\@faculty{\@latex@error{No \noexpand\faculty given}\@ehc}

\def\numero#1{\gdef\@numero{#1}}
\def\@numero{\@latex@error{No \noexpand\faculty given}\@ehc}

\def\received#1{\gdef\@received{#1}}
\def\@received{\@latex@error{No \noexpand\received given}\@ehc}

\def\accepted#1{\gdef\@accepted{#1}}
\def\@accepted{\@latex@error{No \noexpand\accepted given}\@ehc}

\def\published#1{\gdef\@published{#1}}
\def\@published{\@latex@error{No \noexpand\accepted given}\@ehc}

\def\uisauthors{}
\def\pprinttitle{}
\let\authorsep\@empty
\let\sep\@empty
\newcounter{author}
\def\author{\@ifnextchar[{\@@author}{\@author}}

\newtoks\@eadauthor
\def\@@author[#1]#2{\g@addto@macro\uisauthors{%
    \def\baselinestretch{1}%
    \authorsep#2\unskip\textsuperscript{%#1%
      \@for\@@affmark:=#1\do{%
       \edef\affnum{\@ifundefined{X@\@@affmark}{1}{\uisRef{\@@affmark}}}%
     \unskip\sep\affnum\let\sep=,}%
      \ifx\@fnmark\@empty\else\unskip\sep\@fnmark\let\sep=,\fi
      \ifx\@corref\@empty\else\unskip\sep\@corref\let\sep=,\fi
      }%
    \def\authorsep{\unskip,\space}%
    \global\let\sep\@empty\global\let\@corref\@empty
    \global\let\@fnmark\@empty}%
    \@eadauthor={#2}
}

\def\@author#1{\g@addto@macro\uisauthors{\normalsize%
    \def\baselinestretch{1}%
    \upshape\authorsep#1\unskip\textsuperscript{%
      \ifx\@fnmark\@empty\else\unskip\sep\@fnmark\let\sep=,\fi
      \ifx\@corref\@empty\else\unskip\sep\@corref\let\sep=,\fi
      }%
    \def\authorsep{\unskip,\space}%
    \global\let\@fnmark\@empty
    \global\let\@corref\@empty
    \global\let\sep\@empty}%
    \@eadauthor={#1}
}

\def\uisaddress{}
\def\addsep{\par\vskip6pt}
\def\address{\@ifnextchar[{\@@address}{\@address}}

\def\@alph#1{%
  \ifcase#1\or a\or b\or c\or d\or e\or f\or g\or h\or i\or j\or k\or
  l\or m\or n\or o\or p\or q\or r\or s\or t\or u\or v\or w\or x\or
  y\or z%
  \or aa\or ab\or ac\or ad\or ae\or af\or ag\or ah\or ai\or aj\or
  ak\or al\or am\or an\or ao\or ap\or aq\or ar\or as\or at\or au\or
  av\or aw\or ax\or ay\or az%
  \or ba\or bb\or bc\or bd\or be\or bf\or bg\or bh\or bi\or bj\or
  bk\or bl\or bm\or bn\or bo\or bp\or bq\or br\or bs\or bt\or bu\or
  bv\or bw\or bx\or by\or bz%
  \or ca\or cb\or cc\or cd\or ce\or cf\or cg\or ch\or ci\or cj\or
  ck\or cl\or cm\or cn\or co\or cp\or cq\or cr\or cs\or ct\or cu\or
  cv\or cw\or cx\or cy\or cz%
  \or da\or db\or dc\or dd\or de\or df\or dg\or dh\or di\or dj\or
  dk\or dl\or dm\or dn\or do\or dp\or dq\or dr\or ds\or dt\or du\or
  dv\or dw\or dx\or dy\or dz%
  \or ea\or eb\or ec\or ed\or ee\or ef\or eg\or eh\or ei\or ej\or
  ek\or el\or em\or en\or eo\or ep\or eq\or er\or es\or et\or eu\or
  ev\or ew\or ex\or ey\or ez%
  \or fa\or fb\or fc\or fd\or fe\or ff\or fg\or fh\or fi\or fj\or
  fk\or fl\or fm\or fn\or fo\or fp\or fq\or fr\or fs\or ft\or fu\or
  fv\or fw\or fx\or fy\or fz%
  \or ga\or gb\or gc\or gd\or ge\or gf\or gg\or gh\or gi\or gj\or
  gk\or gl\or gm\or gn\or go\or gp\or gq\or gr\or gs\or gt\or gu\or
  gv\or gw\or gx\or gy\or gz%
  \else\@ctrerr\fi}

\newcounter{affn}
\renewcommand\theaffn{\alph{affn}}

\long\def\@@address[#1]#2{\g@addto@macro\uisaddress{%
    \def\baselinestretch{1}%
     \refstepcounter{affn}
     \xdef\@currentlabel{\theaffn}
     \uisLabel{#1}%
    \textsuperscript{\theaffn}#2\par}}

\long\def\@address#1{\g@addto@macro\uisauthors{%
    \def\baselinestretch{1}%
    \addsep\footnotesize\itshape#1\def\addsep{\par\vskip6pt}%
    \def\authorsep{\par\vskip8pt}}}

\newbox\resbox
\newenvironment{resumen}{\global\setbox\resbox=\vbox\bgroup
  \hsize=\textwidth\def\baselinestretch{1}%
  \noindent\unskip \textbf{RESUMEN} 
 \par\medskip\noindent\unskip\ignorespaces}
 {\egroup}

\newbox\absbox
\renewenvironment{abstract}{\global\setbox\absbox=\vbox\bgroup
  \hsize=\textwidth\def\baselinestretch{1}%
  \noindent\unskip \textbf{ABSTRACT} 
 \par\medskip\noindent\unskip\ignorespaces}
 {\egroup}
 
\newbox\keybox
\def\keyword{%
  \def\sep{\unskip, }%
 \def\MSC{\@ifnextchar[{\@MSC}{\@MSC[2000]}}
  \def\@MSC[##1]{\par\leavevmode\hbox {\it ##1~MSC:\space}}%
  \def\PACS{\par\leavevmode\hbox {\it PACS:\space}}%
  \def\JEL{\par\leavevmode\hbox {\it JEL:\space}}%
  \global\setbox\keybox=\vbox\bgroup\hsize=\textwidth
  \normalsize\normalfont\def\baselinestretch{1}
  \parskip\z@
  \noindent\textit{Keywords: }
  \raggedright                         % Keywords are not justified.
  \ignorespaces}
\def\endkeyword{\par \egroup}

\newbox\clavesbox
\def\claves{%
  \def\sep{\unskip, }%
 \def\MSC{\@ifnextchar[{\@MSC}{\@MSC[2000]}}
  \def\@MSC[##1]{\par\leavevmode\hbox {\it ##1~MSC:\space}}%
  \def\PACS{\par\leavevmode\hbox {\it PACS:\space}}%
  \def\JEL{\par\leavevmode\hbox {\it JEL:\space}}%
  \global\setbox\clavesbox=\vbox\bgroup\hsize=\textwidth
  \normalsize\normalfont\def\baselinestretch{1}
  \parskip\z@
  \noindent\textit{Palabras clave: }
  \raggedright                         % Keywords are not justified.
  \ignorespaces}
\def\endclaves{\par \egroup}

\newdimen\Columnwidth
\Columnwidth=\columnwidth

\def\printFirstPageNotes{%
  \iflongmktitle
   \let\columnwidth=\textwidth\fi
  \ifx\@tnotes\@empty\else\@tnotes\fi
  \ifx\@nonumnotes\@empty\else\@nonumnotes\fi
  \ifx\@cornotes\@empty\else\@cornotes\fi
  \ifx\@uiseads\@empty\relax\else
   \let\thefootnote\relax
   \footnotetext{\ifnum\theead=1\relax
      \textit{Email address:\space}\else
      \textit{Email addresses:\space}\fi
     \@uiseads}\fi
  \ifx\@uisuads\@empty\relax\else
   \let\thefootnote\relax
   \footnotetext{\textit{URL:\space}%
     \@uisuads}\fi
  \ifx\@fnotes\@empty\else\@fnotes\fi
  \iflongmktitle\if@twocolumn
   \let\columnwidth=\Columnwidth\fi\fi
}


\long\def\pprintMaketitle{\clearpage
  \iflongmktitle\if@twocolumn\let\columnwidth=\textwidth\fi\fi
  \resetTitleCounters
  \def\baselinestretch{1}%
  \printFirstPageNotes
  \begin{center}%
 	\thispagestyle{pprintTitle}%
   	\def\baselinestretch{1}%
    \Large\bfseries\@titulo\par\vskip10pt
    \hrule height 2pt
    \par\vskip10pt
    \Large\bfseries\@title\par\vskip10pt
    \normalsize\uisauthors\par\vskip10pt
    \footnotesize\itshape\uisaddress\par\vskip36pt
    \hrule\vskip12pt
    \ifvoid\resbox\else\unvbox\resbox\par\vskip10pt\fi
    \ifvoid\clavesbox\else\unvbox\clavesbox\par\vskip10pt\fi
    \ifvoid\absbox\else\unvbox\absbox\par\vskip10pt\fi
    \ifvoid\keybox\else\unvbox\keybox\par\vskip10pt\fi
    \hrule\vskip12pt
    \end{center}%
  	\gdef\thefootnote{\arabic{footnote}}%
  }

\def\printWarning{%
     \mbox{}\par\vfill\par\bgroup
     \fboxsep12pt\fboxrule1pt
     \hspace*{.18\textwidth}
     \fcolorbox{gray50}{gray10}{\box\warnbox}
     \egroup\par\vfill\thispagestyle{empty}
     \setcounter{page}{0}
     \clearpage}

\long\def\finalMaketitle{%
  \resetTitleCounters
  \def\baselinestretch{1}%
   \MaketitleBox
   \thispagestyle{pprintTitle}%
  \gdef\thefootnote{\arabic{footnote}}%
  }

\def\FNtext#1{\par\bgroup\footnotesize#1\egroup}
\newdimen\space@left
\def\alarm#1{\typeout{******************************}%
             \typeout{#1}%
             \typeout{******************************}%
}
\long\def\getSpaceLeft{%\global\@twocolumnfalse%
   \global\setbox0=\vbox{\hsize=\textwidth\MaketitleBox}%
   \global\setbox1=\vbox{\hsize=\textwidth
    \let\footnotetext\FNtext
    \printFirstPageNotes}%
    \xdef\noteheight{\the\ht1}%
    \xdef\titleheight{\the\ht0}%
    \@tempdima=\vsize
    \advance\@tempdima-\noteheight
    \advance\@tempdima-1\baselineskip
}

  \skip\footins=24pt

\newbox\uis@boxa
\newbox\uis@boxb

\ifpreprint
  \def\maketitle{\pprintMaketitle}
\else
   \ifnum\jtype=1
      \def\maketitle{%
        \iflongmktitle\getSpaceLeft
          \global\setbox\uis@boxa=\vsplit0 to \@tempdima
          \box\uis@boxa\par\resetTitleCounters
          \thispagestyle{pprintTitle}%
          \printFirstPageNotes
          \box0%
        \else
          \finalMaketitle\printFirstPageNotes
        \fi
       \gdef\thefootnote{\arabic{footnote}}}%

    \else
      \ifnum\jtype=5
        \def\maketitle{%
        \iflongmktitle\getSpaceLeft
          \global\setbox\uis@boxa=\vsplit0 to \@tempdima
          \box\uis@boxa\par\resetTitleCounters
          \thispagestyle{pprintTitle}%
          \printFirstPageNotes
          \twocolumn[\box0]%\printFirstPageNotes
       \else
         \twocolumn[\finalMaketitle]\printFirstPageNotes
       \fi
       \gdef\thefootnote{\arabic{footnote}}}
    \else
    \if@twocolumn
      \def\maketitle{%
         \iflongmktitle\getSpaceLeft
           \global\setbox\uis@boxa=\vsplit0 to \@tempdima
           \box\uis@boxa\par\resetTitleCounters
           \thispagestyle{pprintTitle}%
           \printFirstPageNotes
           \twocolumn[\box0]%
         \else
         \twocolumn[\finalMaketitle]\printFirstPageNotes
      \fi
      \gdef\thefootnote{\arabic{footnote}}}%
   \else
      \def\maketitle{%
      \iflongmktitle\getSpaceLeft
         \global\setbox\uis@boxa=\vsplit0 to \@tempdima
         \box\uis@boxa\par\resetTitleCounters
         \thispagestyle{pprintTitle}%
         \printFirstPageNotes
         \twocolumn[\box0]%
      \else
         \twocolumn[\finalMaketitle]\printFirstPageNotes
      \fi
      \gdef\thefootnote{\arabic{footnote}}}%
   \fi
  \fi
 \fi
\fi



\if@twocolumn
\def\ps\@headings{%
    \def\@oddhead{ 
       \parbox{12cm}{\renewcommand{\baselinestretch}{1.0}\footnotesize\@titulo }\hfill\parbox{2cm}{\uislogo}\hspace{5mm}{\fontencoding{OT1} %
       \sffamily  \thepage}}%
    \def\@evenhead{{\fontencoding{OT1} \sffamily \large \thepage }\hspace{5mm}\parbox{2cm}{\revlogo} \hfill %
    \parbox{7.5cm}{\footnotesize\hfill\@runauth}}%
    \let\@evenfoot\@empty%
    \let\@oddfoot\@evenfoot
}
\else %RUIEDIT headings
\def\ps@headings{%
   \def\@oddhead{ 
       \parbox{12cm}{\renewcommand{\baselinestretch}{1.0}\footnotesize\@titulo } \hfill \parbox{2cm}{\uislogo}\hspace{5mm}
			 {\fontencoding{OT1} \bfseries \small \sffamily  \thepage}
				}%
    \def\@evenhead{
		   %{\fontencoding{OT1} \sffamily \small \bfseries \thepage}\hspace{5mm} 
			 %\parbox{2cm}{ } \hfill %
       %\parbox{8.5cm}{\footnotesize\hfill
       %\ifnum\jtype=1\else\@runauth\fi}
			
			\parbox{2cm}{\fontencoding{OT1} \sffamily \small \bfseries \thepage }
      \parbox{14.5cm}{ \hfill \footnotesize\@runauth}
			
			}%
    \ifnopreprintline
      \def\@oddfoot{}%
      \else
      \def\@oddfoot{\footnotesize\itshape Artículo en revisión enviado a \@journalname ~(\today)\hfill}%
      \fi
      \let\@evenfoot\@oddfoot}
\fi

\pagestyle{headings}

\def\volume#1{\gdef\@vol{#1}}
\def\firstpage#1{\gdef\@firstpage{#1}%
                  \setcounter{page}{#1}}
                  
\def\lastpage{\pageref{lastpage}}

%\def\jnltitlelogo#1{\gdef\@jnltitlelogo{#1}}

\newdimen\dummylogowidth
\dummylogowidth=87pt

\def\thesection{\arabic{section}}
\def\thesubsection{\thesection.\arabic{subsection}}
\def\thesubsubsection{\thesubsection.\arabic{subsubsection}}
\def\theparagraph{\thesubsubsection.\arabic{paragraph}}

\renewcommand\section{\@startsection{section}{1}{\z@}%
           {18\p@ \@plus 6\p@ \@minus 3\p@}%
           {9\p@ \@plus 6\p@ \@minus 3\p@}%
           {\normalsize\bfseries\boldmath\uppercase}}
\renewcommand\subsection{\@startsection{subsection}{2}{\z@}%
           {12\p@ \@plus 6\p@ \@minus 3\p@}%
           {3\p@ \@plus 6\p@ \@minus 3\p@}%
           {\normalfont\normalsize\bfseries\boldmath}}
\renewcommand\subsubsection{\@startsection{subsubsection}{3}{\z@}%
           {12\p@ \@plus 6\p@ \@minus 3\p@}%
           {\p@}%
           {\normalfont\normalsize\bfseries\boldmath\itshape}}

\renewcommand{\sectionmark}[1]{\markright{\MakeLowercase{\thesection.\ #1}}}
%\renewcommand{\sectionmark}[1]{\markright{\MakeUppercase{\thesection.\ #1}}}

\def\paragraph{\secdef{\uis@aparagraph}{\uis@bparagraph}}
\def\uis@aparagraph[#1]#2{\uisparagraph[#1]{#2.}}
\def\uis@bparagraph#1{\uisparagraph*{#1.}}

\newcommand\uisparagraph{\@startsection{paragraph}{4}{0\z@}%
           {10\p@ \@plus 6\p@ \@minus 3\p@}%
           {-6\p@}%
           {\normalfont\itshape}}
\newdimen\leftMargin
\leftMargin=2em
\newtoks\@enLab  %\newtoks\@enfont
\def\@enQmark{?}
\def\@enLabel#1#2{%
  \edef\@enThe{\noexpand#1{\@enumctr}}%
  \@enLab\expandafter{\the\@enLab\csname the\@enumctr\endcsname}%
  \@enloop}
\def\@enSpace{\afterassignment\@enSp@ce\let\@tempa= }
\def\@enSp@ce{\@enLab\expandafter{\the\@enLab\space}\@enloop}
\def\@enGroup#1{\@enLab\expandafter{\the\@enLab{#1}}\@enloop}
\def\@enOther#1{\@enLab\expandafter{\the\@enLab#1}\@enloop}
\def\@enloop{\futurelet\@entemp\@enloop@}
\def\@enloop@{%
  \ifx A\@entemp         \def\@tempa{\@enLabel\Alph  }\else
  \ifx a\@entemp         \def\@tempa{\@enLabel\alph  }\else
  \ifx i\@entemp         \def\@tempa{\@enLabel\roman }\else
  \ifx I\@entemp         \def\@tempa{\@enLabel\Roman }\else
  \ifx 1\@entemp         \def\@tempa{\@enLabel\arabic}\else
  \ifx \@sptoken\@entemp \let\@tempa\@enSpace         \else
  \ifx \bgroup\@entemp   \let\@tempa\@enGroup         \else
  \ifx \@enum@\@entemp   \let\@tempa\@gobble          \else
                         \let\@tempa\@enOther
             \fi\fi\fi\fi\fi\fi\fi\fi
  \@tempa}
\newlength{\@sep} \newlength{\@@sep}
\setlength{\@sep}{.5\baselineskip plus.2\baselineskip
            minus.2\baselineskip}
\setlength{\@@sep}{.1\baselineskip plus.01\baselineskip
            minus.05\baselineskip}
\providecommand{\sfbc}{\rmfamily\upshape}
\providecommand{\sfn}{\rmfamily\upshape}
\def\@enfont{\ifnum \@enumdepth >1\let\@nxt\sfn \else\let\@nxt\sfbc \fi\@nxt}
\def\enumerate{%
   \ifnum \@enumdepth >3 \@toodeep\else
      \advance\@enumdepth \@ne
      \edef\@enumctr{enum\romannumeral\the\@enumdepth}\fi
   \@ifnextchar[{\@@enum@}{\@enum@}}
\def\@@enum@[#1]{%
  \@enLab{}\let\@enThe\@enQmark
  \@enloop#1\@enum@
  \ifx\@enThe\@enQmark\@warning{The counter will not be printed.%
   ^^J\space\@spaces\@spaces\@spaces The label is: \the\@enLab}\fi
  \expandafter\edef\csname label\@enumctr\endcsname{\the\@enLab}%
  \expandafter\let\csname the\@enumctr\endcsname\@enThe
  \csname c@\@enumctr\endcsname7
  \expandafter\settowidth
            \csname leftmargin\romannumeral\@enumdepth\endcsname
            {\the\@enLab\hskip\labelsep}%
  \@enum@}
\def\@enum@{\list{{\@enfont\csname label\@enumctr\endcsname}}%
           {\usecounter{\@enumctr}\def\makelabel##1{\hss\llap{##1}}%
     \ifnum \@enumdepth>1\setlength{\topsep}{\@@sep}\else
           \setlength{\topsep}{\@sep}\fi
     \ifnum \@enumdepth>1\setlength{\itemsep}{0pt plus1pt minus1pt}%
      \else \setlength{\itemsep}{\@@sep}\fi
     %\setlength\leftmargin{\leftMargin}%%%{1.8em}
     \setlength{\parsep}{0pt plus1pt minus1pt}%
     \setlength{\parskip}{0pt plus1pt minus1pt}
                   }}

\def\endenumerate{\par\ifnum \@enumdepth >1\addvspace{\@@sep}\else
           \addvspace{\@sep}\fi \endlist}

\def\sitem{\@noitemargtrue\@item[\@itemlabel *]}

\def\itemize{\@ifnextchar[{\@Itemize}{\@Itemize[]}}

\def\@Itemize[#1]{\def\next{#1}%
  \ifnum \@itemdepth >\thr@@\@toodeep\else
   \advance\@itemdepth\@ne
  \ifx\next\@empty\else\expandafter\def\csname
   labelitem\romannumeral\the\@itemdepth\endcsname{#1}\fi%
  \edef\@itemitem{labelitem\romannumeral\the\@itemdepth}%
  \expandafter\list\csname\@itemitem\endcsname
  {\def\makelabel##1{\hss\llap{##1}}}%
 \fi}
\def\newdefinition#1{%
  \@ifnextchar[{\@odfn{#1}}{\@ndfn{#1}}}%]
\def\@ndfn#1#2{%
  \@ifnextchar[{\@xndfn{#1}{#2}}{\@yndfn{#1}{#2}}}
\def\@xndfn#1#2[#3]{%
  \expandafter\@ifdefinable\csname #1\endcsname
    {\@definecounter{#1}\@newctr{#1}[#3]%
     \expandafter\xdef\csname the#1\endcsname{%
       \expandafter\noexpand\csname the#3\endcsname \@dfncountersep
          \@dfncounter{#1}}%
     \global\@namedef{#1}{\@dfn{#1}{#2}}%
     \global\@namedef{end#1}{\@enddefinition}}}
\def\@yndfn#1#2{%
  \expandafter\@ifdefinable\csname #1\endcsname
    {\@definecounter{#1}%
     \expandafter\xdef\csname the#1\endcsname{\@dfncounter{#1}}%
     \global\@namedef{#1}{\@dfn{#1}{#2}}%
     \global\@namedef{end#1}{\@enddefinition}}}
\def\@odfn#1[#2]#3{%
  \@ifundefined{c@#2}{\@nocounterr{#2}}%
    {\expandafter\@ifdefinable\csname #1\endcsname
    {\global\@namedef{the#1}{\@nameuse{the#2}}
  \global\@namedef{#1}{\@dfn{#2}{#3}}%
  \global\@namedef{end#1}{\@enddefinition}}}}
\def\@dfn#1#2{%
  \refstepcounter{#1}%
  \@ifnextchar[{\@ydfn{#1}{#2}}{\@xdfn{#1}{#2}}}
\def\@xdfn#1#2{%
  \@begindefinition{#2}{\csname the#1\endcsname}\ignorespaces}
\def\@ydfn#1#2[#3]{%
  \@opargbegindefinition{#2}{\csname the#1\endcsname}{#3}\ignorespaces}
\def\@dfncounter#1{\noexpand\arabic{#1}}
\def\@dfncountersep{.}
\def\@begindefinition#1#2{\trivlist
   \item[\hskip\labelsep{\bfseries #1\ #2.}]\upshape}
\def\@opargbegindefinition#1#2#3{\trivlist
      \item[\hskip\labelsep{\bfseries #1\ #2\ (#3).}]\upshape}
\def\@enddefinition{\endtrivlist}

\def\@begintheorem#1#2{\trivlist
  \let\baselinestretch\@blstr
   \item[\hskip \labelsep{\bfseries #1\ #2.}]\itshape}
\def\@opargbegintheorem#1#2#3{\trivlist
  \let\baselinestretch\@blstr
      \item[\hskip \labelsep{\bfseries #1\ #2\ (#3).}]\itshape}

\def\newproof#1{%
  \@ifnextchar[{\@oprf{#1}}{\@nprf{#1}}}
\def\@nprf#1#2{%
  \@ifnextchar[{\@xnprf{#1}{#2}}{\@ynprf{#1}{#2}}}
\def\@xnprf#1#2[#3]{%
  \expandafter\@ifdefinable\csname #1\endcsname
    {\@definecounter{#1}\@newctr{#1}[#3]%
     \expandafter\xdef\csname the#1\endcsname{%
       \expandafter\noexpand\csname the#3\endcsname \@prfcountersep
          \@prfcounter{#1}}%
     \global\@namedef{#1}{\@prf{#1}{#2}}%
     \global\@namedef{end#1}{\@endproof}}}
\def\@ynprf#1#2{%
  \expandafter\@ifdefinable\csname #1\endcsname
    {\@definecounter{#1}%
     \expandafter\xdef\csname the#1\endcsname{\@prfcounter{#1}}%
     \global\@namedef{#1}{\@prf{#1}{#2}}%
     \global\@namedef{end#1}{\@endproof}}}
\def\@oprf#1[#2]#3{%
  \@ifundefined{c@#2}{\@nocounterr{#2}}%
    {\expandafter\@ifdefinable\csname #1\endcsname
    {\global\@namedef{the#1}{\@nameuse{the#2}}%
  \global\@namedef{#1}{\@prf{#2}{#3}}%
  \global\@namedef{end#1}{\@endproof}}}}
\def\@prf#1#2{%
  \refstepcounter{#1}%
  \@ifnextchar[{\@yprf{#1}{#2}}{\@xprf{#1}{#2}}}
\def\@xprf#1#2{%
  \@beginproof{#2}{\csname the#1\endcsname}\ignorespaces}
\def\@yprf#1#2[#3]{%
  \@opargbeginproof{#2}{\csname the#1\endcsname}{#3}\ignorespaces}
\def\@prfcounter#1{\noexpand\arabic{#1}}
\def\@prfcountersep{.}
\def\@beginproof#1#2{\trivlist\let\baselinestretch\@blstr
   \item[\hskip \labelsep{\scshape #1.}]\rmfamily}
\def\@opargbeginproof#1#2#3{\trivlist\let\baselinestretch\@blstr
      \item[\hskip \labelsep{\scshape #1\ (#3).}]\rmfamily}
\def\@endproof{\endtrivlist}
\newcommand*{\qed}{\hbox{}\hfill$\Box$}

\@ifundefined{@biboptions}{\xdef\@biboptions{numbers}}{}
\InputIfFileExists{\jobname.spl}{}{}
\RequirePackage[\@biboptions]{natbib}

\newwrite\splwrite
\immediate\openout\splwrite=\jobname.spl
\def\biboptions#1{\def\next{#1}\immediate\write\splwrite{%
   \string\g@addto@macro\string\@biboptions{%
    ,\expandafter\strip@prefix\meaning\next}}}

\let\baselinestretch=\@blstr

\ifnum\jtype=1
  \RequirePackage{lineno}
 \RequirePackage{geometry}
 \geometry{twoside,
   paperwidth=8.5in,
      paperheight=11in,
      textheight=22.5cm,
      textwidth=16.5cm,
      centering,
      headheight=12pt,
      headsep=24pt,
      footskip=24pt,
      footnotesep=24pt plus 2pt minus 12pt,
 }

 \global\let\bibfont=\footnotesize
 \global\bibsep=5pt
 \global\bibhang=0pt
 \if@twocolumn\global\@twocolumnfalse\fi
 \linenumbers
\else\ifnum\jtype=3
\global\@twosidetrue
 \RequirePackage{geometry}
 \geometry{twoside,
   paperwidth=8.5in,
      paperheight=11in,
      textheight=22.5cm,
      textwidth=16.5cm,
      centering,
      headheight=12pt,
      headsep=24pt,
      footskip=24pt,
      footnotesep=24pt plus 2pt minus 12pt,
      columnsep=18pt
 }
 \global\let\bibfont=\footnotesize
 \global\bibsep=5pt
 \global\bibhang=0pt
  \setlength{\parindent}{0pt}
  \global\@twocolumntrue
 \if@twocolumn\input{fleqn.clo}\fi
\else\ifnum\jtype=5
\global\@twosidetrue
 \setlength{\parindent}{0pt}
 \RequirePackage{geometry}
 \geometry{twoside,
  paperwidth=8.5in,
    paperheight=11in,
    textheight=22.5cm,
    textwidth=16.5cm,
    centering,
    headheight=12pt,
    headsep=24pt,
    footskip=24pt,
    footnotesep=24pt plus 2pt minus 12pt,
    columnsep=18pt
 }%
 \global\let\bibfont=\footnotesize
 \global\bibsep=5pt
 \global\bibhang=0pt
 \input{fleqn.clo}
 \global\@twocolumntrue
 \global\@twosidetrue
 
 %\pagestyle{headings}
%%
%% End of option '5p'
%%
\fi\fi\fi

\AtEndDocument{\label{lastpage}}

\let\@journalname\@empty
\def\journalname#1{\gdef\@journalname{#1}}

\let\@jid\@empty
\def\jid#1{\gdef\@jid{#1}}

\def\reprintline{%
    \parbox[t]{\@tempdima}{\centering%
    \raisebox{20pt}{\includegraphics{SDlogo-\jtype p}}\\[-12pt]
    \mbox{\footnotesize\@journalname~xx~(xxxx)~xxx-xxx}%
    }}

\def\volume#1{\gdef\@vol{#1}}
\def\firstpage#1{\gdef\@firstpage{#1}%
                  \setcounter{page}{#1}}
                  
\def\lastpage{\pageref{lastpage}}

\ifnopreprintline
\def\marcas{\crop[cross]}
\else
\def\marcas{\@empty}
\fi

\def\jnltitlelogo#1{\gdef\@jnltitlelogo{#1}}

\newdimen\dummylogowidth
\dummylogowidth=87pt

\jnltitlelogo{Journal Logo}

\def\TopRule{\rule{\dummylogowidth}{1.85pt}\\[-9.6pt]
             \rule{\dummylogowidth}{1pt}}

\def\BottomRule{\rule{\dummylogowidth}{1pt}\\[-9pt]        
                \rule{\dummylogowidth}{1.85pt}}  


\def\jnltitlebox{\parbox[c][42pt]{84.37pt}%%
                {\fontsize{18pt}{20pt}\sffamily\selectfont
                \centering\@jnltitlelogo}}

\def\dummyjnllogo{%
      \parbox[c][61pt][c]{\dummylogowidth}%
      {\TopRule%
       \vfill%
        \jnltitlebox%
       \vfill%
       \BottomRule%
}}

\def\uislogo{\includegraphics[width=2cm]{logouis.jpg}}
\def\revlogo{\includegraphics[width=6cm]{logorevista.jpg}}
\def\jnllogo{\IfFileExists{\@jid logo}%
   {\includegraphics{\@jid logo}}%
   {\dummyjnllogo}%
   }

\def\artUIS@titlefont{\bfseries\fontsize{16pt}{16}\selectfont}
\def\artUIS@authorfont{\fontsize{10pt}{15.6}\selectfont}
\def\artUIS@absfont{\fontsize{10pt}{10.8}\selectfont}
\def\artUIS@receivedfont{\fontsize{8pt}{15.6}\selectfont}

\def\runauth#1{\gdef\@runauth{#1}}
\runauth{Author}

\let\@dochead\@gobble
\def\dochead#1{\gdef\@dochead{\centering{\large#1}}}

\def\ps@pprintTitle{%
    \def\@evenhead{
		%HEADER FIRST PAGE
		\centerline{\includegraphics[width=20.83cm]{RUI-header.pdf}}
				
		}
     
		\def\@oddfoot{\parbox[c][10pt][c]{470pt}{\footnotesize %\normalshape 
		ISSN Impreso: $1657 - 4583$, En L\'inea: $2145 - 8456$, \textbf{CC BY-ND} $\mathbf{4}$.$\mathbf{0}$ \\ 	 \@runauth,  "\@titulo," {\itshape Rev. UIS Ing.}, vol.\@vol, no. \@numero, pp. \@firstpage-\lastpage, \@anno, \@doi}\hfil}%
     \let\@evenfoot\@oddfoot
     \let\@oddhead\@evenhead%
     % \let\@oddfoot\@empty
      \let\@oddfoot\@evenfoot
}      

%%RUIEDIT order of items
\long\def\MaketitleBox{%
  \resetTitleCounters
	
  \def\baselinestretch{1}%
  \begin{center}%
   \def\baselinestretch{1}%
    {	\vskip32pt
		\strut\artUIS@titlefont\@titulo\strut\par\vskip10pt %
    \hrule height 1pt
    \par\vskip10pt
    \strut\artUIS@titlefont\@title\strut}
    \par\vskip30pt
%    \normalsize\uisauthors\par\vskip10pt

     \ifnum\jtype=1
     \else
       {\artUIS@authorfont\uisauthors}\par\vskip5pt
       %\footnotesize\itshape\uisaddress\par\vskip5pt
			\small{{Recibido}: \sep{ \@received}. 
       {Aceptado}: \sep{ \@accepted}. 
       {Versi\'on final}: \sep{\@published}.}			
			
    \fi
 
    \vskip6pt
    \vskip12pt
    \ifvoid\resbox\else\unvbox\resbox\par\vskip10pt\fi
    \ifvoid\clavesbox\else\unvbox\clavesbox\par\vskip20pt\fi
    \ifvoid\absbox\else\unvbox\absbox\par\vskip10pt\fi
    \ifvoid\keybox\else\unvbox\keybox\par\vskip10pt\fi
    \vskip3pt
    %\hrule
    \end{center}%
    
}


\def\footnoterule{\kern-3\p@%
  \hrule width 3pc height .25pt
  \kern3\p@}

\newif\if@copyrightline\@copyrightlinefalse
\let\@copyrightyear\@empty
\def\copyrightyear#1{\gdef\@copyrightyear{#1}}
\let\@copyrightowner\@empty
\def\copyrightowner#1{\gdef\@copyrightowner{#1}}

\def\CopyrightLine{\@ifnextchar[{\@CopyrightLine}{\@CopyrightLine[]}}
\def\@CopyrightLine[#1]#2#3{\@copyrightlinetrue%
                       \gdef\@copyrightprefix{#1}%
                       \gdef\@copyrightyear{#2}%
                       \gdef\@copyrighttext{#3}% 
                       \gdef\@CopyrightLine{\par\vskip1pc%
                       \noindent\ifx\@copyrightprefix\@empty\relax%
                       \else\@copyrightprefix~\fi%
                       \textcopyright~\@copyrightyear~%
                       \@copyrighttext~}}

%AGE
%\renewenvironment{abstract}{\global\setbox\absbox=\vbox\bgroup
  %\hsize=\textwidth\def\baselinestretch{1}%
  %\noindent\unskip \textbf{ABSTRACT} %
  %\par\medskip\noindent\unskip\ignorespaces}
  %{\egroup}
  

\def\keyword{%
  \def\sep{\unskip, }%
 \def\MSC{\@ifnextchar[{\@MSC}{\@MSC[2000]}}
  \def\@MSC[##1]{\par\leavevmode\hbox {\it ##1~MSC:\space}}%
  \def\PACS{\par\leavevmode\hbox {\it PACS:\space}}%
  \def\JEL{\par\leavevmode\hbox {\it JEL:\space}}%
  \global\setbox\keybox=\vbox\bgroup\hsize=\textwidth
  \normalsize\normalfont\def\baselinestretch{1}
  \parskip\z@
  \artUIS@absfont\noindent\textbf{KEYWORDS: }
  \raggedright                         % Keywords are not justified.
  \ignorespaces}
\def\endkeyword{\par \egroup}

\def\claves{%
  \def\sep{\unskip, }%
 \def\MSC{\@ifnextchar[{\@MSC}{\@MSC[2000]}}
  \def\@MSC[##1]{\par\leavevmode\hbox {\it ##1~MSC:\space}}%
  \def\PACS{\par\leavevmode\hbox {\it PACS:\space}}%
  \def\JEL{\par\leavevmode\hbox {\it JEL:\space}}%
  \global\setbox\clavesbox=\vbox\bgroup\hsize=\textwidth
  \normalsize\normalfont\def\baselinestretch{1}
  \parskip\z@
  \artUIS@absfont\noindent\textbf{PALABRAS CLAVE: }
  \raggedright                         % Keywords are not justified.
  \ignorespaces}
\def\endkeyword{\par \egroup}

\ifartUIS@nsmodel
\def\figurename{Fig.}
\long\def\@makecaption#1#2{%
  \vskip\abovecaptionskip\footnotesize\bfseries\boldmath
  \sbox\@tempboxa{\textbf{#1. #2}}%
  \ifdim \wd\@tempboxa >\hsize
   \textbf{#1. #2}\par
  \else
    \global \@minipagefalse
    \hb@xt@\hsize{\hfil\box\@tempboxa\hfil}%
  \fi
  \vskip\belowcaptionskip}
\else
\long\def\@makecaption#1#2{%
  \vskip\abovecaptionskip\footnotesize\bfseries\boldmath
  \sbox\@tempboxa{\textbf{#1. #2}}%
  \ifdim \wd\@tempboxa >\hsize
    \textbf{#1. #2}\par
  \else
    \global \@minipagefalse
    \hb@xt@\hsize{\hfil\box\@tempboxa\hfil}%
  \fi
  \vskip\belowcaptionskip}
\fi





\def\journal#1{\gdef\@journal{#1}}
 \let\@journal\@empty
 
\newenvironment{frontmatter}{}{\maketitle}

\long\def\@makecaption#1#2{%
  \vskip\abovecaptionskip\footnotesize
  \sbox\@tempboxa{\textbf{#1.} #2}%
  \ifdim \wd\@tempboxa >\hsize
    \textbf{#1.} #2\par
  \else
    \global \@minipagefalse
    \hb@xt@\hsize{\hfil\box\@tempboxa\hfil}%
  \fi
  \vskip\belowcaptionskip}

\AtBeginDocument{\@ifpackageloaded{hyperref}
  {\def\@linkcolor{blue}
   \def\@anchorcolor{blue}
   \def\@citecolor{blue}
   \def\@filecolor{blue}
   \def\@urlcolor{blue}
   \def\@menucolor{blue}
   \def\@pagecolor{blue}
\begingroup
  \@makeother\`%
  \@makeother\=%
  \edef\x{%
    \edef\noexpand\x{%
      \endgroup
      \noexpand\toks@{%
        \catcode 96=\noexpand\the\catcode`\noexpand\`\relax
        \catcode 61=\noexpand\the\catcode`\noexpand\=\relax
      }%
    }%
    \noexpand\x
  }%
\x
\@makeother\`
\@makeother\=
}{}}
%%
\def\appendixname{Appendice }
\renewcommand\appendix{\par
  \setcounter{section}{0}%
  \setcounter{subsection}{0}%
  \setcounter{equation}{0}
  \gdef\thefigure{\@Alph\c@section.\arabic{figure}}%
  \gdef\thetable{\@Alph\c@section.\arabic{table}}%
  \gdef\thesection{\appendixname~\@Alph\c@section}%
  \@addtoreset{equation}{section}%
  \gdef\theequation{\@Alph\c@section.\arabic{equation}}%
  \addtocontents{toc}{\string\let\string\numberline\string\tmptocnumberline}{}{}
}

%%%% \numberline width calculation for appendix.
\newdimen\appnamewidth
\def\tmptocnumberline#1{%
   \setbox0=\hbox{\appendixname}
   \appnamewidth=\wd0
   \addtolength\appnamewidth{2.5pc}
   \hb@xt@\appnamewidth{#1\hfill}
}

%% Added for work with amsrefs.sty

\@ifpackageloaded{amsrefs}%
  {}
  {%\let\bibsection\relax%
  \AtBeginDocument{\def\cites@b#1#2,#3{%
    \begingroup[%
        \toks@{\InnerCite{#2}#1}%
        \ifx\@empty#3\@xp\@gobble\fi
        \cites@c#3%
}}}
%%
%% Added for avoiding clash with cleveref.sty

\@ifpackageloaded{cleveref}%
 {}
 {\def\tnotetext[#1]#2{\g@addto@macro\@tnotes{%
    \refstepcounter{tnote}%
    \immediate\write\@auxout{\string\Newlabel{#1}{\thetnote}}
    \def\thefootnote{\ifcase\c@tnote\or\ding{73}\or\ding{73}\ding{73}\fi}%
    \footnotetext{#2}}}
%%%
  \def\fntext[#1]#2{\g@addto@macro\@fnotes{%
    \refstepcounter{fnote}%
    \immediate\write\@auxout{\string\Newlabel{#1}{\thefnote}}
    \def\thefootnote{\thefnote}%
    \global\setcounter{footnote}{\thefnote}%
    \footnotetext{#2}}}
%%%
  \def\cortext[#1]#2{\g@addto@macro\@cornotes{%
    \refstepcounter{cnote}%
    \immediate\write\@auxout{\string\Newlabel{#1}{\thecnote}}
    \def\thefootnote{\ifcase\thecnote\or$\ast$\or
    $\ast\ast$\fi}%
    \footnotetext{#2}}}
}


\endinput

\makeatother
%%
%% End of file `artUIS.cls'.
