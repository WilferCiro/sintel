\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Introducción}{8}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Objetivos}{9}{section.1.1}% 
\contentsline {subsection}{\numberline {1.1.1}Objetivo general}{9}{subsection.1.1.1}% 
\contentsline {subsection}{\numberline {1.1.2}Objetivos específicos}{9}{subsection.1.1.2}% 
\contentsline {chapter}{\numberline {2}Marco conceptual}{10}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Sistema digital}{10}{section.2.1}% 
\contentsline {section}{\numberline {2.2}HDL}{11}{section.2.2}% 
\contentsline {subsection}{\numberline {2.2.1}Verilog}{11}{subsection.2.2.1}% 
\contentsline {subsection}{\numberline {2.2.2}VHDL}{12}{subsection.2.2.2}% 
\contentsline {section}{\numberline {2.3}Máquinas de estados finitos}{12}{section.2.3}% 
\contentsline {section}{\numberline {2.4}Microprocesador}{13}{section.2.4}% 
\contentsline {subsection}{\numberline {2.4.1}CISC}{14}{subsection.2.4.1}% 
\contentsline {subsection}{\numberline {2.4.2}RISC}{15}{subsection.2.4.2}% 
\contentsline {subsubsection}{Atmel AVR}{16}{section*.6}% 
\contentsline {subsubsection}{MIPS}{17}{section*.7}% 
\contentsline {section}{\numberline {2.5}FPGA}{18}{section.2.5}% 
\contentsline {chapter}{\numberline {3}Desarrollo e implementación}{20}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Desarrollo del software}{20}{section.3.1}% 
\contentsline {subsection}{\numberline {3.1.1}Conceptos iniciales}{20}{subsection.3.1.1}% 
\contentsline {subsection}{\numberline {3.1.2}Construcción de la interfaz gráfica}{21}{subsection.3.1.2}% 
\contentsline {subsection}{\numberline {3.1.3}Características adicionales}{26}{subsection.3.1.3}% 
\contentsline {subsection}{\numberline {3.1.4}Componentes}{27}{subsection.3.1.4}% 
\contentsline {subsection}{\numberline {3.1.5}Simulación}{30}{subsection.3.1.5}% 
\contentsline {subsubsection}{Python}{31}{section*.8}% 
\contentsline {subsubsection}{Icarus verilog (iverilog)}{33}{section*.9}% 
\contentsline {subsection}{\numberline {3.1.6}Documentación}{34}{subsection.3.1.6}% 
\contentsline {section}{\numberline {3.2}Microprocesador}{35}{section.3.2}% 
\contentsline {chapter}{\numberline {4}Resultados y análisis}{36}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Simulador}{36}{section.4.1}% 
\contentsline {section}{\numberline {4.2}Microprocesador}{39}{section.4.2}% 
\contentsline {subsection}{\numberline {4.2.1}Construcción de los módulos}{39}{subsection.4.2.1}% 
\contentsline {subsubsection}{ALU}{39}{section*.10}% 
\contentsline {subsubsection}{Contador de programa}{40}{section*.11}% 
\contentsline {subsubsection}{Banco de registros, memoria de instrucciones y memoria de datos}{40}{section*.12}% 
\contentsline {subsubsection}{Unidad de control}{41}{section*.13}% 
\contentsline {subsection}{\numberline {4.2.2}Conexión de los módulos}{41}{subsection.4.2.2}% 
\contentsline {subsection}{\numberline {4.2.3}Pruebas al microprocesador}{43}{subsection.4.2.3}% 
\contentsline {subsubsection}{Análisis de código HDL}{46}{section*.14}% 
\contentsline {chapter}{\numberline {5}Conclusiones y trabajos futuros}{48}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Conclusiones}{48}{section.5.1}% 
\contentsline {section}{\numberline {5.2}Trabajos futuros}{49}{section.5.2}% 
