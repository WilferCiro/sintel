\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}Proponente y director}{4}{section.1}% 
\contentsline {section}{\numberline {2}Organización usuaria}{4}{section.2}% 
\contentsline {section}{\numberline {3}Glosario}{4}{section.3}% 
\contentsline {section}{\numberline {4}Área del proyecto}{5}{section.4}% 
\contentsline {section}{\numberline {5}Modalidad}{5}{section.5}% 
\contentsline {section}{\numberline {6}Título}{5}{section.6}% 
\contentsline {section}{\numberline {7}Tema}{5}{section.7}% 
\contentsline {section}{\numberline {8}Antecedentes}{6}{section.8}% 
\contentsline {subsection}{\numberline {8.1}Palabras clave}{6}{subsection.8.1}% 
\contentsline {subsection}{\numberline {8.2}Herramientas de búsqueda}{6}{subsection.8.2}% 
\contentsline {subsection}{\numberline {8.3}Estado del arte}{6}{subsection.8.3}% 
\contentsline {section}{\numberline {9}Descripción del problema}{8}{section.9}% 
\contentsline {section}{\numberline {10}Justificación}{8}{section.10}% 
\contentsline {section}{\numberline {11}Objetivos}{9}{section.11}% 
\contentsline {subsection}{\numberline {11.1}Objetivo general}{9}{subsection.11.1}% 
\contentsline {subsection}{\numberline {11.2}Objetivos específicos}{9}{subsection.11.2}% 
\contentsline {section}{\numberline {12}Alcance y delimitación}{9}{section.12}% 
\contentsline {section}{\numberline {13}Marco teórico}{9}{section.13}% 
\contentsline {subsection}{\numberline {13.1}Sistema digital}{9}{subsection.13.1}% 
\contentsline {subsection}{\numberline {13.2}Microprocesador}{10}{subsection.13.2}% 
\contentsline {subsection}{\numberline {13.3}VHDL y verilog}{11}{subsection.13.3}% 
\contentsline {subsection}{\numberline {13.4}FPGA}{12}{subsection.13.4}% 
\contentsline {subsection}{\numberline {13.5}Simulador}{13}{subsection.13.5}% 
\contentsline {section}{\numberline {14}Metodología}{13}{section.14}% 
\contentsline {section}{\numberline {15}Presupuesto y recursos necesarios}{15}{section.15}% 
\contentsline {section}{\numberline {16}Cronograma}{16}{section.16}% 
