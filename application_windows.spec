# -*- mode: python ; coding: utf-8 -*-
import PyInstaller.config

block_cipher = None
PyInstaller.config.CONF['distpath'] = "Windows"

a = Analysis(['application.py'],
             pathex=['/home/ciro/Documents/programacion/python/Sintel/sintel'],
             binaries=[],
             datas=[
             	('resources/ui/*.ui', 'resources/ui'),
             	('resources/ui/macro/*.ui', 'resources/ui/macro'),
             	('resources/ui/menus/*.ui', 'resources/ui/menus'),
             	('resources/ui/properties/*.ui', 'resources/ui/properties'),
             	('resources/ui/sm/*.ui', 'resources/ui/sm'),
             	('resources/ui/add_component/*.ui', 'resources/ui/add_component'),
             	('resources/css/*.css', 'resources/css'),
             	('resources/css/rc/*.png', 'resources/css/rc'),
             	('resources/db/*', 'resources/db'),
             	('resources/images/*.png', 'resources/images'),
             	('resources/images/*.svg', 'resources/images'),
             	('resources/images/icons/*.png', 'resources/images/icons'),
             	('resources/images/svg/*.svg', 'resources/images/svg'),
             	('resources/templates/*', 'resources/templates')
             ],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='Sintel',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=False )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='Sintel')
