#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''
# Custom libraries
from simulador.main_window import MainWindow
from simulador.projects_handler import ProjectsHandler
from simulador.settings_handler import SettingsHandler
from simulador.home_widget import HomeWidget
from simulador.database_handler import DataBaseHandler
from simulador.components_handler import ComponentsHandler
from simulador.properties_window import PropertiesWindow
from simulador.subsignals_modal import SubSignalModal
from simulador.macro_component import PropertiesMacroWindow
from simulador.src_macro.save_load_macro import SaveLoadMacro
from simulador.notification import Notification
from simulador.tools.utils import get_pycompile_folder

#import resources.resources_rc as resources_rc

# Qt libraries
from PyQt5.QtWidgets import QApplication, QSystemTrayIcon
from PyQt5.QtCore import QTranslator, QRectF
from PyQt5.QtGui import QIcon

# System libraries
import sys
import os

class App(QApplication):
	"""
		Main class, defines the application class
	"""
	def __init__(self):
		super(QApplication, self).__init__(sys.argv)

		self.setWindowIcon(QIcon(self.get_src("images", "sintel.svg")))

		self.notification = Notification()

		# Settings handler
		self.settingsHandler = SettingsHandler()

		# Translations
		self.translator = None
		self.setLanguage(self.settingsHandler.getLanguage())

		self.saveLoadMacro = SaveLoadMacro()

		# Window for edit connections in subSignals
		self.subSignalModal = SubSignalModal()

		# Window for edit component properties
		# self.propertiesWindow = PropertiesWindow()

		# Window fot edit macro component properties
		self.propertiesMacroWindow = PropertiesMacroWindow()

		# Database handler
		self.dataBase = DataBaseHandler()

		# Components handler
		self.componentsHandler = ComponentsHandler()

		# Home view, prev to projectsHandler
		self.homeWidget = HomeWidget()

		# Widgets initialization
		self.projectsHandler = ProjectsHandler()

		# Application window
		self.mainWindow = MainWindow()
		self.mainWindow.setGeometry(100, 100, 1200, 1000)
		self.mainWindow.showMaximized()

		#self.setStyleSheet(open("simulador/css/style.qss", "r").read())
		self.updateTheme()

		f = self.font()
		f.setFamily("Cantarell")
		f.setPointSize(9)
		self.setFont(f)

	def get_src(self, *source):
		""" Get absolute path to resource, works for dev and for PyInstaller """
		#try:
			# PyInstaller creates a temp folder and stores path in _MEIPASS
		#	base_path = sys._MEIPASS
		#except Exception:
		############ Para Linux
		#	# Acceder a ~/.local/share/sintel
		#	# Copy copy_components into ~/home/user/.sintel -> ya
		#	# Copy resources into ~/.local/share/sintel/resources
		########### Para Windows
		#	# Acceder
		#	# Copy copy_components into C:/user/.sintel -> ya
		#	# Copy resources into C:/program Files/sintel/resources
		if sys.platform == "linux" or sys.platform == "linux2":
			dir_compile = os.path.join(os.path.expanduser('~'), ".local", "share", "sintel", "resources", *source)
			#return os.path.join("~/.local", "share", "sintel", "resources", *source)
			return dir_compile
		else:
			base_path = os.path.abspath(".")
			return os.path.join(base_path, "resources", *source)
		#return os.path.join("resources", *source)

	def updateTheme(self):
		if os.path.exists(os.path.join(get_pycompile_folder(), "css", "general.css")):
			text_css = open(os.path.join(get_pycompile_folder(), "css", "general.css"), "r").read()
			text_css = text_css.replace("resources", get_pycompile_folder())
			self.setStyleSheet(text_css)
		else:
			print("Error al cargar el estilo css")
		#if self.settingsHandler.getDarkTheme():
		#	self.setStyleSheet(open(os.path.join("simulador", "css", "general.css"), "r").read())
		#else:
		#	self.setStyleSheet(open("simulador/css/general.css", "r").read())
		#	self.setStyleSheet("")

	def showError(self, error):
		#if len(error) > 0:
		#self._notify.showMessage("Sintel Error", "".join(error), QSystemTrayIcon.Critical)
		#from gi.repository import Notify
		#Notify.init("Sintel error")
		#Notify.Notification.new("".join(error)).show()
		self.notification.setNotify("Sintel Error", "".join(error))

	def showValid(self, text):
		self.notification.setNotify("Successfull", str(text))

	def setLanguage(self, lang):
		self.settingsHandler.saveLang(lang)
		if self.translator != None:
			self.removeTranslator(self.translator)
		self.translator = QTranslator()
		self.translator.load(os.path.join("tr", lang + ".qm"))
		self.installTranslator(self.translator)

	def closeEvent(self):
		self.notification.deleteAll()
		sys.exit()

if __name__ == '__main__':
	"""
		Start application
	"""
	dir_compile = get_pycompile_folder()
	with open(os.path.join(dir_compile, "default.py"), 'w+') as f:
		f.write("")
		f.close()

	application = App()
	sys.exit(application.exec_())
	application.quit()
