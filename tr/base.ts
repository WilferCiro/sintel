<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../resources/ui/search_component.ui" line="17"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/search_component.ui" line="52"/>
        <source>Search component</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/search_component.ui" line="75"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/search_component.ui" line="115"/>
        <source>Photo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/search_component.ui" line="125"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/search_component.ui" line="132"/>
        <source>Family</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/search_component.ui" line="139"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/search_component.ui" line="146"/>
        <source>Pins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/search_component.ui" line="153"/>
        <source>Add to workspace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/search_component.ui" line="173"/>
        <source>Added ALU</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../resources/ui/simulation_view.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/buttons_top.ui" line="39"/>
        <source>Project Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/buttons_top.ui" line="42"/>
        <source>project name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/buttons_top.ui" line="71"/>
        <source>Open project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/buttons_top.ui" line="90"/>
        <source>Save project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/buttons_top.ui" line="109"/>
        <source>Simulation menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/settings.ui" line="20"/>
        <source>LOGO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/help.ui" line="37"/>
        <source>About Sintel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/settings.ui" line="52"/>
        <source>Version 0.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/help.ui" line="95"/>
        <source>Software for modeling and simulation of digital systems.
 Provides tools to export the model to VHDL code and verilog.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/help.ui" line="112"/>
        <source>Project participants:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/help.ui" line="136"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wilfer Daniel Ciro Maya&lt;a href=&quot;wdcirom@uqvirtual.edu.co&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#00a6ff;&quot;&gt;&lt;br/&gt;&lt;/span&gt;&lt;/a&gt;Luis Miguel Capacho Valbuena&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/help.ui" line="143"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;mail:wilcirom@gmail.com&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#00a6ff;&quot;&gt;wilcirom@gmail.com&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; color:#00a6ff;&quot;&gt; - &lt;/span&gt;&lt;a href=&quot;mail:wdcirom@uqvirtual.edu.co&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#00a6ff;&quot;&gt;wdcirom@uqvirtual.edu.co&lt;/span&gt;&lt;/a&gt;&lt;a href=&quot;wdcirom@uqvirtual.edu.co&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#00a6ff;&quot;&gt;&lt;br/&gt;&lt;/span&gt;&lt;/a&gt;&lt;a href=&quot;mail:lmcapacho@uniquindio.edu.co&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#00a6ff;&quot;&gt;lmcapacho@uniquindio.edu.co&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/help.ui" line="158"/>
        <source>
About the project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/new_project_menu.ui" line="22"/>
        <source>Input the project name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/new_project_menu.ui" line="92"/>
        <source>Create project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/properties_menu.ui" line="27"/>
        <source>Time to sleep for any bucle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/properties_menu.ui" line="30"/>
        <source>simulation period</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/properties_menu.ui" line="95"/>
        <source>Number of start cycles to execute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/properties_menu.ui" line="98"/>
        <source>Start number of clock cycles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/properties_menu.ui" line="170"/>
        <source>Starts the simulation paused when you press &quot;run&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/properties_menu.ui" line="173"/>
        <source>Start paused simulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/search_quick.ui" line="41"/>
        <source>Search component</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/settings.ui" line="37"/>
        <source>Sintel settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/settings.ui" line="91"/>
        <source>Visual Aspect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/settings.ui" line="103"/>
        <source>Dark global theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/settings.ui" line="117"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/settings.ui" line="150"/>
        <source>Spanish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/settings.ui" line="155"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/settings.ui" line="186"/>
        <source>Simulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/settings.ui" line="195"/>
        <source>Default delay time (seconds)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/settings.ui" line="262"/>
        <source>Components</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/settings.ui" line="274"/>
        <source>Update components list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/settings.ui" line="325"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/settings.ui" line="352"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://gitlab.com/WilferCiro/sintel/issues&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#ffffff;&quot;&gt;Do you have any suggestion? let us know&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_menu.ui" line="491"/>
        <source>menuBtn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_menu.ui" line="112"/>
        <source>Exports the model like verilog and VHDL project in the project folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_menu.ui" line="189"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_menu.ui" line="211"/>
        <source>100%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_menu.ui" line="236"/>
        <source>+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_menu.ui" line="376"/>
        <source>Cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_menu.ui" line="398"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_menu.ui" line="420"/>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_view.ui" line="63"/>
        <source>Search...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_view.ui" line="69"/>
        <source>searchIn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_view.ui" line="251"/>
        <source>play the simulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_view.ui" line="264"/>
        <source>pause the simulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_view.ui" line="277"/>
        <source>next step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_view.ui" line="307"/>
        <source>draw a circle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_view.ui" line="323"/>
        <source>put text in workspace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_view.ui" line="339"/>
        <source>draw a box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_view.ui" line="362"/>
        <source>Create a macrocomponent with the selected objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_view.ui" line="380"/>
        <source>Core for simulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_view.ui" line="383"/>
        <source>selectRound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_view.ui" line="387"/>
        <source>icarus verilog (Recommended)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_view.ui" line="392"/>
        <source>Native (Python)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_view.ui" line="416"/>
        <source>Ready</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/help.ui" line="183"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Source code:&lt;/span&gt;&lt;a href=&quot;https://gitlab.com/WilferCiro/sintel&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;&lt;br/&gt;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Documentation: &lt;/span&gt;&lt;a href=&quot;https://sintel.readthedocs.io/en/latest/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;&lt;br/&gt;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Bug reports and suggestions:&lt;br/&gt;Icons webpage: &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/help.ui" line="190"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://gitlab.com/WilferCiro/sintel&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#00a6ff;&quot;&gt;https://gitlab.com/WilferCiro/sintel&lt;/span&gt;&lt;/a&gt;&lt;a href=&quot;https://gitlab.com/WilferCiro/sintel&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;&lt;br/&gt;&lt;/span&gt;&lt;/a&gt;&lt;a href=&quot;https://sintel.readthedocs.io/en/latest/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#00a6ff;&quot;&gt;https://sintel.readthedocs.io/en/latest&lt;/span&gt;&lt;/a&gt;&lt;a href=&quot;https://sintel.readthedocs.io/en/latest/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;&lt;br/&gt;&lt;/span&gt;&lt;/a&gt;&lt;a href=&quot;https://gitlab.com/WilferCiro/sintel/issues&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#00a6ff;&quot;&gt;https://gitlab.com/WilferCiro/sintel/issues&lt;br/&gt;&lt;/span&gt;&lt;/a&gt;&lt;a href=&quot;https://iconos8.es/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#00aeff;&quot;&gt;https://iconos8.es&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/new_project_menu.ui" line="41"/>
        <source>Select project folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/new_project_menu.ui" line="77"/>
        <source>/home/user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_menu.ui" line="56"/>
        <source>          Add component</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_menu.ui" line="90"/>
        <source>          Add State Machine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_menu.ui" line="118"/>
        <source>          Export to HDL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_menu.ui" line="164"/>
        <source>          Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_menu.ui" line="357"/>
        <source>labelLine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_menu.ui" line="423"/>
        <source>menuBtnLine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_menu.ui" line="279"/>
        <source>         Export to PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_menu.ui" line="282"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_menu.ui" line="295"/>
        <source>          Export to SVG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_menu.ui" line="308"/>
        <source>          Export to PNG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_menu.ui" line="351"/>
        <source>          Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_menu.ui" line="463"/>
        <source>          About Sintel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/simulation_menu.ui" line="488"/>
        <source>         Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/properties_menu.ui" line="197"/>
        <source>Clock time sampling</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HomeWidget</name>
    <message>
        <location filename="../simulador/home_widget.py" line="100"/>
        <source>Recent projects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../simulador/home_widget.py" line="107"/>
        <source>No recents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../simulador/home_widget.py" line="110"/>
        <source>Open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../simulador/home_widget.py" line="110"/>
        <source>Program files </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../simulador/main_window.py" line="44"/>
        <source>Sintel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PropertiesWindow</name>
    <message>
        <location filename="../simulador/properties_window.py" line="132"/>
        <source>State machine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../simulador/properties_window.py" line="123"/>
        <source>version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../simulador/properties_window.py" line="127"/>
        <source>System input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../simulador/properties_window.py" line="129"/>
        <source>System output</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>homeView</name>
    <message>
        <location filename="../resources/ui/home_view.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/home_view.ui" line="20"/>
        <source>Logo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/home_view_recent.ui" line="35"/>
        <source>Go back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/home_view_recent.ui" line="68"/>
        <source>Sintel examples</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/home_view.ui" line="37"/>
        <source>Welcome to Sintel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/home_view.ui" line="50"/>
        <source>Version 0.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/home_view.ui" line="113"/>
        <source>Open a project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/home_view.ui" line="140"/>
        <source>Search in a location in computer for a project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/home_view.ui" line="231"/>
        <source>Create a project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/home_view.ui" line="258"/>
        <source>Save a new project in a location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/home_view.ui" line="286"/>
        <source>Ctrl+N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/home_view.ui" line="566"/>
        <source>Example projects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/home_view.ui" line="582"/>
        <source>Recent projects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/home_view.ui" line="714"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/home_view.ui" line="724"/>
        <source>Add component</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/home_view.ui" line="734"/>
        <source>Add state machine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/home_view.ui" line="757"/>
        <source>Nothing to show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/home_view.ui" line="704"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>propertiesModal</name>
    <message>
        <location filename="../resources/ui/subsignal_connections.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/properties_window.ui" line="34"/>
        <source>Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/properties_window.ui" line="58"/>
        <source>Component name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/properties_window.ui" line="87"/>
        <source>id component</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/properties_window.ui" line="100"/>
        <source>v. 0.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/properties_window.ui" line="110"/>
        <source>isInputOutput</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/properties_window.ui" line="138"/>
        <source>Connectors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/properties_window.ui" line="148"/>
        <source>Default Values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/properties_window.ui" line="154"/>
        <source>Set to &quot;-1&quot; for evit the default value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/properties_window.ui" line="178"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/properties_window.ui" line="192"/>
        <source>Falling edge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/properties_window.ui" line="197"/>
        <source>Rising edge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/properties_window.ui" line="205"/>
        <source>Clock flank</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/properties_window.ui" line="227"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/properties_window.ui" line="244"/>
        <source>&lt;b&gt;Created by:&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/properties_window.ui" line="257"/>
        <source>Creator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/properties_window.ui" line="280"/>
        <source>&lt;b&gt;Description:&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/properties_window.ui" line="290"/>
        <source>Component Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/subsignal_connections.ui" line="37"/>
        <source>Signal name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/subsignal_connections.ui" line="47"/>
        <source>Select the bits that are connected in the pins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/subsignal_connections.ui" line="71"/>
        <source>Component 1 ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/subsignal_connections.ui" line="85"/>
        <source>Component 2 ID</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>recentWidget</name>
    <message>
        <location filename="../resources/ui/recent_widget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/recent_widget.ui" line="43"/>
        <source>Project Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../resources/ui/recent_widget.ui" line="67"/>
        <source>Project path</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
