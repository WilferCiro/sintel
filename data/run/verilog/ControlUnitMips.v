module ControlUnitMips #(parameter bus6_LEN = 6, parameter bus3_LEN = 3) ( input [bus6_LEN - 1 : 0]opCode, input [bus6_LEN - 1 : 0]funCode, output reg RegWrite, output reg memToReg, output reg memWrite, output reg branch, output reg [bus3_LEN - 1 : 0]aluControl, output reg aluSRC, output reg regDst, output reg jump);

always @(*)
begin
	
	if ( opCode == 6'd0 && funCode == 6'd32 ) begin 
		RegWrite <= 1'd1;
			memToReg <= 1'd0;
			memWrite <= 1'd0;
			branch <= 1'd0;
			aluControl <= 3'd0;
			aluSRC <= 1'd0;
			regDst <= 1'd1;
			jump <= 1'd0;
			
end else 	if ( opCode == 6'd0 && funCode == 6'd34 ) begin 
		RegWrite <= 1'd0;
			memToReg <= 1'd0;
			memWrite <= 1'd0;
			branch <= 1'd0;
			aluControl <= 3'd1;
			aluSRC <= 1'd0;
			regDst <= 1'd1;
			jump <= 1'd0;
			
end else 	if ( opCode == 6'd0 && funCode == 6'd36 ) begin 
		RegWrite <= 1'd0;
			memToReg <= 1'd0;
			memWrite <= 1'd0;
			branch <= 1'd0;
			aluControl <= 3'd2;
			aluSRC <= 1'd0;
			regDst <= 1'd1;
			jump <= 1'd0;
			
end else 	if ( opCode == 6'd35 ) begin 
		RegWrite <= 1'd0;
			memToReg <= 1'd1;
			memWrite <= 1'd0;
			branch <= 1'd0;
			aluControl <= 3'd0;
			aluSRC <= 1'd1;
			regDst <= 1'd0;
			jump <= 1'd0;
			
end else 	if ( opCode == 6'd43 ) begin 
		RegWrite <= 1'd0;
			memToReg <= 1'd0;
			memWrite <= 1'd1;
			branch <= 1'd0;
			aluControl <= 3'd0;
			aluSRC <= 1'd1;
			regDst <= 1'd0;
			jump <= 1'd0;
			
end else 	if ( opCode == 6'd4 ) begin 
		RegWrite <= 1'd0;
			memToReg <= 1'd0;
			memWrite <= 1'd0;
			branch <= 1'd1;
			aluControl <= 3'd1;
			aluSRC <= 1'd0;
			regDst <= 1'd0;
			jump <= 1'd0;
			
end else 	if ( opCode == 6'd2 ) begin 
		RegWrite <= 1'd0;
			memToReg <= 1'd0;
			memWrite <= 1'd0;
			branch <= 1'd0;
			aluControl <= 3'd0;
			aluSRC <= 1'd0;
			regDst <= 1'd0;
			jump <= 1'd1;
			
end else 	if ( opCode == 6'd15 ) begin 
		RegWrite <= 1'd1;
			memToReg <= 1'd0;
			memWrite <= 1'd0;
			branch <= 1'd0;
			aluControl <= 3'd0;
			aluSRC <= 1'd1;
			regDst <= 1'd0;
			jump <= 1'd0;
			
end else 	if ( opCode == 6'd8 ) begin 
		RegWrite <= 1'd1;
			memToReg <= 1'd0;
			memWrite <= 1'd0;
			branch <= 1'd0;
			aluControl <= 3'd0;
			aluSRC <= 1'd1;
			regDst <= 1'd0;
			jump <= 1'd0;
			
	end else begin
		RegWrite <= 1'd0;
			memToReg <= 1'd0;
			memWrite <= 1'd0;
			branch <= 1'd0;
			aluControl <= 3'd0;
			aluSRC <= 1'd0;
			regDst <= 1'd0;
			jump <= 1'd0;
			
	end
end

endmodule
