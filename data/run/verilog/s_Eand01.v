module Eand01
	#(
		parameter buses_LEN = 1
	)
	(
		input [buses_LEN - 1 : 0] pIn1,
		input [buses_LEN - 1 : 0] pIn2,
		output [buses_LEN - 1 : 0] pOut
	);

	assign pOut = pIn1 & pIn2;

endmodule 
