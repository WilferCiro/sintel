module orInternal
	#(
		parameter Bus_LEN = 2
	)
	(
		input [Bus_LEN - 1 : 0] pIn,
		output  pOut
	);
// Put the component code here
assign pOut = | pIn;



endmodule 
