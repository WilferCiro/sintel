module testbench();

	wire [0:0] flagN_input;
	reg [0:0] generalCLK_pOut;
	reg [0:0] generalRST_out;
	integer file, file_read, ri, cont;

  main UUT(
		.flagN_input(flagN_input),
		.generalCLK_pOut(generalCLK_pOut),
		.generalRST_out(generalRST_out)
	);

	initial begin
		cont = 0;
		file = $fopen("output.txt", "w");
		$fwrite(file, "Signal_14,Signal_59,Signal_87,Signal_84,Signal_58,Signal_57,Signal_71,Signal_44,Signal_43,Signal_68,Signal_61,Signal_16,Signal_91,Signal_65,Signal_66,Signal_55,Signal_49,Signal_56\n");

		generalCLK_pOut = 1'd1;
		generalCLK_pOut = 1'd1;
		generalRST_out = 1'd0;
		#5;
		cont = 3;
	end

	always begin
		if (cont == 3) begin
			file_read = $fopenr("io.txt");
			ri = $fscanf(file_read, "%d\n", generalRST_out);
			$fclose(file_read);
		end

		#1
		$fwrite(file, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n", testbench.UUT.Signal_14, testbench.UUT.Signal_59, testbench.UUT.Signal_87, testbench.UUT.Signal_84, testbench.UUT.Signal_58, testbench.UUT.Signal_57, testbench.UUT.Signal_71, testbench.UUT.Signal_44, testbench.UUT.Signal_43, testbench.UUT.Signal_68, testbench.UUT.Signal_61, testbench.UUT.Signal_16, testbench.UUT.Signal_91, testbench.UUT.Signal_65, testbench.UUT.Signal_66, testbench.UUT.Signal_55, testbench.UUT.Signal_49, testbench.UUT.Signal_56);
		#1;
		generalCLK_pOut = ~generalCLK_pOut;
		$stop;
	end
endmodule // testbench end
