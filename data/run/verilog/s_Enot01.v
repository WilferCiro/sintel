module Enot01
	#(
		parameter buses_LEN = 1
	)
	(
		input [buses_LEN - 1 : 0] pIn,
		output [buses_LEN - 1 : 0] pOut
	);
	assign pOut = ~ pIn;

endmodule
