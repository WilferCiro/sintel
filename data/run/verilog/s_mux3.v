module mux3
	#(
		parameter IOBus_LEN = 1
	)
	(
		input [2 : 0] Sel,
		input [IOBus_LEN - 1 : 0] pInA,
		input [IOBus_LEN - 1 : 0] pInB,
		input [IOBus_LEN - 1 : 0] pInC,
		input [IOBus_LEN - 1 : 0] pInD,
		input [IOBus_LEN - 1 : 0] pInE,
		input [IOBus_LEN - 1 : 0] pInF,
		input [IOBus_LEN - 1 : 0] pInG,
		input [IOBus_LEN - 1 : 0] pInH,
		output reg [IOBus_LEN - 1 : 0] pOut
	);
always @(*)
	begin
		case(Sel)
			3'b000: pOut = pInA;
			3'b001: pOut = pInB;
			3'b010: pOut = pInC;
			3'b011: pOut = pInD;
			3'b100: pOut = pInE;
			3'b101: pOut = pInF;
			3'b110: pOut = pInG;
			3'b111: pOut = pInH;
			default: pOut = 0;
		endcase
	end



endmodule
