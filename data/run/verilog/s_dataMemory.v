module dataMemory_rising
	#(
		parameter Bus32_LEN = 8,
		parameter MEMSIZE_PAR = 99
	)
	(
		input WE,
		input [Bus32_LEN - 1 : 0] WD,
		input clk,
		input [Bus32_LEN - 1 : 0] A,
		output [Bus32_LEN - 1 : 0] RD
	);
// Put the component code here
// Internal data definition
reg [Bus32_LEN - 1 : 0] MEM [0 : MEMSIZE_PAR];

// Out assignation
assign RD = MEM[A];

// Index 0 of memory has the 0 value
initial begin
	MEM[0] = {Bus32_LEN{1'b0}};
end

// sram description
always @(posedge clk)
begin
	if (WE)
		MEM[A] = WD;
end


endmodule 

module dataMemory_falling
	#(
		parameter Bus32_LEN = 8,
		parameter MEMSIZE_PAR = 99
	)
	(
		input WE,
		input [Bus32_LEN - 1 : 0] WD,
		input clk,
		input [Bus32_LEN - 1 : 0] A,
		output [Bus32_LEN - 1 : 0] RD
	);
// Put the component code here
// Internal data definition
reg [Bus32_LEN - 1 : 0] MEM [0 : MEMSIZE_PAR];

// Out assignation
assign RD = MEM[A];

// Index 0 of memory has the 0 value
initial begin
	MEM[0] = {Bus32_LEN{1'b0}};
end

// sram description
always @(negedge clk)
begin
	if (WE)
		MEM[A] = WD;
end




endmodule 
