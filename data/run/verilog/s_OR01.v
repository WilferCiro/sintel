module OR01
	#(
		parameter Bus_LEN = 1
	)
	(
		input [Bus_LEN - 1 : 0] pIn1,
		input [Bus_LEN - 1 : 0] pIn2,
		output [Bus_LEN - 1 : 0] pOut
	);
assign pOut = pIn1 | pIn2;




endmodule 
