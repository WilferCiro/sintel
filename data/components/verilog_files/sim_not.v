module not01(pIn, pOut);
	parameter buses_LEN = 8;
	input [buses_LEN - 1 : 0] pIn;
	output [buses_LEN - 1 : 0] pOut;

	assign pOut = ~ pIn;
endmodule
