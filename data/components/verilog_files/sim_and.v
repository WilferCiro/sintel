module and01(pIn1, pIn2, pOut);
	parameter buses_LEN = 8;
	input [buses_LEN - 1 : 0] pIn1;
	input [buses_LEN - 1 : 0] pIn2;
	output [buses_LEN - 1 : 0] pOut;

	assign pOut = pIn1 & pIn2;
endmodule
