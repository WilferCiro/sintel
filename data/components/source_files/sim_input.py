#!/usr/bin/env python
from common import Component

class input01(Component):
	"""
		Class than simulates an alu circuit
	"""
	def setup(self, properties):
		self._addOutput("out")
		self._value = int(1)

		self._startValue = int(properties["start_value"])

	def initialUpdate(self):
		if self._startValue != -1:
			self.write("out", self._startValue)

	def update(self):
		self.write("out", self._value)

	def setProperties(self, properties):
		self._value = [int(prop["value"]) for prop in properties if prop["ID"] == "value"][0]
		#for prop in properties:
		#	if prop["ID"] == "value":
		#		self._value = int(prop["value"])
		#		self.write("out", self._value)
