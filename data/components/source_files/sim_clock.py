#!/usr/bin/env python
from common import Component

class clock01(Component):
	"""
		Class than simulates an alu circuit
	"""
	def setup(self, properties):
		self._addOutput("pOut")
		self._currentValue = 1

	def update(self):
		self._currentValue = 1 if self._currentValue == 0 else 0
		self.write("pOut", self._currentValue)
