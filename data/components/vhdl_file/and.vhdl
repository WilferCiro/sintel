library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity and01 is
	generic(
		buses_LEN:natural := 8
	);
	port(
		-- Inputs
		pIn1: in std_logic_vector(buses_LEN - 1 downto 0);
		pIn2: in std_logic_vector(buses_LEN - 1 downto 0);
		-- Outputs
		pOut: out std_logic_vector(buses_LEN - 1 downto 0)
	);
end entity;

architecture rtl of and01 is
	begin
		pOut <= pIn1 and pIn2;
end architecture;
