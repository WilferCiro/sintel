#!/usr/bin/env python
from common import Component

class FlashMem(Component):
	def setup(self, properties):
		self._addInput("Dir")
		self._addOutput("opCode")
		self.userSetup(properties)

	def userSetup(self, properties):
		file = open("/home/ciro/instructions.txt", "r+")
		self._dataRead = str(file.read()).replace("\n", "").replace("\t", "").replace(" ", "")
		file.close()

	def update(self):
		self._dataSize = self.getSize("opCode")
		dir = self.read("Dir")
		if str(dir).isnumeric():
			self.write("opCode", list(self._dataRead[(self._dataSize) * dir : (self._dataSize * dir) + self._dataSize]), base = 2)
