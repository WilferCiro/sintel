#!/usr/bin/env python
from common import Component
class adder(Component):
	def setup(self, properties):
		self._addInput("in1")
		self._addInput("in2")
		self._addOutput("Out")
		self.userSetup(properties)
	def userSetup(self, properties):
		# Insert your setup code here
		pass
	def update(self):
		# Insert your update code here
		in1 = self.read("in1")
		in2 = self.read("in2")
		if type(in1) == int and type(in2) == int:
			self.write("Out", in1 + in2)
		else:
			self.write("Out", "U")
