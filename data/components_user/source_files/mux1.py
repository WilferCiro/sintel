#!/usr/bin/env python
from common import Component

class mux1(Component):
	def setup(self, properties):
		self._addInput("Sel")
		self._addInput("pInA")
		self._addInput("pInB")
		self._addOutput("pOut")
		self.userSetup(properties)
		
	def userSetup(self, properties):
		# Insert your setup code here
		pass

	def update(self):
		# Insert your update code here
		selValue = self.read("Sel")
		if selValue == 0:
			self.write("pOut", self.read("pInA"))
		elif selValue == 1:
			self.write("pOut", self.read("pInB"))
		else:
			self.write("pOut", 0)
