#!/usr/bin/env python
from common import Component

class sram(Component):
	def setup(self, properties):
		self._addInput("dirA")
		self._addInput("dirB")
		self._addInput("enWr")
		self._addInput("clk")
		self._addInput("valWr")
		self._addInput("rst")
		self._addInput("dirWrite")
		self._addOutput("readA")
		self._addOutput("readB")
		self.userSetup(properties)

	def userSetup(self, properties):
		# Insert your setup code here
		self.memory = ['U'] * 32
		self.memory[0] = 0

	def readUpdate(self):
		# Insert your update code here
		self.dirA = self.read("dirA")
		self.dirB = self.read("dirB")
		self.enWr = self.read("enWr")
		self.valWr = self.read("valWr")
		self.dirWrite = self.read("dirWrite")

	def writeUpdate(self):
		# Insert the write code here
		if self.enWr == 1:
			self.memory[self.dirWrite] = self.valWr
		if type(self.dirA) == int:
			self.dataA = self.memory[self.dirA]
		else:
			self.dataA = "U"
		if type(self.dirB) == int:
			self.dataB = self.memory[self.dirB]
		else:
			self.dataB = "U"

		print(self.memory)
		self.write("readA", self.dataA)
		self.write("readB", self.dataB)
