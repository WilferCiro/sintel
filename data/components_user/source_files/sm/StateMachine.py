#!/usr/bin/env python
from common import Component, bin2dec

class StateMachine(Component):
	"""
		Class than simulates an state machine
	"""
	def setup(self, params):
		self._addInput("pIn1")
		self._addOutput("pOut1")

		self._addInput("clk")
		self._addInput("rst")

		self._currentState = "SS"
		self._nextState = "SS"
		self._resetPos = 0#int(params["reset"])

	def readUpdate(self):
		resetVal = self.read("rst")
		if resetVal == self._resetPos:
			self._nextState = "SS"
		self._currentState = self._nextState
		if self._currentState == 'SS':
			self._nextState = 'FS'
		elif self._currentState == 'FS':
			self._nextState = 'SS'

	def writeUpdate(self):
		if self._currentState == 'SS':
			self.write("pOut1", 30)
		elif self._currentState == 'FS':
			if  self.read("pIn1") == 1 :
				self.write("pOut1", 0)
			else:
				self.write("pOut1", 2)
