#!/usr/bin/env python
from common import Component, bin2dec

class AVRControlUnit(Component):
	"""
		Class than simulates an state machine
	"""
	def setup(self, params):
		self._addInput("opCode")
		self._addOutput("aluSel")
		self._addOutput("enablePC")
		self._addOutput("wrEnable")
		self._addOutput("writeSel")
		self._addOutput("pcSel")

		self._addInput("clk")
		self._addInput("rst")

		self._currentState = "SS"
		self._nextState = "SS"
		self._resetPos = 0#int(params["reset"])

	def readUpdate(self):
		resetVal = self.read("rst")
		if resetVal == self._resetPos:
			self._nextState = "SS"
		self._currentState = self._nextState
		if self._currentState == 'SS':
			self._nextState = 'FS'
		elif self._currentState == 'FS':
			self._nextState = 'SS'

	def writeUpdate(self):
		if self._currentState == 'SS':
			self.write("aluSel", 0)
			self.write("enablePC", 0)
			self.write("wrEnable", 0)
			self.write("writeSel", 0)
			self.write("pcSel", 0)
		elif self._currentState == 'FS':
			self.write("aluSel", 0)
			self.write("enablePC", 0)
			self.write("wrEnable", 0)
			self.write("writeSel", 0)
			self.write("pcSel", 0)
