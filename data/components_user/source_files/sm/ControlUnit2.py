#!/usr/bin/env python
from common import Component, bin2dec

class ControlUnit(Component):
	"""
		Class than simulates an state machine
	"""
	def setup(self, params):
		self._addInput("opCode")
		self._addInput("aluRes")
		self._addInput("flags")
		self._addOutput("aluSel")
		self._addOutput("pcSel")
		self._addOutput("pcValue")
		self._addOutput("WrE")
		self._addOutput("WrV")
		self._addOutput("WrD")
		self._addOutput("readRSel")
		self._addOutput("carryInSel")
		self._addOutput("valR2")
		self._addOutput("flagsMux")
		self._addOutput("selFlags")
		self._addOutput("pcEn")

		self._addInput("clk")
		self._addInput("rst")

		self._currentState = "SS"
		self._nextState = "SS"
		self._resetPos = 0#int(params["reset"])
		self._firstTime = True

	def readUpdate(self):
		self._opCode5 = self.read("opCode", bits = [0,1,2,3,4,5])
		self._opCode3 = self.read("opCode", bits = [0,1,2,3])
		self._opCodeValue = self.read('opCode', bits = [4,5,6,7,12,13,14,15])
		self._opCodeDir = self.read('opCode', bits = [8,9,10,11])
		self._opCode6 = self.read("opCode", bits = [0,1,2,3,4,5,6])

	def writeUpdate(self):
		self._body()

	def _body(self):
		if self.read("rst") == self._resetPos:
			self._nextState = "SS"

		self._currentState = self._nextState
		if self._currentState == 'SS':
			print("--> SS")
			if self.read("rst") != self._resetPos:
				self._nextState = 'FS'
			self.write("aluSel", 0)
			self.write("pcSel", 0)
			self.write("pcValue", 2)
			self.write("WrE", 0)
			self.write("WrV", 0)
			self.write("WrD", 0)
			self.write("readRSel", 0)
			self.write("carryInSel", 0)
			self.write("valR2", 0)
			self.write("flagsMux", 0)
			self.write("selFlags", 0)
			self.write("pcEn", 0)
		elif self._currentState == 'FS':
			self._nextState = 'SS'
			if self._opCode5 == 3:
				print("--> ADD")
				self.write("aluSel", 0)
				self.write("pcSel", 0)
				self.write("pcValue", 0)
				self.write("WrE", 1)
				self.write("WrV", self.read("aluRes"))
				self.write("WrD", self._opCodeDir)
				self.write("readRSel", 0)
				self.write("carryInSel", 0)
				self.write("valR2", 0)
				self.write("flagsMux", 0)
				self.write("selFlags", 0)
				self.write("pcEn", 1)
			elif self._opCode3 == 14:
				print("--> LDI")
				self.write("aluSel", 0)
				self.write("pcSel", 0)
				self.write("pcValue", 0)
				self.write("WrE", 1)
				self.write("WrV", self._opCodeValue)
				self.write("WrD", self._opCodeDir)
				self.write("readRSel", 0)
				self.write("carryInSel", 0)
				self.write("valR2", 0)
				self.write("flagsMux", 0)
				self.write("selFlags", 0)
				self.write("pcEn", 1)
			elif self._opCode6 == 74:
				print("--> JMP")
				self.write("aluSel", 0)
				self.write("pcSel", 1)
				self.write("pcValue", 2)
				self.write("WrE", 0)
				self.write("WrV", 0)
				self.write("WrD", 0)
				self.write("readRSel", 0)
				self.write("carryInSel", 0)
				self.write("valR2", 0)
				self.write("flagsMux", 0)
				self.write("selFlags", 0)
				self.write("pcEn", 1)

		#print(self._currentState, self.read("opCode", bits = [0,1,2,3,4,5,6]))
