#!/usr/bin/env python
from common import Component, bin2dec

class ControlUnit(Component):
	"""
		Class than simulates an state machine
	"""
	def setup(self, params):
		self._addInput("opCode")
		self._addInput("flags")
		self._addOutput("aluSel")
		self._addOutput("pcSel")
		self._addOutput("pcValue")
		self._addOutput("WrE")
		self._addOutput("WrV")
		self._addOutput("WrD")
		self._addOutput("readRSel")
		self._addOutput("carryInSel")
		self._addOutput("valR2")
		self._addOutput("flagsMux")
		self._addOutput("selWriteVal")
		self._addOutput("pcEn")

		self._addInput("clk")
		self._addInput("rst")

		self._currentState = "SS"
		self._nextState = "SS"
		self._resetPos = 0#int(params["reset"])

	def readUpdate(self):
		resetVal = self.read("rst")
		if resetVal == self._resetPos:
			self._nextState = "SS"
		self._currentState = self._nextState
		if self._currentState == 'SS':
			self._nextState = 'FS'
		elif self._currentState == 'FS':
			self._nextState = 'SS'

	def writeUpdate(self):
		if self._currentState == 'SS':
			self.write("aluSel", 0)
			self.write("pcSel", 0)
			self.write("pcValue", 0)
			self.write("WrE", 0)
			self.write("WrV", 0)
			self.write("WrD", 0)
			self.write("readRSel", 0)
			self.write("carryInSel", 0)
			self.write("valR2", 0)
			self.write("flagsMux", 0)
			self.write("selWriteVal", 0)
			self.write("pcEn", 0)
		elif self._currentState == 'FS':
			if  self.read("opCode", bits = '[15:10]') == 3 :
				self.write("aluSel", 0)
				self.write("pcSel", 0)
				self.write("pcValue", 0)
				self.write("WrE", 1)
				self.write("WrV", 0)
				self.write("WrD", self.read('opCode', bits = '[6:4]'))
				self.write("readRSel", 0)
				self.write("carryInSel", 0)
				self.write("valR2", 0)
				self.write("flagsMux", 0)
				self.write("selFlags", 0)
				self.write("pcEn", 1)
			elif  self.read("opCode", bits = '[15:12]') == 14 :
				self.write("aluSel", 0)
				self.write("pcSel", 0)
				self.write("pcValue", 0)
				self.write("WrE", 1)
				self.write("WrV", self.read('opCode', bits = '[3:0]'))
				self.write("WrD", self.read('opCode', bits = '[6:4]'))
				self.write("readRSel", 0)
				self.write("carryInSel", 0)
				self.write("valR2", 0)
				self.write("flagsMux", 0)
				self.write("selFlags", 1)
				self.write("pcEn", 1)
			elif  self.read("opCode", bits = '[15:9]') == 74 :
				self.write("aluSel", 0)
				self.write("pcSel", 1)
				self.write("pcValue", self.read('opCode', bits = '[7:0]'))
				self.write("WrE", 0)
				self.write("WrV", 0)
				self.write("WrD", 0)
				self.write("readRSel", 0)
				self.write("carryInSel", 0)
				self.write("valR2", 0)
				self.write("flagsMux", 0)
				self.write("selFlags", 0)
				self.write("pcEn", 1)
			else:
				self.write("aluSel", 0)
				self.write("pcSel", 0)
				self.write("pcValue", 0)
				self.write("WrE", 0)
				self.write("WrV", 0)
				self.write("WrD", 0)
				self.write("readRSel", 0)
				self.write("carryInSel", 0)
				self.write("valR2", 0)
				self.write("flagsMux", 0)
				self.write("selWriteVal", 0)
				self.write("pcEn", 0)
