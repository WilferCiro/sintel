#!/usr/bin/env python
from common import Component


class mux3(Component):
	def setup(self, properties):
		self._addInput("Sel")
		self._addInput("pInA")
		self._addInput("pInB")
		self._addInput("pInC")
		self._addInput("pInD")
		self._addInput("pInE")
		self._addInput("pInF")
		self._addInput("pInG")
		self._addInput("pInH")
		self._addOutput("pOut")
		self.userSetup(properties)
	def userSetup(self, properties):
		# Insert your setup code here
		pass
		
		
		
		
		
			
	def update(self):
		# Insert your update code here
		selValue = self.read("Sel")
		if selValue == 0:
			self.write("pOut", self.read("pInA"))
		elif selValue == 1:
			self.write("pOut", self.read("pInB"))
		elif selValue == 2:
			self.write("pOut", self.read("pInC"))
		elif selValue == 3:
			self.write("pOut", self.read("pInD"))
		elif selValue == 4:
			self.write("pOut", self.read("pInE"))
		elif selValue == 5:
			self.write("pOut", self.read("pInF"))
		elif selValue == 6:
			self.write("pOut", self.read("pInG"))
		elif selValue == 7:
			self.write("pOut", self.read("pInH"))
		else:
			self.write("pOut", 0)
		
