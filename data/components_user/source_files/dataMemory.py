#!/usr/bin/env python
from common import Component


class dataMemory(Component):
	def setup(self, properties):
		self._addInput("WE")
		self._addInput("WD")
		self._addInput("clk")
		self._addInput("A")
		self._addOutput("RD")
		self.userSetup(properties)
	def userSetup(self, properties):
		# Insert your setup code here
		self.size = int(properties["MEMSIZE"])
		self.memory = ['U'] * self.size
		self.memory[0] = 0

	def readUpdate(self):
		# Insert your update code here
		self.dir = self.read("A")
		self.WE = self.read("WE")
		self.WD = self.read("WD")

	def writeUpdate(self):
		# Insert the write code here
		if self.WE == 1:
			self.memory[self.dir] = self.WD
		if type(self.dir) == int:
			self.dataWr = self.memory[self.dir]
		else:
			self.dataWr = "U"
		self.write("RD", self.dataWr)
