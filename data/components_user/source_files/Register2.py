#!/usr/bin/env python
from common import Component

class Register2(Component):
	def setup(self, properties):
		self._addInput("inD")
		self._addInput("ena")
		self._addInput("rst")
		self._addInput("clk")
		self._addOutput("outQ")
		self.userSetup(properties)

	def userSetup(self, properties):
		self._resetPos = int(properties["reset"])
		self._firstTime = True
		self._lastValue = None
		self._lastReaded = 0

	def readUpdate(self):
		self._readValue = self.read("inD")
		self._enableValue = self.read("ena")

	def writeUpdate(self):
		resetVal = self.read("rst")
		if resetVal == self._resetPos:
			self.write("outQ", 0)
		elif self.read("ena") == 1:
			self.write("outQ", self._readValue)
		else:
			self._outputValue = self.readOutput("outQ")
			self.write("outQ", self._outputValue)
