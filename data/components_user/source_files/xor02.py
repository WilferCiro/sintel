#!/usr/bin/env python
from common import Component

class xor02(Component):
	def setup(self, properties):
		self._addInput("pIn1")
		self._addInput("pIn2")
		self._addOutput("pOut")
		self.userSetup(properties)
	def userSetup(self, properties):
		# Insert your setup code here
		pass
			
	def update(self):
		# Insert your update code here
		value1 = self.read("pIn1", base = 2)
		value2 = self.read("pIn2", base = 2)
		if len(value1) == len(value2):
			write = ['0' if value1[i] == value2[i] else '1' for i in range(len(value1))]
			self.write("pOut", write, base = 2)
		else:
			self.write("pOut", 'U')
		
