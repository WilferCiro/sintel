#!/usr/bin/env python
from common import Component



class orInternal(Component):
	def setup(self, properties):
		self._addInput("pIn")
		self._addOutput("pOut")
		self.userSetup(properties)
	def userSetup(self, properties):
		# Insert your setup code here
		pass
	def update(self):
		# Insert your update code here
		data = self.read("pIn", base = 2)
		self.write("pOut", 1 if 1 in data else 0)
