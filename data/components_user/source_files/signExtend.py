#!/usr/bin/env python
from common import Component



class signExtend(Component):
	def setup(self, properties):
		self._addInput("dataIn")
		self._addOutput("dataOut")
		self.userSetup(properties)
	def userSetup(self, properties):
		# Insert your setup code here
		pass



	def update(self):
		# Insert your update code here
		data = self.read("dataIn", base = 2)
		sizeIn = self.getSize("dataIn")
		sizeOut= self.getSize("dataOut")
		dataWr = [data[0]] * (sizeOut - sizeIn)
		dataWr.extend(data)
		self.write("dataOut",  dataWr)
