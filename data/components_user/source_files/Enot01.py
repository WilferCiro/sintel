#!/usr/bin/env python
from common import Component


class Enot01(Component):
	def setup(self, properties):
		self._addInput("pIn")
		self._addOutput("pOut")
		self.userSetup(properties)
	def userSetup(self, properties):
		pass
		
			
	def update(self):
		value = self.read("pIn", base = 2)
		write = ['0' if i == '1' else '1' if i == '0' else str(i) for i in value]
		self.write("pOut", write, base = 2)
		
