#!/usr/bin/env python
from common import Component
class sub(Component):
	def setup(self, properties):
		self._addInput("in1")
		self._addInput("in2")
		self._addOutput("Out")
		self.userSetup(properties)
	def userSetup(self, properties):
		# Insert your setup code here
		pass
	def update(self):
		# Insert your update code here
		self.write("Out", self.read("in1") - self.read("in2"))
