module sub
	#(
		parameter Bus_LEN = 1
	)
	(
		input [Bus_LEN - 1 : 0] in1,
		input [Bus_LEN - 1 : 0] in2,
		output [Bus_LEN - 1 : 0] Out
	);
// Put the component code here
assign Out = in1 - in2;
endmodule 
