module sram_rising
	#(
		parameter Bus32_LEN = 32
	)
	(
		input [4 : 0] dirA,
		input [4 : 0] dirB,
		input enWr,
		input clk,
		input [Bus32_LEN - 1 : 0] valWr,
		input rst,
		input [4 : 0] dirWrite,
		output [Bus32_LEN - 1 : 0] readA,
		output [Bus32_LEN - 1 : 0] readB
	);
// Put the component code here
reg [Bus32_LEN - 1 : 0] MEM [0 : 31];

assign readA = MEM[dirA];
assign readB = MEM[dirB];

initial begin
	MEM[0] = {Bus32_LEN{1'b0}};
end

always @(posedge clk)
begin
	if (enWr)
		MEM[dirWrite] = valWr;
end




endmodule 

module sram_falling
	#(
		parameter Bus32_LEN = 32
	)
	(
		input [4 : 0] dirA,
		input [4 : 0] dirB,
		input enWr,
		input clk,
		input [Bus32_LEN - 1 : 0] valWr,
		input rst,
		input [4 : 0] dirWrite,
		output [Bus32_LEN - 1 : 0] readA,
		output [Bus32_LEN - 1 : 0] readB
	);
// Put the component code here
reg [Bus32_LEN - 1 : 0] MEM [0 : 31];

assign readA = MEM[dirA];
assign readB = MEM[dirB];

initial begin
	MEM[0] = {Bus32_LEN{1'b0}};
end

always @(posedge clk)
begin
	if (enWr)
		MEM[dirWrite] = valWr;
end




endmodule 
