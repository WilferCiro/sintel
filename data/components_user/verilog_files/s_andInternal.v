module andInternal
	#(
		parameter Bus_LEN = 2
	)
	(
		input [Bus_LEN - 1 : 0] In,
		output  Out
	);
// Put the component code here
assign Out = &In;
endmodule 
