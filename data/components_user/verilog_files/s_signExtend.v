module signExtend
	#(
		parameter BusIn_LEN = 1,
		parameter BusOut_LEN = 1
	)
	(
		input [BusIn_LEN - 1 : 0] dataIn,
		output [BusOut_LEN - 1 : 0] dataOut
	);
// Put the component code here
assign dataOut = {{(BusOut_LEN - BusIn_LEN){dataIn[BusIn_LEN - 1]}}, dataIn};



endmodule 
