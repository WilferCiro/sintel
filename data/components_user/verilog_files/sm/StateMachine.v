module StateMachine_rising #(parameter bus8_LEN = 8) (input clk, input rst, input [bus8_LEN - 1 : 0]pIn1, output reg [bus8_LEN - 1 : 0]pOut1);

parameter SS = 2'd0, FS = 2'd1;
reg [1 : 0] CS;
reg [1 : 0] NS;

always @(posedge clk)
begin
	if (rst == 0)
		CS <= SS;
	else
		CS <= NS;
end

always @(CS, pIn1)
begin
 case(CS)
 		SS : begin
			
NS <= FS;
		end
		FS : begin
			
NS <= SS;
		end
	endcase
end

always @(CS)
begin
	case(CS)
		SS : begin
			
				pOut1 <= 8'd30;
			
		end
		FS : begin
			
			if ( pIn1 == 8'd1 ) begin 
				pOut1 <= 8'd0;
				
			end else begin
				pOut1 <= 8'd2;
				end 
		end
	endcase
end
endmodule


module StateMachine_falling #(parameter bus8_LEN = 8) (input clk, input rst, input [bus8_LEN - 1 : 0]pIn1, output reg [bus8_LEN - 1 : 0]pOut1);

parameter SS = 2'd0, FS = 2'd1;
reg [1 : 0] CS;
reg [1 : 0] NS;

always @(negedge clk)
begin
	if (rst == 0)
		CS <= SS;
	else
		CS <= NS;
end

always @(CS, pIn1)
begin
 case(CS)
 		SS : begin
			
NS <= FS;
		end
		FS : begin
			
NS <= SS;
		end
	endcase
end

always @(CS)
begin
	case(CS)
		SS : begin
			
				pOut1 <= 8'd30;
			
		end
		FS : begin
			
			if ( pIn1 == 8'd1 ) begin 
				pOut1 <= 8'd0;
				
			end else begin
				pOut1 <= 8'd2;
				end 
		end
	endcase
end
endmodule
