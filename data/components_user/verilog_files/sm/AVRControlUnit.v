module AVRControlUnit_rising #(parameter bus16_LEN = 16, parameter bus3_LEN = 3) (input clk, input rst, input [bus16_LEN - 1 : 0]opCode, output reg [bus3_LEN - 1 : 0]aluSel, output reg enablePC, output reg wrEnable, output reg writeSel, output reg pcSel);

parameter SS = 2'd0, FS = 2'd1;
reg [1 : 0] CS;
reg [1 : 0] NS;

always @(posedge clk)
begin
	if (rst == 0)
		CS <= SS;
	else
		CS <= NS;
end

always @(CS, opCode)
begin
 case(CS)
 		SS : begin
			
NS <= FS;
		end
		FS : begin
			
NS <= SS;
		end
	endcase
end

always @(CS)
begin
	case(CS)
		SS : begin
			
				aluSel <= 3'd0;
			enablePC <= 1'd0;
			wrEnable <= 1'd0;
			writeSel <= 1'd0;
			pcSel <= 1'd0;
			
		end
		FS : begin
			
				aluSel <= 3'd0;
			enablePC <= 1'd0;
			wrEnable <= 1'd0;
			writeSel <= 1'd0;
			pcSel <= 1'd0;
			
		end
	endcase
end
endmodule


module AVRControlUnit_falling #(parameter bus16_LEN = 16, parameter bus3_LEN = 3) (input clk, input rst, input [bus16_LEN - 1 : 0]opCode, output reg [bus3_LEN - 1 : 0]aluSel, output reg enablePC, output reg wrEnable, output reg writeSel, output reg pcSel);

parameter SS = 2'd0, FS = 2'd1;
reg [1 : 0] CS;
reg [1 : 0] NS;

always @(negedge clk)
begin
	if (rst == 0)
		CS <= SS;
	else
		CS <= NS;
end

always @(CS, opCode)
begin
 case(CS)
 		SS : begin
			
NS <= FS;
		end
		FS : begin
			
NS <= SS;
		end
	endcase
end

always @(CS)
begin
	case(CS)
		SS : begin
			
				aluSel <= 3'd0;
			enablePC <= 1'd0;
			wrEnable <= 1'd0;
			writeSel <= 1'd0;
			pcSel <= 1'd0;
			
		end
		FS : begin
			
				aluSel <= 3'd0;
			enablePC <= 1'd0;
			wrEnable <= 1'd0;
			writeSel <= 1'd0;
			pcSel <= 1'd0;
			
		end
	endcase
end
endmodule
