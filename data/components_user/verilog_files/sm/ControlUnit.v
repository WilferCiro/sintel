module ControlUnit_rising #(parameter bus16_LEN = 16, parameter bus3_LEN = 3, parameter bus8_LEN = 8) (input clk, input rst, input [bus16_LEN - 1 : 0]opCode, input [bus3_LEN - 1 : 0]flags, output reg [bus3_LEN - 1 : 0]aluSel, output reg pcSel, output reg [bus8_LEN - 1 : 0]pcValue, output reg WrE, output reg [bus8_LEN - 1 : 0]WrV, output reg [bus3_LEN - 1 : 0]WrD, output reg readRSel, output reg carryInSel, output reg [bus8_LEN - 1 : 0]valR2, output reg [bus3_LEN - 1 : 0]flagsMux, output reg selWriteVal, output reg pcEn);

parameter SS = 2'd0, FS = 2'd1;
reg [1 : 0] CS;
reg [1 : 0] NS;

always @(posedge clk)
begin
	if (rst == 0)
		CS <= SS;
	else
		CS <= NS;
end

always @(CS, opCode, flags)
begin
 case(CS)
 		SS : begin
			
NS <= FS;
		end
		FS : begin
			
NS <= SS;
		end
	endcase
end

always @(CS)
begin
	case(CS)
		SS : begin
			
				aluSel <= 3'd0;
			pcSel <= 1'd0;
			pcValue <= 8'd0;
			WrE <= 1'd0;
			WrV <= 8'd0;
			WrD <= 3'd0;
			readRSel <= 1'd0;
			carryInSel <= 1'd0;
			valR2 <= 8'd0;
			flagsMux <= 3'd0;
			selWriteVal <= 1'd0;
			pcEn <= 1'd0;
			
		end
		FS : begin
			
			if ( opCode[15:10] == 6'd3 ) begin 
				aluSel <= 3'd0;
				pcSel <= 1'd0;
				pcValue <= 8'd0;
				WrE <= 1'd1;
				WrV <= 8'd0;
				WrD <= opCode[6:4];
				readRSel <= 1'd0;
				carryInSel <= 1'd0;
				valR2 <= 8'd0;
				flagsMux <= 3'd0;
				pcEn <= 1'd1;
				
			end else if ( opCode[15:12] == 4'd14 ) begin 
				aluSel <= 3'd0;
				pcSel <= 1'd0;
				pcValue <= 8'd0;
				WrE <= 1'd1;
				WrV <= opCode[3:0];
				WrD <= opCode[6:4];
				readRSel <= 1'd0;
				carryInSel <= 1'd0;
				valR2 <= 8'd0;
				flagsMux <= 3'd0;
				pcEn <= 1'd1;
				
			end else if ( opCode[15:9] == 7'd74 ) begin 
				aluSel <= 3'd0;
				pcSel <= 1'd1;
				pcValue <= opCode[7:0];
				WrE <= 1'd0;
				WrV <= 8'd0;
				WrD <= 3'd0;
				readRSel <= 1'd0;
				carryInSel <= 1'd0;
				valR2 <= 8'd0;
				flagsMux <= 3'd0;
				pcEn <= 1'd1;
				
			end else begin
				aluSel <= 3'd0;
				pcSel <= 1'd0;
				pcValue <= 8'd0;
				WrE <= 1'd0;
				WrV <= 8'd0;
				WrD <= 3'd0;
				readRSel <= 1'd0;
				carryInSel <= 1'd0;
				valR2 <= 8'd0;
				flagsMux <= 3'd0;
				selWriteVal <= 1'd0;
				pcEn <= 1'd0;
				end 
		end
	endcase
end
endmodule


module ControlUnit_falling #(parameter bus16_LEN = 16, parameter bus3_LEN = 3, parameter bus8_LEN = 8) (input clk, input rst, input [bus16_LEN - 1 : 0]opCode, input [bus3_LEN - 1 : 0]flags, output reg [bus3_LEN - 1 : 0]aluSel, output reg pcSel, output reg [bus8_LEN - 1 : 0]pcValue, output reg WrE, output reg [bus8_LEN - 1 : 0]WrV, output reg [bus3_LEN - 1 : 0]WrD, output reg readRSel, output reg carryInSel, output reg [bus8_LEN - 1 : 0]valR2, output reg [bus3_LEN - 1 : 0]flagsMux, output reg selWriteVal, output reg pcEn);

parameter SS = 2'd0, FS = 2'd1;
reg [1 : 0] CS;
reg [1 : 0] NS;

always @(negedge clk)
begin
	if (rst == 0)
		CS <= SS;
	else
		CS <= NS;
end

always @(CS, opCode, flags)
begin
 case(CS)
 		SS : begin
			
NS <= FS;
		end
		FS : begin
			
NS <= SS;
		end
	endcase
end

always @(CS)
begin
	case(CS)
		SS : begin
			
				aluSel <= 3'd0;
			pcSel <= 1'd0;
			pcValue <= 8'd0;
			WrE <= 1'd0;
			WrV <= 8'd0;
			WrD <= 3'd0;
			readRSel <= 1'd0;
			carryInSel <= 1'd0;
			valR2 <= 8'd0;
			flagsMux <= 3'd0;
			selWriteVal <= 1'd0;
			pcEn <= 1'd0;
			
		end
		FS : begin
			
			if ( opCode[15:10] == 6'd3 ) begin 
				aluSel <= 3'd0;
				pcSel <= 1'd0;
				pcValue <= 8'd0;
				WrE <= 1'd1;
				WrV <= 8'd0;
				WrD <= opCode[6:4];
				readRSel <= 1'd0;
				carryInSel <= 1'd0;
				valR2 <= 8'd0;
				flagsMux <= 3'd0;
				pcEn <= 1'd1;
				
			end else if ( opCode[15:12] == 4'd14 ) begin 
				aluSel <= 3'd0;
				pcSel <= 1'd0;
				pcValue <= 8'd0;
				WrE <= 1'd1;
				WrV <= opCode[3:0];
				WrD <= opCode[6:4];
				readRSel <= 1'd0;
				carryInSel <= 1'd0;
				valR2 <= 8'd0;
				flagsMux <= 3'd0;
				pcEn <= 1'd1;
				
			end else if ( opCode[15:9] == 7'd74 ) begin 
				aluSel <= 3'd0;
				pcSel <= 1'd1;
				pcValue <= opCode[7:0];
				WrE <= 1'd0;
				WrV <= 8'd0;
				WrD <= 3'd0;
				readRSel <= 1'd0;
				carryInSel <= 1'd0;
				valR2 <= 8'd0;
				flagsMux <= 3'd0;
				pcEn <= 1'd1;
				
			end else begin
				aluSel <= 3'd0;
				pcSel <= 1'd0;
				pcValue <= 8'd0;
				WrE <= 1'd0;
				WrV <= 8'd0;
				WrD <= 3'd0;
				readRSel <= 1'd0;
				carryInSel <= 1'd0;
				valR2 <= 8'd0;
				flagsMux <= 3'd0;
				selWriteVal <= 1'd0;
				pcEn <= 1'd0;
				end 
		end
	endcase
end
endmodule
