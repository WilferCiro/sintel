module Decorer3
	(
		input [2 : 0] pIn,
		output reg [7 : 0] pOut
	);
always @(*)
	begin
		case(pIn)
			'b000: pOut = 8'b00000001;
			'b001: pOut = 8'b00000010;
			'b010: pOut = 8'b00000100;
			'b011: pOut = 8'b00001000;
			'b100: pOut = 8'b00010000;
			'b101: pOut = 8'b00100000;
			'b110: pOut = 8'b01000000;
			'b111: pOut = 8'b10000000;
		endcase
	end

endmodule
