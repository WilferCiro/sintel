module mux1
	#(
		parameter IOBus_LEN = 1
	)
	(
		input Sel,
		input [IOBus_LEN - 1 : 0] pInA,
		input [IOBus_LEN - 1 : 0] pInB,
		output [IOBus_LEN - 1 : 0] pOut
	);
assign pOut = Sel ? pInB : pInA;




endmodule 
