module Register2_rising
	#(
		parameter buses_LEN = 1,
		parameter buses_1 = 1'b0,
		parameter reset_PAR = 1'b0
	)
	(
		input [buses_LEN - 1 : 0] inD,
		input ena,
		input rst,
		input clk,
		output reg [buses_LEN - 1 : 0] outQ
	);
	always @ (posedge clk)
	begin
		if (rst == reset_PAR)
			outQ <= 1'b0;
		else if (ena)
			outQ <= inD;
	end

endmodule

module Register2_falling
	#(
		parameter buses_LEN = 1,
		parameter buses_1 = 1'b0,
		parameter reset_PAR = 1'b0
	)
	(
		input [buses_LEN - 1 : 0] inD,
		input ena,
		input rst,
		input clk,
		output reg [buses_LEN - 1 : 0] outQ
	);
	always @ (negedge clk)
	begin
		if (rst == reset_PAR)
			outQ <= 1'b0;
		else if (ena)
			outQ <= inD;
	end

endmodule
