library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity Decorer3 is

	port(
		pIn: in std_logic_vector(2 downto 0);
		pOut: out std_logic_vector(7 downto 0)
	);
end entity;
architecture rtl of Decorer3 is
-- Signals declarations
	begin
	proccess(pIn)
	begin
		pOut <= "00000000";
		case pIn is
			when "000" => pOut(0) <= '1';
			when "001" => pOut(1) <= '1';
			when "010" => pOut(2) <= '1';
			when "011" => pOut(3) <= '1';
			when "100" => pOut(4) <= '1';
			when "101" => pOut(5) <= '1';
			when "110" => pOut(6) <= '1';
			when "111" => pOut(7) <= '1';
			when others => pOut <= "00000000";
		end case;

	end proccess;

end architecture;
