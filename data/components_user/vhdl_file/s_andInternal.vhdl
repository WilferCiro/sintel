library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;
entity andInternal is
	generic(
		Bus_LEN:natural := 2
	);
	port(
		In: in std_logic_vector(Bus_LEN - 1 downto 0);
		Out: out std_logic
	);
end entity;
architecture rtl of andInternal is
-- Signals declarations
begin
-- Put the code here
Out <= and In;
end architecture;
