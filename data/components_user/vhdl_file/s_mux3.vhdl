library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;



entity mux3 is
	generic(
		IOBus_LEN:natural := 1
	);
	port(
		Sel: in std_logic_vector(2 downto 0);
		pInA: in std_logic_vector(IOBus_LEN - 1 downto 0);
		pInB: in std_logic_vector(IOBus_LEN - 1 downto 0);
		pInC: in std_logic_vector(IOBus_LEN - 1 downto 0);
		pInD: in std_logic_vector(IOBus_LEN - 1 downto 0);
		pInE: in std_logic_vector(IOBus_LEN - 1 downto 0);
		pInF: in std_logic_vector(IOBus_LEN - 1 downto 0);
		pInG: in std_logic_vector(IOBus_LEN - 1 downto 0);
		pInH: in std_logic_vector(IOBus_LEN - 1 downto 0);
		pOut: out std_logic_vector(IOBus_LEN - 1 downto 0)
	);
end entity;
architecture rtl of mux3 is
begin
			-- Put the code here
					PROCESS (Sel, pInA, pInB, pInC, pInD) IS
						BEGIN
							CASE Sel IS
								WHEN "000" => pOut <= pInA;
								WHEN "001" => pOut <= pInB;
								WHEN "010" => pOut <= pInC;
								WHEN "011" => pOut <= pInD;
								WHEN "100" => pOut <= pInE;
								WHEN "101" => pOut <= pInF;
								WHEN "110" => pOut <= pInG;
								WHEN "111" => pOut <= pInH;
								WHEN OTHERS => pOut <= (others => '0');
							END CASE;
					END PROCESS;
				
			
		
	


end architecture;
