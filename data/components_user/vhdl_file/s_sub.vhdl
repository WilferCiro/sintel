library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;
entity sub is
	generic(
		Bus_LEN:natural := 1
	);
	port(
		in1: in std_logic_vector(Bus_LEN - 1 downto 0);
		in2: in std_logic_vector(Bus_LEN - 1 downto 0);
		Out: out std_logic_vector(Bus_LEN - 1 downto 0)
	);
end entity;
architecture rtl of sub is
-- Signals declarations
begin
-- Put the code here
Out <= in1 - in2;
end architecture;
