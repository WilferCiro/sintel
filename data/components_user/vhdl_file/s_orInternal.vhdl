library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;



entity orInternal is
	generic(
		Bus_LEN:natural := 2
	);
	port(
		pIn: in std_logic_vector(Bus_LEN - 1 downto 0);
		pOut: out std_logic
	);
end entity;
architecture rtl of orInternal is
signal temp: std_logic_vector(Bus_LEN - 1 downto 0);

begin
    temp(0) <= pIn(0);
    gen: for i in 1 to Bus_LEN - 1 generate
        temp(i) <= temp(i-1) or pIn(i);
    end generate; 
    pOut <= temp(Bus_LEN - 1); 

end architecture;
