library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity Register2 is
	generic(
		buses_LEN:natural := 1;
		reset_PAR:std_logic := '0'
	);
	port(
		inD: in std_logic_vector(buses_LEN - 1 downto 0);
		ena: in std_logic;
		rst: in std_logic;
		clk: in std_logic;
		outQ: out std_logic_vector(buses_LEN - 1 downto 0)
	);
end entity;
architecture rising_edge_arch of Register2 is
begin
	outQ <= (others => '0') when rst = reset_PAR else inD when rising_Edge(clk) and ena='1';

end architecture;
architecture falling_edge_arch of Register2 is
begin
	outQ <= (others => '0') when rst = reset_PAR else inD when falling_Edge(clk) and ena='1';

end architecture;
