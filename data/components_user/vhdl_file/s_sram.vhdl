library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;





entity sram is
	generic(
		Bus32_LEN:natural := 32
	);
	port(
		dirA: in std_logic_vector(4 downto 0);
		dirB: in std_logic_vector(4 downto 0);
		enWr: in std_logic;
		clk: in std_logic;
		valWr: in std_logic_vector(Bus32_LEN - 1 downto 0);
		rst: in std_logic;
		dirWrite: in std_logic_vector(4 downto 0);
		readA: out std_logic_vector(Bus32_LEN - 1 downto 0);
		readB: out std_logic_vector(Bus32_LEN - 1 downto 0)
	);
end entity;
architecture rising_edge_arch of sram is
type ram_type is array(0 to 31) of std_logic_vector(Bus32_LEN - 1 downto 0);
signal RAM :  ram_type := ("00000000000000000000000000000000");
begin
process(clk)
begin
  if rising_Edge(clk) then
      if enWr = '1' then
        RAM(conv_integer(dirWrite)) <= valWr;
      end if;
  end if;
end process;

readA <= RAM(conv_integer(dirA));
readB <= RAM(conv_integer(dirB));



end architecture;
architecture falling_edge_arch of sram is
type ram_type is array(0 to 31) of std_logic_vector(Bus32_LEN - 1 downto 0);
signal RAM :  ram_type := ("00000000000000000000000000000000");
begin
process(clk)
begin
  if falling_Edge(clk) then
      if enWr = '1' then
        RAM(conv_integer(dirWrite)) <= valWr;
      end if;
  end if;
end process;

readA <= RAM(conv_integer(dirA));
readB <= RAM(conv_integer(dirB));



end architecture;
