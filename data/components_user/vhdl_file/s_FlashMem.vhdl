library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;






entity FlashMem is
	generic(
		BusOut_LEN:natural := 8
	);
	port(
		Dir: in std_logic_vector(BusOut_LEN - 1 downto 0);
		opCode: out std_logic_vector(BusOut_LEN - 1 downto 0)
	);
end entity;
architecture rtl of FlashMem is
type memoria_rom is array (0 to 100) of std_logic_vector (BusOut_LEN - 1 downto 0);
signal ROM : memoria_rom := (
	"00111100000000010000000000000000",
	"00111100000000100000000000000001", 
	"00111100000000110000000000000010",
	"00000000001000100000100000100000",
	"00010000001000101111111111111111",
	"00001011111111111111111111111110"
);
opCode <= ROM(conv_integer(Dir));


end architecture;
