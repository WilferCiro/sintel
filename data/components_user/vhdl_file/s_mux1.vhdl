library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;




entity mux1 is
	generic(
		IOBus_LEN:natural := 1
	);
	port(
		Sel: in std_logic;
		pInA: in std_logic_vector(IOBus_LEN - 1 downto 0);
		pInB: in std_logic_vector(IOBus_LEN - 1 downto 0);
		pOut: out std_logic_vector(IOBus_LEN - 1 downto 0)
	);
end entity;
architecture rtl of mux1 is
begin
				-- Put the code here
				PROCESS (Sel, pInA, pInB) IS
					BEGIN
						CASE Sel IS
							WHEN '0' => pOut <= pInA;
							WHEN '1' => pOut <= pInB;
							WHEN OTHERS => pOut <= (others => '0');
						END CASE;
				END PROCESS;
	



end architecture;
