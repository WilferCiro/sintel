library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity mux2 is
	generic(
		IOBus_LEN:natural := 1
	);
	port(
		Sel: in std_logic_vector(1 downto 0);
		pInA: in std_logic_vector(IOBus_LEN - 1 downto 0);
		pInB: in std_logic_vector(IOBus_LEN - 1 downto 0);
		pInC: in std_logic_vector(IOBus_LEN - 1 downto 0);
		pInD: in std_logic_vector(IOBus_LEN - 1 downto 0);
		pOut: out std_logic_vector(IOBus_LEN - 1 downto 0)
	);
end entity;
architecture rtl of mux2 is
begin
	-- Put the code here
			PROCESS (Sel, pInA, pInB, pInC, pInD) IS
				BEGIN
					CASE Sel IS
						WHEN "00" => pOut <= pInA;
						WHEN "01" => pOut <= pInB;
						WHEN "10" => pOut <= pInC;
						WHEN "11" => pOut <= pInD;
						WHEN OTHERS => pOut <= (others => '0');
					END CASE;
			END PROCESS;
		
	
end architecture;
