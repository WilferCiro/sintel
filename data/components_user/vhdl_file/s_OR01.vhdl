library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;





entity OR01 is
	generic(
		Bus_LEN:natural := 1
	);
	port(
		pIn1: in std_logic_vector(Bus_LEN - 1 downto 0);
		pIn2: in std_logic_vector(Bus_LEN - 1 downto 0);
		pOut: out std_logic_vector(Bus_LEN - 1 downto 0)
	);
end entity;
architecture rtl of OR01 is
begin
		-- Put the code here
		pOut <= pIn1 or pIn2;
		
		
	


end architecture;
