library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;


entity Enot01 is
	generic(
		buses_LEN:natural := 1
	);
	port(
		pIn: in std_logic_vector(buses_LEN - 1 downto 0);
		pOut: out std_logic_vector(buses_LEN - 1 downto 0)
	);
end entity;
architecture rtl of Enot01 is
	begin
		pOut <= not pIn;

end architecture;
