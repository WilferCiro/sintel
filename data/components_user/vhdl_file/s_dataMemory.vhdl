library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;




entity dataMemory is
	generic(
		Bus32_LEN:natural := 8;
		MEMSIZE_PAR:natural := 99
	);
	port(
		WE: in std_logic;
		WD: in std_logic_vector(Bus32_LEN - 1 downto 0);
		clk: in std_logic;
		A: in std_logic_vector(Bus32_LEN - 1 downto 0);
		RD: out std_logic_vector(Bus32_LEN - 1 downto 0)
	);
end entity;
architecture rising_edge_arch of dataMemory is
type ram_type is array(0 to MEMSIZE_PAR) of std_logic_vector(Bus32_LEN - 1 downto 0);
signal RAM :  ram_type;
begin
process(clk)
begin
  if falling_Edge(clk) then
      if WE = '1' then
        RAM(conv_integer(A)) <= WD;
      end if;
  end if;
end process;

RD <= RAM(conv_integer(A));




end architecture;
architecture falling_edge_arch of dataMemory is
type ram_type is array(0 to MEMSIZE_PAR) of std_logic_vector(Bus32_LEN - 1 downto 0);
signal RAM :  ram_type;
begin
process(clk)
begin
  if rising_Edge(clk) then
      if WE = '1' then
        RAM(conv_integer(A)) <= WD;
      end if;
  end if;
end process;

RD <= RAM(conv_integer(A));




end architecture;
