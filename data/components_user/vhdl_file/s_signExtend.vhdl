library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;



entity signExtend is
	generic(
		BusIn_LEN:natural := 1;
		BusOut_LEN:natural := 1
	);
	port(
		dataIn: in std_logic_vector(BusIn_LEN - 1 downto 0);
		dataOut: out std_logic_vector(BusOut_LEN - 1 downto 0)
	);
end entity;
architecture rtl of signExtend is
-- Signals declarations
begin
-- Put the code here
dataOut <= (BusOut_LEN - 1 downto BusIn_LEN => dataIn(BusIn_LEN - 1)) & dataIn;



end architecture;
