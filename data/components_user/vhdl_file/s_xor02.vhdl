library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity xor02 is
	generic(
		Bus_LEN:natural := 1
	);
	port(
		pIn1: in std_logic_vector(Bus_LEN - 1 downto 0);
		pIn2: in std_logic_vector(Bus_LEN - 1 downto 0);
		pOut: out std_logic_vector(Bus_LEN - 1 downto 0)
	);
end entity;
architecture rtl of xor02 is
begin
	pOut <= pIn1 xor pIn2;

end architecture;
