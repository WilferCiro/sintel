library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Control2 is
	
	port(
		clk, rst: in std_logic;
		pIn1 : in std_logic;
		pIn2 : in std_logic;
		pOut1 : out std_logic;
		pOut2 : out std_logic
	);
end entity;

architecture rising_edge_arch of Control2 is
type State is (SS, FS);
signal CS, NS: State;

begin
CS <= SS when rst='0' else NS when rising_edge(clk);

process(CS, pIn1, pIn2)
begin
	case CS is
		when SS =>
			NS <= FS;

		when FS =>
			NS <= SS;

	end case;
end process;

process(CS, NS)
begin
	case CS is
		when SS =>
			pOut1 <= '0';
			pOut2 <= '0';
			
		when FS =>
			pOut1 <= '0';
			pOut2 <= '0';
			
	end case;
end process;
end architecture;


architecture falling_edge_arch of Control2 is
type State is (SS, FS);
signal CS, NS: State;

begin
CS <= SS when rst='0' else NS when falling_edge(clk);

process(CS, pIn1, pIn2)
begin
	case CS is
		when SS =>
			NS <= FS;

		when FS =>
			NS <= SS;

	end case;
end process;

process(CS, NS)
begin
	case CS is
		when SS =>
			pOut1 <= '0';
			pOut2 <= '0';
			
		when FS =>
			pOut1 <= '0';
			pOut2 <= '0';
			
	end case;
end process;
end architecture;
