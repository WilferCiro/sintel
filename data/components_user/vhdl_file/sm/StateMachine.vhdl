library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity StateMachine is
	generic(
		bus8_LEN : natural:=8
	);
	port(
		clk, rst: in std_logic;
		pIn1 : in std_logic_vector(bus8_LEN - 1 downto 0);
		pOut1 : out std_logic_vector(bus8_LEN - 1 downto 0)
	);
end entity;

architecture rising_edge_arch of StateMachine is
type State is (SS, FS);
signal CS, NS: State;

begin
CS <= SS when rst='0' else NS when rising_edge(clk);

process(CS, pIn1)
begin
	case CS is
		when SS =>
			
NS <= FS;
		when FS =>
			
NS <= SS;
	end case;
end process;

process(CS, NS)
begin
	case CS is
		when SS =>
			
				pOut1 <= std_logic_vector(to_unsigned(30, 8));
			
		when FS =>
			
			if  pIn1 = std_logic_vector(to_unsigned(1, 8))  then 
				pOut1 <= std_logic_vector(to_unsigned(0, 8));
				
			else
				pOut1 <= std_logic_vector(to_unsigned(2, 8));
				end if;
	end case;
end process;
end architecture;


architecture falling_edge_arch of StateMachine is
type State is (SS, FS);
signal CS, NS: State;

begin
CS <= SS when rst='0' else NS when falling_edge(clk);

process(CS, pIn1)
begin
	case CS is
		when SS =>
			
NS <= FS;
		when FS =>
			
NS <= SS;
	end case;
end process;

process(CS, NS)
begin
	case CS is
		when SS =>
			
				pOut1 <= std_logic_vector(to_unsigned(30, 8));
			
		when FS =>
			
			if  pIn1 = std_logic_vector(to_unsigned(1, 8))  then 
				pOut1 <= std_logic_vector(to_unsigned(0, 8));
				
			else
				pOut1 <= std_logic_vector(to_unsigned(2, 8));
				end if;
	end case;
end process;
end architecture;
