library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity AVRControlUnit is
	generic(
		bus16_LEN : natural:=16;
		bus3_LEN : natural:=3
	);
	port(
		clk, rst: in std_logic;
		opCode : in std_logic_vector(bus16_LEN - 1 downto 0);
		aluSel : out std_logic_vector(bus3_LEN - 1 downto 0);
		enablePC : out std_logic;
		wrEnable : out std_logic;
		writeSel : out std_logic;
		pcSel : out std_logic
	);
end entity;

architecture rising_edge_arch of AVRControlUnit is
type State is (SS, FS);
signal CS, NS: State;

begin
CS <= SS when rst='0' else NS when rising_edge(clk);

process(CS, opCode)
begin
	case CS is
		when SS =>
			
NS <= FS;
		when FS =>
			
NS <= SS;
	end case;
end process;

process(CS, NS)
begin
	case CS is
		when SS =>
			
				aluSel <= std_logic_vector(to_unsigned(0, 3));
			enablePC <= '0';
			wrEnable <= '0';
			writeSel <= '0';
			pcSel <= '0';
			
		when FS =>
			
				aluSel <= std_logic_vector(to_unsigned(0, 3));
			enablePC <= '0';
			wrEnable <= '0';
			writeSel <= '0';
			pcSel <= '0';
			
	end case;
end process;
end architecture;


architecture falling_edge_arch of AVRControlUnit is
type State is (SS, FS);
signal CS, NS: State;

begin
CS <= SS when rst='0' else NS when falling_edge(clk);

process(CS, opCode)
begin
	case CS is
		when SS =>
			
NS <= FS;
		when FS =>
			
NS <= SS;
	end case;
end process;

process(CS, NS)
begin
	case CS is
		when SS =>
			
				aluSel <= std_logic_vector(to_unsigned(0, 3));
			enablePC <= '0';
			wrEnable <= '0';
			writeSel <= '0';
			pcSel <= '0';
			
		when FS =>
			
				aluSel <= std_logic_vector(to_unsigned(0, 3));
			enablePC <= '0';
			wrEnable <= '0';
			writeSel <= '0';
			pcSel <= '0';
			
	end case;
end process;
end architecture;
