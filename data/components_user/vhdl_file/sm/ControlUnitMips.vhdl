library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ControlUnitMips is
	generic(
		bus6_LEN : natural:=6;
		bus3_LEN : natural:=3
	);
	port(
		
		opCode : in std_logic_vector(bus6_LEN - 1 downto 0);
		funCode : in std_logic_vector(bus6_LEN - 1 downto 0);
		RegWrite : out std_logic;
		memToReg : out std_logic;
		memWrite : out std_logic;
		branch : out std_logic;
		aluControl : out std_logic_vector(bus3_LEN - 1 downto 0);
		aluSRC : out std_logic;
		regDst : out std_logic;
		jump : out std_logic
	);
end entity;

architecture rtl of ControlUnitMips is begin

process(opCode, funCode) begin
	
if  opCode = std_logic_vector(to_unsigned(0, 6)) and funCode = std_logic_vector(to_unsigned(32, 6))  then 
	RegWrite <= '1';
			memToReg <= '0';
			memWrite <= '0';
			branch <= '0';
			aluControl <= std_logic_vector(to_unsigned(0, 3));
			aluSRC <= '0';
			regDst <= '1';
			jump <= '0';
			
elsif  opCode = std_logic_vector(to_unsigned(0, 6)) and funCode = std_logic_vector(to_unsigned(34, 6))  then 
	RegWrite <= '0';
			memToReg <= '0';
			memWrite <= '0';
			branch <= '0';
			aluControl <= std_logic_vector(to_unsigned(1, 3));
			aluSRC <= '0';
			regDst <= '1';
			jump <= '0';
			
elsif  opCode = std_logic_vector(to_unsigned(0, 6)) and funCode = std_logic_vector(to_unsigned(36, 6))  then 
	RegWrite <= '0';
			memToReg <= '0';
			memWrite <= '0';
			branch <= '0';
			aluControl <= std_logic_vector(to_unsigned(2, 3));
			aluSRC <= '0';
			regDst <= '1';
			jump <= '0';
			
elsif  opCode = std_logic_vector(to_unsigned(35, 6))  then 
	RegWrite <= '0';
			memToReg <= '1';
			memWrite <= '0';
			branch <= '0';
			aluControl <= std_logic_vector(to_unsigned(0, 3));
			aluSRC <= '1';
			regDst <= '0';
			jump <= '0';
			
elsif  opCode = std_logic_vector(to_unsigned(43, 6))  then 
	RegWrite <= '0';
			memToReg <= '0';
			memWrite <= '1';
			branch <= '0';
			aluControl <= std_logic_vector(to_unsigned(0, 3));
			aluSRC <= '1';
			regDst <= '0';
			jump <= '0';
			
elsif  opCode = std_logic_vector(to_unsigned(4, 6))  then 
	RegWrite <= '0';
			memToReg <= '0';
			memWrite <= '0';
			branch <= '1';
			aluControl <= std_logic_vector(to_unsigned(1, 3));
			aluSRC <= '0';
			regDst <= '0';
			jump <= '0';
			
elsif  opCode = std_logic_vector(to_unsigned(2, 6))  then 
	RegWrite <= '0';
			memToReg <= '0';
			memWrite <= '0';
			branch <= '0';
			aluControl <= std_logic_vector(to_unsigned(0, 3));
			aluSRC <= '0';
			regDst <= '0';
			jump <= '1';
			
elsif  opCode = std_logic_vector(to_unsigned(15, 6))  then 
	RegWrite <= '1';
			memToReg <= '0';
			memWrite <= '0';
			branch <= '0';
			aluControl <= std_logic_vector(to_unsigned(0, 3));
			aluSRC <= '1';
			regDst <= '0';
			jump <= '0';
			
elsif  opCode = std_logic_vector(to_unsigned(8, 6))  then 
	RegWrite <= '1';
			memToReg <= '0';
			memWrite <= '0';
			branch <= '0';
			aluControl <= std_logic_vector(to_unsigned(0, 3));
			aluSRC <= '1';
			regDst <= '0';
			jump <= '0';
			
else
	RegWrite <= '0';
			memToReg <= '0';
			memWrite <= '0';
			branch <= '0';
			aluControl <= std_logic_vector(to_unsigned(0, 3));
			aluSRC <= '0';
			regDst <= '0';
			jump <= '0';
			
end if;
end process;

end architecture;
