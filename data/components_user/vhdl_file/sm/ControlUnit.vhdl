library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ControlUnit is
	generic(
		bus16_LEN : natural:=16;
		bus3_LEN : natural:=3;
		bus8_LEN : natural:=8
	);
	port(
		clk, rst: in std_logic;
		opCode : in std_logic_vector(bus16_LEN - 1 downto 0);
		flags : in std_logic_vector(bus3_LEN - 1 downto 0);
		aluSel : out std_logic_vector(bus3_LEN - 1 downto 0);
		pcSel : out std_logic;
		pcValue : out std_logic_vector(bus8_LEN - 1 downto 0);
		WrE : out std_logic;
		WrV : out std_logic_vector(bus8_LEN - 1 downto 0);
		WrD : out std_logic_vector(bus3_LEN - 1 downto 0);
		readRSel : out std_logic;
		carryInSel : out std_logic;
		valR2 : out std_logic_vector(bus8_LEN - 1 downto 0);
		flagsMux : out std_logic_vector(bus3_LEN - 1 downto 0);
		selWriteVal : out std_logic;
		pcEn : out std_logic
	);
end entity;

architecture rising_edge_arch of ControlUnit is
type State is (SS, FS);
signal CS, NS: State;

begin
CS <= SS when rst='0' else NS when rising_edge(clk);

process(CS, opCode, flags)
begin
	case CS is
		when SS =>
			
NS <= FS;
		when FS =>
			
NS <= SS;
	end case;
end process;

process(CS, NS)
begin
	case CS is
		when SS =>
			
				aluSel <= std_logic_vector(to_unsigned(0, 3));
			pcSel <= '0';
			pcValue <= std_logic_vector(to_unsigned(0, 8));
			WrE <= '0';
			WrV <= std_logic_vector(to_unsigned(0, 8));
			WrD <= std_logic_vector(to_unsigned(0, 3));
			readRSel <= '0';
			carryInSel <= '0';
			valR2 <= std_logic_vector(to_unsigned(0, 8));
			flagsMux <= std_logic_vector(to_unsigned(0, 3));
			selWriteVal <= '0';
			pcEn <= '0';
			
		when FS =>
			
			if  opCode(15 downto 10) = std_logic_vector(to_unsigned(3, 6))  then 
				aluSel <= std_logic_vector(to_unsigned(0, 3));
				pcSel <= '0';
				pcValue <= std_logic_vector(to_unsigned(0, 8));
				WrE <= '1';
				WrV <= std_logic_vector(to_unsigned(0, 8));
				WrD <= opCode(6 downto 4)
				readRSel <= '0';
				carryInSel <= '0';
				valR2 <= std_logic_vector(to_unsigned(0, 8));
				flagsMux <= std_logic_vector(to_unsigned(0, 3));
				pcEn <= '1';
				
			elsif  opCode(15 downto 12) = std_logic_vector(to_unsigned(14, 4))  then 
				aluSel <= std_logic_vector(to_unsigned(0, 3));
				pcSel <= '0';
				pcValue <= std_logic_vector(to_unsigned(0, 8));
				WrE <= '1';
				WrV <= opCode(3 downto 0)
				WrD <= opCode(6 downto 4)
				readRSel <= '0';
				carryInSel <= '0';
				valR2 <= std_logic_vector(to_unsigned(0, 8));
				flagsMux <= std_logic_vector(to_unsigned(0, 3));
				pcEn <= '1';
				
			elsif  opCode(15 downto 9) = std_logic_vector(to_unsigned(74, 7))  then 
				aluSel <= std_logic_vector(to_unsigned(0, 3));
				pcSel <= '1';
				pcValue <= opCode(7 downto 0)
				WrE <= '0';
				WrV <= std_logic_vector(to_unsigned(0, 8));
				WrD <= std_logic_vector(to_unsigned(0, 3));
				readRSel <= '0';
				carryInSel <= '0';
				valR2 <= std_logic_vector(to_unsigned(0, 8));
				flagsMux <= std_logic_vector(to_unsigned(0, 3));
				pcEn <= '1';
				
			else
				aluSel <= std_logic_vector(to_unsigned(0, 3));
				pcSel <= '0';
				pcValue <= std_logic_vector(to_unsigned(0, 8));
				WrE <= '0';
				WrV <= std_logic_vector(to_unsigned(0, 8));
				WrD <= std_logic_vector(to_unsigned(0, 3));
				readRSel <= '0';
				carryInSel <= '0';
				valR2 <= std_logic_vector(to_unsigned(0, 8));
				flagsMux <= std_logic_vector(to_unsigned(0, 3));
				selWriteVal <= '0';
				pcEn <= '0';
				end if;
	end case;
end process;
end architecture;


architecture falling_edge_arch of ControlUnit is
type State is (SS, FS);
signal CS, NS: State;

begin
CS <= SS when rst='0' else NS when falling_edge(clk);

process(CS, opCode, flags)
begin
	case CS is
		when SS =>
			
NS <= FS;
		when FS =>
			
NS <= SS;
	end case;
end process;

process(CS, NS)
begin
	case CS is
		when SS =>
			
				aluSel <= std_logic_vector(to_unsigned(0, 3));
			pcSel <= '0';
			pcValue <= std_logic_vector(to_unsigned(0, 8));
			WrE <= '0';
			WrV <= std_logic_vector(to_unsigned(0, 8));
			WrD <= std_logic_vector(to_unsigned(0, 3));
			readRSel <= '0';
			carryInSel <= '0';
			valR2 <= std_logic_vector(to_unsigned(0, 8));
			flagsMux <= std_logic_vector(to_unsigned(0, 3));
			selWriteVal <= '0';
			pcEn <= '0';
			
		when FS =>
			
			if  opCode(15 downto 10) = std_logic_vector(to_unsigned(3, 6))  then 
				aluSel <= std_logic_vector(to_unsigned(0, 3));
				pcSel <= '0';
				pcValue <= std_logic_vector(to_unsigned(0, 8));
				WrE <= '1';
				WrV <= std_logic_vector(to_unsigned(0, 8));
				WrD <= opCode(6 downto 4)
				readRSel <= '0';
				carryInSel <= '0';
				valR2 <= std_logic_vector(to_unsigned(0, 8));
				flagsMux <= std_logic_vector(to_unsigned(0, 3));
				pcEn <= '1';
				
			elsif  opCode(15 downto 12) = std_logic_vector(to_unsigned(14, 4))  then 
				aluSel <= std_logic_vector(to_unsigned(0, 3));
				pcSel <= '0';
				pcValue <= std_logic_vector(to_unsigned(0, 8));
				WrE <= '1';
				WrV <= opCode(3 downto 0)
				WrD <= opCode(6 downto 4)
				readRSel <= '0';
				carryInSel <= '0';
				valR2 <= std_logic_vector(to_unsigned(0, 8));
				flagsMux <= std_logic_vector(to_unsigned(0, 3));
				pcEn <= '1';
				
			elsif  opCode(15 downto 9) = std_logic_vector(to_unsigned(74, 7))  then 
				aluSel <= std_logic_vector(to_unsigned(0, 3));
				pcSel <= '1';
				pcValue <= opCode(7 downto 0)
				WrE <= '0';
				WrV <= std_logic_vector(to_unsigned(0, 8));
				WrD <= std_logic_vector(to_unsigned(0, 3));
				readRSel <= '0';
				carryInSel <= '0';
				valR2 <= std_logic_vector(to_unsigned(0, 8));
				flagsMux <= std_logic_vector(to_unsigned(0, 3));
				pcEn <= '1';
				
			else
				aluSel <= std_logic_vector(to_unsigned(0, 3));
				pcSel <= '0';
				pcValue <= std_logic_vector(to_unsigned(0, 8));
				WrE <= '0';
				WrV <= std_logic_vector(to_unsigned(0, 8));
				WrD <= std_logic_vector(to_unsigned(0, 3));
				readRSel <= '0';
				carryInSel <= '0';
				valR2 <= std_logic_vector(to_unsigned(0, 8));
				flagsMux <= std_logic_vector(to_unsigned(0, 3));
				selWriteVal <= '0';
				pcEn <= '0';
				end if;
	end case;
end process;
end architecture;
