#!/usr/bin/env python
'''
	Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>
	Trabajo de grado para la Universidad del Quindío

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

class Component:
	def __init__(self, ID, isStateMachine = False, parent = None, properties = {}):
		# Input and ouput vectors
		self.__inputs = {}
		self.__outputs = {}

		self._ID = ID
		self._parent = parent

		# Control Variables
		self.__clock = False
		self._updatedByClock = False
		self._compClockWrited = None
		self._isStateMachine = isStateMachine

		# Initial configuration
		self.setup(properties)

	def isStateMachine(self):
		return self._isStateMachine

	def getID(self):
		return self._ID

	def reset(self):
		"""
			Emits when a reset input is writen
		"""
		pass

	def updatedByClock(self):
		return self._updatedByClock

	def setCompClockWrited(self, component):
		self._compClockWrited = component

	def setup(self, properties):
		pass

	def update(self):
		pass

	def program_start(self):
		pass

	def upd(self):
		self.update()

	def write(self, name, value, base = 10):
		self.__outputs[name].write(value, base = base, imclock = self.__clock, parent = self._parent)

	def read(self, name, base = 10, bits = None):
		if self.__inputs[name]["default"] == None:
			return self.__inputs[name]["signal"].read(base = base, imclock = self.__clock, bits = bits)
		else:
			data = str(self.__inputs[name]["default"])
			if base == 10:
				return int(data)
			elif base == 2:
				return list(bin(int(data)).replace("0b", ""))

	def readOutput(self, name, base = 10):
		return self.__outputs[name].read(base = base)

	def _addInput(self, name):
		self.__inputs[name] = {
			"signal" : SimulationSignal(),
			"default" : ""
		}

	def getSize(self, name):
		if name in self.__inputs:
			return self.__inputs[name]["signal"].getSize()
		elif name in self.__outputs:
			return self.__outputs[name].getSize()

	def writedByClock(self, name):
		if name in self.__inputs:
			return self.__inputs[name]["signal"].writedByClock()

	def setDefaultValue(self, name, value):
		self.__inputs[name]["default"] = int(value)
		self.__inputs[name]["signal"].write(int(self.__inputs[name]["default"]), base = 10)

	def _addOutput(self, name):
		self.__outputs[name] = SimulationSignal()

	def assingSignal(self, name, signal):
		if name in self.__inputs:
			self.__inputs[name] = {
				"signal" : signal,
				"default" : None
			}
		elif name in self.__outputs:
			self.__outputs[name] = signal

	def assignClock(self):
		self.__clock = True

	def dependClock(self):
		return self.__clock

	def hasRead(self):
		readed = {True if self.__inputs[a]["signal"].hasWrite() or self.__inputs[a]["default"] != None else False for a in self.__inputs}
		if not False in readed:
			return True
		return False

	def addToUpdate(self, components):
		self._parent.addToUpdate(components)

class UnionMacro(Component):
	def setup(self, properties):
		self._addInput("pIn")
		self._addOutput("pOut")

	def update(self):
		value = self.read("pIn")
		self.write("pOut", value)

class ClockEdit:
	def __init__(self, max_val):
		self._value = 0
		self._maxValue = max_val

	def update(self):
		self._value += 1
		if self._value > self._maxValue:
			self._value = 1

	def read(self):
		return self._value

class SimulationSignal:
	"""
		Signal to comunicate the components in the simulation
	"""
	def __init__(self, size = 1):
		self.__size = size
		self.__subSignalSims = []
		self._components = []
		self._componentsClockRissing = []
		self._componentsClockFalling = []
		self._componentsReset = []

		self._lastValue = []

		self.__writedByClock = False
		self._compClockWrited = None

		self.__subSignalSims = [subSignalSim() for a in range(size)]

	def writedComponent(self):
		return self._compClockWrited

	def reset(self):
		[sub.reset() for sub in self.__subSignalSims]

	def connectTo(self, obj, clock = False, flank = 0, reset = False):
		if clock:
			if flank == 0:
				self._componentsClockFalling.append(obj)
			else:
				self._componentsClockRissing.append(obj)
		#elif reset:
		#	self._componentsReset.append(obj)
		else:
			self._components.append(obj)

	def getComponents(self):
		return self._componentsReset

	def getSize(self):
		return int(self.__size)

	def setSubSignal(self, position, subSignalSimParam):
		self.__subSignalSims[position] = subSignalSimParam

	def getSubSignal(self, position):
		return self.__subSignalSims[position]

	def writedByClock(self):
		return self.__writedByClock

	def read(self, visual = False, imclock = False, base = 10, bits = None):
		"""
			Returns the signal value
		"""

		returnData = [str(a.read()) for a in self.__subSignalSims[self.__size - 1::-1]]
		if bits != None:
			bits = bits.replace("[", "").replace("]", "").split(":")
			if len(bits) == 1:
				returnData = returnData[self.__size - int(bits)]
			else:
				splited = [int(bits[0]), int(bits[1])]
				splited.sort(reverse = True)
				#returnData2 = [returnData[len(returnData) - int(a)] for a in bits]
				returnData2 = [returnData[self.__size - int(a) - 1] for a in range(splited[1], splited[0] + 1)]
				returnData2.reverse()
				returnData = returnData2
		#returnData = []
		#for a in self.__subSignalSims[self.__size - 1::-1]:
		#	returnData.append(str(a.read()))

		#if not visual and imclock and self.__writedByClock:
		#	forReturn = self._lastValue
		#else:
		forReturn = returnData

		#['0', '1', '0', '1', '0']

		try:
			if base == 10:
				forReturn = int("".join(forReturn), 2)
		except:
			pass
		return forReturn


	def twosComplement(self, value, bitLength):
		return bin(value & (2**bitLength - 1))

	def write(self, data, base = 10, imclock = False, parent = None):
		if self.read(base = base) != data:
			try:
				data2 = data
				if base == 8:
					data = int(data.replace("0o", ""), 8)
				elif base == 16:
					data = int(data.replace("0x", ""), 16)
				if base != 2:
					if data < 0:
						data2 = list(self.twosComplement(data, self.__size).replace("0b", ""))
					else:
						data2 = list(bin(data).replace("0b", ""))
			except:
				data2 = data
			self.__writeData(data2, imclock = imclock, parent = parent)

	def __writeData(self, value, imclock = False, parent = None):
		"""
			Writes the signal value
			@para value as int
		"""
		self.__writedByClock = imclock

		if value == 'U' or value == 'Z':
			[sub.write(value) for sub in self.__subSignalSims]
		else:
			for i in range(self.__size):
				if i < len(value):
					self.__subSignalSims[i].write(value[len(value) - i - 1])
				else:
					self.__subSignalSims[i].write(0)

		if len(self._components) > 0:
			[comp.update() for comp in self._components if (not comp.dependClock() and comp.hasRead())]
			#[comp.setCompClockWrited(component = component) for comp in self._components if comp.dependClock()]

		#for comp in self._components:
		#	if not comp.dependClock() and comp.hasRead():
		#		comp.upd()

		#if len(self._componentsReset) > 0:
		#	[comp.reset() for comp in self._componentsReset]
		#for comp in self._componentsReset:
		#	comp.reset()

		if len(self._componentsClockFalling) > 0 and int(value[0]) == 0:
			parent.addToUpdate(self._componentsClockFalling)
			#[comp.updateRead() for comp in self._componentsClockFalling]
			#[comp.updateWrite() for comp in self._componentsClockFalling]
			#for comp in self._componentsClockFalling:
			#	comp.upd()

		if len(self._componentsClockRissing) > 0 and int(value[0]) == 1:
			parent.addToUpdate(self._componentsClockRissing)
			#[comp.updateRead() for comp in self._componentsClockRissing]
			#[comp.updateWrite() for comp in self._componentsClockRissing]
			#for comp in self._componentsClockRissing:
			#	comp.upd()


	def hasWrite(self):
		writed = {a.hasWrite() for a in self.__subSignalSims}
		if True in writed:
			return True
		return False

class subSignalSim:
	def __init__(self):
		self._value = 'U'
		self._flagWrite = False

	def hasWrite(self):
		return self._flagWrite

	def reset(self):
		self._flagWrite = False

	def write(self, value):
		if value not in {'U', 'Z'}:
			value = int(value)

		if value in {0, 1, 'U', 'Z'}:
			self._value = value
		else:
			self._value = 'U'
		self._flagWrite = True

	def read(self):
		return self._value

def bin2dec(number):
	try:
		number = [str(a) for a in number]
		number10 = int(''.join(number), 2)
		return number10
	except:
		return number
