#################
About the project
#################

Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>, Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>. Degree work for Universidad del Quindío, Colombia.

Sintel is a simulator and modeler of basic digital systems that allows, in addition to visualizing inputs and outputs in real time, to export the circuit to a hardware description language such as VHDL and verilog.

Sintel is written in python with the QT graphics library, it is an open source project with GPLv3 license or later.