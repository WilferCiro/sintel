############
Installation
############

Dependency Icarus Verilog
#########################

For windows, download the .exe file in https://iverilog.fandom.com/wiki/Main_Page and follow steps and install, restart your computer and follow the nexts steps

For Ubuntu:

.. code-block:: bash

	sudo apt install iverilog

For Arch:

.. code-block:: bash

	sudo pacman -S iverilog

Binary
######

For install in Windows, download the https://gitlab.com/WilferCiro/sintel/-/raw/master/instals/Sintel.exe and open the Sintel.exe file and continue with the steps

For install in Linux

.. code-block:: bash

	$ wget https://gitlab.com/WilferCiro/sintel/-/raw/master/instals/LinuxInstaller.tar.gz
	$ tar xf LinuxInstaller.tar.gz
	$ cd LinuxInstaller
	$ sh install.sh


For developers
##############

for install in Ubuntu

.. code-block:: bash

	sudo apt install python3-pip qttools5-dev-tools pyqt5-dev-tools iverilog python3-matplotlib

for install in arch

.. code-block:: bash

   sudo pacman -S python3-pip pyqt5 iverilog python3-matplotlib

Next, exec on all platforms

.. code-block:: bash

	sudo pip3 install shutil sqlite3

Execution
#########
For exec Sintel, cd into Sintel's folder and put in your terminal.

.. code-block:: bash

	git clone https://gitlab.com/WilferCiro/sintel.git
	cd sintel/
	python3 application.py
