#######################
Export the model to HDL
#######################

You can export your simulation model to

*   VHDL
*   Verilog

For export the model you have to press the "Export to HDL" button ubicated in the simulation view menu, this action creates two folders in the project path with the vhdl and verilog files.

The files in the folders are:

*   vhdl and verilog component files.
*   verilog/main.v and vhdl/main.vhdl, this file contains the connections of all components made visually, and is the top level entity.
*   verilog/testbench.v and vhdl/testbench.vhdl, this file contains a simple workbench for simulate the model, you can edit this file to create more complex simulations.
*   verilog/Makefile and vhdl/Makefile, this file contains the commands to run the testbench in gtkwave.

(Optional) Install free simulation tools (Linux)
================================================

GTKWave
*******
For simulate the generated HDL model, you can use IDEs like Quartus with the modelsim, and we recommend the GTKWave (http://gtkwave.sourceforge.net/) application, this applicatión is free and can be installed in Windows, Mac OSX and Linux.

For install in linux, it depends of your distribution, for example, for ubuntu you can install it with:

.. code-block:: bash

   sudo apt install gtkwave

for arch

.. code-block:: bash

   sudo pacman -S gtkwave

GHDL
****
ghdl is a open source simulator for VHDL language (more info in http://ghdl.free.fr/), for install it you can execute

In Ubuntu

.. code-block:: bash

   sudo apt install ghdl

In arch

.. code-block:: bash

   yaourt -S ghdl-llvm-git

iverilog
********
icarus verilog is a open source verilog compiler (mode info in https://iverilog.fandom.com/wiki/Main_Page), for install it you can execute


In Ubuntu

.. code-block:: bash

   sudo apt install iverilog

In arch

.. code-block:: bash

   sudo pacman -S iverilog

(Optional) simulate the vhdl model in gtkwave with ghdl
=======================================================
For simulate the generated code, there is a Makefile in the vhdl directory of your project, this file contains the compiler and run commands

.. code-block:: bash

   ghdl -a *.vhdl
   ghdl -e testbench
   ghdl -r testbench --vcd=testbench.vcd
   gtkwave testbench.vcd

When these commands are finished, you will have gtkwave open with a default simulation created in the testbench.vhdl, you can edit this file and improve the simulation at your convenience.

(Optional) simulate the verilog model in gtkwave with iverilog
==============================================================
For simulate the generated code, there is a Makefile in the verilog directory of your project, this file contains the compiler and run commands

.. code-block:: bash

   iverilog -o simulation *.v
   vvp simulation
   gtkwave test.vcd &

When these commands are finished, you will have gtkwave open with a default simulation created in the testbench.v, you can edit this file and improve the simulation at your convenience.
