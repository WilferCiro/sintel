.. morph documentation master file, created by
   sphinx-quickstart on Sat May  4 12:36:52 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Sintel's documentation!
==================================

Copyright (c) 2019 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>, Director Luis Miguel Capacho <lmcapacho@uniquindio.edu.co>. Degree work for Universidad del Quindío, Colombia.

Sintel is a simulator and modeler of basic digital systems that allows, in addition to visualizing inputs and outputs in real time, to export the model to a hardware description language such as VHDL and verilog.

Sintel is written in python with the QT graphics library, it's an open source project with GPLv3 license or later.

In this manual we hope to give you a guide to the use and operation of the different parts of the software, as well as to invite you to contribute with various elements within it such as the creation of components.

You can find the master code in this link <https://gitlab.com/WilferCiro/sintel>

.. toctree::
	:glob:
	:numbered:
	:caption: Software

	install/*
	graph/*

.. toctree::
	:glob:
	:numbered:
	:caption: Simulation

	export/*

.. toctree::
	:glob:
	:numbered:
	:caption: Contibute to project

	sm/*
	addEdit/*

.. toctree::
	:glob:
	:caption: About the project

	about
