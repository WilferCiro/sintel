#!/usr/bin/env python
from common import Component, bin2dec

class {{smName}}(Component):
	"""
		Class than simulates an state machine like a decoder
	"""
	def setup(self, params):
		{{smConnectors}}

	def update(self):
		{{smOutputs}}
