library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity {{machine_name}} is
	{{generics}}
	port(
		{{input_output}}
	);
end entity;

architecture rtl of {{machine_name}} is begin

process({{inputs_proccess}}) begin
	{{cases_input}}
end process;

end architecture;
