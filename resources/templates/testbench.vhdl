library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity testbench is
end testbench;

architecture behavior of testbench is
component main is
	port(
		{{input_output}}
	);
end component;

	{{input_output_signals}}

begin

uut: main port map(
	{{connect_io_signals}}
);


stim_proc: process
  begin

	{{input_values}}
	wait for 10 ns;

	report "End testbench";
	wait;

  end process;

end;
