#!/usr/bin/env python

# Custom libraries
import importlib

# Qt libraries
from PyQt5.QtCore import *

# System libraries
import sys
import os
dir_compile = os.path.join(os.path.expanduser('~'), ".sintel")
sys.path.insert(1, os.path.join(dir_compile, "run_python"))
sys.path.insert(1, os.path.join(dir_compile, "components", "source_files"))
sys.path.insert(1, os.path.join(dir_compile, "components_user", "source_files", "sm"))
sys.path.insert(1, os.path.join(dir_compile, "components_user", "source_files"))
import time

#from simulador.tools.common import SimulationSignal, ClockEdit, UnionMacro
import common
common = importlib.import_module('.', package='common')
SimulationSignal = common.SimulationSignal
ClockEdit = common.ClockEdit
UnionMacro = common.UnionMacro

current_milli_time = lambda: int(round(time.time() * 1000))

# Components libraries
{{components_files}}
# Class Declaration
class SimulationRun(QObject):

	excecuteUpdate = pyqtSignal(list)
	def __init__(self, functionUpdateGui, is_initial = True):
		if not is_initial:
			{{reload_import}}

		super(QObject, self).__init__()
		self._clockObj = ClockEdit({{max_clock_cicle}})
		self._clkCount = 0

		self._hasClock = {{hasClock}}

		self._firsTime = True

		self._clockUpdated = True
		self._lastTime = 0

		self.dataUpdate = []

		# Components creation
		{{components}}

		# Default values
		{{defaultValues}}

		# signals declaration
		{{signals}}

		# Connect subSignals to subSignals
		{{connSubSignals}}

		# signals to signals
		{{connSignals}}

		# Update default values components
		#{{defaulValuesUpdate}}

		# Update gui function
		self._functionUpdateGui = functionUpdateGui
		self._initialTime = current_milli_time()
		self._lastTime = current_milli_time()

		self._emit = True

		self._componentsUpdate = []

		#self._resetFirst()

	def updateDefault(self):
		pass
		{{defaultValuesUpdate}}
		self._emitSignalValue([])

	def inputInitial(self):
		pass
		{{initial_input}}

	def resetComponents(self):
		pass
		{{reset_components}}

	def updateComponent(self, component, properties):
		comp = getattr(self, component)
		if comp != None:
			comp.setProperties(properties)
		#if not self._hasClock:
		#self.updateAll()
		comp.upd()
		#print("Final")
		self._emitSignalValue()

	def _updateGraph(self):
		dataUpdateGraph = []
		{{data_update_graph}}
		return dataUpdateGraph

	# Updates all inputs
	def updateAll(self, extendData = []):
		self._updateInputs()
		self._emitSignalValue(extendData)

	def _emitSignalValue(self, extendData = []):
		if self._emit:
			dataUpdate = []
			if extendData == []:
				extendData = self._updateGraph()

			extendData.insert(0, {"ID" : "simulation_time", "value" : current_milli_time() - self._initialTime})
			dataUpdate = extendData
			{{data_update}}

			self.excecuteUpdate.emit(dataUpdate)

			self._resetSignals()

	def _updateInputs(self):
		pass
		{{update_inputs}}

	def _resetSignals(self):
		pass
		{{reset_signals}}

	def _resetFirst(self):
		pass
		{{reset_first}}

	# Run program:
	def run(self, emit = True):
		self._emit = emit
		if self._hasClock:
			self._runAllClock()
		elif self._clockUpdated:
			self._clockUpdated = True
			self.updateAll()

	def _runAllClock(self):
		self._clockObj.update()
		self._clkCount = self._clockObj.read()
		dataUpdateGraph = []
		{{update_clock}}

		if self._clockUpdated:
			self._clockUpdated = False
			#self.updateAll(dataUpdateGraph)
			self.updateNoUpdated()
			print("clock")
			self._emitSignalValue(dataUpdateGraph)

		#self._clockObj.update()

	def updateNoUpdated(self):
		[comp.readUpdate() for comp in self._componentsUpdate if comp.isStateMachine()]
		[comp.writeUpdate() for comp in self._componentsUpdate if comp.isStateMachine()]

		[comp.readUpdate() for comp in self._componentsUpdate if not comp.isStateMachine()]
		[comp.writeUpdate() for comp in self._componentsUpdate if not comp.isStateMachine()]

		self._componentsUpdate = []

	def addToUpdate(self, componentsUpdate):
		self._componentsUpdate.extend(componentsUpdate)
