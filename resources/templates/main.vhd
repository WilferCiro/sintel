library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.components_package.all;

entity main is
	port(
		{{input_output}}
	);
end entity;

architecture SintelProject of main is
{{signals}}
begin
	{{signal_signal}}
	{{signal_subsignal}}
	{{components}}

end architecture;
