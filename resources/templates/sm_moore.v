module {{machine_name}}_rising {{generics}} (input clk, input rst{{input_output}});

parameter {{states_assignation}};
reg [{{number_states}} : 0] CS;
reg [{{number_states}} : 0] NS;

always @(posedge clk)
begin
	if (rst == 0)
		CS <= SS;
	else
		CS <= NS;
end

always @(CS, {{inputs_always}})
begin
 case(CS)
 		{{cases_input}}
	endcase
end

always @(CS)
begin
	case(CS)
		{{cases_output}}
	endcase
end
endmodule


module {{machine_name}}_falling {{generics}} (input clk, input rst{{input_output}});

parameter {{states_assignation}};
reg [{{number_states}} : 0] CS;
reg [{{number_states}} : 0] NS;

always @(negedge clk)
begin
	if (rst == 0)
		CS <= SS;
	else
		CS <= NS;
end

always @(CS, {{inputs_always}})
begin
 case(CS)
 		{{cases_input}}
	endcase
end

always @(CS)
begin
	case(CS)
		{{cases_output}}
	endcase
end
endmodule
