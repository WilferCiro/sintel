#!/usr/bin/env python
from common import Component, bin2dec

class {{smName}}(Component):
	"""
		Class than simulates an state machine
	"""
	def setup(self, params):
		{{smConnectors}}

		self._addInput("clk")
		self._addInput("rst")

		self._currentState = "SS"
		self._nextState = "SS"
		self._resetPos = 0#int(params["reset"])

	def readUpdate(self):
		resetVal = self.read("rst")
		if resetVal == self._resetPos:
			self._nextState = "SS"
		self._currentState = self._nextState
		{{smConditions}}

	def writeUpdate(self):
		{{smOutputs}}
