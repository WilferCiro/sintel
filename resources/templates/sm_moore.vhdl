library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity {{machine_name}} is
	{{generics}}
	port(
		clk, rst: in std_logic{{input_output}}
	);
end entity;

architecture rising_edge_arch of {{machine_name}} is
type State is ({{states}});
signal CS, NS: State;

begin
CS <= SS when rst='0' else NS when rising_edge(clk);

process(CS, {{inputs_proccess}})
begin
	case CS is
		{{cases_input}}
	end case;
end process;

process(CS, NS)
begin
	case CS is
		{{cases_output}}
	end case;
end process;
end architecture;


architecture falling_edge_arch of {{machine_name}} is
type State is ({{states}});
signal CS, NS: State;

begin
CS <= SS when rst='0' else NS when falling_edge(clk);

process(CS, {{inputs_proccess}})
begin
	case CS is
		{{cases_input}}
	end case;
end process;

process(CS, NS)
begin
	case CS is
		{{cases_output}}
	end case;
end process;
end architecture;
