module testbench();

	{{input_output_wires}}

  main simulation_component(
		{{connect_io_signals}}
  );

	initial begin
		$dumpfile("test.vcd");
		$dumpvars(0, testbench);

		{{input_values}}
		{{next_values}}
		#100
		$finish;
	end

	always begin
		#1;
		{{clock_signal}}
	end

endmodule // testbench end
