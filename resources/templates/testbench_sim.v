module testbench();

	{{input_output_wires}}
	integer file, file_read, ri, cont;

  main UUT(
		{{connect_io_signals}}
	);

	initial begin
		cont = 0;
		file = $fopen("output.txt", "w");
		$fwrite(file, "{{signals_read_name}}\n");

		{{input_values}}
		cont = 3;
	end

	always begin
		{{text_read}}
		#1
		$fwrite(file, "{{format_signals_read}}\n", {{signals_read}});
		{{clock_signal}}
		$stop;
	end
endmodule // testbench end
