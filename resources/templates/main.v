module main (
	{{input_output}}
);

	{{wires}}
	{{signal_subsignal}}
	{{signal_signal}}

	{{components}}
endmodule
