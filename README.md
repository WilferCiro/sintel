# Sintel
![Logo](resources/images/logo.png)

[![License](http://img.shields.io/:license-gpl-blue.svg)](http://opensource.org/licenses/GPL-3.0)
[![Documentation Status](https://readthedocs.org/projects/apiodoc/badge/?version=stable)](https://sintel.readthedocs.io/en/latest/)

Sintel is a simulator and modeler of basic digital systems that allows, in addition to visualizing inputs and outputs in real time, to export the circuit to a hardware description language such as VHDL and verilog.

Sintel is written in python with the QT graphics library, it's an open source project with GPLv3 license or later.

In this manual we hope to give you a guide to the use and operation of the different parts of the software, as well as to invite you to contribute with various elements within it such as the creation of components.

Documentation: https://sintel.readthedocs.io/en/latest/
Bugs report: https://gitlab.com/WilferCiro/sintel/issues/
Web page: https://wilferciro.gitlab.io/sintel/
Internationalization: https://translate.zanata.org/iteration/view/sintel/current/languages?dswid=836

## TODO:
- [x] Search name and logo
- [x] Wiring between components
- [ ] Algorithm to avoid twisted wiring, and to improve wiring
- [x] Component Properties Menu
- [x] IMPORTANT: connection of nodes
- [x] Cascade components
- [x] Subcomponents
- [ ] Virtual wiring, creating nodes where cables identified with ID are connected
- [x] Simulation Startup
- [x] Measurement of outputs and inputs
- [x] Interface for the entry of new components
- [ ] Polish file handling, open, close, save, replace, save as
- [x] Configuration menu
- [x] Save and load program files
- [x] Improve saved and loaded program
- [ ] Add function to resize all buses at once
- [ ] Change signals name


## Dependency Icarus Verilog

For windows, download the .exe file in https://iverilog.fandom.com/wiki/Main_Page and follow steps and install, restart your computer and follow the nexts steps

For Ubuntu:

.. code-block:: bash

	sudo apt install iverilog

For Arch:

.. code-block:: bash

	sudo pacman -S iverilog

## Installation
For install in Windows, download the https://gitlab.com/WilferCiro/sintel/-/raw/master/instals/Sintel.exe and open the Sintel.exe file and continue with the steps

For install in Linux (no-root user)

	$ wget https://gitlab.com/WilferCiro/sintel/-/raw/master/instals/LinuxInstaller.tar.gz
	$ tar xf LinuxInstaller.tar.gz
	$ cd LinuxInstaller
	$ sh install.sh

## Dependencies for developers
- Install pyQT5 and matplotlib with its dependencies -
	- Ubuntu:
		```
		sudo apt install python3-pip qttools5-dev-tools pyqt5-dev-tools iverilog python-matplotlib
		```
	- Arch Linux:
		```
		sudo pacman -S python3-pip pyqt5 iverilog python-matplotlib
		```
- Python
	```
	sudo pip3 install shutil sqlite3
	```

## Exec the software
```
git clone https://gitlab.com/WilferCiro/sintel.git
cd sintel/
python3 application.py
```

## Optional
- Install ghdl and gtkwave for simulate the vhdl and verilog code and show the signal values.
	- Ubuntu:
		```
		sudo apt install gtkwave ghdl
		```
	- Arch Linux:
		```
		sudo pacman -S gtkwave
		yaourt -S ghdl-llvm-git
		```
