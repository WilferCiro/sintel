cd tr
make initial_tr
cd ..
name=$1

source ~/Documents/programacion/python/virtualenvs/envSintel/bin/activate
rm -rf Linux/Sintel
pyinstaller application.spec

tar -cvf instals/LinuxInstaller.tar.gz Linux/ data/ install.sh uninstall.sh sintel.png README.md LICENSE.txt

if [[ -n "$name" ]]; then
	rm -rf Linux/Sintel
	rm -rf build
	#rm -rf instals/*
	rm -rf Windows/Sintel
	rm -rf Windows/Output/*
	read -p "Ingrese el mensaje del commit: " mensaje
	git add .
	#git reset -- automatic.sh
	git commit -m "$mensaje"
	git push
fi
